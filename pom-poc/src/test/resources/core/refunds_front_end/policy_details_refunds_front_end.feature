@RefundsFrontEnd
Feature: Refunds

  Scenario: Refunds
    Given Flow Access For Refunds
    Then Authenticate The User For Refunds
    Then Get Queries Sheet For Refunds "QUERIES_SHEET,QUERIES_SHEET_NAME"
    Then Get Refunds Data Sheet "POLICY_DETAILS_REFUNDS_DATA_SHEET,POLICY_DETAILS_REFUNDS_SHEET_NAME"
    Then Refunds Takeup Scenarios Data "POLICY_TAKEUP_DATA_SHEET,POLICY_TAKEUP_SHEET_NAME"
    Then Test Refunds