@PolicyDetailsDeleteMemberFIFrontEnd
Feature: Policy Servicing

  Scenario: Delete Member
    Given Flow Access For Policy Details Delete Member FI
    Then Authenticate The User For Policy Details Delete Member FI
    Then Get Queries Sheet For Policy Details Delete Member FI "QUERIES_SHEET,QUERIES_SHEET_NAME"
    Then Get Policy Details Delete Member FI Data Sheet "POLICY_DETAILS_DELETED_MEMBER_DATA_SHEET,POLICY_DETAILS_DELETE_MEMBER_SHEET_NAME"
    Then Policy Details Delete Member FI Takeup Scenarios Data "POLICY_TAKEUP_DATA_SHEET,POLICY_TAKEUP_SHEET_NAME"
    Then Test Policy Details Delete Member FI