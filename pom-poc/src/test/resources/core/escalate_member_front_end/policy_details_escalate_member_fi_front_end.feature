@PolicyDetailsEscalateMemberFIFrontEnd
Feature: Policy Servicing

  Scenario: Escalate Member
    Given Flow Access For Policy Details Escalate Member FI
    Then Authenticate The User For Policy Details Escalate Member FI
    Then Get Queries Sheet For Policy Details Escalate Member FI "QUERIES_SHEET,QUERIES_SHEET_NAME"
    Then Get Policy Details Escalate Member FI Data Sheet "POLICY_DETAILS_ESCALATE_MEMBER_DATA_SHEET_FI,POLICY_DETAILS_ESCALATE_MEMBER_SHEET_NAME"
    Then Policy Details Escalate Member FI Takeup Scenarios Data "POLICY_TAKEUP_DATA_SHEET,POLICY_TAKEUP_SHEET_NAME"
    Then Test Policy Details Escalate Member FI