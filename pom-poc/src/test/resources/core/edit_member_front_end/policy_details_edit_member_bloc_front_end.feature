@PolicyDetailsEditMemberBLOCFrontEnd
Feature: Policy Servicing

  Scenario: Edit Member
    Given Flow Access For Policy Details Edit Member BLOC
    Then Authenticate The User For Policy Details Edit Member BLOC
    Then Get Queries Sheet For Policy Details Edit Member BLOC "QUERIES_SHEET,QUERIES_SHEET_NAME"
    Then Get Policy Details Edit Member BLOC Data Sheet "POLICY_DETAILS_EDIT_MEMBER_DATA_SHEET_LOC_B,POLICY_DETAILS_EDIT_MEMBER_SHEET_NAME"
    Then Policy Details Edit Member BLOC Takeup Scenarios Data "POLICY_TAKEUP_DATA_SHEET,POLICY_TAKEUP_SHEET_NAME"
    Then Test Policy Details Edit Member BLOC