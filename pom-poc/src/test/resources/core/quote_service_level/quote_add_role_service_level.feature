@QuoteAddRoleServiceLevel
Feature: Flow Service Level Tests: Quote

Scenario: Quote Add Role
    Given Quote Add Role Policy Takeup Scenarios Data at "POLICY_TAKEUP_DATA_SHEET,POLICY_TAKEUP_SHEET_NAME"
        Then Get Quote Add Role Test Data at "QUOTE_ADD_ROLE_DATA_SHEET,QUOTE_ADD_ROLE_SHEET_NAME"
        Then Test Quote Add Role Service
