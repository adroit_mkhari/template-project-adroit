@QuoteDeleteRoleServiceLevel
Feature: Flow Service Level Tests: Quote

Scenario: Quote Delete Role
    Given Quote Delete Role Policy Takeup Scenarios Data at "POLICY_TAKEUP_DATA_SHEET,POLICY_TAKEUP_SHEET_NAME"
        Then Get Quote Delete Role Test Data at "QUOTE_DELETE_ROLE_DATA_SHEET,QUOTE_DELETE_ROLE_SHEET_NAME"
        Then Test Quote Delete Role Service
