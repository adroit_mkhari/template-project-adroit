@AttorneyManagementFrontEnd
Feature: Attorney Management

  Scenario: Attorney Management
    Given Flow Access For Attorney Management
    Then Authenticate The User For Attorney Management
    Then Get Queries Sheet For Attorney Management "QUERIES_SHEET,QUERIES_SHEET_NAME"
    Then Get Attorney Management Data Sheet "POLICY_DETAILS_ATTORNEY_MANAGEMENT_DATA_SHEET,POLICY_DETAILS_ATTORNEY_MANAGEMENT_SHEET_NAME"
    Then Attorney Management Takeup Scenarios Data "POLICY_TAKEUP_DATA_SHEET,POLICY_TAKEUP_SHEET_NAME"
    Then Test Attorney Management