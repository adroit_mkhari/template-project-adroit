@Indexing
Feature: Google Search Engine

  Scenario: Search
    Given We Access Flow
    Then Authenticate The User For Indexing
    Then Get Database Queries At "QUERIES_SHEET,QUERIES_SHEET_NAME"
    Then Get Indexing Data At "INDEXING_DATA_SHEET,INDEXING_SHEET_NAME"
    Then Run Indexing Regression
