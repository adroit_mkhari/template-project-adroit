@PolicyDetailsCancelPolicyFIFrontEnd
Feature: Policy Servicing

  Scenario: Cancel Policy
    Given Flow Access For Policy Details Cancel Policy FI
    Then Authenticate The User For Policy Details Cancel Policy FI
    Then Get Queries Sheet For Policy Details Cancel Policy FI "QUERIES_SHEET,QUERIES_SHEET_NAME"
    Then Get Policy Details Cancel Policy FI Data Sheet "POLICY_DETAILS_CANCEL_POLICY_DATA_SHEET_FI,POLICY_DETAILS_CANCEL_POLICY_SHEET_NAME"
    Then Policy Details Cancel Policy FI Takeup Scenarios Data "POLICY_TAKEUP_DATA_SHEET,POLICY_TAKEUP_SHEET_NAME"
    Then Test Policy Details Cancel Policy FI