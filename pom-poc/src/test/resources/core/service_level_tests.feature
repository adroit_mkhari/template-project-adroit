@ServiceLevel
Feature: Flow Service Level Tests

Scenario: Policy Takeup
    Given Get Policy Takeup Scenarios Data at "POLICY_TAKEUP_DATA_SHEET,POLICY_TAKEUP_SHEET_NAME"
        # Then Test Policy Takeup Service using Specific Scenarios
        # Then Get Update Policy Test Data at "UPDATE_POLICY_DATA_SHEET,UPDATE_POLICY_SHEET_NAME"
        Then Get Quote Test Data at "QUOTE_DATA_SHEET,QUOTE_SHEET_NAME"
        # Then Test Update Policy Service
        Then Test Quote Service
