@UpadatePrincipalMemberDetailsServiceLevel
Feature: Flow Service Level Tests: Upadate Principal Member Details

Scenario: Upadate Principal Member Details
    Given Upadate Principal Member Details Policy Takeup Scenarios Data at "POLICY_TAKEUP_DATA_SHEET,POLICY_TAKEUP_SHEET_NAME"
        Then Get Upadate Principal Member Details Test Data at "UPDATE_PRINCIPAL_MEMBER_DETAILS_DATA_SHEET,UPDATE_PRINCIPAL_MEMBER_DETAILS_SHEET_NAME"
        Then Test Upadate Principal Member Details Service
