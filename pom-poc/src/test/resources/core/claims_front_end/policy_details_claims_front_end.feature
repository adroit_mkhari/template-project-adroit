@ClaimsFrontEnd
Feature: Claims

  Scenario: Claims
    Given Flow Access For Claims
    Then Authenticate The User For Claims
    Then Get Queries Sheet For Claims "QUERIES_SHEET,QUERIES_SHEET_NAME"
    Then Get Claims Data Sheet "POLICY_DETAILS_CLAIMS_DATA_SHEET,POLICY_DETAILS_CLAIMS_SHEET_NAME"
    Then Claims Takeup Scenarios Data "POLICY_TAKEUP_DATA_SHEET,POLICY_TAKEUP_SHEET_NAME"
    Then Test Claims