@UpdatePolicyEditMemberServiceLevel
Feature: Flow Service Level Tests: Update Policy

Scenario: Update Policy Edit Member
    Given Update Policy Edit Member Policy Takeup Scenarios Data at "POLICY_TAKEUP_DATA_SHEET,POLICY_TAKEUP_SHEET_NAME"
        Then Get Update Policy Edit Member Test Data at "UPDATE_POLICY_EDIT_MEMBER_DATA_SHEET,UPDATE_POLICY_EDIT_MEMBER_SHEET_NAME"
        Then Test Update Policy Edit Member Service
