package retentions;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        tags = {"@RetentionsFIFrontEnd"},
        features = {"src/test/resources/core/retentions_front_end"},
        glue = {"retentions/steps/retentions_front_end"}
)

public class RetentionsFIFrontEnd {
}
