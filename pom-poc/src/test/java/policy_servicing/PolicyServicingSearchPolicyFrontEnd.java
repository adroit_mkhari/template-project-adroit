package policy_servicing;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        tags = {"@PolicyDetailsSearchPolicyFrontEnd"},
        features = {"src/test/resources/core/search_policy_front_end"},
        glue = {"policy_servicing/steps/search_policy_front_end"}
)

public class PolicyServicingSearchPolicyFrontEnd {
}
