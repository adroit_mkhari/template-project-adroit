package policy_servicing;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        tags = {"@PolicyDetailsCancelPolicyHCPFrontEnd"},
        features = {"src/test/resources/core/cancel_policy_front_end"},
        glue = {"policy_servicing/steps/cancel_policy_front_end"}
)

public class PolicyServicingCancelPolicyHCPFrontEnd {
}
