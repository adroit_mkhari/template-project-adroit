package policy_servicing;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        tags = {"@PolicyDetailsEditMemberBLOCFrontEnd"},
        features = {"src/test/resources/core/edit_member_front_end"},
        glue = {"policy_servicing/steps/edit_member_front_end"}
)

public class PolicyServicingEditMemberBLOCFrontEnd {
}
