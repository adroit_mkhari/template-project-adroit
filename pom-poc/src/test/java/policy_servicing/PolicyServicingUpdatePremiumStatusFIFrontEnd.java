package policy_servicing;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        tags = {"@PolicyDetailsUpdatePremiumStatusFIFrontEnd"},
        features = {"src/test/resources/core/update_premium_status_front_end"},
        glue = {"policy_servicing/steps/update_premium_status_front_end"}
)

public class PolicyServicingUpdatePremiumStatusFIFrontEnd {
}
