package policy_servicing;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        tags = {"@PolicyDetailsAddMemberADFrontEnd"},
        features = {"src/test/resources/core/add_member_front_end"},
        glue = {"policy_servicing/steps/add_member_front_end"}
)

public class PolicyServicingAddMemberADFrontEnd {
}
