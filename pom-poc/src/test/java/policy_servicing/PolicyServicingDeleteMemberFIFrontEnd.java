package policy_servicing;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        tags = {"@PolicyDetailsDeleteMemberFIFrontEnd"},
        features = {"src/test/resources/core/delete_member_front_end"},
        glue = {"policy_servicing/steps/delete_member_front_end"}
)

public class PolicyServicingDeleteMemberFIFrontEnd {
}
