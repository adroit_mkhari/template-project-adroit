package policy_servicing;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        tags = {"@PolicyDetailsQuoteFrontEnd"},
        features = {"src/test/resources/core/quote_front_end"},
        glue = {"policy_servicing/steps/quote_front_end"}
)

public class PolicyServicingQuoteFrontEnd {
}
