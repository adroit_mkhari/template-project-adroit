package policy_servicing.steps.edit_member_front_end;

import co.za.fnb.flow.handlers.transform.ExcelToDataTable;
import co.za.fnb.flow.models.QueryFactory;
import co.za.fnb.flow.pages.Config;
import co.za.fnb.flow.pages.HomePage;
import co.za.fnb.flow.pages.LoginPage;
import co.za.fnb.flow.setup.DriverSetup;
import co.za.fnb.flow.setup.PropertiesSetup;
import co.za.fnb.flow.tester.ScenarioTester;
import co.za.fnb.flow.tester.TestComponent;
import co.za.fnb.flow.tester.services.models.TakeupScenarioFactory;
import cucumber.api.DataTable;
import cucumber.api.Transform;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class PolicyDetailsEditMemberPPFrontEndSteps implements Config {
    private Logger log  = LogManager.getLogger(PolicyDetailsEditMemberPPFrontEndSteps.class);
    Properties properties;
    WebDriver driver;
    LoginPage loginPage;
    HomePage homePage;
    DataTable queriesTable;
    DataTable dataTable;
    private String application;

    @Before("@PolicyDetailsEditMemberPPFrontEnd")
    public void setup() {
        log.info("Running Setup");
        PropertiesSetup propertiesSetup = new PropertiesSetup();
        propertiesSetup.loadProperties();
        properties = propertiesSetup.getProperties();
        DriverSetup driverSetup = new DriverSetup();
        driverSetup.setProperties(properties);
        driverSetup.setBrowserCapabilities();
        driver = driverSetup.getDriver();
        application = properties.getProperty("TARGET_APPLICATION");
        log.info("Done Running Setup");
    }

    @After
    public void cleanup() {
        if (driver != null) {
            driver.quit();
        }
    }

    @Given("^Flow Access For Policy Details Edit Member PP$")
    public void loginToFlow() {
        log.info("Running Step: Given We can access FLOW open the login page");
        try {
            loginPage = new LoginPage(driver, null);
            loginPage.open(application);
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }
        log.info("Done running step");
    }

    @Then("^Authenticate The User For Policy Details Edit Member PP$")
    public void authenticateTheUser() {
        log.info("Running Step: Authenticate the user");
        try {
            String username = properties.getProperty("USERNAME");
            String password = properties.getProperty("PASSWORD");
            homePage = loginPage.login(username, password);
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }
        log.info("Done running step");
    }

    @Then("^Get Queries Sheet For Policy Details Edit Member PP \"([^\"]*)\"$")
    public void getQueries(@Transform(ExcelToDataTable.class)
                                                        DataTable queriesTable
    ) {
        log.info("Running Step: Get Queries Sheet to use");
        this.queriesTable = queriesTable;
        log.info("Done running step");
    }

    @Then("^Get Policy Details Edit Member PP Data Sheet \"([^\"]*)\"$")
    public void getTestData(@Transform(ExcelToDataTable.class)
                                         DataTable dataTable
    ) {
        log.info("Running Step: Get Data Sheet to use");
        this.dataTable = dataTable;
        QueryFactory.loadQueries(this.queriesTable);
        log.info("Done running step");
    }

    @Then("^Policy Details Edit Member PP Takeup Scenarios Data \"([^\"]*)\"$")
    public void getTakeUpTestData(@Transform(ExcelToDataTable.class)
                                          DataTable dataTable
    ) {
        log.info("Running Step: Takeup");
        TakeupScenarioFactory.loadTakeupScenarios(dataTable);
        log.info("Done running step");
    }

    @Then("^Test Policy Details Edit Member PP$")
    public void testPolicyDetailsFunctionality() throws IOException {
        log.info("Running Step: Test Policy Details Search Policy Functionality");
        ScenarioTester scenarioTester = new ScenarioTester(TestComponent.POLICY_DETAILS, dataTable, driver);
        scenarioTester.test();
        log.info("Done running step");
    }

    private int getHeaderIndex(Object[] headers, String header) {
        return Arrays.asList(headers).indexOf(header);
    }

    private String getCellValue(Object[] headers, List<String> dataEntry, String field) {
        return getHeaderIndex(headers, field) != -1 ? dataEntry.get(getHeaderIndex(headers, field)) : "";
    }

}
