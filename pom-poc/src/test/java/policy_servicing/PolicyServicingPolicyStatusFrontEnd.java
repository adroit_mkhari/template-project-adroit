package policy_servicing;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        tags = {"@PolicyDetailsPolicyStatusFrontEnd"},
        features = {"src/test/resources/core/policy_status_front_end"},
        glue = {"policy_servicing/steps/policy_status_front_end"}
)

public class PolicyServicingPolicyStatusFrontEnd {
}
