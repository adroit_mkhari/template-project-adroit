package policy_servicing;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        tags = {"@PolicyDetailsReinstatePlanFrontEnd"},
        features = {"src/test/resources/core/reinstate_plan_front_end"},
        glue = {"policy_servicing/steps/reinstate_plan_front_end"}
)

public class PolicyServicingReinstatePlanFrontEnd {
}
