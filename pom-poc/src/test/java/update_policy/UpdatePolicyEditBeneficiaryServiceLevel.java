package update_policy;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        tags = {"@UpdatePolicyEditBeneficiaryServiceLevel"},
        features = {"src/test/resources/core/update_policy"},
        glue = {"update_policy/steps/update_policy"}
)

public class UpdatePolicyEditBeneficiaryServiceLevel {
}
