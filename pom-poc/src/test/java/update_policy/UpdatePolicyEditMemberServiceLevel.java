package update_policy;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        tags = {"@UpdatePolicyEditMemberServiceLevel"},
        features = {"src/test/resources/core"},
        glue = {"update_policy/steps/update_policy"}
)

public class UpdatePolicyEditMemberServiceLevel {
}
