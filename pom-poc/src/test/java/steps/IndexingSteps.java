package steps;

import co.za.fnb.flow.handlers.database.QueryHandler;
import co.za.fnb.flow.handlers.transform.ExcelToDataTable;
import co.za.fnb.flow.handlers.transform.TextFileToDataTable;
import co.za.fnb.flow.models.QueryFactory;
import co.za.fnb.flow.pages.Config;
import co.za.fnb.flow.pages.HomePage;
import co.za.fnb.flow.pages.LoginPage;
import co.za.fnb.flow.setup.DriverSetup;
import co.za.fnb.flow.setup.PropertiesSetup;
import co.za.fnb.flow.tester.ScenarioTester;
import co.za.fnb.flow.tester.TestComponent;
import cucumber.api.DataTable;
import cucumber.api.Transform;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import gherkin.formatter.model.Row;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class IndexingSteps implements Config {
    private Logger log  = LogManager.getLogger(IndexingSteps.class);
    Properties properties;
    WebDriver driver;
    LoginPage loginPage;
    HomePage homePage;
    DataTable queriesTable;
    DataTable dataTable;
    private String application;

    @Before("@Indexing")
    public void setup() {
        log.info("Running Setup");
        PropertiesSetup propertiesSetup = new PropertiesSetup();
        propertiesSetup.loadProperties();
        properties = propertiesSetup.getProperties();
        DriverSetup driverSetup = new DriverSetup();
        driverSetup.setProperties(properties);
        driverSetup.setBrowserCapabilities();
        driver = driverSetup.getDriver();
        application = properties.getProperty("TARGET_APPLICATION");
        log.info("Done Running Setup");
        // QueriesEnumToExcel queriesEnumToExcel = new QueriesEnumToExcel();
        // queriesEnumToExcel.generate("Queries");
    }

    @After
    public void cleanup() {
        if (driver != null) {
            driver.quit();
        }
    }

    @Given("^We Access Flow$")
    public void accessFlow() {
        log.info("Running Step: Access Flow Web Application.");
        try {
            loginPage = new LoginPage(driver, null);
            loginPage.open(application);
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }
        log.info("Done running step");
    }

    @Then("^Authenticate The User For Indexing$")
    public void authenticateUserForIndexing() {
        log.info("Running Step: Authenticate User For Indexing.");
        try {
            String username = properties.getProperty("USERNAME");
            String password = properties.getProperty("PASSWORD");
            homePage = loginPage.login(username, password);
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }
        log.info("Done running step");
    }

    @Then("^Get Database Queries At \"([^\"]*)\"$")
    public void getDatabaseQueries(@Transform(ExcelToDataTable.class)
                                                        DataTable queriesTable
    ) {
        log.info("Running Step: Get Database Queries.");
        this.queriesTable = queriesTable;
        log.info("Done running step");
    }

    @Then("^Get Indexing Data At \"([^\"]*)\"$")
    public void getIndexingData(@Transform(ExcelToDataTable.class)
                                         DataTable dataTable
    ) {
        log.info("Running Step: Get Indexing Data.");
        this.dataTable = dataTable;
        QueryFactory.loadQueries(this.queriesTable);
        log.info("Done running step");
    }

    @Then("^Run Indexing Regression$")
    public void runIndexingRegression() throws IOException {
        log.info("Running Step: Run Indexing Regression.");
        ScenarioTester scenarioTester = new ScenarioTester(TestComponent.INDEXING, dataTable, driver);
        scenarioTester.test();
        log.info("Done running step");
    }

    private int getHeaderIndex(Object[] headers, String header) {
        return Arrays.asList(headers).indexOf(header);
    }

    private String getCellValue(Object[] headers, List<String> dataEntry, String field) {
        return getHeaderIndex(headers, field) != -1 ? dataEntry.get(getHeaderIndex(headers, field)) : "";
    }

}
