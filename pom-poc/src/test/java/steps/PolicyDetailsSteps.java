package steps;

import co.za.fnb.flow.handlers.QueriesEnumToExcel;
import co.za.fnb.flow.handlers.database.QueryHandler;
import co.za.fnb.flow.handlers.transform.TextFileToDataTable;
import co.za.fnb.flow.models.QueryFactory;
import cucumber.api.DataTable;
import cucumber.api.Transform;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import co.za.fnb.flow.handlers.transform.ExcelToDataTable;
import gherkin.formatter.model.Row;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import co.za.fnb.flow.pages.Config;
import co.za.fnb.flow.pages.HomePage;
import co.za.fnb.flow.pages.LoginPage;
import co.za.fnb.flow.setup.DriverSetup;
import co.za.fnb.flow.setup.PropertiesSetup;
import co.za.fnb.flow.tester.ScenarioTester;
import co.za.fnb.flow.tester.TestComponent;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class PolicyDetailsSteps implements Config {
    private Logger log  = LogManager.getLogger(PolicyDetailsSteps.class);
    Properties properties;
    WebDriver driver;
    LoginPage loginPage;
    HomePage homePage;
    DataTable queriesTable;
    DataTable dataTable;
    private String application;

    @Before("@PolicyDetails")
    public void setup() {
        log.info("Running Setup");
        PropertiesSetup propertiesSetup = new PropertiesSetup();
        propertiesSetup.loadProperties();
        properties = propertiesSetup.getProperties();
        DriverSetup driverSetup = new DriverSetup();
        driverSetup.setProperties(properties);
        driverSetup.setBrowserCapabilities();
        driver = driverSetup.getDriver();
        application = properties.getProperty("TARGET_APPLICATION");
        log.info("Done Running Setup");
        // QueriesEnumToExcel queriesEnumToExcel = new QueriesEnumToExcel();
        // queriesEnumToExcel.generate("Queries");
    }

    @After
    public void cleanup() {
        if (driver != null) {
            driver.quit();
        }
    }

    @Given("^We can access FLOW open the login page$")
    public void we_can_access_FLOW_open_the_login_page() {
        log.info("Running Step: Given We can access FLOW open the login page");
        try {
            loginPage = new LoginPage(driver, null);
            loginPage.open(application);
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }
        log.info("Done running step");
    }

    @Then("^Authenticate the user$")
    public void authenticate_the_user() {
        log.info("Running Step: Authenticate the user");
        try {
            String username = properties.getProperty("USERNAME");
            String password = properties.getProperty("PASSWORD");
            homePage = loginPage.login(username, password);
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }
        log.info("Done running step");
    }

    @Then("^Get Queries Sheet to use at \"([^\"]*)\"$")
    public void get_Queries_Sheet_to_use_at(@Transform(ExcelToDataTable.class)
                                                        DataTable queriesTable
    ) {
        log.info("Running Step: Get Queries Sheet to use");
        this.queriesTable = queriesTable;
        log.info("Done running step");
    }

    @Then("^Get Data Sheet to use at \"([^\"]*)\"$")
    public void get_Data_Sheet_to_use_at(@Transform(ExcelToDataTable.class)
                                         DataTable dataTable
    ) {
        log.info("Running Step: Get Data Sheet to use");
        this.dataTable = dataTable;
        QueryFactory.loadQueries(this.queriesTable);
        log.info("Done running step");
    }

    @Then("^Get Data to use From Text File \"([^\"]*)\"$")
    public void get_Data_to_use_From_Text_File(@Transform(TextFileToDataTable.class)
                                                           DataTable dataTable
    ) {
        this.dataTable = dataTable;
    }

    @Then("^Test Policy Details Functionality$")
    public void test_Policy_Details_Functionality() throws IOException {
        log.info("Running Step: Test Policy Details Functionality");
        ScenarioTester scenarioTester = new ScenarioTester(TestComponent.POLICY_DETAILS, dataTable, driver);
        scenarioTester.test();
        log.info("Done running step");
    }

    @Then("^Change Policy Status$")
    public void change_Policy_Status() {
        Object[] headers = dataTable.getGherkinRows().get(0).getCells().toArray();
        List<String> dataEntry;
        String run, policyNumber = null, statusId = null;
        for (Row row : dataTable.getGherkinRows()) {
            if (row.getLine() != 1) {
                dataEntry = row.getCells();
                run = getCellValue(headers, dataEntry, "Run");

                if (run.equalsIgnoreCase("YES")) {
                    try {
                        policyNumber = getCellValue(headers, dataEntry, "Policy Number");
                        statusId = getCellValue(headers, dataEntry, "Status Id");
                        QueryHandler queryHandler = new QueryHandler();
                        queryHandler.changePolicyStatus(policyNumber, statusId);
                    } catch (Exception e) {
                        log.error("Error while trying to change Policy Status for " + policyNumber + " to " + statusId + " -> " + e.getMessage());
                    }
                }
            }
        }
    }

    private int getHeaderIndex(Object[] headers, String header) {
        return Arrays.asList(headers).indexOf(header);
    }

    private String getCellValue(Object[] headers, List<String> dataEntry, String field) {
        return getHeaderIndex(headers, field) != -1 ? dataEntry.get(getHeaderIndex(headers, field)) : "";
    }

}
