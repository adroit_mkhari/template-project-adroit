package steps;

import co.za.fnb.flow.handlers.transform.ExcelToDataTable;
import co.za.fnb.flow.handlers.transform.TextFileToDataTable;
import co.za.fnb.flow.models.QueryFactory;
import co.za.fnb.flow.pages.Config;
import co.za.fnb.flow.pages.HomePage;
import co.za.fnb.flow.pages.LoginPage;
import co.za.fnb.flow.setup.DriverSetup;
import co.za.fnb.flow.setup.PropertiesSetup;
import co.za.fnb.flow.tester.ScenarioTester;
import co.za.fnb.flow.tester.TestComponent;
import cucumber.api.DataTable;
import cucumber.api.Transform;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;

import java.io.IOException;
import java.util.Properties;

public class LettersSteps implements Config {
    private Logger log  = LogManager.getLogger(LettersSteps.class);
    Properties properties;
    WebDriver driver;
    DataTable queriesTable;
    DataTable dataTable;
    private String application;

    @Before("@Letters")
    public void setup() {
        log.info("Running Setup");
        PropertiesSetup propertiesSetup = new PropertiesSetup();
        propertiesSetup.loadProperties();
        properties = propertiesSetup.getProperties();
        application = properties.getProperty("TSM_TARGET_APPLICATION");
        log.info("Done Running Setup");
    }

    @After
    public void cleanup() {
        if (driver != null) {
            driver.quit();
        }
    }

    @Then("^Get Queries Sheet To Use For Letters at \"([^\"]*)\"$")
    public void get_Queries_Sheet_to_use_at(@Transform(ExcelToDataTable.class)
                                                        DataTable queriesTable
    ) {
        log.info("Running Step: Get Queries Sheet to use");
        this.queriesTable = queriesTable;
        log.info("Done running step");
    }

    @Then("^Get Data Sheet To Use For Letters at \"([^\"]*)\"$")
    public void get_Data_Sheet_to_use_at(@Transform(ExcelToDataTable.class)
                                         DataTable dataTable
    ) {
        log.info("Running Step: Get Data Sheet to use");
        this.dataTable = dataTable;
        QueryFactory.loadQueries(this.queriesTable);
        log.info("Done running step");
    }

    @Then("^Test Letters Functionality$")
    public void test_Letters_Functionality() throws IOException {
        log.info("Running Step: Test Letters Functionality");
        ScenarioTester scenarioTester = new ScenarioTester(TestComponent.LETTERS, dataTable, driver);
        scenarioTester.test();
        log.info("Done running step");
    }

}
