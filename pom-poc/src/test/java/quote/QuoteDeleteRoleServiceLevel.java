package quote;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        tags = {"@QuoteDeleteRoleServiceLevel"},
        features = {"src/test/resources/core/quote_service_level"},
        glue = {"quote/steps/quote_service_level"}
)

public class QuoteDeleteRoleServiceLevel {
}
