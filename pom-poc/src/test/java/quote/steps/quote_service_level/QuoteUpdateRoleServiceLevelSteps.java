package quote.steps.quote_service_level;

import co.za.fnb.flow.handlers.transform.ExcelToDataTable;
import co.za.fnb.flow.pages.Config;
import co.za.fnb.flow.setup.PropertiesSetup;
import co.za.fnb.flow.tester.ScenarioTester;
import co.za.fnb.flow.tester.TestComponent;
import co.za.fnb.flow.tester.services.models.TakeupScenarioFactory;
import cucumber.api.DataTable;
import cucumber.api.Transform;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Then;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;

import java.io.IOException;
import java.util.Properties;

public class QuoteUpdateRoleServiceLevelSteps implements Config {
    private Logger log  = LogManager.getLogger(QuoteUpdateRoleServiceLevelSteps.class);
    Properties properties;
    WebDriver driver;
    DataTable queriesTable;
    DataTable dataTable;
    DataTable testDataTable;

    @Before("@QuoteUpdateRoleServiceLevel")
    public void setup() {
        log.info("Running Setup");
        PropertiesSetup propertiesSetup = new PropertiesSetup();
        propertiesSetup.loadProperties();
        properties = propertiesSetup.getProperties();
        log.info("Done Running Setup");
    }

    @After
    public void cleanup() {
        if (driver != null) {
            driver.quit();
        }
    }

    @Then("^Quote Update Role Policy Takeup Scenarios Data at \"([^\"]*)\"$")
    public void getTakeupTestData(@Transform(ExcelToDataTable.class)
                                         DataTable dataTable
    ) {
        log.info("Running Step: Get Data Sheet to use");
        this.dataTable = dataTable;
        TakeupScenarioFactory.loadTakeupScenarios(dataTable);
        log.info("Done running step");
    }

    @Then("^Get Quote Update Role Test Data at \"([^\"]*)\"$")
    public void getQuoteTestData(@Transform(ExcelToDataTable.class)
                                                DataTable dataTable
    ) {
        log.info("Running Step: Get Quote Data Sheet to use");
        this.testDataTable = dataTable;
        log.info("Done running step");
    }

    @Then("^Test Quote Update Role Service$")
    public void testQuoteServices() {
        log.info("Running Step: Testing Quote Services Tests:");
        ScenarioTester scenarioTester = new ScenarioTester(TestComponent.QUOTE_SERVICE_LEVEL, testDataTable, driver);
        try {
            scenarioTester.test();
        } catch (IOException e) {
            // TODO: Handle Exceptions
            e.printStackTrace();
        }
        log.info("Done running Quote Services Tests..");
    }

}
