package claims;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        tags = {"@ClaimsFrontEnd"},
        features = {"src/test/resources/core/claims_front_end"},
        glue = {"claims/steps/claims_front_end"}
)

public class ClaimsFrontEnd {
}
