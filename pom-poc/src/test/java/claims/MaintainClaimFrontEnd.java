package claims;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        tags = {"@MaintainClaimFrontEnd"},
        features = {"src/test/resources/core/claims_front_end"},
        glue = {"claims/steps/maintain_claim_front_end"}
)

public class MaintainClaimFrontEnd {
}
