package take_up;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        tags = {"@BLOCTakeUpServiceLevel"},
        features = {"src/test/resources/core/take_up_service_level/bloc"},
        glue = {"take_up/steps/take_up_service_level/bloc"}
)

public class BLOCTakeUpServiceLevel {
}
