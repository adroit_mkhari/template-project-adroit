package take_up;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        tags = {"@HCPTakeUpServiceLevel"},
        features = {"src/test/resources/core/take_up_service_level/hcp"},
        glue = {"take_up/steps/take_up_service_level/hcp"}
)

public class HCPTakeUpServiceLevel {
}
