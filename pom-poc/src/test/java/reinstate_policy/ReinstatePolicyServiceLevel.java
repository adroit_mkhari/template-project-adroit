package reinstate_policy;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        tags = {"@ReinstatePolicyServiceLevel"},
        features = {"src/test/resources/core/reinstate_policy_service_level"},
        glue = {"reinstate_policy/steps/reinstate_policy_service_level"}
)

public class ReinstatePolicyServiceLevel {
}
