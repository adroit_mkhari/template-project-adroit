package cancel_policy;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        tags = {"@CancelPolicyServiceLevel"},
        features = {"src/test/resources/core/cancel_policy_service_level"},
        glue = {"cancel_policy/steps/cancel_policy_service_level"}
)

public class CancelPolicyServiceLevel {
}
