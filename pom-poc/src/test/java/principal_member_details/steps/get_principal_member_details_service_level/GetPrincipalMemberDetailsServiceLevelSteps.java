package principal_member_details.steps.get_principal_member_details_service_level;

import cancel_policy.steps.cancel_policy_service_level.CancelPolicyServiceLevelSteps;
import co.za.fnb.flow.handlers.transform.ExcelToDataTable;
import co.za.fnb.flow.pages.Config;
import co.za.fnb.flow.setup.PropertiesSetup;
import co.za.fnb.flow.tester.ScenarioTester;
import co.za.fnb.flow.tester.TestComponent;
import co.za.fnb.flow.tester.services.models.TakeupScenarioFactory;
import cucumber.api.DataTable;
import cucumber.api.Transform;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;

import java.io.IOException;
import java.util.Properties;

public class GetPrincipalMemberDetailsServiceLevelSteps implements Config {
    private Logger log  = LogManager.getLogger(CancelPolicyServiceLevelSteps.class);
    Properties properties;
    WebDriver driver;
    DataTable queriesTable;
    DataTable dataTable;
    DataTable testDataTable;

    @Before("@GetPrincipalMemberDetailsServiceLevel")
    public void setup() {
        log.info("Running Setup");
        PropertiesSetup propertiesSetup = new PropertiesSetup();
        propertiesSetup.loadProperties();
        properties = propertiesSetup.getProperties();
        log.info("Done Running Setup");
    }

    @After
    public void cleanup() {
        if (driver != null) {
            driver.quit();
        }
    }

    @Given("^Get Principal Member Details Test Data at \"([^\"]*)\"$")
    public void getCancelPolicyTestData(@Transform(ExcelToDataTable.class)
                                                DataTable dataTable
    ) {
        log.info("Running Step: Get Quote Data Sheet to use");
        this.testDataTable = dataTable;
        log.info("Done running step");
    }

    @Then("^Test Get Principal Member Details Service$")
    public void testGetPrincipalMemberDetailsService() {
        log.info("Running Step: Testing Get Principal Member Details Service Tests:");
        ScenarioTester scenarioTester = new ScenarioTester(TestComponent.GET_PRINCIPAL_MEMBER_DETAILS_SERVICE_LEVEL, testDataTable, driver);
        try {
            scenarioTester.test();
        } catch (IOException e) {
            // TODO: Handle Exceptions
            e.printStackTrace();
        }
        log.info("Done running Get Principal Member Details Service Tests..");
    }

}
