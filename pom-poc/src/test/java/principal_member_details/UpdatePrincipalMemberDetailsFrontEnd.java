package principal_member_details;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        tags = {"@UpdatePrincipalMemberDetailsFrontEnd"},
        features = {"src/test/resources/core/update_principal_member_details_front_end"},
        glue = {"principal_member_details/steps/update_principal_member_details_front_end"}
)

public class UpdatePrincipalMemberDetailsFrontEnd {
}
