package principal_member_details;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        tags = {"@GetPrincipalMemberDetailsServiceLevel"},
        features = {"src/test/resources/core/get_principal_member_details_service_level"},
        glue = {"principal_member_details/steps/get_principal_member_details_service_level"}
)

public class GetPrincipalMemberDetailsServiceLevel {
}
