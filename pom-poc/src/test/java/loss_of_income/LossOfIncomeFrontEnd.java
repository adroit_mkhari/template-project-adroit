package loss_of_income;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        tags = {"@LossOfIncomeFrontEnd"},
        features = {"src/test/resources/core/loss_of_income_front_end"},
        glue = {"loss_of_income/steps/loss_of_income_front_end"}
)

public class LossOfIncomeFrontEnd {
}
