package convert_policy;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        tags = {"@ConvertPolicyServiceLevel"},
        features = {"src/test/resources/core/convert_policy_service_level"},
        glue = {"convert_policy/steps/convert_policy_service_level"}
)

public class ConvertPolicyServiceLevel {
}
