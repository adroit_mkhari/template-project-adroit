import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        tags = {"@Letters"},
        features = {"src/test/resources/core"},
        glue = {"steps"}
)

public class Letters {
}
