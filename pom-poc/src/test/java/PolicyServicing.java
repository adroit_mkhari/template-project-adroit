import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        tags = {"@PolicyDetails"},
        features = {"src/test/resources/core"},
        glue = {"steps"}
)

public class PolicyServicing {
}
