package refunds;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        tags = {"@RefundsFrontEnd"},
        features = {"src/test/resources/core/refunds_front_end"},
        glue = {"refunds/steps/refunds_front_end"}
)

public class RefundsFrontEnd {
}
