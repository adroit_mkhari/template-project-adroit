package attorney_management;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        tags = {"@AttorneyManagementFrontEnd"},
        features = {"src/test/resources/core/attorney_management_front_end"},
        glue = {"attorney_management/steps/attorney_management_front_end"}
)

public class AttorneyManagementFrontEnd {
}
