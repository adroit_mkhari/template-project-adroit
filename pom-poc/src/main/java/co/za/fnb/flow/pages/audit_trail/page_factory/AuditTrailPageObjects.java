package co.za.fnb.flow.pages.audit_trail.page_factory;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AuditTrailPageObjects {
    private String auditTrailDateLocator = "createSearchItemView:mainTabView:policyAuditTrailTable:0:auditTrailDate";
    private WebElement auditTrailDate;

    private String auditTrailTimeLocator = "createSearchItemView:mainTabView:policyAuditTrailTable:0:auditTrailTime";
    private WebElement auditTrailTime;

    private String auditTrailUserLocator = "createSearchItemView:mainTabView:policyAuditTrailTable:0:auditTrailUser";
    private WebElement auditTrailUser;

    private String auditTrailNameLocator = "createSearchItemView:mainTabView:policyAuditTrailTable:0:auditTrailName";
    private WebElement auditTrailName;

    private String auditTrailEventLocator = "createSearchItemView:mainTabView:policyAuditTrailTable:0:auditTrailEvent";
    private WebElement auditTrailEvent;

    @FindBy(xpath = "//*//tbody[contains(@id, 'createSearchItemView:mainTabView:policyAuditTrailTable_data')]//tr")
    private WebElement auditTrailTableData;

    private By auditTrailTableDataBy = By.xpath("//*//tbody[contains(@id, 'createSearchItemView:mainTabView:policyAuditTrailTable_data')]//tr");

    public AuditTrailPageObjects(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public String getAuditTrailDateLocator() {
        return auditTrailDateLocator;
    }

    public WebElement getAuditTrailDate() {
        return auditTrailDate;
    }

    public String getAuditTrailTimeLocator() {
        return auditTrailTimeLocator;
    }

    public WebElement getAuditTrailTime() {
        return auditTrailTime;
    }

    public String getAuditTrailUserLocator() {
        return auditTrailUserLocator;
    }

    public WebElement getAuditTrailUser() {
        return auditTrailUser;
    }

    public String getAuditTrailNameLocator() {
        return auditTrailNameLocator;
    }

    public WebElement getAuditTrailName() {
        return auditTrailName;
    }

    public String getAuditTrailEventLocator() {
        return auditTrailEventLocator;
    }

    public WebElement getAuditTrailEvent() {
        return auditTrailEvent;
    }

    public WebElement getAuditTrailTableData() {
        return auditTrailTableData;
    }

    public By getAuditTrailTableDataBy() {
        return auditTrailTableDataBy;
    }
}
