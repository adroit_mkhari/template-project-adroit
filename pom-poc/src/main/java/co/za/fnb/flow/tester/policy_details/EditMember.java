package co.za.fnb.flow.tester.policy_details;

import co.za.fnb.flow.models.policy_details.Member;
import co.za.fnb.flow.pages.CreateMultipleMintItemTable;
import co.za.fnb.flow.pages.ErrorHandle;
import co.za.fnb.flow.pages.TestHandle;
import co.za.fnb.flow.pages.policy_details.information.*;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;

public class EditMember {
    private Logger log  = LogManager.getLogger(EditMember.class);
    WebDriver driver;
    Member member;
    PolicyInformation policyInformation;
    MemberInformation memberInformation;
    BeneficiaryInformation beneficiaryInformation;
    BankInformation bankInformation;
    private String comment;
    private boolean valid;
    private ErrorHandle errorHandle = new ErrorHandle();
    private TestHandle testHandle = new TestHandle();

    public EditMember(WebDriver driver, Member member, ScenarioOperator scenarioOperator) {
        this.driver = driver;
        this.member = member;
        policyInformation = new PolicyInformation(driver, scenarioOperator);
        memberInformation = new MemberInformation(driver, scenarioOperator);
        beneficiaryInformation = new BeneficiaryInformation(driver, scenarioOperator);
        bankInformation = new BankInformation(driver, scenarioOperator);
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public void setExpectedResults() {
        policyInformation.setExpectedResults(member.getExpectResult());
    }

    public void edit() throws Exception {
        String clearValueFlag = "DELETE";
        log.info("Handling Edit Member logic");
        boolean isLawOnCall = member.getProductName().contains("LOC");
        int numberOfMembersToEdit = getIntegerValue(member.getNumberOfMembers()) != -1 ? getIntegerValue(member.getNumberOfMembers()) : 1;
        int numberOfExistingMembers = memberInformation.getNumberOfExistingMembers();

        log.info("This is a Law On Call Product: " + isLawOnCall);
        if (!isLawOnCall) {
            log.info("Updating Beneficiary Details.");
            if (!member.getBeneficiaryName().isEmpty()) {
                if (member.getBeneficiaryName().equalsIgnoreCase(clearValueFlag)) {
                    beneficiaryInformation.clearBeneficiaryName();
                } else {
                    beneficiaryInformation.updateBeneficiaryName(member.getBeneficiaryName());
                }
                Thread.sleep(1000);
            }

            if (!member.getBeneficiaryContactNumber().isEmpty()) {
                if (member.getBeneficiaryContactNumber().equalsIgnoreCase(clearValueFlag)) {
                    beneficiaryInformation.clearBeneficiaryContactNumber();
                } else {
                    beneficiaryInformation.updateBeneficiaryContactNumber(member.getBeneficiaryContactNumber());
                }
                Thread.sleep(1000);
            }

            if (!member.getBeneficiaryDateOfBirth().isEmpty()) {
                if (member.getBeneficiaryDateOfBirth().equalsIgnoreCase(clearValueFlag)) {
                    beneficiaryInformation.clearBeneficiaryDateOfBirth();
                } else {
                    beneficiaryInformation.updateBeneficiaryDateOfBirth(member.getBeneficiaryDateOfBirth());
                }
                Thread.sleep(1000);
            }

            if (!member.getBeneficiaryIdNumber().isEmpty()) {
                if (member.getBeneficiaryIdNumber().equalsIgnoreCase(clearValueFlag)) {
                    beneficiaryInformation.clearBeneficiaryIdNumber();
                } else {
                    beneficiaryInformation.updateBeneficiaryIdNumber(member.getBeneficiaryIdNumber());
                }
                Thread.sleep(1000);
            }

            if (!member.getBeneficiaryEmailAddress().isEmpty()) {
                if (member.getBeneficiaryEmailAddress().equalsIgnoreCase(clearValueFlag)) {
                    beneficiaryInformation.clearBeneficiaryEmailAddress();
                } else {
                    beneficiaryInformation.updateBeneficiaryEmailAddress(member.getBeneficiaryEmailAddress());
                }
                Thread.sleep(1000);
            }
        }

        String existingMemberRole;
        int editCount = 0;
        int lastEditedFlag = 0;
        for (int i = 0; i < numberOfMembersToEdit; i++) {
            if (editCount < numberOfMembersToEdit) {
                for (int j = 0; j < numberOfExistingMembers; j++) {
                    j = lastEditedFlag > j ? lastEditedFlag + 1 : j;
                    memberInformation.selectMember(j);
                    existingMemberRole = memberInformation.getMemberRelationship(j);

                    if (existingMemberRole.equalsIgnoreCase(member.getRelationship())) {
                        lastEditedFlag = j;
                        editCount++;

                        if (!member.getCompanyOrFullName().isEmpty()) {
                            if (member.getCompanyOrFullName().equalsIgnoreCase(clearValueFlag)) {
                                memberInformation.clearCompanyOrFullName(i);
                            } else {
                                memberInformation.updateCompanyOrFullName(i, member.getCompanyOrFullName());
                            }
                            Thread.sleep(1000);
                        }

                        if (!member.getProductName().equalsIgnoreCase("LOC-Business")) {
                            if (!member.getTradingAsOrMiddleName().isEmpty()) {
                                if (member.getTradingAsOrMiddleName().equalsIgnoreCase(clearValueFlag)) {
                                    memberInformation.clearMiddleName(i);
                                } else {
                                    memberInformation.updateMiddleName(i, member.getTradingAsOrMiddleName());
                                }
                                Thread.sleep(1000);
                            }
                        }

                        if (!member.getCompanyRegistrationOrIdNumber().isEmpty()) {
                            if (member.getCompanyRegistrationOrIdNumber().equalsIgnoreCase(clearValueFlag)) {
                                memberInformation.clearIdNumber(i);
                            } else {
                                memberInformation.updateIdNumber(i, member.getCompanyRegistrationOrIdNumber());
                            }
                            Thread.sleep(1000);
                        }

                        // Re-click at the member to load the Id Number.
                        memberInformation.selectMember(i);
                        Thread.sleep(1000);

                        if (!member.getProductName().equalsIgnoreCase("LOC-Business")) {
                            if (!member.getDateOfBirth().isEmpty()) {
                                if (member.getDateOfBirth().equalsIgnoreCase(clearValueFlag)) {
                                    memberInformation.clearDateOfBirth(i);
                                } else {
                                    memberInformation.updateDateOfBirth(i, member.getDateOfBirth());
                                }
                                Thread.sleep(1000);
                            }

                            // Re-click at the member to reload/refresh.
                            memberInformation.selectMember(i);
                            Thread.sleep(1000);

                            if (!member.getGender().isEmpty()) {
                                memberInformation.moveToMemberGenderAndDoubleClick(i);
                                memberInformation.selectMemberGender(i, member.getGender());
                                Thread.sleep(1000);
                            }
                        } else {
                            // TODO: Check if the email and contact number logic should go here. But at this point it does not make sense for it to be here.
                            if (!member.getEmailAddress().isEmpty()) {
                                if (member.getEmailAddress().equalsIgnoreCase(clearValueFlag)) {
                                    memberInformation.clearEmailAddress(i);
                                } else {
                                    memberInformation.updateEmailAddress(i, member.getEmailAddress());
                                }
                                Thread.sleep(1000);
                            }

                            if (!member.getCellPhoneNumber().isEmpty()) {
                                if (member.getCellPhoneNumber().equalsIgnoreCase(clearValueFlag)) {
                                    memberInformation.clearCellPhoneNumber(i);
                                } else {
                                    memberInformation.updateCellPhoneNumber(i, member.getCellPhoneNumber());
                                }
                                Thread.sleep(1000);
                            }
                        }

                        if (!isLawOnCall) {
                            // Load Cover Amount
                            memberInformation.loadCoverAmount(i);

                            if (!member.getCoverAmount().isEmpty()) {
                                if (member.getCoverAmount().equalsIgnoreCase(clearValueFlag)) {
                                    // TODO: Check If we will ever need to clear cover amount.
                                    // Note: Clear does not work.
                                    // quoteInformation.doubleClickCoverAmount(i);
                                } else {
                                    Double amount = Double.valueOf(member.getCoverAmount());
                                    memberInformation.updateCoverAmount(i, amount.toString());
                                }
                                Thread.sleep(1000);
                            }
                        }
                        break;
                    }
                }
            }
        }
        Thread.sleep(1000);

        log.info("Done Handling Edit Member logic");
    }

    private String getExpectedDoubleNumberFormat(String premiumAmountDataValue) {
        if (!premiumAmountDataValue.isEmpty()) {
            if (!premiumAmountDataValue.contains(".")) {
                if (premiumAmountDataValue.contains(",")) {
                    return premiumAmountDataValue.replace(",", ".");
                } else {
                    return premiumAmountDataValue + ".00";
                }
            }
        } else {
            log.error("Got an Empty String Value" );
            premiumAmountDataValue = "0.00";
        }
        return premiumAmountDataValue;
    }

    private int getIntegerValue(String value) {
        try {
            return !value.isEmpty() ? Integer.valueOf(value) : 1;
        } catch (Exception e) {
            log.debug(e.getMessage());
            try {
                if (value.contains(".")) {
                    String[] numberProps = value.split("\\.");
                    return Integer.valueOf(numberProps[0]);
                }
            } catch (Exception ex) {
                return 1;
            }
        }
        return -1;
    }

    public void save() throws Exception {
        try {
            log.info("Saving Edit Member.");
            String bankName = member.getBankName();
            String debitOrderDate = member.getDebitOrderDate();
            String nextDueDate = member.getNextDueDate();

            // Note: Should our premium status change to have more of these popup values we need to update these.
            String[] popUpWorkType = {member.getPopupWorkTypeZero(), member.getPopupWorkTypeOne()};
            String[] popUpStatus = {member.getPopupStatusZero(), member.getPopupStatusOne()};
            String[] popUpQueue = {member.getPopupQueueZero(), member.getPopupQueueOne()};
            // TODO: Check if we have any of these values for Premium Status ? For now we don't but still to confirm with @Shirley if we may possibly have at some point.
            String commentCategoryValue = "";
            String commentValue = "";

            if (!bankName.isEmpty()) {
                bankInformation.changeBankName(bankName);
            }

            if (!debitOrderDate.isEmpty()) {
                bankInformation.changeDebitOrderDate(debitOrderDate);
            }

            if (!nextDueDate.isEmpty()) {
                bankInformation.changeNextDueDate(nextDueDate);
            }

            if (commentCategoryValue.isEmpty() && commentValue.isEmpty()) {
                policyInformation.save(popUpWorkType, popUpStatus, popUpQueue);
            } else {
                policyInformation.save(popUpWorkType, popUpStatus, popUpQueue, commentCategoryValue, commentValue);
            }

            testHandle = policyInformation.getTestHandle();
            errorHandle = policyInformation.getErrorHandle();
        } catch (Exception e) {
            testHandle.setSuccess(false);
            errorHandle.setError(e.getMessage());
            throw new Exception("ERROR on Save: " + e.getMessage());
        }
    }

    public CreateMultipleMintItemTable getCreateMultipleMintItemTable() {
        return policyInformation.getCreateMultipleMintItemTable();
    }

    public void validate() {
        // TODO: Implement Validation Logic
        setValid(true);
    }

    public ErrorHandle getErrorHandle() {
        return this.errorHandle;
    }

    public void setErrorHandle(ErrorHandle errorHandle) {
        this.errorHandle = errorHandle;
    }

    public TestHandle getTestHandle() {
        return this.testHandle;
    }

    public void setTestHandle(TestHandle testHandle) {
        this.testHandle = testHandle;
    }

}