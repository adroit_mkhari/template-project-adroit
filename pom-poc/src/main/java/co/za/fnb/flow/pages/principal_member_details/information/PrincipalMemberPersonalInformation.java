package co.za.fnb.flow.pages.principal_member_details.information;

import co.za.fnb.flow.pages.principal_member_details.PrincipalMemberDetails;
import co.za.fnb.flow.pages.principal_member_details.page_factory.PrincipalMemberPersonalInformationPageObjects;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class PrincipalMemberPersonalInformation extends PrincipalMemberDetails {
    private Logger log  = LogManager.getLogger(PrincipalMemberPersonalInformation.class);
    PrincipalMemberPersonalInformationPageObjects principalMemberPersonalInformationPageObjects
            = new PrincipalMemberPersonalInformationPageObjects(driver, scenarioOperator);

    public PrincipalMemberPersonalInformation(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
        PageFactory.initElements(driver, this);
    }

    public void clearPolicyHolder() throws Exception {
        try {
            clear(principalMemberPersonalInformationPageObjects.getPolicyHolder());
        } catch (Exception e) {
            log.error("Error while clearing Policy Holder.");
            throw new Exception("Error while clearing Policy Holder.");
        }
    }

    public void updatePolicyHolder(String policyHolderData) throws Exception {
        try {
            type(principalMemberPersonalInformationPageObjects.getPolicyHolder(), policyHolderData);
        } catch (Exception e) {
            log.error("Error while updating Policy Holder.");
            throw new Exception("Error while updating Policy Holder.");
        }
    }

    public void clearMiddleName() throws Exception {
        try {
            clear(principalMemberPersonalInformationPageObjects.getMiddleName());
        } catch (Exception e) {
            log.error("Error while clearing Middle Name.");
            throw new Exception("Error while clearing Middle Name.");
        }
    }

    public void updateMiddleName(String middleName) throws Exception {
        try {
            type(principalMemberPersonalInformationPageObjects.getMiddleName(), middleName);
        } catch (Exception e) {
            log.error("Error while updating Middle Name.");
            throw new Exception("Error while updating Middle Name.");
        }
    }

    public void clearIdNumber() throws Exception {
        try {
            clear(principalMemberPersonalInformationPageObjects.getIdNumber());
        } catch (Exception e) {
            log.error("Error while clearing Id Number.");
            throw new Exception("Error while clearing Id Number.");
        }
    }

    public void updateIdNumber(String idNumber) throws Exception {
        try {
            type(principalMemberPersonalInformationPageObjects.getIdNumber(), idNumber);
        } catch (Exception e) {
            log.error("Error while clearing Id Number.");
            throw new Exception("Error while clearing Id Number.");
        }
    }

    public String getTypeOfId() throws Exception {
        try {
            String typeOfId = getAttribute(principalMemberPersonalInformationPageObjects.getTypeOfId(), "value");
            return typeOfId;
        } catch (Exception e) {
            log.error("Error while getting Type Of Id");
            throw new Exception("Error while getting Type Of Id");
        }
    }

    public void clearIncome() throws Exception {
        try {
            clear(principalMemberPersonalInformationPageObjects.getIncome());
        } catch (Exception e) {
            log.error("Error while clearing income.");
            throw new Exception("Error while clearing income.");
        }
    }

    public void updateIncome(String income) throws Exception {
        try {
            type(principalMemberPersonalInformationPageObjects.getIncome(), income);
        } catch (Exception e) {
            log.error("Error while updating income");
            throw new Exception("Error while updating income");
        }
    }

    public void moveToGender() throws Exception {
        try {
            moveToElement(principalMemberPersonalInformationPageObjects.getGender());
        } catch (Exception e) {
            log.error("Error while moving to Gender");
            throw new Exception("Error while moving to Gender");
        }
    }

    public void selectGender(String gender) throws Exception {
        try {
            selectUsingDataLabel(principalMemberPersonalInformationPageObjects.getGenderSelect(),
                    principalMemberPersonalInformationPageObjects.getGenderSelectItems(),
                    gender);
        } catch (Exception e) {
            log.error("Error while selecting Gender.");
            throw new Exception("Error while selecting Gender.");
        }
    }

    public void moveToCorrespondenceLanguage() throws Exception {
        try {
            moveToElement(principalMemberPersonalInformationPageObjects.getCorrespondenceLanguage());
        } catch (Exception e) {
            log.error("Error while moving to Correspondence Language");
            throw new Exception("Error while moving to Correspondence Language");
        }
    }

    public void selectCorrespondenceLanguage(String gender) throws Exception {
        try {
            selectUsingDataLabel(principalMemberPersonalInformationPageObjects.getCorrespondenceLanguageSelect(),
                    principalMemberPersonalInformationPageObjects.getCorrespondenceLanguageSelectItems(),
                    gender);
        } catch (Exception e) {
            log.error("Error while selecting Correspondence Language.");
            throw new Exception("Error while selecting Correspondence Language.");
        }
    }

    public void moveToMaritalStatus() throws Exception {
        try {
            moveToElement(principalMemberPersonalInformationPageObjects.getMaritalStatus());
        } catch (Exception e) {
            log.error("Error while moving to Marital Status");
            throw new Exception("Error while moving to Marital Status");
        }
    }

    public void selectMaritalStatus(String gender) throws Exception {
        try {
            selectUsingDataLabel(principalMemberPersonalInformationPageObjects.getMaritalStatusSelect(),
                    principalMemberPersonalInformationPageObjects.getMaritalStatusSelectItems(),
                    gender);
        } catch (Exception e) {
            log.error("Error while selecting Marital Status.");
            throw new Exception("Error while selecting Marital Status.");
        }
    }

    public String getDateOfBirth() throws Exception {
        try {
            String dateOfBirth = getAttribute(principalMemberPersonalInformationPageObjects.getDateOfBirth(), "value");
            return dateOfBirth;
        } catch (Exception e) {
            log.error("Error while getting Date Of Birth");
            throw new Exception("Error while getting Date Of Birth");
        }
    }

}
