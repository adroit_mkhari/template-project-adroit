package co.za.fnb.flow.tester.policy_details;

import co.za.fnb.flow.models.policy_details.Member;
import co.za.fnb.flow.pages.CreateMultipleMintItemTable;
import co.za.fnb.flow.pages.ErrorHandle;
import co.za.fnb.flow.pages.TestHandle;
import co.za.fnb.flow.pages.policy_details.information.BankInformation;
import co.za.fnb.flow.pages.policy_details.information.BeneficiaryInformation;
import co.za.fnb.flow.pages.policy_details.information.PolicyInformation;
import co.za.fnb.flow.pages.policy_details.information.QuoteInformation;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;

public class AddMember {
    private Logger log  = LogManager.getLogger(AddMember.class);
    WebDriver driver;
    Member member;
    PolicyInformation policyInformation;
    BeneficiaryInformation beneficiaryInformation;
    QuoteInformation quoteInformation;
    BankInformation bankInformation;
    private String comment;
    private boolean valid;
    private ErrorHandle errorHandle = new ErrorHandle();
    private TestHandle testHandle = new TestHandle();

    public AddMember(WebDriver driver, Member member, ScenarioOperator scenarioOperator) {
        this.driver = driver;
        this.member = member;
        policyInformation = new PolicyInformation(driver, scenarioOperator);
        beneficiaryInformation = new BeneficiaryInformation(driver, scenarioOperator);
        bankInformation = new BankInformation(driver, scenarioOperator);
        quoteInformation = new QuoteInformation(driver, scenarioOperator);
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public void setExpectedResults() {
        policyInformation.setExpectedResults(member.getExpectResult());
    }

    public void add() throws Exception {
        String clearValueFlag = "DELETE";
        log.info("Handling Add Member logic");
        Double premiumAmount = 0.00;
        Double totalQ = 0.00;
        boolean isLawOnCall = member.getProductName().contains("LOC");
        int numberOfMembersToAdd = getIntegerValue(member.getNumberOfMembers()) != -1 ? getIntegerValue(member.getNumberOfMembers()) : 1;

        log.info("This is a Law On Call Product: " + isLawOnCall);
        if (!isLawOnCall) {
            log.info("Updating Beneficiary Details.");
            if (!member.getBeneficiaryName().isEmpty()) {
                if (member.getBeneficiaryName().equalsIgnoreCase(clearValueFlag)) {
                    beneficiaryInformation.clearBeneficiaryName();
                } else {
                    beneficiaryInformation.updateBeneficiaryName(member.getBeneficiaryName());
                }
                Thread.sleep(1000);
            }

            if (!member.getBeneficiaryContactNumber().isEmpty()) {
                if (member.getBeneficiaryContactNumber().equalsIgnoreCase(clearValueFlag)) {
                    beneficiaryInformation.clearBeneficiaryContactNumber();
                } else {
                    beneficiaryInformation.updateBeneficiaryContactNumber(member.getBeneficiaryContactNumber());
                }
                Thread.sleep(1000);
            }

            if (!member.getBeneficiaryDateOfBirth().isEmpty()) {
                if (member.getBeneficiaryDateOfBirth().equalsIgnoreCase(clearValueFlag)) {
                    beneficiaryInformation.clearBeneficiaryDateOfBirth();
                } else {
                    beneficiaryInformation.updateBeneficiaryDateOfBirth(member.getBeneficiaryDateOfBirth());
                }
                Thread.sleep(1000);
            }

            if (!member.getBeneficiaryIdNumber().isEmpty()) {
                if (member.getBeneficiaryIdNumber().equalsIgnoreCase(clearValueFlag)) {
                    beneficiaryInformation.clearBeneficiaryIdNumber();
                } else {
                    beneficiaryInformation.updateBeneficiaryIdNumber(member.getBeneficiaryIdNumber());
                }
                Thread.sleep(1000);
            }

            if (!member.getBeneficiaryEmailAddress().isEmpty()) {
                if (member.getBeneficiaryEmailAddress().equalsIgnoreCase(clearValueFlag)) {
                    beneficiaryInformation.clearBeneficiaryEmailAddress();
                } else {
                    beneficiaryInformation.updateBeneficiaryEmailAddress(member.getBeneficiaryEmailAddress());
                }
                Thread.sleep(1000);
            }

            // TODO: Move This Out Of The Conditional Segment
            Thread.sleep(4000);
            premiumAmount = Double.valueOf(policyInformation.getTotalPremiumAmount());
            log.info("Done Updating Beneficiary Details.");
        }

        Thread.sleep(5000);
        for (int i = 0; i < numberOfMembersToAdd; i++) {
            Thread.sleep(3000);
            quoteInformation = policyInformation.newMember();
            Thread.sleep(2000);
            quoteInformation.selectMember(0);
            quoteInformation.moveToMemberRelationshipAndDoubleClick(0);

            String relationship = member.getRelationship();
            try {
                relationship = member.getRelationship().split(",")[i];
            } catch (Exception e) {
                e.printStackTrace();
            }

            quoteInformation.selectMemberRelationship(0, relationship);
            Thread.sleep(2000);

            if (!member.getCompanyOrFullName().isEmpty()) {
                String nameValue = member.getCompanyOrFullName();
                try {
                    nameValue = member.getCompanyOrFullName().split(",")[i];
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (member.getCompanyOrFullName().equalsIgnoreCase(clearValueFlag)) {
                    quoteInformation.clearCompanyOrFullName(0);
                } else {
                    quoteInformation.updateCompanyOrFullName(0, nameValue);
                }
                Thread.sleep(1000);
            }

            if (!member.getProductName().equalsIgnoreCase("LOC-Business")) {
                if (!member.getTradingAsOrMiddleName().isEmpty()) {
                    if (member.getTradingAsOrMiddleName().equalsIgnoreCase(clearValueFlag)) {
                        quoteInformation.clearMiddleName(0);
                    } else {
                        quoteInformation.updateMiddleName(0, member.getTradingAsOrMiddleName());
                    }
                    Thread.sleep(1000);
                }
            }

            if (!member.getCompanyRegistrationOrIdNumber().isEmpty()) {
                if (member.getCompanyRegistrationOrIdNumber().equalsIgnoreCase(clearValueFlag)) {
                    quoteInformation.clearIdNumber(0);
                } else {
                    quoteInformation.updateIdNumber(0, member.getCompanyRegistrationOrIdNumber());
                }
                Thread.sleep(1000);
            }

            // Re-click at the member to load the Id Number.
            quoteInformation.selectMember(0);
            Thread.sleep(1000);

            if (!member.getProductName().equalsIgnoreCase("LOC-Business")) {
                if (!member.getDateOfBirth().isEmpty()) {
                    String dobValue = member.getDateOfBirth();
                    try {
                        dobValue = member.getDateOfBirth().split(",")[i];
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (member.getDateOfBirth().equalsIgnoreCase(clearValueFlag)) {
                        quoteInformation.clearDateOfBirth(0);
                    } else {
                        quoteInformation.updateDateOfBirth(0, dobValue);
                    }
                    Thread.sleep(1000);
                }

                // Re-click at the member to reload/refresh.
                quoteInformation.selectMember(0);
                Thread.sleep(1000);

                if (!member.getGender().isEmpty()) {
                    quoteInformation.moveToMemberGenderAndDoubleClick(0);
                    quoteInformation.selectMemberGender(0, member.getGender());
                    Thread.sleep(1000);
                }
            } else {
                // TODO: Check if the email and contact number logic should go here. But at this point it does not make sense for it to be here.
                if (!member.getEmailAddress().isEmpty()) {
                    if (member.getEmailAddress().equalsIgnoreCase(clearValueFlag)) {
                        quoteInformation.clearEmailAddress(0);
                    } else {
                        quoteInformation.updateEmailAddress(0, member.getEmailAddress());
                    }
                    Thread.sleep(1000);
                }

                if (!member.getCellPhoneNumber().isEmpty()) {
                    if (member.getCellPhoneNumber().equalsIgnoreCase(clearValueFlag)) {
                        quoteInformation.clearCellPhoneNumber(0);
                    } else {
                        quoteInformation.updateCellPhoneNumber(0, member.getCellPhoneNumber());
                    }
                    Thread.sleep(1000);
                }
            }

            if (!member.getProductName().contains("LOC")) {
                // Load Cover Amount
                quoteInformation.loadCoverAmount(0);

                if (!member.getCoverAmount().isEmpty()) {
                    if (member.getCoverAmount().equalsIgnoreCase(clearValueFlag)) {
                        // TODO: Check If we will ever need to clear cover amount.
                        // Note: Clear does not work.
                        // quoteInformation.doubleClickCoverAmount(i);
                    } else {
                        Double amount = Double.valueOf(member.getCoverAmount());
                        quoteInformation.updateCoverAmount(0, amount.toString());
                    }
                    Thread.sleep(2000);
                    quoteInformation.selectMember(0);
                    Thread.sleep(1000);
                }

                String policyDetailsCoverAmount = quoteInformation.getPolicyDetailsCoverAmount(0);
                if (policyDetailsCoverAmount.isEmpty()) {
                    quoteInformation.takeScreenShot("");
                    log.error("Cover Amount Is Empty After Update.");
                    throw new Exception("Cover Amount Is Empty After Update.");
                }

                Thread.sleep(1000);
                quoteInformation.selectMember(0);
                Thread.sleep(1000);

                String premiumAmountDataValue = getExpectedDoubleNumberFormat(member.getPremiumAmount());
                String quotePremium = quoteInformation.getQuotePremium(0);
                quotePremium = !quotePremium.isEmpty() ? quotePremium : "0.00";

                Double premiumAmountValue = Double.valueOf(premiumAmountDataValue);
                Double quotePremiumValue = Double.valueOf(quotePremium);

                if (!member.getProductName().contains("LOC")) {
                    if (premiumAmountValue.equals(quotePremiumValue)) {
                        totalQ += quotePremiumValue;
                    } else {
                        quoteInformation.takeScreenShot("");
                        log.error("Premium on the DataSheet: \"" + premiumAmountValue + "\" is not equal to the one on screen: " + quotePremiumValue);
                        throw new Exception("Premium on the DataSheet: \"" + premiumAmountValue + "\" is not equal to the one on screen: " + quotePremiumValue);
                    }
                }
                log.info("Total Quote: " + totalQ);
            }

            // TODO: Find out if reassigning quoteInformation changes anything.
            quoteInformation = policyInformation.addToPolicy();
            Thread.sleep(1000);

            // TODO: Check if we need to get any responses.
            policyInformation.getResponses(false);
        }

        Double newPremiumAmount  = totalQ + premiumAmount;
        log.info("Total Quote: \"" +  totalQ + "\", New Premium Amount: \"" +  newPremiumAmount + "\"");
        // TODO: Find a way to Keep Record of this.
        // reportHandler.setCalculatedPremiumOrQuote("Total Quote: \"" +  totalQ + "\", New Premium Amount: \"" +  newPremiumAmount + "\"");
        Thread.sleep(1000);

        log.info("Done Handling Add Member logic");
    }

    private String getExpectedDoubleNumberFormat(String premiumAmountDataValue) {
        if (!premiumAmountDataValue.isEmpty()) {
            if (!premiumAmountDataValue.contains(".")) {
                if (premiumAmountDataValue.contains(",")) {
                    return premiumAmountDataValue.replace(",", ".");
                } else {
                    return premiumAmountDataValue + ".00";
                }
            }
        } else {
            log.error("Got an Empty String Value" );
            premiumAmountDataValue = "0.00";
        }
        return premiumAmountDataValue;
    }

    private int getIntegerValue(String value) {
        try {
            return !value.isEmpty() ? Integer.valueOf(value) : 1;
        } catch (Exception e) {
            log.debug(e.getMessage());
            try {
                if (value.contains(".")) {
                    String[] numberProps = value.split("\\.");
                    return Integer.valueOf(numberProps[0]);
                }
            } catch (Exception ex) {
                return 1;
            }
        }
        return -1;
    }

    public void save() throws Exception {
        try {
            log.info("Saving Premium Status.");
            String bankName = member.getBankName();
            String debitOrderDate = member.getDebitOrderDate();
            String nextDueDate = member.getNextDueDate();

            // Note: Should our premium status change to have more of these popup values we need to update these.
            String[] popUpWorkType = {member.getPopupWorkTypeZero(), member.getPopupWorkTypeOne()};
            String[] popUpStatus = {member.getPopupStatusZero(), member.getPopupStatusOne()};
            String[] popUpQueue = {member.getPopupQueueZero(), member.getPopupQueueOne()};
            // TODO: Check if we have any of these values for Premium Status ? For now we don't but still to confirm with @Shirley if we may possibly have at some point.
            String commentCategoryValue = "";
            String commentValue = "";

            if (!bankName.isEmpty()) {
                bankInformation.changeBankName(bankName);
            }

            if (!debitOrderDate.isEmpty()) {
                bankInformation.changeDebitOrderDate(debitOrderDate);
            }

            if (!nextDueDate.isEmpty()) {
                bankInformation.changeNextDueDate(nextDueDate);
            }

            bankInformation.selectAccountType("CHEQUE"); // CREDIT CARD, SAVINGS, TRANSMISSION

            if (commentCategoryValue.isEmpty() && commentValue.isEmpty()) {
                policyInformation.save(popUpWorkType, popUpStatus, popUpQueue);
            } else {
                policyInformation.save(popUpWorkType, popUpStatus, popUpQueue, commentCategoryValue, commentValue);
            }

            testHandle = policyInformation.getTestHandle();
            errorHandle = policyInformation.getErrorHandle();
        } catch (Exception e) {
            policyInformation.takeScreenShot("");
            testHandle.setSuccess(false);
            errorHandle.setError(e.getMessage());
            throw new Exception("ERROR on Save: " + e.getMessage());
        }
    }

    public CreateMultipleMintItemTable getCreateMultipleMintItemTable() {
        return policyInformation.getCreateMultipleMintItemTable();
    }

    public void validate() {
        // TODO: Implement Validation Logic
        setValid(true);
    }

    public ErrorHandle getErrorHandle() {
        return this.errorHandle;
    }

    public void setErrorHandle(ErrorHandle errorHandle) {
        this.errorHandle = errorHandle;
    }

    public TestHandle getTestHandle() {
        return this.testHandle;
    }

    public void setTestHandle(TestHandle testHandle) {
        this.testHandle = testHandle;
    }

}