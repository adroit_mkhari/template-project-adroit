package co.za.fnb.flow.pages;

import co.za.fnb.flow.pages.root_page_factory.PercentageAdministrationPageObjects;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.openqa.selenium.WebDriver;

public class PercentageAdministration extends BasePage {
    PercentageAdministrationPageObjects percentageAdministrationPageObjects = new PercentageAdministrationPageObjects(driver);

    public PercentageAdministration(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
    }
}
