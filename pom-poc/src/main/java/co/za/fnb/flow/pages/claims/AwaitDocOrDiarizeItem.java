package co.za.fnb.flow.pages.claims;

import co.za.fnb.flow.pages.BasePage;
import co.za.fnb.flow.pages.claims.page_factory.AwaitDocOrDiarizeItemPageObjects;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AwaitDocOrDiarizeItem extends BasePage {
    private Logger log  = LogManager.getLogger(AwaitDocOrDiarizeItem.class);
    AwaitDocOrDiarizeItemPageObjects awaitdocOrDiarizeItemPageObjects = new AwaitDocOrDiarizeItemPageObjects(driver);

    public AwaitDocOrDiarizeItem(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
    }

    public boolean waitForContent() throws Exception {
        log.info("Wiating For Content On: awaitdocOrDiarizeItemz");
        try {
            try {
                driver.switchTo().frame(0);
            } catch (Exception e) {
                // TODO: Handle Exception
                log.debug("Error switching to frame: " + e.getMessage());
            }
            WebElement time = awaitdocOrDiarizeItemPageObjects.getTime();
            WebDriverWait webDriverWait = new WebDriverWait(driver, 15);
            webDriverWait.until(ExpectedConditions.visibilityOf(time));
            return true;
        } catch (Exception e) {
            log.error("Content Not Displayed.");
            return false;
        }
    }

    public void clickTime() throws Exception {
        try {
            click(awaitdocOrDiarizeItemPageObjects.getTime());
        } catch (Exception e) {
            log.error("Error while clicking time (duration).");
            throw new Exception("Error while clicking on time (duration).");
        }
    }

    public void selectTime(String duration) throws Exception {
        try {
            selectExactMatchingDataLabel(awaitdocOrDiarizeItemPageObjects.getTimeLabel(), awaitdocOrDiarizeItemPageObjects.getTimeItems(), duration);
        } catch (Exception e) {
            log.error("Error while selecting time (duration).");
            throw new Exception("Error while selecting time (duration).");
        }
    }

    public void clickComments() throws Exception {
        try {
            click(awaitdocOrDiarizeItemPageObjects.getComments());
        } catch (Exception e) {
            log.error("Error while clicking on comments.");
            throw new Exception("Error while clicking on comments.");
        }
    }

    public void doubleClickComments() throws Exception {
        try {
            moveToElementAndDoubleClick(awaitdocOrDiarizeItemPageObjects.getComments());
        } catch (Exception e) {
            log.error("Error while double clicking on comments.");
            throw new Exception("Error while double clicking on comments.");
        }
    }

    public void updateComments(String comment) throws Exception {
        try {
            type(awaitdocOrDiarizeItemPageObjects.getComments(), comment);
        } catch (Exception e) {
            log.error("Error while updating comments.");
            throw new Exception("Error while updating comments.");
        }
    }

    public void clickReasons() throws Exception {
        try {
            click(awaitdocOrDiarizeItemPageObjects.getReasons());
        } catch (Exception e) {
            log.error("Error while clicking on reasons.");
            throw new Exception("Error while clicking on reasons.");
        }
    }

    public void selectReason(String reason) throws Exception {
        try {
            selectExactMatchingDataLabel(awaitdocOrDiarizeItemPageObjects.getReasonsLabel(), awaitdocOrDiarizeItemPageObjects.getReasonsItems(), reason);
        } catch (Exception e) {
            log.error("Error while selecting reason.");
            throw new Exception("Error while selecting reason.");
        }
    }

    public void clickSubmit() throws Exception {
        try {
            click(awaitdocOrDiarizeItemPageObjects.getSubmit());
        } catch (Exception e) {
            log.error("Error while clicking on submit.");
            throw new Exception("Error while clicking on submit.");
        }
    }

    public void clickCancel() throws Exception {
        try {
            click(awaitdocOrDiarizeItemPageObjects.getCancel());
        } catch (Exception e) {
            log.error("Error while clicking on cancel.");
            throw new Exception("Error while clicking on cancel.");
        }
    }

}
