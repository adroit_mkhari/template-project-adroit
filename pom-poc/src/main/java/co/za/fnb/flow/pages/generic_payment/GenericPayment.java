package co.za.fnb.flow.pages.generic_payment;

import co.za.fnb.flow.pages.generic_payment.page_factory.GenericPaymentPageObjects;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import co.za.fnb.flow.pages.BasePage;
import org.openqa.selenium.WebElement;

public class GenericPayment extends BasePage {
    private Logger log  = LogManager.getLogger(GenericPayment.class);
    GenericPaymentPageObjects genericPaymentPageObjects = new GenericPaymentPageObjects(driver);

    public GenericPayment(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
    }

    public int getRefundList() throws Exception {
        try {
            return getTableDataRiSize(genericPaymentPageObjects.getRefundListTableData());
        } catch (Exception e) {
            log.error("Error while getting refund list table size.");
            throw new Exception("Error while getting refund list table size.");
        }
    }

    public String getPolicyNumber(int index) throws Exception {
        try {
            String locator = getLocatorForIndex(genericPaymentPageObjects.getPolicyNumber(), index);
            WebElement policyNumberField = driver.findElement(By.id(locator));
            return getText(policyNumberField);
        } catch (Exception e) {
            log.error("Error while getting Policy Number.");
            throw new Exception("Error while getting Policy Number.");
        }
    }

    public void doublePolicyNumber(int index) throws Exception {
        try {
            String locator = getLocatorForIndex(genericPaymentPageObjects.getPolicyNumber(), index);
            WebElement policyNumberField = driver.findElement(By.id(locator));
            moveToElementAndDoubleClick(policyNumberField);
        } catch (Exception e) {
            log.error("Error while getting Policy Number.");
            throw new Exception("Error while getting Policy Number.");
        }
    }

    public String getStatus(int index) throws Exception {
        try {
            String locator = getLocatorForIndex(genericPaymentPageObjects.getStatus(), index);
            WebElement statusField = driver.findElement(By.id(locator));
            return getText(statusField);
        } catch (Exception e) {
            log.error("Error while getting Status.");
            throw new Exception("Error while getting Status.");
        }
    }

    public String getPayee(int index) throws Exception {
        try {
            String locator = getLocatorForIndex(genericPaymentPageObjects.getPayee(), index);
            WebElement payeeField = driver.findElement(By.id(locator));
            return getText(payeeField);
        } catch (Exception e) {
            log.error("Error while getting Payee.");
            throw new Exception("Error while getting Payee.");
        }
    }

    public String getAmount(int index) throws Exception {
        try {
            String locator = getLocatorForIndex(genericPaymentPageObjects.getAmount(), index);
            WebElement amountField = driver.findElement(By.id(locator));
            return getText(amountField);
        } catch (Exception e) {
            log.error("Error while getting Amount.");
            throw new Exception("Error while getting Amount.");
        }
    }

}
