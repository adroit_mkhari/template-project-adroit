package co.za.fnb.flow.pages.claims;

import co.za.fnb.flow.pages.BasePage;
import co.za.fnb.flow.pages.claims.page_factory.ApproveCaptureAttorneyPaymentPageObjects;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;

public class ApproveCaptureAttorneyPaymentDetails extends BasePage {
    private Logger log  = LogManager.getLogger(ApproveCaptureAttorneyPaymentDetails.class);
    ApproveCaptureAttorneyPaymentPageObjects approveCaptureAttorneyPaymentPageObjects = new ApproveCaptureAttorneyPaymentPageObjects(driver);

    public ApproveCaptureAttorneyPaymentDetails(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
    }

    public void clickApprove() throws Exception {
        try {
            moveToElementAndClickJsExec(approveCaptureAttorneyPaymentPageObjects.getApprove());
        } catch (Exception e) {
            log.error("Error while clicking on Attorney Payment Approve.");
            throw new Exception("Error while clicking on Attorney Payment Approve.");
        }
    }

    public void clickFail() throws Exception {
        try {
            moveToElementAndClickJsExec(approveCaptureAttorneyPaymentPageObjects.getDecline());
        } catch (Exception e) {
            log.error("Error while clicking on Attorney Payment Fail.");
            throw new Exception("Error while clicking on Attorney Payment Fail.");
        }
    }

    public void clickYesApprove() throws Exception {
        try {
            click(approveCaptureAttorneyPaymentPageObjects.getYesApprove());
        } catch (Exception e) {
            log.error("Error while clicking on Yes Approve.");
            throw new Exception("Error while clicking on Yes Approve.");
        }
    }

    public void clickNoDoNotApprove() throws Exception {
        try {
            click(approveCaptureAttorneyPaymentPageObjects.getNoApprove());
        } catch (Exception e) {
            log.error("Error while clicking on No Do Not Approve.");
            throw new Exception("Error while clicking on No Do Not Approve.");
        }
    }

}
