package co.za.fnb.flow.pages.principal_member_details.page_factory;

import co.za.fnb.flow.tester.ScenarioOperator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import co.za.fnb.flow.pages.principal_member_details.PrincipalMemberDetails;

public class PrincipalMemberPersonalInformationPageObjects extends PrincipalMemberDetails {

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:personalInformationPanel_content')]//table//tbody//tr//td//input[contains (@id, 'createSearchItemView:mainTabView:policyHolder')]")
    private WebElement policyHolder;

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:personalInformationPanel_content')]//table//tbody//tr//td//input[contains (@id, 'createSearchItemView:mainTabView:middleName')]")
    private WebElement middleName;

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:personalInformationPanel_content')]//table//tbody//tr//td//input[contains (@id, 'createSearchItemView:mainTabView:idNumber')]")
    private WebElement idNumber;

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:personalInformationPanel_content')]//table//tbody//tr//td//input[contains (@id, 'createSearchItemView:mainTabView:typeOfId')]")
    private WebElement typeOfId;

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:personalInformationPanel_content')]//table//tbody//tr//td//span[contains (@id, 'createSearchItemView:mainTabView:income')]//input[contains (@id, 'createSearchItemView:mainTabView:income_input')]")
    private WebElement income;

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:personalInformationPanel_content')]//table//tbody//tr//td//span[contains (@id, 'createSearchItemView:mainTabView:dateOfBirth')]//input[contains (@id, 'createSearchItemView:mainTabView:dateOfBirth_input')]")
    private WebElement dateOfBirth;

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:personalInformationPanel_content')]//table//tbody//tr//td//div[contains (@id, 'createSearchItemView:mainTabView:gender')]")
    private WebElement gender;

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:personalInformationPanel_content')]//table//tbody//tr//td//div[contains (@id, 'createSearchItemView:mainTabView:gender')]//label[contains (@id, 'createSearchItemView:mainTabView:gender_label')]")
    private WebElement genderSelect;

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:personalInformationPanel_content')]//table//tbody//tr//td//div[contains (@id, 'createSearchItemView:mainTabView:gender')]//div[contains (@id, 'createSearchItemView:mainTabView:gender_panel')]//div//ul[contains (@id, 'createSearchItemView:mainTabView:gender_items')]")
    private WebElement genderSelectItems;

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:personalInformationPanel_content')]//table//tbody//tr//td//div[contains (@id, 'createSearchItemView:mainTabView:correspondenceLanguage')]")
    private WebElement correspondenceLanguage;

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:personalInformationPanel_content')]//table//tbody//tr//td//div[contains (@id, 'createSearchItemView:mainTabView:correspondenceLanguage')]//div[contains (@class, 'ui-selectonemenu-trigger')]")
    private WebElement correspondenceLanguageSelect;

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:correspondenceLanguage_panel')]//div//ul[contains (@id, 'createSearchItemView:mainTabView:correspondenceLanguage_items')]")
    private WebElement correspondenceLanguageSelectItems;

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:personalInformationPanel_content')]//table//tbody//tr//td//div[contains (@id, 'createSearchItemView:mainTabView:maritalStatus')]")
    private WebElement maritalStatus;

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:personalInformationPanel_content')]//table//tbody//tr//td//div[contains (@id, 'createSearchItemView:mainTabView:maritalStatus')]//div[contains (@class, 'ui-selectonemenu-trigger')]")
    private WebElement maritalStatusSelect;

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:maritalStatus_panel')]//div//ul[contains (@id, 'createSearchItemView:mainTabView:maritalStatus_items')]")
    private WebElement maritalStatusSelectItems;

    public PrincipalMemberPersonalInformationPageObjects(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
        PageFactory.initElements(driver, this);
    }

    public WebElement getPolicyHolder() {
        return policyHolder;
    }

    public WebElement getMiddleName() {
        return middleName;
    }

    public WebElement getIdNumber() {
        return idNumber;
    }

    public WebElement getTypeOfId() {
        return typeOfId;
    }

    public WebElement getIncome() {
        return income;
    }

    public WebElement getDateOfBirth() {
        return dateOfBirth;
    }

    public WebElement getGender() {
        return gender;
    }

    public WebElement getGenderSelect() {
        return genderSelect;
    }

    public WebElement getGenderSelectItems() {
        return genderSelectItems;
    }

    public WebElement getCorrespondenceLanguage() {
        return correspondenceLanguage;
    }

    public WebElement getCorrespondenceLanguageSelect() {
        return correspondenceLanguageSelect;
    }

    public WebElement getCorrespondenceLanguageSelectItems() {
        return correspondenceLanguageSelectItems;
    }

    public WebElement getMaritalStatus() {
        return maritalStatus;
    }

    public WebElement getMaritalStatusSelect() {
        return maritalStatusSelect;
    }

    public WebElement getMaritalStatusSelectItems() {
        return maritalStatusSelectItems;
    }
}
