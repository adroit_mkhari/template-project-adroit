package co.za.fnb.flow.pages.principal_member_details;

import co.za.fnb.flow.pages.*;
import co.za.fnb.flow.pages.policy_details.page_factory.PolicyDetailsPageObjects;
import co.za.fnb.flow.pages.principal_member_details.page_factory.PrincipalMemberDetailsPageObjects;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class PrincipalMemberDetails extends BasePage {
    private Logger log  = LogManager.getLogger(PrincipalMemberDetails.class);
    PrincipalMemberDetailsPageObjects principalMemberDetailsPageObjects = new PrincipalMemberDetailsPageObjects(driver);
    // TODO: Check if Principal Member Use Any Of these PolicyDetailsPageObjects
    PolicyDetailsPageObjects policyDetailsPageObjects = new PolicyDetailsPageObjects(driver);

    PolicyDetailsLinkedPoliciesSave policyDetailsLinkedPoliciesSave;
    CreateMultipleMintItemTable createMultipleMintItemtable;
    private ErrorHandle errorHandle = new ErrorHandle();
    private TestHandle testHandle = new TestHandle();

    public PrincipalMemberDetails(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
        PageFactory.initElements(driver, this);
    }

    public void clickPersonalInformation() throws Exception {
        try {
            moveToElementAndClick(principalMemberDetailsPageObjects.getPersonalInformation());
        } catch (Exception e) {
            log.error("Error while clicking on Personal Information.");
            throw new Exception("Error while clicking on Personal Information.");
        }
    }

    public void clickResidentialInformation() throws Exception {
        try {
            moveToElementAndClick(principalMemberDetailsPageObjects.getResidentialInformation());
        } catch (Exception e) {
            log.error("Error while clicking on Residential Information.");
            throw new Exception("Error while clicking on Residential Information.");
        }
    }

    public void clickContactInformation() throws Exception {
        try {
            moveToElementAndClick(principalMemberDetailsPageObjects.getContactInformation());
        } catch (Exception e) {
            log.error("Error while clicking on Contact Information.");
            throw new Exception("Error while clicking on Contact Information.");
        }
    }

    public void clickPostalInformation() throws Exception {
        try {
            moveToElementAndClick(principalMemberDetailsPageObjects.getPostalInformation());
        } catch (Exception e) {
            log.error("Error while clicking on Postal Information.");
            throw new Exception("Error while clicking on Postal Information.");
        }
    }

    public void clickSave() throws Exception {
        try {
            click(principalMemberDetailsPageObjects.getSave());
        } catch (Exception e) {
            log.error("Error while clicking on Save.");
            throw new Exception("Error while clicking on Save.");
        }
    }

    public boolean getShowPrincipalMemberPoliciesDlg() throws Exception {
        try {
            boolean displayed = principalMemberDetailsPageObjects.getShowPrincipalMemberPoliciesDlg().isDisplayed();
            return displayed;
        } catch (Exception e) {
            log.error("Error while getting ShowPrincipalMemberPoliciesDlg.");
            throw new Exception("Error while getting ShowPrincipalMemberPoliciesDlg.");
        }
    }

    public void clickPrincipalMemberLinkedPoliciesSave() throws Exception {
        try {
            click(principalMemberDetailsPageObjects.getPrincipalMemberLinkedPoliciesSave());
        } catch (Exception e) {
            log.error("Error while clicking on Principal Member Linked Policies Save.");
            throw new Exception("Error while clicking on Principal Member Linked Policies Save.");
        }
    }

    @Override
    public ErrorHandle getErrorHandle() {
        return errorHandle;
    }

    @Override
    public TestHandle getTestHandle() {
        return testHandle;
    }

    @Override
    public void setErrorHandle(ErrorHandle errorHandle) {
        super.setErrorHandle(errorHandle);
    }

    @Override
    public void setTestHandle(TestHandle testHandle) {
        super.setTestHandle(testHandle);
    }

    public boolean checkShowPolicyDetailPoliciesDialog() {
        try {
            return policyDetailsPageObjects.getShowPolicyDetailPoliciesDialogTitle().isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    public void save(String[] popUpWorkType, String[] popUpStatus,String[] popUpQueue, String commentCategoryValue,String commentValue) throws Exception {
        log.info("Clicking on save.");
        clickSave();
        Thread.sleep(500);

        getResponses(true);
        Thread.sleep(1000);
        try {
            Thread.sleep(2000);
            boolean principalMemberPoliciesDlgShown = getShowPrincipalMemberPoliciesDlg();

            if (principalMemberPoliciesDlgShown) {
                clickPrincipalMemberLinkedPoliciesSave();
            }

            Thread.sleep(3000);
            getResponses(true);
        } catch (Exception e) {
            log.error("Error on save: " + e.getMessage());
            errorHandle = createMultipleMintItemtable.getErrorHandle();
            testHandle = createMultipleMintItemtable.getTestHandle();
            throw new Exception("Error on save: " + e.getMessage());
        }
        createMultipleMintItemtable.setPopUpWorkType(popUpWorkType);
        createMultipleMintItemtable.setPopUpStatus(popUpStatus);
        createMultipleMintItemtable.setPopUpQueue(popUpQueue);
        createMultipleMintItemtable.setCommentCategoryValue(commentCategoryValue);
        createMultipleMintItemtable.setCommentValue(commentValue);
        createMultipleMintItemtable.createMintItemForm();
        createMultipleMintItemtable.getTestResults();
        errorHandle = createMultipleMintItemtable.getErrorHandle();
        testHandle = createMultipleMintItemtable.getTestHandle();
    }

    public void save(String[] popUpWorkType, String[] popUpStatus, String[] popUpQueue) throws Exception {
        log.info("Clicking on save.");
        clickSave();
        Thread.sleep(500);

        getResponses(true);
        Thread.sleep(500);

        // TODO: Re-Visit the rest of the Save logic
        /*int numberOfFrames = 0;
        int waitCount = 0;
        boolean policiesDialog = false;
        final int countStop = 10;
        while (numberOfFrames == 0 && waitCount < countStop) {
            log.info("Waiting 2 seconds for create item popup after save.");
            Thread.sleep(2000);
            numberOfFrames = driver.findElements(policyDetailsPageObjects.iFrameTagBy).size();
            waitCount++;

            policiesDialog = waitCount == 1 && checkShowPolicyDetailPoliciesDialog();
            if (policiesDialog) {
                break;
            }

            if (numberOfFrames == 0 && waitCount == countStop - 1) {
                if (!checkShowPolicyDetailPoliciesDialog()) {
                    throw new Exception("Create/update popup not yet displayed. Probably a performance issue.");
                } else {
                    break;
                }
            }
        }
        log.info("Number of iFrames on screen: " + numberOfFrames);
        if (!policiesDialog && numberOfFrames >= 1) {
            getResponses(true);
        }*/

        try {
            boolean principalMemberPoliciesDlgShown = getShowPrincipalMemberPoliciesDlg();

            if (principalMemberPoliciesDlgShown) {
                clickPrincipalMemberLinkedPoliciesSave();
                Thread.sleep(1000);
            }
        } catch (Exception e) {
            log.error("Extra Popup: " + e.getMessage());
            errorHandle = createMultipleMintItemtable.getErrorHandle();
            testHandle = createMultipleMintItemtable.getTestHandle();
            throw new Exception("Extra Popup: " + e.getMessage());
        }
        getResponses(true);
        Thread.sleep(5000);
        createMultipleMintItemtable.setPopUpWorkType(popUpWorkType);
        createMultipleMintItemtable.setPopUpStatus(popUpStatus);
        createMultipleMintItemtable.setPopUpQueue(popUpQueue);
        createMultipleMintItemtable.createMintItemForm();
        getResponses(true);
        createMultipleMintItemtable.getTestResults();
        errorHandle = createMultipleMintItemtable.getErrorHandle();
        testHandle = createMultipleMintItemtable.getTestHandle();
    }

    public void proceedAfterClickingSave(String[] popUpWorkType, String[] popUpStatus, String[] popUpQueue) throws Exception {
        log.info("Proceeding after save.");
        getResponses(true);
        int numberOfFrames = 0;
        int waitCount = 0;
        boolean policiesDialog = false;
        final int countStop = 10;
        while (numberOfFrames == 0 && waitCount < countStop) {
            log.info("Waiting 2 seconds for create item popup after save.");
            Thread.sleep(2000);
            numberOfFrames = driver.findElements(policyDetailsPageObjects.iFrameTagBy).size();
            waitCount++;

            policiesDialog = waitCount == 1 && checkShowPolicyDetailPoliciesDialog();
            if (policiesDialog) {
                break;
            }

            if (numberOfFrames == 0 && waitCount == countStop - 1) {
                if (!checkShowPolicyDetailPoliciesDialog()) {
                    throw new Exception("Create/update popup not yet displayed. Probably a performance issue.");
                } else {
                    break;
                }
            }
        }
        log.info("Number of iFrames on screen: " + numberOfFrames);
        if (!policiesDialog && numberOfFrames >= 1) {
            getResponses(true);
        }

        try {
            policyDetailsLinkedPoliciesSave.save();
            getResponses(true);
        } catch (Exception e) {
            log.error("Extra Popup: " + e.getMessage());
            errorHandle = createMultipleMintItemtable.getErrorHandle();
            testHandle = createMultipleMintItemtable.getTestHandle();
            throw new Exception("Extra Popup: " + e.getMessage());
        }
        getResponses(true);
        Thread.sleep(5000);
        createMultipleMintItemtable.setPopUpWorkType(popUpWorkType);
        createMultipleMintItemtable.setPopUpStatus(popUpStatus);
        createMultipleMintItemtable.setPopUpQueue(popUpQueue);
        createMultipleMintItemtable.createMintItemForm();
        getResponses(true);
        createMultipleMintItemtable.getTestResults();
        errorHandle = createMultipleMintItemtable.getErrorHandle();
        testHandle = createMultipleMintItemtable.getTestHandle();
    }

    public void proceedAfterClickingSubmit(String[] popUpWorkType, String[] popUpStatus, String[] popUpQueue) throws Exception {
        log.info("Proceeding after submit.");
        getResponses(true);
        int numberOfFrames = 0;
        int waitCount = 0;
        boolean policiesDialog = false;
        final int countStop = 10;
        while (numberOfFrames == 0 && waitCount < countStop) {
            log.info("Waiting 2 seconds for create item popup after save.");
            Thread.sleep(2000);
            numberOfFrames = driver.findElements(policyDetailsPageObjects.iFrameTagBy).size();
            waitCount++;

            policiesDialog = waitCount == 1 && checkShowPolicyDetailPoliciesDialog();
            if (policiesDialog) {
                break;
            }

            if (numberOfFrames == 0 && waitCount == countStop - 1) {
                if (!checkShowPolicyDetailPoliciesDialog()) {
                    throw new Exception("Create/update popup not yet displayed. Probably a performance issue.");
                } else {
                    break;
                }
            }
        }
        log.info("Number of iFrames on screen: " + numberOfFrames);
        if (!policiesDialog && numberOfFrames >= 1) {
            getResponses(true);
        }

        try {
            // policyDetailsLinkedPoliciesSave.save();
            // getResponses(true);
        } catch (Exception e) {
            log.error("Extra Popup: " + e.getMessage());
            errorHandle = createMultipleMintItemtable.getErrorHandle();
            testHandle = createMultipleMintItemtable.getTestHandle();
            throw new Exception("Extra Popup: " + e.getMessage());
        }
        getResponses(true);
        Thread.sleep(5000);
        createMultipleMintItemtable.setPopUpWorkType(popUpWorkType);
        createMultipleMintItemtable.setPopUpStatus(popUpStatus);
        createMultipleMintItemtable.setPopUpQueue(popUpQueue);
        createMultipleMintItemtable.createMintItemForm();
        getResponses(true);
        createMultipleMintItemtable.getTestResults();
        errorHandle = createMultipleMintItemtable.getErrorHandle();
        testHandle = createMultipleMintItemtable.getTestHandle();
    }

    public void proceedAfterClickingSave(String[] popUpWorkType, String[] popUpStatus,String[] popUpQueue, String commentCategoryValue,String commentValue) throws Exception {
        log.info("Clicking on save.");
        click(policyDetailsPageObjects.getSave());
        Thread.sleep(1000);
        getResponses(true);
        try {
            Thread.sleep(2000);
            policyDetailsLinkedPoliciesSave.save();
            Thread.sleep(3000);
            getResponses(true);
        } catch (Exception e) {
            log.error("Error on save: " + e.getMessage());
            errorHandle = createMultipleMintItemtable.getErrorHandle();
            testHandle = createMultipleMintItemtable.getTestHandle();
            throw new Exception("Error on save: " + e.getMessage());
        }
        createMultipleMintItemtable.setPopUpWorkType(popUpWorkType);
        createMultipleMintItemtable.setPopUpStatus(popUpStatus);
        createMultipleMintItemtable.setPopUpQueue(popUpQueue);
        createMultipleMintItemtable.setCommentCategoryValue(commentCategoryValue);
        createMultipleMintItemtable.setCommentValue(commentValue);
        createMultipleMintItemtable.createMintItemForm();
        // TODO: Decide when to get the audit trail (before or after processing the results)
        createMultipleMintItemtable.getTestResults();
        errorHandle = createMultipleMintItemtable.getErrorHandle();
        testHandle = createMultipleMintItemtable.getTestHandle();
    }

    public void submitClaim(String[] popUpWorkType, String[] popUpStatus,String[] popUpQueue, String commentCategoryValue,String commentValue) throws Exception {
        log.info("Clicking on submit logic.");
        click(policyDetailsPageObjects.getSave());
        Thread.sleep(1000);
        getResponses(true);
        try {
            Thread.sleep(2000);
            policyDetailsLinkedPoliciesSave.save();
            Thread.sleep(3000);
            getResponses(true);
        } catch (Exception e) {
            log.error("Error on save: " + e.getMessage());
            errorHandle = createMultipleMintItemtable.getErrorHandle();
            testHandle = createMultipleMintItemtable.getTestHandle();
            throw new Exception("Error on save: " + e.getMessage());
        }
        createMultipleMintItemtable.setPopUpWorkType(popUpWorkType);
        createMultipleMintItemtable.setPopUpStatus(popUpStatus);
        createMultipleMintItemtable.setPopUpQueue(popUpQueue);
        createMultipleMintItemtable.setCommentCategoryValue(commentCategoryValue);
        createMultipleMintItemtable.setCommentValue(commentValue);
        createMultipleMintItemtable.createMintItemForm();
        // TODO: Decide when to get the audit trail (before or after processing the results)
        createMultipleMintItemtable.getTestResults();
        errorHandle = createMultipleMintItemtable.getErrorHandle();
        testHandle = createMultipleMintItemtable.getTestHandle();
    }

    public void submitClaim(String[] popUpWorkType, String[] popUpStatus, String[] popUpQueue) throws Exception {
        int numberOfFrames = 0;
        int waitCount = 0;
        final int countStop = 10;
        while (numberOfFrames == 0 && waitCount < countStop) {
            log.info("Waiting 2 seconds for create item popup after save.");
            Thread.sleep(2000);
            numberOfFrames = driver.findElements(policyDetailsPageObjects.iFrameTagBy).size();
            waitCount++;

            if (numberOfFrames == 0 && waitCount == countStop - 1) {
                throw new Exception("Create/update popup not yet displayed. Probably a performance issue.");
            }
        }
        log.info("Number of iFrames on screen: " + numberOfFrames);

        createMultipleMintItemtable.setPopUpWorkType(popUpWorkType);
        createMultipleMintItemtable.setPopUpStatus(popUpStatus);
        createMultipleMintItemtable.setPopUpQueue(popUpQueue);
        createMultipleMintItemtable.createMintItemFormSubmit();
        getResponses(true);
        createMultipleMintItemtable.getTestResults();
        errorHandle = createMultipleMintItemtable.getErrorHandle();
        testHandle = createMultipleMintItemtable.getTestHandle();
    }

    public void getResults() throws Exception {
        createMultipleMintItemtable.getTestResults();
        errorHandle = createMultipleMintItemtable.getErrorHandle();
        testHandle = createMultipleMintItemtable.getTestHandle();
    }

    public CreateMultipleMintItemTable getCreateMultipleMintItemTable() {
        return createMultipleMintItemtable;
    }

}
