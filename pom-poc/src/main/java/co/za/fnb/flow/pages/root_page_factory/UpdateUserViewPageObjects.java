package co.za.fnb.flow.pages.root_page_factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class UpdateUserViewPageObjects {

    @FindBy(id = "frmUserAdmin:tbvUserAdmin:tblMembers:userAdminFirstName:filter")
    private WebElement firstNameFilter;

    @FindBy(id = "frmUserAdmin:tbvUserAdmin:tblMembers:userAdminLastName:filter")
    private WebElement lastNameFilter;

    @FindBy(id = "frmUserAdmin:tbvUserAdmin:tblMembers:userAdminEmployeeNumber")
    private WebElement employeeNumberFilter;

    @FindBy(id = "frmUserAdmin:tbvUserAdmin:tblMembers:userAdminActive:filter")
    private WebElement activeFilter;

    @FindBy(id = "frmUserAdmin:tbvUserAdmin:tblMembers_data")
    private WebElement membersData;

    @FindBy(id = "frmUserAdmin:tbvUserAdmin:MenuItemView")
    private WebElement view;

    @FindBy(id = "frmUserAdmin:tbvUserAdmin:MenuItemEdit")
    private WebElement edit;

    @FindBy(id = "frmUserAdmin:tbvUserAdmin:MenuIemClone")
    private WebElement clone;

    @FindBy(id = "frmUserAdmin:tbvUserAdmin:MenuIemDelete")
    private WebElement delete;

    @FindBy(id = "frmUserAdmin:tbvUserAdmin:btnConfirmUserDeleteYes")
    private WebElement confirmDelete;

    public UpdateUserViewPageObjects(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public WebElement getFirstNameFilter() {
        return firstNameFilter;
    }

    public WebElement getLastNameFilter() {
        return lastNameFilter;
    }

    public WebElement getEmployeeNumberFilter() {
        return employeeNumberFilter;
    }

    public WebElement getActiveFilter() {
        return activeFilter;
    }

    public WebElement getMembersData() {
        return membersData;
    }

    public WebElement getView() {
        return view;
    }

    public WebElement getEdit() {
        return edit;
    }

    public WebElement getClone() {
        return clone;
    }

    public WebElement getDelete() {
        return delete;
    }

    public WebElement getConfirmDelete() {
        return confirmDelete;
    }
}
