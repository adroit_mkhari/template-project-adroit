package co.za.fnb.flow.models.policy_details;

public class Refund extends PolicyDetailsBase {
    private String transactionName;
    private String reason;
    private String pleaseProvideDetails;
    private String amount;
    private String refundType;
    private String payee;
    private String accountNumber;
    private String branchCode;
    private String accountType;
    private String approve;
    private String yesDo;
    private String secondExpectedResult;
    // Note, List Of Fields in Constructor: testCaseNumber|productName|scenarioDescription|policyNumberCode|policyNumber|function|run|firstPopup|secondPopup|debitOrderDate|nextDueDate|bankName|expectResult|workTypeZero|statusZero|queueZero|popupWorkTypeZero|popupStatusZero|popupQueueZero|auditTrail|updatedTe|risk|sanction|edd|kyc|status|transactionName|reason|pleaseProvideDetails|amount|refundType|payee|accountNumber|branchCode|accountType

    public Refund(String testCaseNumber, String productName, String scenarioDescription, String policyNumberCode, String policyNumber, String function, String run, String firstPopup, String secondPopup, String debitOrderDate, String nextDueDate, String bankName, String expectResult, String workTypeZero, String statusZero, String queueZero, String popupWorkTypeZero, String popupStatusZero, String popupQueueZero, String auditTrail, String updatedTe, String risk, String sanction, String edd, String kyc, String status) {
        super(testCaseNumber, productName, scenarioDescription, policyNumberCode, policyNumber, function, run, firstPopup, secondPopup, debitOrderDate, nextDueDate, bankName, expectResult, workTypeZero, statusZero, queueZero, popupWorkTypeZero, popupStatusZero, popupQueueZero, auditTrail, updatedTe, risk, sanction, edd, kyc, status);
    }

    public Refund(String testCaseNumber, String productName, String scenarioDescription, String policyNumberCode, String policyNumber, String function, String run, String firstPopup, String secondPopup, String debitOrderDate, String nextDueDate, String bankName, String expectResult, String workTypeZero, String statusZero, String queueZero, String popupWorkTypeZero, String popupStatusZero, String popupQueueZero, String auditTrail, String updatedTe, String risk, String sanction, String edd, String kyc, String status, String transactionName, String reason, String pleaseProvideDetails, String amount, String refundType, String payee, String accountNumber, String branchCode, String accountType, String approve, String yesDo, String secondExpectedResult) {
        super(testCaseNumber, productName, scenarioDescription, policyNumberCode, policyNumber, function, run, firstPopup, secondPopup, debitOrderDate, nextDueDate, bankName, expectResult, workTypeZero, statusZero, queueZero, popupWorkTypeZero, popupStatusZero, popupQueueZero, auditTrail, updatedTe, risk, sanction, edd, kyc, status);
        this.transactionName = transactionName;
        this.reason = reason;
        this.pleaseProvideDetails = pleaseProvideDetails;
        this.amount = amount;
        this.refundType = refundType;
        this.payee = payee;
        this.accountNumber = accountNumber;
        this.branchCode = branchCode;
        this.accountType = accountType;
        this.approve = approve;
        this.yesDo = yesDo;
        this.secondExpectedResult = secondExpectedResult;
    }

    public String getTransactionName() {
        return transactionName;
    }

    public void setTransactionName(String transactionName) {
        this.transactionName = transactionName;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getPleaseProvideDetails() {
        return pleaseProvideDetails;
    }

    public void setPleaseProvideDetails(String pleaseProvideDetails) {
        this.pleaseProvideDetails = pleaseProvideDetails;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getRefundType() {
        return refundType;
    }

    public void setRefundType(String refundType) {
        this.refundType = refundType;
    }

    public String getPayee() {
        return payee;
    }

    public void setPayee(String payee) {
        this.payee = payee;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getApprove() {
        return approve;
    }

    public void setApprove(String approve) {
        this.approve = approve;
    }

    public String getYesDo() {
        return yesDo;
    }

    public void setYesDo(String yesDo) {
        this.yesDo = yesDo;
    }

    public String getSecondExpectedResult() {
        return secondExpectedResult;
    }

    public void setSecondExpectedResult(String secondExpectedResult) {
        this.secondExpectedResult = secondExpectedResult;
    }

    @Override
    public String toString() {
        return "Refund{" +
                "transactionName='" + transactionName + '\'' +
                ", reason='" + reason + '\'' +
                ", pleaseProvideDetails='" + pleaseProvideDetails + '\'' +
                ", amount='" + amount + '\'' +
                ", refundType='" + refundType + '\'' +
                ", payee='" + payee + '\'' +
                ", accountNumber='" + accountNumber + '\'' +
                ", branchCode='" + branchCode + '\'' +
                ", accountType='" + accountType + '\'' +
                ", secondExpectedResult='" + secondExpectedResult + '\'' +
                '}';
    }
}
