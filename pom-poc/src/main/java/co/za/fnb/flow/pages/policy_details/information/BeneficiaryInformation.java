package co.za.fnb.flow.pages.policy_details.information;

import co.za.fnb.flow.pages.policy_details.page_factory.BeneficiaryInformationPageObjects;
import co.za.fnb.flow.tester.PolicyDetailsRunner;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import co.za.fnb.flow.pages.policy_details.PolicyDetails;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BeneficiaryInformation extends PolicyDetails {
    BeneficiaryInformationPageObjects beneficiaryInformationPageObjects = new BeneficiaryInformationPageObjects(driver);
    private Logger log  = LogManager.getLogger(PolicyDetailsRunner.class);

    public BeneficiaryInformation(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
    }

    public void clearBeneficiaryName() throws Exception {
        try {
            WebElement nameField = beneficiaryInformationPageObjects.getName();
            WebDriverWait webDriverWait = new WebDriverWait(driver, 90);
            webDriverWait.until(ExpectedConditions.visibilityOf(nameField));
            clear(nameField);
        } catch (Exception e) {
            log.error("Error while clearing Beneficiary Name.");
            throw new Exception("Error while clearing Beneficiary Name.");
        }
    }

    public void updateBeneficiaryName(String name) throws Exception {
        try {
            clearBeneficiaryName();
            type(beneficiaryInformationPageObjects.getName(), name);
        } catch (Exception e) {
            log.error("Error while updating Beneficiary Name.");
            throw new Exception("Error while updating Beneficiary Name.");
        }
    }

    public void clearBeneficiaryContactNumber() throws Exception {
        try {
            clear(beneficiaryInformationPageObjects.getContactNumber());
        } catch (Exception e) {
            log.error("Error while clearing Beneficiary Contact Number.");
            throw new Exception("Error while clearing Beneficiary Contact Number.");
        }
    }

    public void updateBeneficiaryContactNumber(String contactNumber) throws Exception {
        try {
            clearBeneficiaryContactNumber();
            type(beneficiaryInformationPageObjects.getContactNumber(), contactNumber);
        } catch (Exception e) {
            log.error("Error while updating Beneficiary Contact Number.");
            throw new Exception("Error while updating Beneficiary Contact Number.");
        }
    }

    public void clearBeneficiaryDateOfBirth() throws Exception {
        try {
            clear(beneficiaryInformationPageObjects.getDateOfBirth());
        } catch (Exception e) {
            log.error("Error while clearing Beneficiary Date Of Birth.");
            throw new Exception("Error while clearing Beneficiary Date Of Birth.");
        }
    }

    public void updateBeneficiaryDateOfBirth(String dateOfBirth) throws Exception {
        try {
            clearBeneficiaryDateOfBirth();
            Thread.sleep(3000);
            type(beneficiaryInformationPageObjects.getDateOfBirth(), dateOfBirth);
        } catch (Exception e) {
            log.error("Error while updating Beneficiary Date Of Birth.");
            throw new Exception("Error while updating Beneficiary Date Of Birth.");
        }
    }

    public void clearBeneficiaryIdNumber() throws Exception {
        try {
            clear(beneficiaryInformationPageObjects.getIdNumber());
        } catch (Exception e) {
            log.error("Error while clearing Beneficiary Id Number.");
            throw new Exception("Error while clearing Beneficiary Id Number.");
        }
    }

    public void updateBeneficiaryIdNumber(String idNumber) throws Exception {
        try {
            clearBeneficiaryIdNumber();
            type(beneficiaryInformationPageObjects.getIdNumber(), idNumber);
        } catch (Exception e) {
            log.error("Error while updating Beneficiary Id Number.");
            throw new Exception("Error while updating Beneficiary Id Number.");
        }
    }

    public void clearBeneficiaryEmailAddress() throws Exception {
        try {
            clear(beneficiaryInformationPageObjects.getEmailAddress());
        } catch (Exception e) {
            log.error("Error while clearing Beneficiary Email Address");
            throw new Exception("Error while clearing Beneficiary Email Address.");
        }
    }

    public void updateBeneficiaryEmailAddress(String idNumber) throws Exception {
        try {
            clearBeneficiaryEmailAddress();
            type(beneficiaryInformationPageObjects.getEmailAddress(), idNumber);
        } catch (Exception e) {
            log.error("Error while updating Beneficiary Email Address.");
            throw new Exception("Error while updating Beneficiary EmailAddress.");
        }
    }

}
