package co.za.fnb.flow.pages.sales_entity;

import co.za.fnb.flow.pages.sales_entity.page_factory.VoiceRecordingRetrievalPopupPageObjects;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.openqa.selenium.WebDriver;

public class VoiceRecordingRetrievalPopup extends SalesEntity {
    VoiceRecordingRetrievalPopupPageObjects voiceRecordingRetrievalPopupPageObjects = new VoiceRecordingRetrievalPopupPageObjects(driver);

    public VoiceRecordingRetrievalPopup(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
    }
}
