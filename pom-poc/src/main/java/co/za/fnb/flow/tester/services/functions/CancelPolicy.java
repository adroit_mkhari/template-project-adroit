package co.za.fnb.flow.tester.services.functions;

import co.za.fnb.flow.models.custom_exceptions.ServiceException;
import co.za.fnb.flow.tester.ScenarioOperator;
import co.za.fnb.flow.tester.ScenarioTester;
import co.za.fnb.flow.tester.services.CancelPolicyTester;
import generated.CancelPolicyRequestInput;
import generated.CancelPolicyResponseOutput;
import generated.Error;
import generated.Errors;
import generated.ResponseErrors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class CancelPolicy {
    private Logger log  = LogManager.getLogger(CancelPolicy.class);
    private ResponseErrors responseErrors;
    private ScenarioTester scenarioTester;
    private CancelPolicyRequestInput cancelPolicyRequestInput;
    private CancelPolicyResponseOutput cancelPolicyResponseOutput;
    private final String CLEAR = "CLEAR";
    private ScenarioOperator scenarioOperator;
    private String username;

    public CancelPolicy(ScenarioTester scenarioTester) {
        this.scenarioTester = scenarioTester;
    }

    public ScenarioOperator getScenarioOperator() {
        return scenarioOperator;
    }

    public void setScenarioOperator(ScenarioOperator scenarioOperator) {
        this.scenarioOperator = scenarioOperator;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void sendRequest() throws Exception {
        scenarioOperator = scenarioTester.getScenarioOperator();
        String policyNumber = scenarioOperator.getPolicyNumber();
        username = scenarioTester.getCellValue("Username");
        String function = scenarioOperator.getFunction();
        String reason = scenarioTester.getCellValue("Reason");
        String comments = scenarioTester.getCellValue("Comments");

        if (cancelPolicyRequestInput == null) {
            cancelPolicyRequestInput = new CancelPolicyRequestInput();
            cancelPolicyRequestInput.setUsername(username);
            cancelPolicyRequestInput.setPolicy(policyNumber);
            cancelPolicyRequestInput.setReason(reason);
            cancelPolicyRequestInput.setComments(comments);
        }

        CancelPolicyTester cancelPolicyTester = new CancelPolicyTester(cancelPolicyRequestInput);

        StringBuilder updatePolicyErrorStringBuilder = new StringBuilder();
        try {
            cancelPolicyResponseOutput = cancelPolicyTester.sendCancelPolicyRequest();
        } catch (Exception e) {
            responseErrors = cancelPolicyTester.getResponseErrors();
            Errors errors = responseErrors.getErrors();
            List<generated.Error> errorList = errors.getError();
            for (Error error : errorList) {
                updatePolicyErrorStringBuilder.append("Code: ").append(error.getCode())
                        .append(", Subject: ").append(error.getSubject())
                        .append(", Description: ").append(error.getDescription())
                        .append(", Adjunct: ").append(error.getAdjunct())
                        .append("\n");
            }
            throw new ServiceException(updatePolicyErrorStringBuilder.toString());
        }
    }

    public void performValidations() throws Exception {
        String expectedPolicyStatus = scenarioTester.getCellValue("Expected Policy Status");
        String expectedPolicyStatusDate = scenarioTester.getCellValue("Expected Policy Status Date");
    }

}
