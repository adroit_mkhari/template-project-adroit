package co.za.fnb.flow.tester.services;

import co.za.fnb.flow.handlers.JAXBHelper;
import co.za.fnb.flow.setup.PropertiesSetup;
import generated.*;
import generated.ObjectFactory;
import iseries.wsbeans.gsd00030pr.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.util.Properties;

public class UpdatePrincipalMemberDetailsTester {
    private Logger log  = LogManager.getLogger(UpdatePrincipalMemberDetailsTester.class);
    private UpdatePrincipalMemberDetailRequestInput updatePrincipalMemberDetailsRequestInput;
    private JAXBHelper marshallerJaxbHelper = new JAXBHelper();
    private JAXBHelper unMarshallerJaxbHelper = new JAXBHelper();
    private JAXBElement<Object> input;
    private UpdatePrincipalMemberDetailResponseOutput updatePrincipalMemberDetailResponseOutput;
    private ResponseErrors responseErrors;
    private Properties properties = null;
    private String environment = "";

    public UpdatePrincipalMemberDetailsTester(UpdatePrincipalMemberDetailRequestInput updatePrincipalMemberDetailsRequestInput) {
        this.updatePrincipalMemberDetailsRequestInput = updatePrincipalMemberDetailsRequestInput;
        try {
            // Setup The Marshalling Context
            marshallerJaxbHelper.setCustomJaxbContext(UpdatePrincipalMemberDetailRequestInput.class);
            ObjectFactory objectFactory = new ObjectFactory();
            input = objectFactory.createInput(updatePrincipalMemberDetailsRequestInput);
            marshallerJaxbHelper.setDataObject(input);

            // Setup The Expected Output UnMarshalling Context
            unMarshallerJaxbHelper.setCustomJaxbContext(UpdatePrincipalMemberDetailResponseOutput.class);

            log.info("Loading Properties");
            PropertiesSetup propertiesSetup = new PropertiesSetup();
            propertiesSetup.loadProperties();
            properties = propertiesSetup.getProperties();
            environment = properties.getProperty("ENVIROMENT");
            log.info("Done Loading Properties");
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    public UpdatePrincipalMemberDetailRequestInput getUpdatePrincipalMemberDetailsRequestInput() {
        return updatePrincipalMemberDetailsRequestInput;
    }

    public JAXBHelper getMarshallerJaxbHelper() {
        return marshallerJaxbHelper;
    }

    public JAXBHelper getUnMarshallerJaxbHelper() {
        return unMarshallerJaxbHelper;
    }

    public JAXBElement<Object> getInput() {
        return input;
    }

    public UpdatePrincipalMemberDetailResponseOutput getUpdatePrincipalMemberDetailResponseOutput() {
        return updatePrincipalMemberDetailResponseOutput;
    }

    public ResponseErrors getResponseErrors() {
        return responseErrors;
    }

    public UpdatePrincipalMemberDetailResponseOutput sendUpdatePrincipalMemberRequest() throws Exception {
        String marshalResult = marshallerJaxbHelper.marshal(false);

        GSD00030PRPre gsd00030PRPre = new GSD00030PRPre();
        GSD00030PRTst gsd00030PRTst = new GSD00030PRTst();
        GSD00030PRServices gsd00030PRServicesPort = null;

        if (!environment.isEmpty()) {
            if (environment.equalsIgnoreCase("pre")) {
                gsd00030PRServicesPort = gsd00030PRPre.getGSD00030PRServicesPort();
            } else {
                gsd00030PRServicesPort = gsd00030PRTst.getGSD00030PRServicesPort();
            }
        } else {
            log.error("Error: No Target Environment Specified.");
            throw new Exception("Error: No Target Environment Specified.");
        }

        iseries.wsbeans.gsd00030pr.ObjectFactory serviceObjectFactory = new iseries.wsbeans.gsd00030pr.ObjectFactory();
        GensrvUPDATEPRINCIPALMEMBERDETAILInput updatePrincipalMemberDetailsInput = serviceObjectFactory.createGensrvUPDATEPRINCIPALMEMBERDETAILInput();
        PXMLIN pxmlin = serviceObjectFactory.createPXMLIN();
        pxmlin.setLength(10000);
        pxmlin.setString(marshalResult);
        updatePrincipalMemberDetailsInput.setPXMLIN(pxmlin);
        GensrvUPDATEPRINCIPALMEMBERDETAILResult gensrvUPDATEPRINCIPALMEMBERDETAILResult = gsd00030PRServicesPort.gensrvUpdateprincipalmemberdetail(updatePrincipalMemberDetailsInput);
        PXMLOUT updatePrincipalMemberDetailsResponseOutput = gensrvUPDATEPRINCIPALMEMBERDETAILResult.getPXMLOUT();
        String pXmlOutString = updatePrincipalMemberDetailsResponseOutput.getString();

        unMarshallerJaxbHelper.formatOutputXml(pXmlOutString, UpdatePrincipalMemberDetailResponseOutput.class);
        Object unMarshal = unMarshallerJaxbHelper.unMarshal();
         if (!(unMarshal instanceof ResponseErrors)) {
             updatePrincipalMemberDetailResponseOutput = (UpdatePrincipalMemberDetailResponseOutput) unMarshal;
             return updatePrincipalMemberDetailResponseOutput;
         } else {
             responseErrors = (ResponseErrors) unMarshal;
             throw new Exception("Error while getting UpdatePrincipalMemberDetailResponseOutput: " + ((ResponseErrors) unMarshal).getErrors());
         }
    }
}
