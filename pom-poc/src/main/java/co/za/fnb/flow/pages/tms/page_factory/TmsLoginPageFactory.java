package co.za.fnb.flow.pages.tms.page_factory;

import co.za.fnb.flow.pages.BasePage;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class TmsLoginPageFactory extends BasePage {
    // Signin F Number
    // @id = fNumber
    @FindBy(id = "fNumber")
    private WebElement fNumber;

    // Signin Password
    // @id = password
    @FindBy(id = "password")
    private WebElement password;

    // Login Submit
    // @xpath = //*[@id="loginForm"]/div[2]/div[2]/button
    @FindBy(xpath = "//*[@id=\"loginForm\"]/div[2]/div[2]/button")
    private WebElement loginSubmit;

    public TmsLoginPageFactory(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
        PageFactory.initElements(driver, this);
    }

    public WebElement getfNumber() {
        return fNumber;
    }

    public WebElement getPassword() {
        return password;
    }

    public WebElement getLoginSubmit() {
        return loginSubmit;
    }

}
