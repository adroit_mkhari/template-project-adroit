package co.za.fnb.flow.tester.services.models;

public class Beneficiary {
    private String add;
    private String relationship;
    private String name;
    private String birthDate;
    private String idNumber;
    private String gender;
    private String cellNumber;
    private String email;

    public Beneficiary() {
    }

    public Beneficiary(String add, String relationship, String name, String birthDate, String idNumber, String gender, String cellNumber, String email) {
        this.add = add;
        this.relationship = relationship;
        this.name = name;
        this.birthDate = birthDate;
        this.idNumber = idNumber;
        this.gender = gender;
        this.cellNumber = cellNumber;
        this.email = email;
    }

    public String getAdd() {
        return add;
    }

    public void setAdd(String add) {
        this.add = add;
    }

    public String getRelationship() {
        return relationship;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCellNumber() {
        return cellNumber;
    }

    public void setCellNumber(String cellNumber) {
        this.cellNumber = cellNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
