package co.za.fnb.flow.pages.sales_entity.page_factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SalesEntityPageObjects {

    @FindBy(id = "createSearchItemView:mainTabView:cisRefNumber")
    private WebElement cisRefNumber;

    @FindBy(id = "createSearchItemView:mainTabView:salesPersonNumber")
    private WebElement salesPersonNumber;

    @FindBy(id = "createSearchItemView:mainTabView:salesPerson")
    private WebElement salesPerson;

    @FindBy(id = "createSearchItemView:mainTabView:branch")
    private WebElement branch;

    @FindBy(id = "createSearchItemView:mainTabView:branchNumber")
    private WebElement branchNumber;

    @FindBy(id = "createSearchItemView:mainTabView:origChannelDate")
    private WebElement originalChannelDate;

    @FindBy(id = "createSearchItemView:mainTabView:origChannel")
    private WebElement originalChannel;

    @FindBy(id = "createSearchItemView:mainTabView:origLoadDate")
    private WebElement originalLoadDate;

    @FindBy(id = "createSearchItemView:mainTabView:lastChannelDate")
    private WebElement lastChannelDate;

    @FindBy(id = "createSearchItemView:mainTabView:lastChannel")
    private WebElement lastChannel;

    @FindBy(id = "createSearchItemView:mainTabView:lastLoadDate")
    private WebElement lastLoadDate;

    @FindBy(id = "createSearchItemView:mainTabView:ajax")
    private WebElement voiceRecordingDetailsLink;

    public SalesEntityPageObjects(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public WebElement getCisRefNumber() {
        return cisRefNumber;
    }

    public WebElement getSalesPersonNumber() {
        return salesPersonNumber;
    }

    public WebElement getSalesPerson() {
        return salesPerson;
    }

    public WebElement getBranch() {
        return branch;
    }

    public WebElement getBranchNumber() {
        return branchNumber;
    }

    public WebElement getOriginalChannelDate() {
        return originalChannelDate;
    }

    public WebElement getOriginalChannel() {
        return originalChannel;
    }

    public WebElement getOriginalLoadDate() {
        return originalLoadDate;
    }

    public WebElement getLastChannelDate() {
        return lastChannelDate;
    }

    public WebElement getLastChannel() {
        return lastChannel;
    }

    public WebElement getLastLoadDate() {
        return lastLoadDate;
    }

    public WebElement getVoiceRecordingDetailsLink() {
        return voiceRecordingDetailsLink;
    }
}
