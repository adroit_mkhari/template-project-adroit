package co.za.fnb.flow.pages;

import co.za.fnb.flow.pages.root_page_factory.UpdateUserViewPageObjects;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.openqa.selenium.WebDriver;

public class UpdateUserView extends BasePage {
    UpdateUserViewPageObjects updateUserViewPageObjects = new UpdateUserViewPageObjects(driver);

    public UpdateUserView(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
    }
}
