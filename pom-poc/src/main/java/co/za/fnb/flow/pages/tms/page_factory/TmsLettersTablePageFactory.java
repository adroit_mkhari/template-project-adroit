package co.za.fnb.flow.pages.tms.page_factory;

import co.za.fnb.flow.pages.BasePage;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class TmsLettersTablePageFactory extends BasePage {
    @FindBy(xpath = "//*[@id=\"search\"]")
    private WebElement searchTextInputField;

    // Letters Table Tows List
    // @xpath = //*[@id="distributionSearch"]/tbody/tr
    private String lettersTableRowsList = "//*[@id=\"distributionSearch\"]/tbody/tr";

    // Letters Table
    // @xpath = //*[@id="distributionSearch"]/tbody/tr[1]/td[1]/input
    private String lettersTableCellLocatorTemplateXpath = "//*[@id=\"distributionSearch\"]/tbody/tr[1]/td[1]/input";

    // Letters Table
    // @xpath = //*[@id="distributionSearch"]/tbody/tr[1]
    // Note: 1 is the row index
    private String rowLocatorTemplateXpath = "//*[@id=\"distributionSearch\"]/tbody/tr[1]";

    // Check Box
    // @xpath = //*[@id="distributionSearch"]/tbody/tr[1]/td[1]/input
    private String checkBoxLocatorTemplateXpath = "//*[@id=\"distributionSearch\"]/tbody/tr[1]/td[1]/input";

    // Document Description
    // @xpath = //*[@id="masterTemplateName-1"]
    private String documentDescriptionLocatorTemplateXpath = "//*[@id=\"masterTemplateName-1\"]";

    // Company Code
    // @xpath = //*[@id="company-1"]
    private String companyCodeLocatorTemplateXpath = "//*[@id=\"company-1\"]";

    // Document Code
    // @xpath = //*[@id="docType-1"]
    private String documentCodeLocatorTemplateXpath = "//*[@id=\"docType-1\"]";

    // Document Name
    // @xpath = //*[@id="docName-1"]
    private String documentNameLocatorTemplateXpath = "//*[@id=\"docName-1\"]";

    // Distribution Method
    // @xpath = //*[@id="method-1"]
    private String distributionMethodLocatorTemplateXpath = "//*[@id=\"method-1\"]";

    // Last Modified User
    // @xpath = //*[@id="user-1"]
    private String lastModifiedUserLocatorTemplateXpath = "//*[@id=\"user-1\"]";

    // Last Modified Date
    // @xpath = //*[@id="modifyDate-1"]
    private String lastModifiedDateLocatorTemplateXpath = "//*[@id=\"modifyDate-1\"]";

    // Send Address
    // @xpath = //*[@id="address-1"]
    private String sendAddressLocatorTemplateXpath = "//*[@id=\"address-1\"]";

    // Theme
    // @xpath = //*[@id="theme-1"]
    private String themeLocatorTemplateXpath = "//*[@id=\"theme-1\"]";

    public TmsLettersTablePageFactory(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
        PageFactory.initElements(driver, this);
    }

    public String getLettersTableRowsList() {
        return lettersTableRowsList;
    }

    public String getLettersTableCellLocatorTemplateXpath() {
        return lettersTableCellLocatorTemplateXpath;
    }

    public String getRowLocatorTemplateXpath() {
        return rowLocatorTemplateXpath;
    }

    public String getCheckBoxLocatorTemplateXpath() {
        return checkBoxLocatorTemplateXpath;
    }

    public String getDocumentDescriptionLocatorTemplateXpath() {
        return documentDescriptionLocatorTemplateXpath;
    }

    public String getCompanyCodeLocatorTemplateXpath() {
        return companyCodeLocatorTemplateXpath;
    }

    public String getDocumentCodeLocatorTemplateXpath() {
        return documentCodeLocatorTemplateXpath;
    }

    public String getDocumentNameLocatorTemplateXpath() {
        return documentNameLocatorTemplateXpath;
    }

    public String getDistributionMethodLocatorTemplateXpath() {
        return distributionMethodLocatorTemplateXpath;
    }

    public String getLastModifiedUserLocatorTemplateXpath() {
        return lastModifiedUserLocatorTemplateXpath;
    }

    public String getLastModifiedDateLocatorTemplateXpath() {
        return lastModifiedDateLocatorTemplateXpath;
    }

    public String getSendAddressLocatorTemplateXpath() {
        return sendAddressLocatorTemplateXpath;
    }

    public String getThemeLocatorTemplateXpath() {
        return themeLocatorTemplateXpath;
    }

    public WebElement getSearchTextInputField() {
        return searchTextInputField;
    }
}
