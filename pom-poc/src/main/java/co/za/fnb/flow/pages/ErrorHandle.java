package co.za.fnb.flow.pages;

public class ErrorHandle {
    private String error;

    public ErrorHandle() {
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
