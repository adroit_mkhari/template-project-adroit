package co.za.fnb.flow.tester.policy_details;

import co.za.fnb.flow.handlers.database.QueryHandler;
import co.za.fnb.flow.models.policy_details.Member;
import co.za.fnb.flow.models.policy_details.PolicyInformationModel;
import co.za.fnb.flow.pages.CreateMultipleMintItemTable;
import co.za.fnb.flow.pages.ErrorHandle;
import co.za.fnb.flow.pages.TestHandle;
import co.za.fnb.flow.pages.policy_details.information.*;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class EscalateMember {
    private Logger log  = LogManager.getLogger(EscalateMember.class);
    WebDriver driver;
    Member member;
    PolicyInformation policyInformation;
    ReinstatePolicyPopupDialog reinstatePolicyPopupDialog;
    BeneficiaryInformation beneficiaryInformation;
    MemberInformation memberInformation;
    BankInformation bankInformation;
    private String comment;
    private boolean valid;
    private ErrorHandle errorHandle = new ErrorHandle();
    private TestHandle testHandle = new TestHandle();
    QueryHandler queryHandler = new QueryHandler();
    PolicyInformationModel policyInformationModel;
    private String escalationStatusValue;
    private String totalPremiumAmountValue;
    private List<PolicyInformationModel> policyInformationBeforeEscalation = new ArrayList<PolicyInformationModel>();
    private List<PolicyInformationModel> policyInformationAfterEscalation = new ArrayList<PolicyInformationModel>();

    public EscalateMember(WebDriver driver, Member member, ScenarioOperator scenarioOperator) {
        this.driver = driver;
        this.member = member;
        policyInformation = new PolicyInformation(driver, scenarioOperator);
        reinstatePolicyPopupDialog = new ReinstatePolicyPopupDialog(driver, scenarioOperator);
        beneficiaryInformation = new BeneficiaryInformation(driver, scenarioOperator);
        bankInformation = new BankInformation(driver, scenarioOperator);
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public String getEscalationStatusValue() {
        return escalationStatusValue;
    }

    public void setEscalationStatusValue(String escalationStatusValue) {
        this.escalationStatusValue = escalationStatusValue;
    }

    public String getTotalPremiumAmountValue() {
        return totalPremiumAmountValue;
    }

    public void setTotalPremiumAmountValue(String totalPremiumAmountValue) {
        this.totalPremiumAmountValue = totalPremiumAmountValue;
    }

    public List<PolicyInformationModel> getPolicyInformationBeforeEscalation() {
        return policyInformationBeforeEscalation;
    }

    public void setPolicyInformationBeforeEscalation(List<PolicyInformationModel> policyInformationBeforeEscalation) {
        this.policyInformationBeforeEscalation = policyInformationBeforeEscalation;
    }

    public void addPolicyInformationBeforeEscalation(PolicyInformationModel policyInformation) {
        policyInformationBeforeEscalation.add(policyInformation);
    }

    public List<PolicyInformationModel> getPolicyInformationAfterEscalation() {
        return policyInformationAfterEscalation;
    }

    public void setPolicyInformationAfterEscalation(List<PolicyInformationModel> policyInformationAfterEscalation) {
        this.policyInformationAfterEscalation = policyInformationAfterEscalation;
    }

    public void addPolicyInformationAfterEscalation(PolicyInformationModel policyInformation) {
        policyInformationAfterEscalation.add(policyInformation);
    }

    public void setExpectedResults() {
        policyInformation.setExpectedResults(member.getExpectResult());
    }

    public void escalate(String dbPolicyNumber, String escalationAction) throws Exception {
        // region Values Before
        ResultSet policyInformationSet = queryHandler.queryForPolicyInformation(dbPolicyNumber);

        while (policyInformationSet.next()) {
            policyInformationModel = new PolicyInformationModel(
                    policyInformationSet.getString("ESCALDATE").trim(),
                    policyInformationSet.getString("POLICYNO").trim(),
                    policyInformationSet.getString("YEAR").trim(),
                    policyInformationSet.getString("STRTPREM").trim(),
                    policyInformationSet.getString("STRTCOVER").trim(),
                    policyInformationSet.getString("PREVPREM").trim(),
                    policyInformationSet.getString("PREVCOVER").trim(),
                    policyInformationSet.getString("NEWPREM").trim(),
                    policyInformationSet.getString("NEWCOVER").trim(),
                    policyInformationSet.getString("OPTOUT").trim(),
                    policyInformationSet.getString("ROLETYP_ID").trim()
            );
            addPolicyInformationBeforeEscalation(policyInformationModel);
        }

        setEscalationStatusValue(policyInformation.getEscalationStatus());
        setTotalPremiumAmountValue(policyInformation.getTotalPremiumAmount());
        // endregion

        policyInformation.changeEscalationAction(escalationAction);
    }

    private Double getPolicyMembersTotalPremium() throws Exception {
        Double totalPremium = 0.00;
        String premium;
        Double premiumValue;
        try {
            int numberOfExistingMembers = memberInformation.getNumberOfExistingMembers();

            for(int j = 0; j < numberOfExistingMembers ; j++) {
                premium = memberInformation.getPolicyDetailsPremium(j);
                premium = getDoubleString(premium);
                premiumValue = stringToDouble(premium);
                totalPremium += premiumValue;
            }
            return totalPremium;
        } catch (Exception e) {
            log.error("Error while getting policy members total premium.");
            throw new Exception("Error while getting policy members total premium.");
        }
    }

    private String getExpectedDoubleNumberFormat(String premiumAmountDataValue) {
        if (!premiumAmountDataValue.isEmpty()) {
            if (!premiumAmountDataValue.contains(".")) {
                if (premiumAmountDataValue.contains(",")) {
                    return premiumAmountDataValue.replace(",", ".");
                } else {
                    return premiumAmountDataValue + ".00";
                }
            }
        } else {
            log.error("Got an Empty String Value" );
            premiumAmountDataValue = "0.00";
        }
        return premiumAmountDataValue;
    }

    private int getIntegerValue(String value) {
        try {
            return !value.isEmpty() ? Integer.valueOf(value) : 1;
        } catch (Exception e) {
            log.debug(e.getMessage());
            try {
                if (value.contains(".")) {
                    String[] numberProps = value.split("\\.");
                    return Integer.valueOf(numberProps[0]);
                }
            } catch (Exception ex) {
                return 1;
            }
        }
        return -1;
    }

    private Double stringToDouble(String policyHolderCoverAmount) throws Exception {
        try {
            return Double.valueOf(policyHolderCoverAmount);
        } catch (Exception e) {
            log.error("Error while getting double value: " + e.getMessage());
            throw new Exception("Error while getting double value: " + e.getMessage());
        }
    }

    private String getDoubleString(String numberString) {
        // TODO: Try to get the double value first.
        if (numberString.isEmpty()) {
            numberString = "0.00";
        }

        if (!numberString.contains(".")) {
            return numberString + ".00";
        } else {
            return numberString;
        }
    }

    public void save() throws Exception {
        try {
            log.info("Saving Escalate Member.");
            String bankName = member.getBankName();
            String debitOrderDate = member.getDebitOrderDate();
            String nextDueDate = member.getNextDueDate();

            // Note: Should our premium status change to have more of these popup values we need to update these.
            String[] popUpWorkType = {member.getPopupWorkTypeZero(), member.getPopupWorkTypeOne()};
            String[] popUpStatus = {member.getPopupStatusZero(), member.getPopupStatusOne()};
            String[] popUpQueue = {member.getPopupQueueZero(), member.getPopupQueueOne()};
            // TODO: Check if we have any of these values for Premium Status ? For now we don't but still to confirm with @Shirley if we may possibly have at some point.
            String commentCategoryValue = "";
            String commentValue = "";

            if (!bankName.isEmpty()) {
                bankInformation.changeBankName(bankName);
            }

            if (!debitOrderDate.isEmpty()) {
                bankInformation.changeDebitOrderDate(debitOrderDate);
            }

            if (!nextDueDate.isEmpty()) {
                bankInformation.changeNextDueDate(nextDueDate);
            }

            if (commentCategoryValue.isEmpty() && commentValue.isEmpty()) {
                policyInformation.save(popUpWorkType, popUpStatus, popUpQueue);
            } else {
                policyInformation.save(popUpWorkType, popUpStatus, popUpQueue, commentCategoryValue, commentValue);
            }

            testHandle = policyInformation.getTestHandle();
            errorHandle = policyInformation.getErrorHandle();
        } catch (Exception e) {
            testHandle.setSuccess(false);
            errorHandle.setError(e.getMessage());
            throw new Exception("ERROR on Save: " + e.getMessage());
        }
    }

    public CreateMultipleMintItemTable getCreateMultipleMintItemTable() {
        return policyInformation.getCreateMultipleMintItemTable();
    }

    public void validate(String dbPolicyNumber, String escalationAction) throws Exception {
        // TODO: Remember to click and be on Policy details.

        // region Values After Escalation
        ResultSet policyInformationSet = queryHandler.queryForPolicyInformation(dbPolicyNumber);

        while (policyInformationSet.next()) {
            policyInformationModel = new PolicyInformationModel(
                    policyInformationSet.getString("ESCALDATE").trim(),
                    policyInformationSet.getString("POLICYNO").trim(),
                    policyInformationSet.getString("YEAR").trim(),
                    policyInformationSet.getString("STRTPREM").trim(),
                    policyInformationSet.getString("STRTCOVER").trim(),
                    policyInformationSet.getString("PREVPREM").trim(),
                    policyInformationSet.getString("PREVCOVER").trim(),
                    policyInformationSet.getString("NEWPREM").trim(),
                    policyInformationSet.getString("NEWCOVER").trim(),
                    policyInformationSet.getString("OPTOUT").trim(),
                    policyInformationSet.getString("ROLETYP_ID").trim()
            );
            addPolicyInformationAfterEscalation(policyInformationModel);
        }
        // endregion

        setEscalationStatusValue(policyInformation.getEscalationStatus());

        PolicyInformationModel policyInfoBefore, policyInfoAfter;
        Double previousPremium, newPremium, previousCoverAmount, newCoverAmount;
        int escalationYearBefore, escalationYearAfter, yearBefore, yearAfter;

        if (escalationAction.equalsIgnoreCase("Opt-In to Escalation")) {
            if (!escalationStatusValue.equalsIgnoreCase("OPT IN")) {
                setValid(false);
                setComment("Failed: Opt-In to Escalation");
            } else {
                if (policyInformationBeforeEscalation.size() > 0) {
                    for (int i = 0; i < policyInformationAfterEscalation.size(); i++) {
                        policyInfoBefore = policyInformationBeforeEscalation.get(i);
                        policyInfoAfter = policyInformationAfterEscalation.get(i);

                        if (policyInfoBefore.getOptOut().equalsIgnoreCase("I")) {
                            escalationYearBefore = Integer.parseInt(policyInfoBefore.getEscalationDate().split("/")[0]);
                            escalationYearAfter = Integer.parseInt(policyInfoAfter.getEscalationDate().split("/")[0]);
                            yearBefore = Integer.parseInt(policyInfoBefore.getYear());
                            yearAfter = Integer.parseInt(policyInfoAfter.getYear());

                            if ((escalationYearAfter - escalationYearBefore != -1) && (yearBefore - yearAfter != -1)) {
                                setValid(false);
                                setComment("Failed: Opt-In to Escalation. Reason -> Expected Escalation Date/Year not correct.");
                            }
                        }
                    }
                }
                setValid(true);
            }
        } else if (escalationAction.equalsIgnoreCase("Opt-Out of Escalation Temporary")) {
            if (!escalationStatusValue.equalsIgnoreCase("OPT IN")) {
                setValid(false);
                setComment("Failed: Opt-Out of Escalation Temporary");
            } else {
                for (int i = 0; i < policyInformationAfterEscalation.size(); i++) {
                    policyInfoBefore = policyInformationBeforeEscalation.get(i);
                    policyInfoAfter = policyInformationAfterEscalation.get(i);

                    if (policyInfoBefore.getOptOut().equalsIgnoreCase("I")) {
                        escalationYearBefore = Integer.parseInt(policyInfoBefore.getEscalationDate().split("/")[0]);
                        escalationYearAfter = Integer.parseInt(policyInfoAfter.getEscalationDate().split("/")[0]);
                        yearBefore = Integer.parseInt(policyInfoBefore.getYear());
                        yearAfter = Integer.parseInt(policyInfoAfter.getYear());

                        if ((escalationYearAfter - escalationYearBefore != 1) && (yearBefore - yearAfter != 1)) {
                            setValid(false);
                            setComment("Failed: Opt-Out of Escalation Temporary. Reason -> Expected Escalation Date/Year not correct.");
                        }
                    }
                }
                setValid(true);
            }
        }  else if (escalationAction.equalsIgnoreCase("Opt-Out of Escalation Permanently")) {
            if (!escalationStatusValue.equalsIgnoreCase("OPT OUT")) {
                setValid(false);
                setComment("Failed: Opt-Out of Escalation Permanently");
            } else {
                for (int i = 0; i < policyInformationAfterEscalation.size(); i++) {
                    policyInfoBefore = policyInformationBeforeEscalation.get(i);
                    policyInfoAfter = policyInformationAfterEscalation.get(i);

                    if (policyInfoBefore.getOptOut().equalsIgnoreCase("I")) {
                        if (!policyInfoAfter.getOptOut().equalsIgnoreCase("P")) {
                            setValid(false);
                            setComment("Failed: Opt-Out of Escalation Permanently. Reason -> Opt-Out status not 'P' as expected.");
                        }
                    }
                }
                setValid(true);
            }
        } else if (escalationAction.equalsIgnoreCase("Revert Escalation on Policy")) {
            if (!escalationStatusValue.equalsIgnoreCase("OPT IN")) {
                // TODO: Decide whether or not to check the escalation status.
                setValid(false);
                setComment("Failed: Revert Escalation on Policy");
            } else {
                for (int i = 0; i < policyInformationAfterEscalation.size(); i++) {
                    policyInfoBefore = policyInformationBeforeEscalation.get(i);
                    policyInfoAfter = policyInformationAfterEscalation.get(i);

                    if (policyInfoBefore.getOptOut().equalsIgnoreCase("I")) {
                        escalationYearBefore = Integer.parseInt(policyInfoBefore.getEscalationDate().split("/")[0]);
                        escalationYearAfter = Integer.parseInt(policyInfoAfter.getEscalationDate().split("/")[0]);
                        yearBefore = Integer.parseInt(policyInfoBefore.getYear());
                        yearAfter = Integer.parseInt(policyInfoAfter.getYear());

                        if (yearBefore > 1) {
                            if ((escalationYearAfter - escalationYearBefore != 1) && (yearBefore - yearAfter != 1)) {
                                setValid(false);
                                setComment("Failed: Revert Escalation on Policy. Reason -> Expected Escalation Date not correct.");
                            }

                            previousPremium = Double.valueOf(policyInfoBefore.getPreviousPremium());
                            previousCoverAmount = Double.valueOf(policyInfoBefore.getPreviousCover());
                            newPremium = Double.valueOf(policyInfoAfter.getNewPremium());
                            newCoverAmount = Double.valueOf(policyInfoAfter.getNewCover());

                            if (!(newPremium.equals(previousPremium) && newCoverAmount.equals(previousCoverAmount))) {
                                setValid(false);
                                setComment("Failed: Revert Escalation on Policy. Reason -> Expected Escalation Premium or Cover Amount not correct.");
                            }
                        }
                    }
                }
                setValid(true);
            }

            // TODO: Front-end Validations
        }
        // endregion
    }

    public ErrorHandle getErrorHandle() {
        return this.errorHandle;
    }

    public void setErrorHandle(ErrorHandle errorHandle) {
        this.errorHandle = errorHandle;
    }

    public TestHandle getTestHandle() {
        return this.testHandle;
    }

    public void setTestHandle(TestHandle testHandle) {
        this.testHandle = testHandle;
    }

}