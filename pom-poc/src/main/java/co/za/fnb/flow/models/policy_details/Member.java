package co.za.fnb.flow.models.policy_details;

public class Member extends PolicyDetailsBase {
    private String relationship;
    private String companyOrFullName;
    private String tradingAsOrMiddleName;
    private String companyRegistrationOrIdNumber;
    private String dateOfBirth;
    private String gender;
    private String coverAmount;
    private String premiumAmount;
    private String discount;
    private String emailAddress;
    private String cellPhoneNumber;
    private String bankName;
    private String numberOfMembers; // Number Of Members we want to add.
    private String beneficiaryName;
    private String beneficiaryDateOfBirth;
    private String beneficiaryIdNumber;
    private String beneficiaryContactNumber;
    private String beneficiaryEmailAddress;
    private String workTypeOne;
    private String statusOne;
    private String queueOne;
    private String popupWorkTypeOne;
    private String popupStatusOne;
    private String popupQueueOne;
    private boolean familyBundle;

    public Member(String testCaseNumber, String productName, String scenarioDescription, String policyNumberCode, String policyNumber, String function, String run, String firstPopup, String secondPopup, String debitOrderDate, String nextDueDate, String bankName, String expectResult, String workTypeZero, String statusZero, String queueZero, String popupWorkTypeZero, String popupStatusZero, String popupQueueZero, String auditTrail, String updatedTe, String risk, String sanction, String edd, String kyc, String status) {
        super(testCaseNumber, productName, scenarioDescription, policyNumberCode, policyNumber, function, run, firstPopup, secondPopup, debitOrderDate, nextDueDate, bankName, expectResult, workTypeZero, statusZero, queueZero, popupWorkTypeZero, popupStatusZero, popupQueueZero, auditTrail, updatedTe, risk, sanction, edd, kyc, status);
    }

    public Member(String testCaseNumber, String productName, String scenarioDescription, String policyNumberCode, String policyNumber, String function, String run, String firstPopup, String secondPopup, String debitOrderDate, String nextDueDate, String bankName, String expectResult, String workTypeZero, String statusZero, String queueZero, String workTypeOne, String statusOne, String queueOne, String popupWorkTypeZero, String popupStatusZero, String popupQueueZero, String popupWorkTypeOne, String popupStatusOne, String popupQueueOne, String auditTrail, String updatedTe, String risk, String sanction, String edd, String kyc, String status, String relationship, String companyOrFullName, String tradingAsOrMiddleName, String companyRegistrationOrIdNumber, String dateOfBirth, String gender, String coverAmount, String premiumAmount, String discount, String emailAddress, String cellPhoneNumber, String bankName1, String numberOfMembers, String beneficiaryName, String beneficiaryDateOfBirth, String beneficiaryIdNumber, String beneficiaryContactNumber, String beneficiaryEmailAddress, boolean familyBundle) {
        super(testCaseNumber, productName, scenarioDescription, policyNumberCode, policyNumber, function, run, firstPopup, secondPopup, debitOrderDate, nextDueDate, bankName, expectResult, workTypeZero, statusZero, queueZero, popupWorkTypeZero, popupStatusZero, popupQueueZero, auditTrail, updatedTe, risk, sanction, edd, kyc, status);
        this.relationship = relationship;
        this.companyOrFullName = companyOrFullName;
        this.tradingAsOrMiddleName = tradingAsOrMiddleName;
        this.companyRegistrationOrIdNumber = companyRegistrationOrIdNumber;
        this.dateOfBirth = dateOfBirth;
        this.gender = gender;
        this.coverAmount = coverAmount;
        this.premiumAmount = premiumAmount;
        this.discount = discount;
        this.emailAddress = emailAddress;
        this.cellPhoneNumber = cellPhoneNumber;
        this.bankName = bankName1;
        this.numberOfMembers = numberOfMembers;
        this.beneficiaryName = beneficiaryName;
        this.beneficiaryDateOfBirth = beneficiaryDateOfBirth;
        this.beneficiaryIdNumber = beneficiaryIdNumber;
        this.beneficiaryContactNumber = beneficiaryContactNumber;
        this.beneficiaryEmailAddress = beneficiaryEmailAddress;
        this.workTypeOne = workTypeOne;
        this.statusOne = statusOne;
        this.queueOne = queueOne;
        this.popupWorkTypeOne = popupWorkTypeOne;
        this.popupStatusOne = popupStatusOne;
        this.popupQueueOne = popupQueueOne;
        this.familyBundle = familyBundle;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }

    public void setCompanyOrFullName(String companyOrFullName) {
        this.companyOrFullName = companyOrFullName;
    }

    public void setTradingAsOrMiddleName(String tradingAsOrMiddleName) {
        this.tradingAsOrMiddleName = tradingAsOrMiddleName;
    }

    public void setCompanyRegistrationOrIdNumber(String companyRegistrationOrIdNumber) {
        this.companyRegistrationOrIdNumber = companyRegistrationOrIdNumber;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setCoverAmount(String coverAmount) {
        this.coverAmount = coverAmount;
    }

    public void setPremiumAmount(String premiumAmount) {
        this.premiumAmount = premiumAmount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public void setCellPhoneNumber(String cellPhoneNumber) {
        this.cellPhoneNumber = cellPhoneNumber;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public void setNumberOfMembers(String numberOfMembers) {
        this.numberOfMembers = numberOfMembers;
    }

    public void setBeneficiaryName(String beneficiaryName) {
        this.beneficiaryName = beneficiaryName;
    }

    public void setBeneficiaryDateOfBirth(String beneficiaryDateOfBirth) {
        this.beneficiaryDateOfBirth = beneficiaryDateOfBirth;
    }

    public void setBeneficiaryIdNumber(String beneficiaryIdNumber) {
        this.beneficiaryIdNumber = beneficiaryIdNumber;
    }

    public void setBeneficiaryContactNumber(String beneficiaryContactNumber) {
        this.beneficiaryContactNumber = beneficiaryContactNumber;
    }

    public void setBeneficiaryEmailAddress(String beneficiaryEmailAddress) {
        this.beneficiaryEmailAddress = beneficiaryEmailAddress;
    }

    public void setWorkTypeOne(String workTypeOne) {
        this.workTypeOne = workTypeOne;
    }

    public void setStatusOne(String statusOne) {
        this.statusOne = statusOne;
    }

    public void setQueueOne(String queueOne) {
        this.queueOne = queueOne;
    }

    public void setPopupWorkTypeOne(String popupWorkTypeOne) {
        this.popupWorkTypeOne = popupWorkTypeOne;
    }

    public void setPopupStatusOne(String popupStatusOne) {
        this.popupStatusOne = popupStatusOne;
    }

    public void setPopupQueueOne(String popupQueueOne) {
        this.popupQueueOne = popupQueueOne;
    }

    public void setFamilyBundle(boolean familyBundle) {
        this.familyBundle = familyBundle;
    }

    public String getRelationship() {
        return relationship;
    }

    public String getCompanyOrFullName() {
        return companyOrFullName;
    }

    public String getTradingAsOrMiddleName() {
        return tradingAsOrMiddleName;
    }

    public String getCompanyRegistrationOrIdNumber() {
        return companyRegistrationOrIdNumber;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public String getGender() {
        return gender;
    }

    public String getCoverAmount() {
        return coverAmount;
    }

    public String getPremiumAmount() {
        return premiumAmount;
    }

    public String getDiscount() {
        return discount;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public String getCellPhoneNumber() {
        return cellPhoneNumber;
    }

    @Override
    public String getBankName() {
        return bankName;
    }

    public String getNumberOfMembers() {
        return numberOfMembers;
    }

    public String getBeneficiaryName() {
        return beneficiaryName;
    }

    public String getBeneficiaryDateOfBirth() {
        return beneficiaryDateOfBirth;
    }

    public String getBeneficiaryIdNumber() {
        return beneficiaryIdNumber;
    }

    public String getBeneficiaryContactNumber() {
        return beneficiaryContactNumber;
    }

    public String getBeneficiaryEmailAddress() {
        return beneficiaryEmailAddress;
    }

    public String getWorkTypeOne() {
        return workTypeOne;
    }

    public String getStatusOne() {
        return statusOne;
    }

    public String getQueueOne() {
        return queueOne;
    }

    public String getPopupWorkTypeOne() {
        return popupWorkTypeOne;
    }

    public String getPopupStatusOne() {
        return popupStatusOne;
    }

    public String getPopupQueueOne() {
        return popupQueueOne;
    }

    public boolean isFamilyBundle() {
        return familyBundle;
    }

    @Override
    public String toString() {
        return "Member{" +
                "relationship='" + relationship + '\'' +
                ", companyOrFullName='" + companyOrFullName + '\'' +
                ", tradingAsOrMiddleName='" + tradingAsOrMiddleName + '\'' +
                ", companyRegistrationOrIdNumber='" + companyRegistrationOrIdNumber + '\'' +
                ", dateOfBirth='" + dateOfBirth + '\'' +
                ", gender='" + gender + '\'' +
                ", coverAmount='" + coverAmount + '\'' +
                ", premiumAmount='" + premiumAmount + '\'' +
                ", discount='" + discount + '\'' +
                ", emailAddress='" + emailAddress + '\'' +
                ", cellPhoneNumber='" + cellPhoneNumber + '\'' +
                ", bankName='" + bankName + '\'' +
                ", numberOfMembers='" + numberOfMembers + '\'' +
                ", beneficiaryName='" + beneficiaryName + '\'' +
                ", beneficiaryDateOfBirth='" + beneficiaryDateOfBirth + '\'' +
                ", beneficiaryIdNumber='" + beneficiaryIdNumber + '\'' +
                ", beneficiaryContactNumber='" + beneficiaryContactNumber + '\'' +
                ", beneficiaryEmailAddress='" + beneficiaryEmailAddress + '\'' +
                ", workTypeOne='" + workTypeOne + '\'' +
                ", statusOne='" + statusOne + '\'' +
                ", queueOne='" + queueOne + '\'' +
                ", popupWorkTypeOne='" + popupWorkTypeOne + '\'' +
                ", popupStatusOne='" + popupStatusOne + '\'' +
                ", popupQueueOne='" + popupQueueOne + '\'' +
                ", familyBundle=" + familyBundle +
                '}';
    }

}
