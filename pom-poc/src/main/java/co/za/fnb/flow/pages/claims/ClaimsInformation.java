package co.za.fnb.flow.pages.claims;

import co.za.fnb.flow.pages.BasePage;
import co.za.fnb.flow.pages.claims.page_factory.ClaimsInformatoinPageObjects;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ClaimsInformation extends BasePage {
    private Logger log  = LogManager.getLogger(ClaimsInformation.class);
    ClaimsInformatoinPageObjects claimsInformatoinPageObjects = new ClaimsInformatoinPageObjects(driver);

    public ClaimsInformation(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
    }

    public void doubleClickMatchingClaim(int row) throws Exception {
        try {
            log.info("Double clicking matching claim.");
            String tableCellValueLocatorTemplate = claimsInformatoinPageObjects.getClaimsInformationTableDataXpathLocator();
            String tableCellValueLocator = getLocatorForTableRowWithTrTd(tableCellValueLocatorTemplate, row, 1);
            WebElement tableCellValue = driver.findElement(By.xpath(tableCellValueLocator));
            moveToElementAndDoubleClick(tableCellValue);
        } catch (Exception e) {
            log.error("Error while clicking on matching claim.");
            throw new Exception("Error while clicking on matching claim.");
        }
    }

    public String getClaimsReferenceNumber(int row) throws Exception {
        try {
            log.info("Getting claims reference number.");
            String tableCellValueLocatorTemplate = claimsInformatoinPageObjects.getClaimsInformationTableDataXpathLocator();
            String tableCellValueLocator = getLocatorForTableRowWithTrTdNoCommas(tableCellValueLocatorTemplate, row, 2);
            WebElement tableCellValue = driver.findElement(By.xpath(tableCellValueLocator));
            return getText(tableCellValue);
        } catch (Exception e) {
            log.error("Error while getting Claims Reference Number.");
            throw new Exception("Error while getting Claims Reference Number.");
        }
    }

    public String getClaimsEventDate(int row) throws Exception {
        try {
            log.info("Getting claims event date.");
            String tableCellValueLocatorTemplate = claimsInformatoinPageObjects.getClaimsInformationTableDataXpathLocator();
            String tableCellValueLocator = getLocatorForTableRowWithTrTd(tableCellValueLocatorTemplate, row, 3);
            WebElement tableCellValue = driver.findElement(By.xpath(tableCellValueLocator));
            return getText(tableCellValue);
        } catch (Exception e) {
            log.error("Error while getting Event Date.");
            throw new Exception("Error while getting Event Date.");
        }
    }

    public String getClaimsEventType(int row) throws Exception {
        try {
            log.info("Getting claims event type.");
            String tableCellValueLocatorTemplate = claimsInformatoinPageObjects.getClaimsInformationTableDataXpathLocator();
            String tableCellValueLocator = getLocatorForTableRowWithTrTd(tableCellValueLocatorTemplate, row, 4);
            WebElement tableCellValue = driver.findElement(By.xpath(tableCellValueLocator));
            return getText(tableCellValue);
        } catch (Exception e) {
            log.error("Error while getting Event Type.");
            throw new Exception("Error while getting Event Type.");
        }
    }

    public String getClaimsDateRegistered(int row) throws Exception {
        try {
            log.info("Getting claims date registered.");
            String tableCellValueLocatorTemplate = claimsInformatoinPageObjects.getClaimsInformationTableDataXpathLocator();
            String tableCellValueLocator = getLocatorForTableRowWithTrTd(tableCellValueLocatorTemplate, row, 5);
            WebElement tableCellValue = driver.findElement(By.xpath(tableCellValueLocator));
            return getText(tableCellValue);
        } catch (Exception e) {
            log.error("Error while getting Date Registered.");
            throw new Exception("Error while getting Date Registered.");
        }
    }

    public String getClaimant(int row) throws Exception {
        try {
            log.info("Getting claimant.");
            String tableCellValueLocatorTemplate = claimsInformatoinPageObjects.getClaimsInformationTableDataXpathLocator();
            String tableCellValueLocator = getLocatorForTableRowWithTrTd(tableCellValueLocatorTemplate, row, 6);
            WebElement tableCellValue = driver.findElement(By.xpath(tableCellValueLocator));
            return getText(tableCellValue);
        } catch (Exception e) {
            log.error("Error while getting Claimant.");
            throw new Exception("Error while getting Claimant.");
        }
    }

    public String getClaimStatus(int row) throws Exception {
        try {
            log.info("Getting claim status.");
            String tableCellValueLocatorTemplate = claimsInformatoinPageObjects.getClaimsInformationTableDataXpathLocator();
            String tableCellValueLocator = getLocatorForTableRowWithTrTd(tableCellValueLocatorTemplate, row, 7);
            WebElement tableCellValue = driver.findElement(By.xpath(tableCellValueLocator));
            return getText(tableCellValue);
        } catch (Exception e) {
            log.error("Error while getting Claim Status.");
            throw new Exception("Error while getting Claim Status.");
        }
    }

    public String getClaimAmount(int row) throws Exception {
        try {
            log.info("Getting claim amount.");
            String tableCellValueLocatorTemplate = claimsInformatoinPageObjects.getClaimsInformationTableDataXpathLocator();
            String tableCellValueLocator = getLocatorForTableRowWithTrTd(tableCellValueLocatorTemplate, row, 8);
            WebElement tableCellValue = driver.findElement(By.xpath(tableCellValueLocator));
            return getText(tableCellValue);
        } catch (Exception e) {
            log.error("Error while getting Claim Amount.");
            throw new Exception("Error while getting Claim Amount.");
        }
    }

    public String getCaseLimit(int row) throws Exception {
        try {
            log.info("Getting case limit.");
            String tableCellValueLocatorTemplate = claimsInformatoinPageObjects.getClaimsInformationTableDataXpathLocator();
            String tableCellValueLocator = getLocatorForTableRowWithTrTd(tableCellValueLocatorTemplate, row, 9);
            WebElement tableCellValue = driver.findElement(By.xpath(tableCellValueLocator));
            return getText(tableCellValue);
        } catch (Exception e) {
            log.error("Error while getting Case Limit.");
            throw new Exception("Error while getting Case Limit.");
        }
    }

    public void clickUpdate() throws Exception {
        try {
            log.info("Clicking Update.");
            click(claimsInformatoinPageObjects.getUpdate());
        } catch (Exception e) {
            log.error("Error while clicking on Update.");
            throw new Exception("Error while clicking on Update.");
        }
    }

    public CaptureAttorneyPaymentDetails clickPayAttorney() throws Exception {
        try {
            log.info("Clicking Pay Attorney.");
            click(claimsInformatoinPageObjects.getBtnMakePayment());
            return new CaptureAttorneyPaymentDetails(driver, scenarioOperator);
        } catch (Exception e) {
            log.error("Error while clicking on Pay Attorney.");
            throw new Exception("Error while clicking on Pay Attorney.");
        }
    }

    public CapturePolicyHolderPaymentDetails clickPayPolicyHolder() throws Exception {
        try {
            log.info("Clicking Pay Policy Holder.");
            click(claimsInformatoinPageObjects.getBtnMakePaymentOther());
            return new CapturePolicyHolderPaymentDetails(driver, scenarioOperator);
        } catch (Exception e) {
            log.error("Error while clicking on Pay Policy Holder.");
            throw new Exception("Error while clicking on Pay Policy Holder.");
        }
    }

    public int getClaimsInformationTableList() throws Exception {
        try {
            log.info("Getting Claims Information Table List Size.");
            int tableDataRiSize = getTableDataRiSize(claimsInformatoinPageObjects.getClaimsInformationTableData());
            log.info("Claims Information Table List Size: " + tableDataRiSize);
            return tableDataRiSize;
        } catch (Exception e) {
            log.error("Error while getting claims information list table size.");
            throw new Exception("Error while getting claims information list table size.");
        }
    }

    public int getMatchingClaimIndex(String referenceNumber) throws Exception {
        try {
            log.info("Getting matching claim index.");
            int list = getClaimsInformationTableList();
            for (int i = 0; i < list; i++) {
                String claimsReferenceNumber = getClaimsReferenceNumber(i);
                if (claimsReferenceNumber.trim().equalsIgnoreCase(referenceNumber)) {
                    return i;
                }
            }
            return -1;
        } catch (Exception e) {
            log.error("Error while getting matching claim index.");
            throw new Exception("Error while getting matching claim index.");
        }
    }

    public void selectStatus(String status) throws Exception {
        try {
            log.info("Selecting Claim Status.");
            WebElement selectStatusLabel = claimsInformatoinPageObjects.getSelectStatusLabel();
            WebElement selectStatusItems = claimsInformatoinPageObjects.getSelectStatusItems();
            selectExactMatchingDataLabel(selectStatusLabel, selectStatusItems, status);
        } catch (Exception e) {
            log.error("Error while selecting claim status.");
            throw new Exception("Error while selecting claim status.");
        }
    }

    public void selectEvent(String event) throws Exception {
        try {
            log.info("Selecting Claim Event.");
            WebElement selectEventLabel = claimsInformatoinPageObjects.getSelectEventLabel();
            WebElement selectEventItems = claimsInformatoinPageObjects.getSelectEventItems();
            selectExactMatchingDataLabel(selectEventLabel, selectEventItems, event);
        } catch (Exception e) {
            log.error("Error while selecting claim event.");
            throw new Exception("Error while selecting claim event.");
        }
    }

    public void updateEventDescription(String eventDescription) throws Exception {
        try {
            log.info("Updating Event Description.");
            WebElement eventDescriptionField = claimsInformatoinPageObjects.getEventDescription();
            clear(eventDescriptionField);
            type(eventDescriptionField, eventDescription);
        } catch (Exception e) {
            log.error("Error while updating Event Description.");
            throw new Exception("Error while updating Event Description.");
        }
    }

    public void scrollToProvince() throws Exception {
        try {
            scrollToElement(claimsInformatoinPageObjects.getProvince());
        } catch (Exception e) {
            log.error("Error while scrolling on province.");
            throw new Exception("Error while scrolling on province.");
        }
    }

    public void selectProvince(String province) throws Exception {
        try {
            selectMatchingDataLabel(claimsInformatoinPageObjects.getProvinceLabel(), claimsInformatoinPageObjects.getProvinceItems(), province.toUpperCase());
        } catch (Exception e) {
            log.error("Error while selecting province.");
            throw new Exception("Error while selecting province.");
        }
    }

    public void selectCity(String city) throws Exception {
        try {
            selectMatchingDataLabel(claimsInformatoinPageObjects.getSelectCityLabel(), claimsInformatoinPageObjects.getSelectCityItems(), city.toUpperCase());
        } catch (Exception e) {
            log.error("Error while selecting city.");
            throw new Exception("Error while selecting city.");
        }
    }

    public void clickSearch() throws Exception {
        try {
            click(claimsInformatoinPageObjects.getSearch());
        } catch (Exception e) {
            log.error("Error while clicking on Search.");
            throw new Exception("Error while clicking on Search.");
        }
    }

    public void clearCellNo() throws Exception {
        try {
            clear(claimsInformatoinPageObjects.getCellNo());
        } catch (Exception e) {
            log.error("Error while clearing Attorney Search Cell Number.");
            throw new Exception("Error while clearing Attorney Search Cell Number.");
        }
    }

    public void updateCellNo(String attorneyCompanyName) throws Exception {
        try {
            clearCellNo();
            type(claimsInformatoinPageObjects.getCellNo(), attorneyCompanyName);
        } catch (Exception e) {
            log.error("Error while updating Attorney Search Cell Number.");
            throw new Exception("Error while updating Attorney Search Cell Number.");
        }
    }

    public String getCellNo() throws Exception {
        try {
            return getText(claimsInformatoinPageObjects.getCellNo());
        } catch (Exception e) {
            log.error("Error while getting Attorney Search Cell Number.");
            throw new Exception("Error while getting Attorney Search Cell Number.");
        }
    }

    public void clickAttorneyName() throws Exception {
        try {
            click(claimsInformatoinPageObjects.getAttorneyName());
        } catch (Exception e) {
            log.error("Error while clicking on Attorney Search Name.");
            throw new Exception("Error while clicking on Attorney Search Name.");
        }
    }

    public void clearAttorneyName() throws Exception {
        try {
            clear(claimsInformatoinPageObjects.getAttorneyName());
        } catch (Exception e) {
            log.error("Error while clearing Attorney Search Name.");
            throw new Exception("Error while clearing Attorney Search Name.");
        }
    }

    public void updateAttorneyName(String attorneyCompanyName) throws Exception {
        try {
            clearAttorneyName();
            type(claimsInformatoinPageObjects.getAttorneyName(), attorneyCompanyName);
        } catch (Exception e) {
            log.error("Error while updating Attorney Search Name.");
            throw new Exception("Error while updating Attorney Search Name.");
        }
    }

    public String getAttorneyName() throws Exception {
        try {
            return getText(claimsInformatoinPageObjects.getAttorneyName());
        } catch (Exception e) {
            log.error("Error while getting Attorney Search Name.");
            throw new Exception("Error while getting Attorney Search Name.");
        }
    }

    public int getAttorneyInformationTableSize() throws Exception {
        try {
            return getDataRiSize(claimsInformatoinPageObjects.getAttorneyInformationTableData());
        } catch (Exception e) {
            log.error("Error while selecting attorney information table size.");
            throw new Exception("Error while selecting attorney information table size.");
        }
    }

    public int getDataRiSize(WebElement tableDataItems) throws Exception {
        try {
            return tableDataItems.findElements(claimsInformatoinPageObjects.getAttorneyInformationTable()).size();
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    public void selectAttorneyInformationTableDataRow(int row) throws Exception {
        try {
            selectUsingTableRowData(claimsInformatoinPageObjects.getAttorneyInformationTableData(), row);
        } catch (Exception e) {
            log.error("Error while selecting claims information table row.");
            throw new Exception("Error while selecting claims information table row.");
        }
    }

    public void clickAddAttorney() throws Exception {
        try {
            click(claimsInformatoinPageObjects.getAddAttorney());
        } catch (Exception e) {
            log.error("Error while clicking on Add Attorney.");
            throw new Exception("Error while clicking on Add Attorney.");
        }
    }

}
