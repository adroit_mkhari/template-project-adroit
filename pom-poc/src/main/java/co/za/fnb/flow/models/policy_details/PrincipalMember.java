package co.za.fnb.flow.models.policy_details;

public class PrincipalMember extends PolicyDetailsBase {
    private String principalMemberInformationToUpdate;
    private String policyHolder;
    private String tradingAsNameOrMiddleName;
    private String companyRegNumberOrIdNumber;
    private String vatRegNumber;
    private String typeOfId;
    private String income;
    private String fNumber;
    private String expectedPremiumAmountValue;
    private String dateOfBirth;
    private String gender;
    private String correspondenceLanguage;
    private String maritalStatus;
    private String residentialStreetOrBuildingLineOne;
    private String residentialStreetOrBuildingLineTwo;
    private String residentialSuburb;
    private String residentialCity;
    private String residentialPostalCode;
    private String preferredContactMethod;
    private String emailAddress;
    private String cisEmailAddress;
    private String inContactEmailAddress;
    private String cellPhoneNumber;
    private String cisCellPhoneNumber;
    private String inContactCellPhoneNumber;
    private String workPhoneNumber;
    private String homePhoneNumber;
    private String postalSameAsResidentialAddressOne;
    private String postalStreetOrBuildingLineOne;
    private String postalStreetOrBuildingLineTwo;
    private String postalSuburb;
    private String postalCity;
    private String postalPostalCode;
    private String postalPostalCodeSearch;
    private String postalPostalCodeSearchLocation;
    private String postalPostalCodeSearchCode;
    private String postalSameAsResidentialAddressTwo;
    private String postalCisStreetOrBuildingLineOne;
    private String postalCisStreetOrBuildingLineTwo;
    private String postalCisPostalCity;
    private String postalCisPostalCode;
    private String postalCisPostalCodeSearch;
    private String postalCisPostalCodeSearchLocation;
    private String postalCisPostalCodeSearchCode;
    private String postalCisPostalState;
    private String postalCisPostalCountry;
    private String postalCisZipCode;
    private String postalCisZipCodeSearch;
    private String postalCisZipCodeSearchLocation;
    private String postalCisZipCodeSearchCode;

    public PrincipalMember(String testCaseNumber, String productName, String scenarioDescription, String policyNumberCode, String policyNumber, String function, String run, String firstPopup, String secondPopup, String debitOrderDate, String nextDueDate, String bankName, String expectResult, String workTypeZero, String statusZero, String queueZero, String popupWorkTypeZero, String popupStatusZero, String popupQueueZero, String auditTrail, String updatedTe, String risk, String sanction, String edd, String kyc, String status) {
        super(testCaseNumber, productName, scenarioDescription, policyNumberCode, policyNumber, function, run, firstPopup, secondPopup, debitOrderDate, nextDueDate, bankName, expectResult, workTypeZero, statusZero, queueZero, popupWorkTypeZero, popupStatusZero, popupQueueZero, auditTrail, updatedTe, risk, sanction, edd, kyc, status);
    }

    public PrincipalMember(String testCaseNumber, String productName, String scenarioDescription, String policyNumberCode, String policyNumber, String function, String run, String firstPopup, String secondPopup, String debitOrderDate, String nextDueDate, String bankName, String expectResult, String workTypeZero, String statusZero, String queueZero, String popupWorkTypeZero, String popupStatusZero, String popupQueueZero, String auditTrail, String updatedTe, String risk, String sanction, String edd, String kyc, String status, String principalMemberInformationToUpdate, String policyHolder, String tradingAsNameOrMiddleName, String companyRegNumberOrIdNumber, String vatRegNumber, String typeOfId, String income, String fNumber, String expectedPremiumAmountValue, String dateOfBirth, String gender, String correspondenceLanguage, String maritalStatus, String residentialStreetOrBuildingLineOne, String residentialStreetOrBuildingLineTwo, String residentialSuburb, String residentialCity, String residentialPostalCode, String preferredContactMethod, String emailAddress, String cisEmailAddress, String inContactEmailAddress, String cellPhoneNumber, String cisCellPhoneNumber, String inContactCellPhoneNumber, String workPhoneNumber, String homePhoneNumber, String postalSameAsResidentialAddressOne, String postalStreetOrBuildingLineOne, String postalStreetOrBuildingLineTwo, String postalSuburb, String postalCity, String postalPostalCode, String postalPostalCodeSearch, String postalPostalCodeSearchLocation, String postalPostalCodeSearchCode, String postalSameAsResidentialAddressTwo, String postalCisStreetOrBuildingLineOne, String postalCisStreetOrBuildingLineTwo, String postalCisPostalCity, String postalCisPostalCode, String postalCisPostalCodeSearch, String postalCisPostalCodeSearchLocation, String postalCisPostalCodeSearchCode, String postalCisPostalState, String postalCisPostalCountry, String postalCisZipCode, String postalCisZipCodeSearch, String postalCisZipCodeSearchLocation, String postalCisZipCodeSearchCode) {
        super(testCaseNumber, productName, scenarioDescription, policyNumberCode, policyNumber, function, run, firstPopup, secondPopup, debitOrderDate, nextDueDate, bankName, expectResult, workTypeZero, statusZero, queueZero, popupWorkTypeZero, popupStatusZero, popupQueueZero, auditTrail, updatedTe, risk, sanction, edd, kyc, status);
        this.principalMemberInformationToUpdate = principalMemberInformationToUpdate;
        this.policyHolder = policyHolder;
        this.tradingAsNameOrMiddleName = tradingAsNameOrMiddleName;
        this.companyRegNumberOrIdNumber = companyRegNumberOrIdNumber;
        this.vatRegNumber = vatRegNumber;
        this.typeOfId = typeOfId;
        this.income = income;
        this.fNumber = fNumber;
        this.expectedPremiumAmountValue = expectedPremiumAmountValue;
        this.dateOfBirth = dateOfBirth;
        this.gender = gender;
        this.correspondenceLanguage = correspondenceLanguage;
        this.maritalStatus = maritalStatus;
        this.residentialStreetOrBuildingLineOne = residentialStreetOrBuildingLineOne;
        this.residentialStreetOrBuildingLineTwo = residentialStreetOrBuildingLineTwo;
        this.residentialSuburb = residentialSuburb;
        this.residentialCity = residentialCity;
        this.residentialPostalCode = residentialPostalCode;
        this.preferredContactMethod = preferredContactMethod;
        this.emailAddress = emailAddress;
        this.cisEmailAddress = cisEmailAddress;
        this.inContactEmailAddress = inContactEmailAddress;
        this.cellPhoneNumber = cellPhoneNumber;
        this.cisCellPhoneNumber = cisCellPhoneNumber;
        this.inContactCellPhoneNumber = inContactCellPhoneNumber;
        this.workPhoneNumber = workPhoneNumber;
        this.homePhoneNumber = homePhoneNumber;
        this.postalSameAsResidentialAddressOne = postalSameAsResidentialAddressOne;
        this.postalStreetOrBuildingLineOne = postalStreetOrBuildingLineOne;
        this.postalStreetOrBuildingLineTwo = postalStreetOrBuildingLineTwo;
        this.postalSuburb = postalSuburb;
        this.postalCity = postalCity;
        this.postalPostalCode = postalPostalCode;
        this.postalPostalCodeSearch = postalPostalCodeSearch;
        this.postalPostalCodeSearchLocation = postalPostalCodeSearchLocation;
        this.postalPostalCodeSearchCode = postalPostalCodeSearchCode;
        this.postalSameAsResidentialAddressTwo = postalSameAsResidentialAddressTwo;
        this.postalCisStreetOrBuildingLineOne = postalCisStreetOrBuildingLineOne;
        this.postalCisStreetOrBuildingLineTwo = postalCisStreetOrBuildingLineTwo;
        this.postalCisPostalCity = postalCisPostalCity;
        this.postalCisPostalCode = postalCisPostalCode;
        this.postalCisPostalCodeSearch = postalCisPostalCodeSearch;
        this.postalCisPostalCodeSearchLocation = postalCisPostalCodeSearchLocation;
        this.postalCisPostalCodeSearchCode = postalCisPostalCodeSearchCode;
        this.postalCisPostalState = postalCisPostalState;
        this.postalCisPostalCountry = postalCisPostalCountry;
        this.postalCisZipCode = postalCisZipCode;
        this.postalCisZipCodeSearch = postalCisZipCodeSearch;
        this.postalCisZipCodeSearchLocation = postalCisZipCodeSearchLocation;
        this.postalCisZipCodeSearchCode = postalCisZipCodeSearchCode;
    }

    public String getPrincipalMemberInformationToUpdate() {
        return principalMemberInformationToUpdate;
    }

    public void setPrincipalMemberInformationToUpdate(String principalMemberInformationToUpdate) {
        this.principalMemberInformationToUpdate = principalMemberInformationToUpdate;
    }

    public String getPolicyHolder() {
        return policyHolder;
    }

    public void setPolicyHolder(String policyHolder) {
        this.policyHolder = policyHolder;
    }

    public String getTradingAsNameOrMiddleName() {
        return tradingAsNameOrMiddleName;
    }

    public void setTradingAsNameOrMiddleName(String tradingAsNameOrMiddleName) {
        this.tradingAsNameOrMiddleName = tradingAsNameOrMiddleName;
    }

    public String getCompanyRegNumberOrIdNumber() {
        return companyRegNumberOrIdNumber;
    }

    public void setCompanyRegNumberOrIdNumber(String companyRegNumberOrIdNumber) {
        this.companyRegNumberOrIdNumber = companyRegNumberOrIdNumber;
    }

    public String getVatRegNumber() {
        return vatRegNumber;
    }

    public void setVatRegNumber(String vatRegNumber) {
        this.vatRegNumber = vatRegNumber;
    }

    public String getTypeOfId() {
        return typeOfId;
    }

    public void setTypeOfId(String typeOfId) {
        this.typeOfId = typeOfId;
    }

    public String getIncome() {
        return income;
    }

    public void setIncome(String income) {
        this.income = income;
    }

    public String getfNumber() {
        return fNumber;
    }

    public void setfNumber(String fNumber) {
        this.fNumber = fNumber;
    }

    public String getExpectedPremiumAmountValue() {
        return expectedPremiumAmountValue;
    }

    public void setExpectedPremiumAmountValue(String expectedPremiumAmountValue) {
        this.expectedPremiumAmountValue = expectedPremiumAmountValue;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCorrespondenceLanguage() {
        return correspondenceLanguage;
    }

    public void setCorrespondenceLanguage(String correspondenceLanguage) {
        this.correspondenceLanguage = correspondenceLanguage;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getResidentialStreetOrBuildingLineOne() {
        return residentialStreetOrBuildingLineOne;
    }

    public void setResidentialStreetOrBuildingLineOne(String residentialStreetOrBuildingLineOne) {
        this.residentialStreetOrBuildingLineOne = residentialStreetOrBuildingLineOne;
    }

    public String getResidentialStreetOrBuildingLineTwo() {
        return residentialStreetOrBuildingLineTwo;
    }

    public void setResidentialStreetOrBuildingLineTwo(String residentialStreetOrBuildingLineTwo) {
        this.residentialStreetOrBuildingLineTwo = residentialStreetOrBuildingLineTwo;
    }

    public String getResidentialSuburb() {
        return residentialSuburb;
    }

    public void setResidentialSuburb(String residentialSuburb) {
        this.residentialSuburb = residentialSuburb;
    }

    public String getResidentialCity() {
        return residentialCity;
    }

    public void setResidentialCity(String residentialCity) {
        this.residentialCity = residentialCity;
    }

    public String getResidentialPostalCode() {
        return residentialPostalCode;
    }

    public void setResidentialPostalCode(String residentialPostalCode) {
        this.residentialPostalCode = residentialPostalCode;
    }

    public String getPreferredContactMethod() {
        return preferredContactMethod;
    }

    public void setPreferredContactMethod(String preferredContactMethod) {
        this.preferredContactMethod = preferredContactMethod;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getCisEmailAddress() {
        return cisEmailAddress;
    }

    public void setCisEmailAddress(String cisEmailAddress) {
        this.cisEmailAddress = cisEmailAddress;
    }

    public String getInContactEmailAddress() {
        return inContactEmailAddress;
    }

    public void setInContactEmailAddress(String inContactEmailAddress) {
        this.inContactEmailAddress = inContactEmailAddress;
    }

    public String getCellPhoneNumber() {
        return cellPhoneNumber;
    }

    public void setCellPhoneNumber(String cellPhoneNumber) {
        this.cellPhoneNumber = cellPhoneNumber;
    }

    public String getCisCellPhoneNumber() {
        return cisCellPhoneNumber;
    }

    public void setCisCellPhoneNumber(String cisCellPhoneNumber) {
        this.cisCellPhoneNumber = cisCellPhoneNumber;
    }

    public String getInContactCellPhoneNumber() {
        return inContactCellPhoneNumber;
    }

    public void setInContactCellPhoneNumber(String inContactCellPhoneNumber) {
        this.inContactCellPhoneNumber = inContactCellPhoneNumber;
    }

    public String getWorkPhoneNumber() {
        return workPhoneNumber;
    }

    public void setWorkPhoneNumber(String workPhoneNumber) {
        this.workPhoneNumber = workPhoneNumber;
    }

    public String getHomePhoneNumber() {
        return homePhoneNumber;
    }

    public void setHomePhoneNumber(String homePhoneNumber) {
        this.homePhoneNumber = homePhoneNumber;
    }

    public String getPostalSameAsResidentialAddressOne() {
        return postalSameAsResidentialAddressOne;
    }

    public void setPostalSameAsResidentialAddressOne(String postalSameAsResidentialAddressOne) {
        this.postalSameAsResidentialAddressOne = postalSameAsResidentialAddressOne;
    }

    public String getPostalStreetOrBuildingLineOne() {
        return postalStreetOrBuildingLineOne;
    }

    public void setPostalStreetOrBuildingLineOne(String postalStreetOrBuildingLineOne) {
        this.postalStreetOrBuildingLineOne = postalStreetOrBuildingLineOne;
    }

    public String getPostalStreetOrBuildingLineTwo() {
        return postalStreetOrBuildingLineTwo;
    }

    public void setPostalStreetOrBuildingLineTwo(String postalStreetOrBuildingLineTwo) {
        this.postalStreetOrBuildingLineTwo = postalStreetOrBuildingLineTwo;
    }

    public String getPostalSuburb() {
        return postalSuburb;
    }

    public void setPostalSuburb(String postalSuburb) {
        this.postalSuburb = postalSuburb;
    }

    public String getPostalCity() {
        return postalCity;
    }

    public void setPostalCity(String postalCity) {
        this.postalCity = postalCity;
    }

    public String getPostalPostalCode() {
        return postalPostalCode;
    }

    public void setPostalPostalCode(String postalPostalCode) {
        this.postalPostalCode = postalPostalCode;
    }

    public String getPostalPostalCodeSearch() {
        return postalPostalCodeSearch;
    }

    public void setPostalPostalCodeSearch(String postalPostalCodeSearch) {
        this.postalPostalCodeSearch = postalPostalCodeSearch;
    }

    public String getPostalPostalCodeSearchLocation() {
        return postalPostalCodeSearchLocation;
    }

    public void setPostalPostalCodeSearchLocation(String postalPostalCodeSearchLocation) {
        this.postalPostalCodeSearchLocation = postalPostalCodeSearchLocation;
    }

    public String getPostalPostalCodeSearchCode() {
        return postalPostalCodeSearchCode;
    }

    public void setPostalPostalCodeSearchCode(String postalPostalCodeSearchCode) {
        this.postalPostalCodeSearchCode = postalPostalCodeSearchCode;
    }

    public String getPostalSameAsResidentialAddressTwo() {
        return postalSameAsResidentialAddressTwo;
    }

    public void setPostalSameAsResidentialAddressTwo(String postalSameAsResidentialAddressTwo) {
        this.postalSameAsResidentialAddressTwo = postalSameAsResidentialAddressTwo;
    }

    public String getPostalCisStreetOrBuildingLineOne() {
        return postalCisStreetOrBuildingLineOne;
    }

    public void setPostalCisStreetOrBuildingLineOne(String postalCisStreetOrBuildingLineOne) {
        this.postalCisStreetOrBuildingLineOne = postalCisStreetOrBuildingLineOne;
    }

    public String getPostalCisStreetOrBuildingLineTwo() {
        return postalCisStreetOrBuildingLineTwo;
    }

    public void setPostalCisStreetOrBuildingLineTwo(String postalCisStreetOrBuildingLineTwo) {
        this.postalCisStreetOrBuildingLineTwo = postalCisStreetOrBuildingLineTwo;
    }

    public String getPostalCisPostalCity() {
        return postalCisPostalCity;
    }

    public void setPostalCisPostalCity(String postalCisPostalCity) {
        this.postalCisPostalCity = postalCisPostalCity;
    }

    public String getPostalCisPostalCode() {
        return postalCisPostalCode;
    }

    public void setPostalCisPostalCode(String postalCisPostalCode) {
        this.postalCisPostalCode = postalCisPostalCode;
    }

    public String getPostalCisPostalCodeSearch() {
        return postalCisPostalCodeSearch;
    }

    public void setPostalCisPostalCodeSearch(String postalCisPostalCodeSearch) {
        this.postalCisPostalCodeSearch = postalCisPostalCodeSearch;
    }

    public String getPostalCisPostalCodeSearchLocation() {
        return postalCisPostalCodeSearchLocation;
    }

    public void setPostalCisPostalCodeSearchLocation(String postalCisPostalCodeSearchLocation) {
        this.postalCisPostalCodeSearchLocation = postalCisPostalCodeSearchLocation;
    }

    public String getPostalCisPostalCodeSearchCode() {
        return postalCisPostalCodeSearchCode;
    }

    public void setPostalCisPostalCodeSearchCode(String postalCisPostalCodeSearchCode) {
        this.postalCisPostalCodeSearchCode = postalCisPostalCodeSearchCode;
    }

    public String getPostalCisPostalState() {
        return postalCisPostalState;
    }

    public void setPostalCisPostalState(String postalCisPostalState) {
        this.postalCisPostalState = postalCisPostalState;
    }

    public String getPostalCisPostalCountry() {
        return postalCisPostalCountry;
    }

    public void setPostalCisPostalCountry(String postalCisPostalCountry) {
        this.postalCisPostalCountry = postalCisPostalCountry;
    }

    public String getPostalCisZipCode() {
        return postalCisZipCode;
    }

    public void setPostalCisZipCode(String postalCisZipCode) {
        this.postalCisZipCode = postalCisZipCode;
    }

    public String getPostalCisZipCodeSearch() {
        return postalCisZipCodeSearch;
    }

    public void setPostalCisZipCodeSearch(String postalCisZipCodeSearch) {
        this.postalCisZipCodeSearch = postalCisZipCodeSearch;
    }

    public String getPostalCisZipCodeSearchLocation() {
        return postalCisZipCodeSearchLocation;
    }

    public void setPostalCisZipCodeSearchLocation(String postalCisZipCodeSearchLocation) {
        this.postalCisZipCodeSearchLocation = postalCisZipCodeSearchLocation;
    }

    public String getPostalCisZipCodeSearchCode() {
        return postalCisZipCodeSearchCode;
    }

    public void setPostalCisZipCodeSearchCode(String postalCisZipCodeSearchCode) {
        this.postalCisZipCodeSearchCode = postalCisZipCodeSearchCode;
    }

    @Override
    public String toString() {
        return "PrincipalMemberDetails{" +
                "principalMemberInformationToUpdate='" + principalMemberInformationToUpdate + '\'' +
                ", policyHolder='" + policyHolder + '\'' +
                ", tradingAsNameOrMiddleName='" + tradingAsNameOrMiddleName + '\'' +
                ", companyRegNumberOrIdNumber='" + companyRegNumberOrIdNumber + '\'' +
                ", vatRegNumber='" + vatRegNumber + '\'' +
                ", typeOfId='" + typeOfId + '\'' +
                ", income='" + income + '\'' +
                ", fNumber='" + fNumber + '\'' +
                ", expectedPremiumAmountValue='" + expectedPremiumAmountValue + '\'' +
                ", dateOfBirth='" + dateOfBirth + '\'' +
                ", gender='" + gender + '\'' +
                ", correspondenceLanguage='" + correspondenceLanguage + '\'' +
                ", maritalStatus='" + maritalStatus + '\'' +
                ", residentialStreetOrBuildingLineOne='" + residentialStreetOrBuildingLineOne + '\'' +
                ", residentialStreetOrBuildingLineTwo='" + residentialStreetOrBuildingLineTwo + '\'' +
                ", residentialSuburb='" + residentialSuburb + '\'' +
                ", residentialCity='" + residentialCity + '\'' +
                ", residentialPostalCode='" + residentialPostalCode + '\'' +
                ", preferredContactMethod='" + preferredContactMethod + '\'' +
                ", emailAddress='" + emailAddress + '\'' +
                ", cisEmailAddress='" + cisEmailAddress + '\'' +
                ", inContactEmailAddress='" + inContactEmailAddress + '\'' +
                ", cellPhoneNumber='" + cellPhoneNumber + '\'' +
                ", cisCellPhoneNumber='" + cisCellPhoneNumber + '\'' +
                ", inContactCellPhoneNumber='" + inContactCellPhoneNumber + '\'' +
                ", workPhoneNumber='" + workPhoneNumber + '\'' +
                ", homePhoneNumber='" + homePhoneNumber + '\'' +
                ", postalSameAsResidentialAddressOne='" + postalSameAsResidentialAddressOne + '\'' +
                ", postalStreetOrBuildingLineOne='" + postalStreetOrBuildingLineOne + '\'' +
                ", postalStreetOrBuildingLineTwo='" + postalStreetOrBuildingLineTwo + '\'' +
                ", postalSuburb='" + postalSuburb + '\'' +
                ", postalCity='" + postalCity + '\'' +
                ", postalPostalCode='" + postalPostalCode + '\'' +
                ", postalPostalCodeSearch='" + postalPostalCodeSearch + '\'' +
                ", postalPostalCodeSearchLocation='" + postalPostalCodeSearchLocation + '\'' +
                ", postalPostalCodeSearchCode='" + postalPostalCodeSearchCode + '\'' +
                ", postalSameAsResidentialAddressTwo='" + postalSameAsResidentialAddressTwo + '\'' +
                ", postalCisStreetOrBuildingLineOne='" + postalCisStreetOrBuildingLineOne + '\'' +
                ", postalCisStreetOrBuildingLineTwo='" + postalCisStreetOrBuildingLineTwo + '\'' +
                ", postalCisPostalCity='" + postalCisPostalCity + '\'' +
                ", postalCisPostalCode='" + postalCisPostalCode + '\'' +
                ", postalCisPostalCodeSearch='" + postalCisPostalCodeSearch + '\'' +
                ", postalCisPostalCodeSearchLocation='" + postalCisPostalCodeSearchLocation + '\'' +
                ", postalCisPostalCodeSearchCode='" + postalCisPostalCodeSearchCode + '\'' +
                ", postalCisPostalState='" + postalCisPostalState + '\'' +
                ", postalCisPostalCountry='" + postalCisPostalCountry + '\'' +
                ", postalCisZipCode='" + postalCisZipCode + '\'' +
                ", postalCisZipCodeSearch='" + postalCisZipCodeSearch + '\'' +
                ", postalCisZipCodeSearchLocation='" + postalCisZipCodeSearchLocation + '\'' +
                ", postalCisZipCodeSearchCode='" + postalCisZipCodeSearchCode + '\'' +
                '}';
    }

}
