package co.za.fnb.flow.pages.root_page_factory;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CreateMultipleMintItemTablePageObjects {
    private By itemCommentCESize = By.xpath("//*[contains(@id, ':mintItemCommentCE')]");

    @FindBy(id = "createMintItemForm:createMultipleMintItemtable:create")
    private WebElement create;

    @FindBy(xpath = "//*[contains(@id,'addCommentsView:selectCategory')]")
    private WebElement commentCategorySelect;

    @FindBy(xpath = "//*[contains(@id,'addCommentsView:selectComment_items')]")
    private WebElement commentCategorySelectItems;

    @FindBy(xpath = "//*[contains(@id,'addCommentsView:selectComment')]")
    private WebElement commentSelect;

    @FindBy(xpath = "//*[contains(@id,'addCommentsView:selectComment_items')]")
    private WebElement commentSelectItems;

    @FindBy(xpath = "//*[contains(@id,'addCommentsView:j_idt17')]")
    private WebElement submitComment;

    public CreateMultipleMintItemTablePageObjects(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public By getItemCommentCESize() {
        return itemCommentCESize;
    }

    public WebElement getCreate() {
        return create;
    }

    public WebElement getCommentCategorySelect() {
        return commentCategorySelect;
    }

    public WebElement getCommentCategorySelectItems() {
        return commentCategorySelectItems;
    }

    public WebElement getCommentSelect() {
        return commentSelect;
    }

    public WebElement getCommentSelectItems() {
        return commentSelectItems;
    }

    public WebElement getSubmitComment() {
        return submitComment;
    }
}
