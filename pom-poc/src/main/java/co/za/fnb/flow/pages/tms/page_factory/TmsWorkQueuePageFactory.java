package co.za.fnb.flow.pages.tms.page_factory;

import co.za.fnb.flow.pages.BasePage;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class TmsWorkQueuePageFactory extends BasePage {
    // Home
    // @xpath = /html/body/ng-include[1]/div/ul/div[2]
    @FindBy(xpath = "/html/body/ng-include[1]/div/ul/div[2]")
    private WebElement home;

    // Search Work Queue
    // @xpath = //*[@id="searchWorkQ"]
    @FindBy(xpath = "//*[@id=\"searchWorkQ\"]")
    private WebElement searchWorkQueue;

    // Search
    // @xpath = //*[@id="searchValue"]
    @FindBy(xpath = "//*[@id=\"searchValue\"]")
    private WebElement search;

    // Clear
    // @xpath = //*[@id="clearValue"]
    @FindBy(xpath = "//*[@id=\"clearValue\"]")
    private WebElement clear;

    public TmsWorkQueuePageFactory(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
        PageFactory.initElements(driver, this);
    }

    public WebElement getHome() {
        return home;
    }

    public WebElement getSearchWorkQueue() {
        return searchWorkQueue;
    }

    public WebElement getSearch() {
        return search;
    }

    public WebElement getClear() {
        return clear;
    }
}
