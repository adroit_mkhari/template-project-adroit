package co.za.fnb.flow.pages.work_items;

import co.za.fnb.flow.models.policy_details.*;
import co.za.fnb.flow.pages.CreateOrUpdateWorkItem;
import co.za.fnb.flow.pages.ErrorHandle;
import co.za.fnb.flow.pages.TestHandle;
import co.za.fnb.flow.pages.work_items.page_factory.WorkItemsPageObjects;
import co.za.fnb.flow.setup.PropertiesSetup;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import co.za.fnb.flow.pages.BasePage;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Properties;

public class WorkItems extends BasePage {
    private Logger log  = LogManager.getLogger(WorkItems.class);
    WorkItemsPageObjects workItemsPageObjects = new WorkItemsPageObjects(driver);

    Actions actions;

    String[] workType;
    String[] queue;
    String[] status;
    String[] createdDate;

    private ErrorHandle errorHandle = new ErrorHandle();
    private TestHandle testHandle = new TestHandle();

    ScenarioOperator scenarioOperator;

    public WorkItems(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
        PageFactory.initElements(driver, this);
        actions = new Actions(driver);
        this.scenarioOperator = scenarioOperator;
    }

    public int getWorkItemTableSize() throws Exception {
        try {
            int tableDataRiSize = getTableDataRiSize(workItemsPageObjects.getWorkItemTable());
            log.info("Work Items Table Size: " + tableDataRiSize);
            return tableDataRiSize;
        } catch (Exception e) {
            log.error("Error while getting Work Item Table Size.");
            throw new Exception("Error while getting Work Item Table Size.");
        }
    }

    public String[] getWorkType() {
        return workType;
    }

    public String[] getQueue() {
        return queue;
    }

    public String[] getStatus() {
        return status;
    }

    public void setWorkType(String[] workType) {
        this.workType = workType;
    }

    public void setQueue(String[] queue) {
        this.queue = queue;
    }

    public void setStatus(String[] status) {
        this.status = status;
    }

    public String[] getCreatedDate() {
        return createdDate;
    }

    public void getWorkItems(int mintItemCommentCESize) throws Exception {
        log.info("Getting Work Items.");
        workType = new String[mintItemCommentCESize];
        status = new String[mintItemCommentCESize];
        queue = new String[mintItemCommentCESize];
        createdDate = new String[mintItemCommentCESize];
        WebDriverWait webDriverWait = new WebDriverWait(driver, 120);

        for (int i = 0 ; i < mintItemCommentCESize; i++) {
            webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.id(getLocatorForIndex(workItemsPageObjects.getWorkItemTableWorkTypeLocator(), i))));

            WebElement workItemTableWorkType = driver.findElement(By.id(getLocatorForIndex(workItemsPageObjects.getWorkItemTableWorkTypeLocator(), i)));
            WebElement workItemTableStatus = driver.findElement(By.id(getLocatorForIndex(workItemsPageObjects.getWorkItemTableStatusLocator(), i)));
            WebElement workItemTableQueue = driver.findElement(By.id(getLocatorForIndex(workItemsPageObjects.getWorkItemTableQueueLocator(), i)));
            WebElement workItemTableCreatedDate = driver.findElement(By.id(getLocatorForIndex(workItemsPageObjects.getWorkItemTableCreatedDateLocator(), i)));

            workType[i] = workItemTableWorkType.getText();
            status[i] = workItemTableStatus.getText();
            queue[i] = workItemTableQueue.getText();
            createdDate[i] = workItemTableCreatedDate.getText();
        }
    }

    public void compareWorkItems(PremiumStatus premiumStatus) throws Exception {
        log.info("Comparing Work Items.");
        String[] workTypeData = {premiumStatus.getWorkTypeZero()};
        String[] queueData = {premiumStatus.getQueueZero()};
        String[] statusData = {premiumStatus.getStatusZero()};
        for (int i = 0; i < workType.length; i++) {
            String workTypeValue = workTypeData[i];
            String queueValue = queueData[i];
            String statusValue = statusData[i];

            log.info("Work Type: " + workTypeValue + " == " + workType[i] + " , Status: " + statusValue + " == " + status[i] + " , Queue: " + queueValue + " == " + queue[i]);
            if (workTypeValue.equalsIgnoreCase(workType[i]) && statusValue.equalsIgnoreCase(status[i]) && queueValue.equalsIgnoreCase(queue[i])) {
                testHandle.setSuccess(true);
            }  else {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("Work Items do not match\n");
                stringBuilder.append("Work Type: ").append(Arrays.toString(workType)).append("\n");
                stringBuilder.append("Status: ").append(Arrays.toString(status)).append("\n");
                stringBuilder.append("Queue: ").append(Arrays.toString(queue));
                testHandle.setSuccess(false);
                errorHandle.setError(stringBuilder.toString());
                log.info(stringBuilder.toString());
            }
        }
    }

    public void compareWorkItems(PrincipalMember principalMember) throws Exception {
        log.info("Comparing Work Items.");
        String[] workTypeData = {principalMember.getWorkTypeZero()};
        String[] queueData = {principalMember.getQueueZero()};
        String[] statusData = {principalMember.getStatusZero()};
        for (int i = 0; i < workType.length; i++) {
            String workTypeValue = workTypeData[i];
            String queueValue = queueData[i];
            String statusValue = statusData[i];

            log.info("Work Type: " + workTypeValue + " == " + workType[i] + " , Status: " + statusValue + " == " + status[i] + " , Queue: " + queueValue + " == " + queue[i]);
            if (workTypeValue.equalsIgnoreCase(workType[i]) && statusValue.equalsIgnoreCase(status[i]) && queueValue.equalsIgnoreCase(queue[i])) {
                testHandle.setSuccess(true);
            }  else {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("Work Items do not match\n");
                stringBuilder.append("Work Type: ").append(Arrays.toString(workType)).append("\n");
                stringBuilder.append("Status: ").append(Arrays.toString(status)).append("\n");
                stringBuilder.append("Queue: ").append(Arrays.toString(queue));
                testHandle.setSuccess(false);
                errorHandle.setError(stringBuilder.toString());
                log.info(stringBuilder.toString());
            }
        }
    }

    public void compareWorkItems(ClaimDetails claimDetails) throws Exception {
        log.info("Comparing Work Items.");
        String[] workTypeData = {claimDetails.getWorkTypeZero()};
        String[] queueData = {claimDetails.getQueueZero()};
        String[] statusData = {claimDetails.getStatusZero()};
        for (int i = 0; i < workType.length; i++) {
            String workTypeValue = workTypeData[i];
            String queueValue = queueData[i];
            String statusValue = statusData[i];

            log.info("Work Type: " + workTypeValue + " == " + workType[i] + " , Status: " + statusValue + " == " + status[i] + " , Queue: " + queueValue + " == " + queue[i]);
            if (workTypeValue.equalsIgnoreCase(workType[i]) && statusValue.equalsIgnoreCase(status[i]) && queueValue.equalsIgnoreCase(queue[i])) {
                testHandle.setSuccess(true);
            }  else {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("Work Items do not match\n");
                stringBuilder.append("Work Type: ").append(Arrays.toString(workType)).append("\n");
                stringBuilder.append("Status: ").append(Arrays.toString(status)).append("\n");
                stringBuilder.append("Queue: ").append(Arrays.toString(queue));
                testHandle.setSuccess(false);
                errorHandle.setError(stringBuilder.toString());
                log.info(stringBuilder.toString());
            }
        }
    }

    public void compareApprovePaymentWorkItems(ClaimDetails claimDetails) throws Exception {
        log.info("Comparing Work Items.");
        String[] workTypeData = {claimDetails.getApproveWorkTypeZero()};
        String[] queueData = {claimDetails.getApproveQueueZero()};
        String[] statusData = {claimDetails.getApproveStatusZero()};
        for (int i = 0; i < workType.length; i++) {
            String workTypeValue = workTypeData[i];
            String queueValue = queueData[i];
            String statusValue = statusData[i];

            log.info("Work Type: " + workTypeValue + " == " + workType[i] + " , Status: " + statusValue + " == " + status[i] + " , Queue: " + queueValue + " == " + queue[i]);
            if (workTypeValue.equalsIgnoreCase(workType[i]) && statusValue.equalsIgnoreCase(status[i]) && queueValue.equalsIgnoreCase(queue[i])) {
                testHandle.setSuccess(true);
            }  else {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("Work Items do not match\n");
                stringBuilder.append("Work Type: ").append(Arrays.toString(workType)).append("\n");
                stringBuilder.append("Status: ").append(Arrays.toString(status)).append("\n");
                stringBuilder.append("Queue: ").append(Arrays.toString(queue));
                testHandle.setSuccess(false);
                errorHandle.setError(stringBuilder.toString());
                log.info(stringBuilder.toString());
            }
        }
    }

    public void compareWorkItems(Member member) throws Exception {
        log.info("Comparing Work Items.");
        String[] workTypeData = {member.getWorkTypeZero(), member.getWorkTypeOne()};
        String[] queueData = {member.getQueueZero(), member.getQueueOne()};
        String[] statusData = {member.getStatusZero(), member.getStatusOne()};
        for (int i = 0; i < workType.length; i++) {
            String workTypeValue = workTypeData[i];
            String queueValue = queueData[i];
            String statusValue = statusData[i];

            log.info("Work Type: " + workTypeValue + " == " + workType[i] + " , Status: " + statusValue + " == " + status[i] + " , Queue: " + queueValue + " == " + queue[i]);
            if (workTypeValue.equalsIgnoreCase(workType[i]) && statusValue.equalsIgnoreCase(status[i]) && queueValue.equalsIgnoreCase(queue[i])) {
                testHandle.setSuccess(true);
            }  else {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("Work Items do not match\n");
                stringBuilder.append("Work Type: ").append(Arrays.toString(workType)).append("\n");
                stringBuilder.append("Status: ").append(Arrays.toString(status)).append("\n");
                stringBuilder.append("Queue: ").append(Arrays.toString(queue));
                testHandle.setSuccess(false);
                errorHandle.setError(stringBuilder.toString());
                log.info(stringBuilder.toString());
            }
        }
    }

    public void compareWorkItems(Refund refund) throws Exception {
        log.info("Comparing Work Items.");
        String[] workTypeData = {refund.getWorkTypeZero()};
        String[] queueData = {refund.getQueueZero()};
        String[] statusData = {refund.getStatusZero()};
        for (int i = 0; i < workType.length; i++) {
            String workTypeValue = workTypeData[i];
            String queueValue = queueData[i];
            String statusValue = statusData[i];

            log.info("Work Type: " + workTypeValue + " == " + workType[i] + " , Status: " + statusValue + " == " + status[i] + " , Queue: " + queueValue + " == " + queue[i]);
            if (workTypeValue.equalsIgnoreCase(workType[i]) && statusValue.equalsIgnoreCase(status[i]) && queueValue.equalsIgnoreCase(queue[i])) {
                testHandle.setSuccess(true);
            }  else {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("Work Items do not match\n");
                stringBuilder.append("Work Type: ").append(Arrays.toString(workType)).append("\n");
                stringBuilder.append("Status: ").append(Arrays.toString(status)).append("\n");
                stringBuilder.append("Queue: ").append(Arrays.toString(queue));
                testHandle.setSuccess(false);
                errorHandle.setError(stringBuilder.toString());
                log.info(stringBuilder.toString());
            }
        }
    }

    public void compareWorkItems(AttorneyDetails attorneyDetails) throws Exception {
        log.info("Comparing Work Items.");
        String[] workTypeData = {attorneyDetails.getWorkTypeZero()};
        String[] queueData = {attorneyDetails.getQueueZero()};
        String[] statusData = {attorneyDetails.getStatusZero()};
        for (int i = 0; i < workType.length; i++) {
            String workTypeValue = workTypeData[i];
            String queueValue = queueData[i];
            String statusValue = statusData[i];

            log.info("Work Type: " + workTypeValue + " == " + workType[i] + " , Status: " + statusValue + " == " + status[i] + " , Queue: " + queueValue + " == " + queue[i]);
            if (workTypeValue.equalsIgnoreCase(workType[i]) && statusValue.equalsIgnoreCase(status[i]) && queueValue.equalsIgnoreCase(queue[i])) {
                testHandle.setSuccess(true);
            }  else {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("Work Items do not match\n");
                stringBuilder.append("Work Type: ").append(Arrays.toString(workType)).append("\n");
                stringBuilder.append("Status: ").append(Arrays.toString(status)).append("\n");
                stringBuilder.append("Queue: ").append(Arrays.toString(queue));
                testHandle.setSuccess(false);
                errorHandle.setError(stringBuilder.toString());
                log.info(stringBuilder.toString());
            }
        }
    }

    public void doubleClickWorkItem(int index) throws Exception {
        try {
            log.info("Double Clinking On Work Item.");
            WebElement workItemTableWorkType = driver.findElement(By.id(getLocatorForIndex(workItemsPageObjects.getWorkItemTableWorkTypeLocator(), index)));
            moveToElementAndDoubleClick(workItemTableWorkType);
        } catch (Exception e) {
            log.error("Error while double clicking on work item.");
            throw new Exception("Error while double clicking on work item.");
        }
    }

    public void checkAndClickOnWorkItem(Refund refund) throws Exception {
        log.info("Comparing Work Items.");
        String[] workTypeData = {refund.getWorkTypeZero()};
        String[] queueData = {refund.getQueueZero()};
        String[] statusData = {refund.getStatusZero()};
        boolean foundMatchingWorkItem = false;
        for (int i = 0; i < workType.length; i++) {
            String workTypeValue = workTypeData[i];
            String queueValue = queueData[i];
            String statusValue = statusData[i];

            log.info("Work Type: " + workTypeValue + " == " + workType[i] + " , Status: " + statusValue + " == " + status[i] + " , Queue: " + queueValue + " == " + queue[i]);
            if (workTypeValue.equalsIgnoreCase(workType[i]) && statusValue.equalsIgnoreCase(status[i]) && queueValue.equalsIgnoreCase(queue[i])) {
                // TODO: Click on the matching one and break out the loop.
                doubleClickWorkItem(i);
                testHandle.setSuccess(true);
                foundMatchingWorkItem = true;
                break;
            }  else {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("Work Items do not match\n");
                stringBuilder.append("Work Type: ").append(Arrays.toString(workType)).append("\n");
                stringBuilder.append("Status: ").append(Arrays.toString(status)).append("\n");
                stringBuilder.append("Queue: ").append(Arrays.toString(queue));
                log.info(stringBuilder.toString());
            }
        }

        if (!foundMatchingWorkItem) {
            testHandle.setSuccess(false);
            errorHandle.setError("No matching work item found for the claim.");
            log.info("No matching work item found for the claim.");
        }
    }

    public void checkAndClickOnWorkItem(AttorneyDetails attorneyDetails) throws Exception {
        log.info("Comparing Work Items.");
        String[] workTypeData = {attorneyDetails.getAddAttorneyPopUpWorkType()};
        String[] queueData = {"LOC MANAGEMENT"}; // TODO: Have this on the model and datasheet.
        String[] statusData = {attorneyDetails.getAddAttorneyWorkItemStatusUpdate()};
        boolean foundMatchingWorkItem = false;
        for (int i = 0; i < workType.length; i++) {
            String workTypeValue = workTypeData[i];
            String queueValue = queueData[i];
            String statusValue = statusData[i];

            log.info("Work Type: " + workTypeValue + " == " + workType[i] + " , Status: " + statusValue + " == " + status[i] + " , Queue: " + queueValue + " == " + queue[i]);
            if (workTypeValue.equalsIgnoreCase(workType[i]) && statusValue.equalsIgnoreCase(status[i]) && queueValue.equalsIgnoreCase(queue[i])) {
                // TODO: Click on the matching one and break out the loop.
                doubleClickWorkItem(i);
                testHandle.setSuccess(true);
                foundMatchingWorkItem = true;
                break;
            }  else {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("Work Items do not match\n");
                stringBuilder.append("Work Type: ").append(Arrays.toString(workType)).append("\n");
                stringBuilder.append("Status: ").append(Arrays.toString(status)).append("\n");
                stringBuilder.append("Queue: ").append(Arrays.toString(queue));
                log.info(stringBuilder.toString());
            }
        }

        if (!foundMatchingWorkItem) {
            testHandle.setSuccess(false);
            errorHandle.setError("No matching work item found for the claim.");
            log.info("No matching work item found for the claim.");
        }
    }

    public void getComments(String productName, String testCaseNo, String function, int mintItemCommentCESize) throws Exception {
        for (int i = 0 ; i < mintItemCommentCESize; i++) {
            getComment(workType[i], productName, testCaseNo, function, i);
        }
    }

    private void getComment(String workType, String productName, String testCaseNo, String function, int index) throws Exception {
        WebElement workItemTableCreatedDate = driver.findElement(By.id(getLocatorForIndex(workItemsPageObjects.getWorkItemTableCreatedDateLocator(), index)));
        actions.moveToElement(workItemTableCreatedDate).doubleClick().build().perform();
        Thread.sleep(1000);
        click(workItemsPageObjects.getCommentsSummary());
        Thread.sleep(1000);
        driver.switchTo().defaultContent();
        Thread.sleep(1000);
        driver.switchTo().frame(0);
        Thread.sleep(1000);
        WebElement getCommentsTextArea = workItemsPageObjects.getCommentsTextArea();
        String text = getCommentsTextArea.getText();
        Thread.sleep(2000);
        PrintWriter out = new PrintWriter(reportSourcePath + "\\comment_summary\\"
                + workType
                + productName
                + testCaseNo
                + function
                + getDateTimeStamp() + ".txt");

        out.println(text);
        out.close();
        closeCommentsSummaryDialog();
    }

    public String getComment(int index) throws Exception {
        WebElement workItemTableCreatedDate = driver.findElement(By.id(getLocatorForIndex(workItemsPageObjects.getWorkItemTableCreatedDateLocator(), index)));
        actions.moveToElement(workItemTableCreatedDate).doubleClick().build().perform();
        Thread.sleep(3000);

        takeWorkItemScreenshot();

        click(workItemsPageObjects.getCommentsSummary());
        Thread.sleep(7000);
        driver.switchTo().defaultContent();
        Thread.sleep(1000);
        driver.switchTo().frame(0);
        Thread.sleep(1000);
        WebElement getCommentsTextArea = workItemsPageObjects.getCommentsTextArea();
        String text = getCommentsTextArea.getText();
        Thread.sleep(2000);
        closeCommentsSummaryDialog();
        return text;
    }

    private void takeWorkItemScreenshot() throws Exception {
        try {
            log.info("Taking Work Item Screen Shot.");
            PropertiesSetup propertiesSetup = new PropertiesSetup();
            propertiesSetup.loadProperties();
            Properties properties = propertiesSetup.getProperties();
            String takeWorkItemScreenshotBeforeCommentSummary = properties.getProperty("TAKE_WORK_ITEM_SCREENSHOT_BEFORE_COMMENT_SUMMARY");

            if (takeWorkItemScreenshotBeforeCommentSummary.equalsIgnoreCase("Yes")) {
                String screenShotPath;
                if (scenarioOperator != null) {
                    screenShotPath = reportSourcePath + "/screenshots/work_items/" + scenarioOperator.getScreenShotFileName() + " " + getDateTimeStamp();
                } else {
                    screenShotPath = reportSourcePath + "/screenshots/work_items/" + "work item " + getDateTimeStamp();
                }
                takeScreenShot(screenShotPath);
                Thread.sleep(3000);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while taking work item screenshot: " + e.getMessage());
        }
    }

    private String getDateTimeStamp() {
        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd_MM_yyyy_HH_mm_ss");
        LocalDateTime now = LocalDateTime.now();
        return format.format(now);
    }

    public void closeCommentsSummaryDialog() throws Exception {
        try {
            driver.switchTo().defaultContent();
            while (workItemsPageObjects.getCommentsSummaryDialog().isDisplayed()) {
                click(workItemsPageObjects.getCommentsSummaryDialogClose());
                Thread.sleep(1000);
                break;
            }
        } catch (Exception e) {
            log.error("Error while closing Comments Summary Dialog");
            throw new Exception("Error while closing Comments Summary Dialog");
        }
    }

    public String getReferenceNumber() throws Exception {
        try {
            WebElement referenceNumber = workItemsPageObjects.getReferenceNumber();
            String referenceNumberText = referenceNumber.getAttribute("value");
            return referenceNumberText;
        } catch (Exception e) {
            log.error("Error while getting Reference Number.");
            throw new Exception("Error while getting Reference Number.");
        }
    }

    public CreateOrUpdateWorkItem selectPendingClaimLOCWhereWorkTypeMatches(ClaimDetails claimDetails) throws Exception {
        log.info("Comparing Work Items.");
        String[] workTypeData = {claimDetails.getWorkTypeZero()};
        String[] queueData = {claimDetails.getQueueZero()};
        String[] statusData = {claimDetails.getStatusZero()};
        for (int i = 0; i < workType.length; i++) {
            String workTypeValue = workTypeData[i];
            String queueValue = queueData[i];
            String statusValue = statusData[i];

            log.info("Work Type: " + workTypeValue + " == " + workType[i] + " , Status: " + statusValue + " == " + status[i] + " , Queue: " + queueValue + " == " + queue[i]);
            if (workTypeValue.equalsIgnoreCase(workType[i]) && statusValue.equalsIgnoreCase("PENDING") && queueValue.equalsIgnoreCase(queue[i])) {
                doubleClickWorkItem(i);
                return new CreateOrUpdateWorkItem(driver, scenarioOperator);
            }  else {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("Work Items do not match\n");
                stringBuilder.append("Work Type: ").append(Arrays.toString(workType)).append("\n");
                stringBuilder.append("Status: ").append(Arrays.toString(status)).append("\n");
                stringBuilder.append("Queue: ").append(Arrays.toString(queue));
                log.info(stringBuilder.toString());
            }
        }

        testHandle.setSuccess(false);
        errorHandle.setError("No matching pending work item found for the claim.");
        log.info("No matching pending work item found for the claim.");
        throw new Exception("No matching pending work item found for the claim.");
    }

    public CreateOrUpdateWorkItem selectNewlyCreatedWorkItem(ClaimDetails claimDetails) throws Exception {
        log.info("Comparing Work Items.");
        String[] workTypeData = {claimDetails.getSubmitWorkTypeZero()};
        String[] queueData = {claimDetails.getSubmitQueueZero()};
        String[] statusData = {claimDetails.getSubmitStatusZero()};
        for (int i = 0; i < workType.length; i++) {
            String workTypeValue = workTypeData[i];
            String queueValue = queueData[i];
            String statusValue = statusData[i];

            log.info("Work Type: " + workTypeValue + " == " + workType[i] + " , Status: " + statusValue + " == " + status[i] + " , Queue: " + queueValue + " == " + queue[i]);
            if (workTypeValue.equalsIgnoreCase(workType[i]) && statusValue.equalsIgnoreCase(status[i]) && queueValue.equalsIgnoreCase(queue[i])) {
                doubleClickWorkItem(i);
                Thread.sleep(2000);
                return new CreateOrUpdateWorkItem(driver, scenarioOperator);
            }  else {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("Work Items do not match\n");
                stringBuilder.append("Work Type: ").append(Arrays.toString(workType)).append("\n");
                stringBuilder.append("Status: ").append(Arrays.toString(status)).append("\n");
                stringBuilder.append("Queue: ").append(Arrays.toString(queue));
                log.info(stringBuilder.toString());
            }
        }

        testHandle.setSuccess(false);
        errorHandle.setError("No matching pending work item found for the claim.");
        log.info("No matching pending work item found for the claim.");
        throw new Exception("No matching pending work item found for the claim.");
    }

    public CreateOrUpdateWorkItem selectNewlyCreatedWorkItem(AttorneyDetails claimDetails) throws Exception {
        log.info("Comparing Work Items.");
        String[] workTypeData = {claimDetails.getAddAttorneyPopUpWorkType()};
        String[] queueData = {claimDetails.getAddAttorneyPopUpQueue()};
        String[] statusData = {claimDetails.getAddAttorneyPopUpStatus()};
        for (int i = 0; i < workType.length; i++) {
            String workTypeValue = workTypeData[i];
            String queueValue = queueData[i];
            String statusValue = statusData[i];

            log.info("Work Type: " + workTypeValue + " == " + workType[i] + " , Status: " + statusValue + " == " + status[i] + " , Queue: " + queueValue + " == " + queue[i]);
            if (workTypeValue.equalsIgnoreCase(workType[i]) && statusValue.equalsIgnoreCase(status[i]) && queueValue.equalsIgnoreCase(queue[i])) {
                doubleClickWorkItem(i);
                Thread.sleep(2000);
                return new CreateOrUpdateWorkItem(driver, scenarioOperator);
            }  else {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("Work Items do not match\n");
                stringBuilder.append("Work Type: ").append(Arrays.toString(workType)).append("\n");
                stringBuilder.append("Status: ").append(Arrays.toString(status)).append("\n");
                stringBuilder.append("Queue: ").append(Arrays.toString(queue));
                log.info(stringBuilder.toString());
            }
        }

        testHandle.setSuccess(false);
        errorHandle.setError("No matching pending work item found for the claim.");
        log.info("No matching pending work item found for the claim.");
        throw new Exception("No matching pending work item found for the claim.");
    }

    public CreateOrUpdateWorkItem selectUpdatedPendingCreatedWorkItem(ClaimDetails claimDetails) throws Exception {
        log.info("Comparing Work Items.");
        String[] workTypeData = {claimDetails.getSubmitWorkTypeZero()};
        String[] queueData = {claimDetails.getPaymentWorkItemsQueue()};
        String[] statusData = {claimDetails.getPaymentWorkItemsStatus()};
        for (int i = 0; i < workType.length; i++) {
            String workTypeValue = workTypeData[i];
            String queueValue = queueData[i];
            String statusValue = statusData[i];

            log.info("Work Type: " + workTypeValue + " == " + workType[i] + " , Status: " + statusValue + " == " + status[i] + " , Queue: " + queueValue + " == " + queue[i]);
            if (workTypeValue.equalsIgnoreCase(workType[i]) && statusValue.equalsIgnoreCase(status[i]) && queueValue.equalsIgnoreCase(queue[i])) {
                doubleClickWorkItem(i);
                Thread.sleep(5000);
                return new CreateOrUpdateWorkItem(driver, scenarioOperator);
            }  else {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("Work Items do not match\n");
                stringBuilder.append("Work Type: ").append(Arrays.toString(workType)).append("\n");
                stringBuilder.append("Status: ").append(Arrays.toString(status)).append("\n");
                stringBuilder.append("Queue: ").append(Arrays.toString(queue));
                log.info(stringBuilder.toString());
            }
        }

        testHandle.setSuccess(false);
        errorHandle.setError("No matching pending work item found for the claim.");
        log.info("No matching pending work item found for the claim.");
        throw new Exception("No matching pending work item found for the claim.");
    }

    public String getFirstWorkItemDocumentsUploadedDate() throws Exception {
        try {
            WebElement firstItemDocumentsTableDataWorkItemDocumentsUploadedDate = workItemsPageObjects.getFirstItemDocumentsTableDataWorkItemDocumentsUploadedDate();
            String workItemDocumentsUploadedDate = getText(firstItemDocumentsTableDataWorkItemDocumentsUploadedDate);
            return workItemDocumentsUploadedDate;
        } catch (Exception e) {
            log.error("Error while getting WorkItem Documents Uploaded Date");
            throw new Exception("Error while getting WorkItem Documents Uploaded Date");
        }
    }

    public void doubleClickFirstWorkItemDocumentsUploadedDate() throws Exception {
        try {
            WebElement firstItemDocumentsTableDataWorkItemDocumentsUploadedDate = workItemsPageObjects.getFirstItemDocumentsTableDataWorkItemDocumentsUploadedDate();
            moveToElementAndDoubleClick(firstItemDocumentsTableDataWorkItemDocumentsUploadedDate);
        } catch (Exception e) {
            log.error("Error while clicking WorkItem Documents Uploaded Date");
            throw new Exception("Error while clicking WorkItem Documents Uploaded Date");
        }
    }

    public String getFirstWorkItemDocumentName () throws Exception {
        try {
            WebElement firstItemDocumentsTableDataWorkItemDocumentName = workItemsPageObjects.getFirstItemDocumentsTableDataWorkItemDocumentName();
            String workItemDocumentName = getText(firstItemDocumentsTableDataWorkItemDocumentName);
            return workItemDocumentName;
        } catch (Exception e) {
            log.error("Error while getting Work Item Document Name");
            throw new Exception("Error while getting Work Item Document Name");
        }
    }

    public String getFirstWorkItemDocumentStatus () throws Exception {
        try {
            WebElement firstItemDocumentsTableDataWorkItemDocumentStatus = workItemsPageObjects.getFirstItemDocumentsTableDataWorkItemDocumentStatus();
            String workItemDocumentStatus = getText(firstItemDocumentsTableDataWorkItemDocumentStatus);
            return workItemDocumentStatus;
        } catch (Exception e) {
            log.error("Error while getting Work Item Document Status");
            throw new Exception("Error while getting Work Item Document Status");
        }
    }

    @Override
    public ErrorHandle getErrorHandle() {
        return this.errorHandle;
    }

    @Override
    public void setErrorHandle(ErrorHandle errorHandle) {
        this.errorHandle = errorHandle;
    }

    @Override
    public TestHandle getTestHandle() {
        return this.testHandle;
    }

    @Override
    public void setTestHandle(TestHandle testHandle) {
        this.testHandle = testHandle;
    }

}
