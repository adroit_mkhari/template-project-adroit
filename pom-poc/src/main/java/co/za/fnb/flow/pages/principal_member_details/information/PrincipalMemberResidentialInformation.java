package co.za.fnb.flow.pages.principal_member_details.information;

import co.za.fnb.flow.pages.principal_member_details.PrincipalMemberDetails;
import co.za.fnb.flow.pages.principal_member_details.page_factory.PrincipalMemberResidentialInformationPageObjects;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class PrincipalMemberResidentialInformation extends PrincipalMemberDetails {
    private Logger log  = LogManager.getLogger(PrincipalMemberResidentialInformation.class);
    PrincipalMemberResidentialInformationPageObjects principalMemberResidentialInformationPageObjects
            = new PrincipalMemberResidentialInformationPageObjects(driver, scenarioOperator);

    public PrincipalMemberResidentialInformation(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
        PageFactory.initElements(driver, this);
    }

    public void clearStreetLineOne() throws Exception {
        try {
            clear(principalMemberResidentialInformationPageObjects.getStreetLine_1());
        } catch (Exception e) {
            log.error("Error while clearing Street Line One.");
            throw new Exception("Error while clearing Street Line One.");
        }
    }

    public void updateStreetLineOne(String streetLineOne) throws Exception {
        try {
            type(principalMemberResidentialInformationPageObjects.getStreetLine_1(), streetLineOne);
        } catch (Exception e) {
            log.error("Error while updating Street Line One.");
            throw new Exception("Error while updating Street Line One.");
        }
    }

    public void clearStreetLineTwo() throws Exception {
        try {
            clear(principalMemberResidentialInformationPageObjects.getStreetLine_2());
        } catch (Exception e) {
            log.error("Error while clearing Street Line Two.");
            throw new Exception("Error while clearing Street Line Two.");
        }
    }

    public void updateStreetLineTwo(String streetLineTwo) throws Exception {
        try {
            type(principalMemberResidentialInformationPageObjects.getStreetLine_2(), streetLineTwo);
        } catch (Exception e) {
            log.error("Error while updating Street Line Two.");
            throw new Exception("Error while updating Street Line Two.");
        }
    }

    public void clearStreetSuburb() throws Exception {
        try {
            clear(principalMemberResidentialInformationPageObjects.getStreetSuburb());
        } catch (Exception e) {
            log.error("Error while clearing Street Suburb.");
            throw new Exception("Error while clearing Street Suburb.");
        }
    }

    public void updateStreetSuburb(String streetSuburb) throws Exception {
        try {
            type(principalMemberResidentialInformationPageObjects.getStreetSuburb(), streetSuburb);
        } catch (Exception e) {
            log.error("Error while updating Street Suburb.");
            throw new Exception("Error while updating Street Suburb.");
        }
    }

    public void clearStreetCity() throws Exception {
        try {
            clear(principalMemberResidentialInformationPageObjects.getStreetCity());
        } catch (Exception e) {
            log.error("Error while clearing Street City.");
            throw new Exception("Error while clearing Street City.");
        }
    }

    public void updateStreetCity(String streetCity) throws Exception {
        try {
            type(principalMemberResidentialInformationPageObjects.getStreetCity(), streetCity);
        } catch (Exception e) {
            log.error("Error while updating Street City.");
            throw new Exception("Error while updating Street City.");
        }
    }

    public void clearPostalCode() throws Exception {
        try {
            clear(principalMemberResidentialInformationPageObjects.getPostalCode());
        } catch (Exception e) {
            log.error("Error while clearing Postal Code.");
            throw new Exception("Error while clearing Postal Code.");
        }
    }

    public void updatePostalCode(String postalCode) throws Exception {
        try {
            type(principalMemberResidentialInformationPageObjects.getPostalCode(), postalCode);
        } catch (Exception e) {
            log.error("Error while updating Postal Code.");
            throw new Exception("Error while updating Postal Code.");
        }
    }

    public String getPostalCode() throws Exception {
        try {
            String postalCode = getAttribute(principalMemberResidentialInformationPageObjects.getPostalCode(), "value");
            return postalCode;
        } catch (Exception e) {
            log.error("Error while getting Postal Code.");
            throw new Exception("Error while getting Postal Code.");
        }
    }

    public String getCisPostalCode() throws Exception {
        try {
            String cisPostalCode = getAttribute(principalMemberResidentialInformationPageObjects.getCisPostalCode(), "value");
            return cisPostalCode;
        } catch (Exception e) {
            log.error("Error while getting Cis Postal Code.");
            throw new Exception("Error while getting Cis Postal Code.");
        }
    }

    public String getCisZipCode() throws Exception {
        try {
            String cisPostalCode = getAttribute(principalMemberResidentialInformationPageObjects.getCisZipCode(), "value");
            return cisPostalCode;
        } catch (Exception e) {
            log.error("Error while getting Cis Zip Code.");
            throw new Exception("Error while getting Cis Zip Code.");
        }
    }

}
