package co.za.fnb.flow.pages.administration.page_factory;

import co.za.fnb.flow.pages.BasePage;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ApproveOrRejectAttorneyPageObjects extends BasePage {
    // region Attorney Details
    @FindBy(id = "frmApproveUser:companyNameApprove")
    private WebElement companyNameApprove;

    @FindBy(id = "frmApproveUser:vatFlagApprove")
    private WebElement vatFlagApprove;

    @FindBy(id = "frmApproveUser:vatRegNoApprove")
    private WebElement vatRegNoApprove;

    @FindBy(id = "frmApproveUser:emailApprove")
    private WebElement emailApprove;

    @FindBy(id = "frmApproveUser:cellNoApprove")
    private WebElement cellNoApprove;

    @FindBy(id = "frmApproveUser:workNoApprove")
    private WebElement workNoApprove;

    @FindBy(id = "frmApproveUser:AttstatusApprove")
    private WebElement attStatusApprove;

    @FindBy(id = "frmApproveUser:AttstatusApprove_label")
    private WebElement attStatusApproveLabel;
    // endregion

    // region Buttons
    @FindBy(id = "btnApproveUser")
    private WebElement approveUser;

    @FindBy(id = "btnFailUser")
    private WebElement failUser;
    // endregion

    @FindBy(id = "frmConfimeApproveAttorney:btnApproveAttorneyConfirmationYes")
    private WebElement yesApprove;

    @FindBy(id = "frmConfimeApproveAttorney:btnApproveAttorneyConfirmationNo")
    private WebElement noDoNotApprove;

    public ApproveOrRejectAttorneyPageObjects(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
        PageFactory.initElements(driver, this);
    }

    public WebElement getCompanyNameApprove() {
        return companyNameApprove;
    }

    public WebElement getVatFlagApprove() {
        return vatFlagApprove;
    }

    public WebElement getVatRegNoApprove() {
        return vatRegNoApprove;
    }

    public WebElement getEmailApprove() {
        return emailApprove;
    }

    public WebElement getCellNoApprove() {
        return cellNoApprove;
    }

    public WebElement getWorkNoApprove() {
        return workNoApprove;
    }

    public WebElement getAttStatusApprove() {
        return attStatusApprove;
    }

    public WebElement getAttStatusApproveLabel() {
        return attStatusApproveLabel;
    }

    public WebElement getApproveUser() {
        return approveUser;
    }

    public WebElement getFailUser() {
        return failUser;
    }

    public WebElement getYesApprove() {
        return yesApprove;
    }

    public WebElement getNoDoNotApprove() {
        return noDoNotApprove;
    }
}
