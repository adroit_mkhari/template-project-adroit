package co.za.fnb.flow.pages.policy_details.information;

import co.za.fnb.flow.pages.policy_details.page_factory.PolicyInformationPageObjects;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import co.za.fnb.flow.pages.policy_details.PolicyDetails;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PolicyInformation extends PolicyDetails {
    private Logger log  = LogManager.getLogger(PolicyInformation.class);
    PolicyInformationPageObjects policyInformationPageObjects = new PolicyInformationPageObjects(driver);

    public PolicyInformation(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
        PageFactory.initElements(driver, this);
    }

    public String getTotalPremiumAmount() throws Exception {
        try {
            WebElement totalPremiumAmount = policyInformationPageObjects.getTotalPremiumAmount();
            WebDriverWait webDriverWait = new WebDriverWait(driver, 90);
            webDriverWait.until(ExpectedConditions.visibilityOf(totalPremiumAmount));
            return getAttribute(totalPremiumAmount, "value");
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while getting total premium amount: " + e.getMessage());
            throw new Exception("Error while getting total premium amount: " + e.getMessage());
        }
    }

    public void changePremiumStatus(String option) throws Exception {
        try {
            log.info("Selecting Premium Status: " + option);
            selectUsingDataLabel(policyInformationPageObjects.getPremiumStatusSelect(), policyInformationPageObjects.getPremiumStatusSelectItems(), option);
        } catch (Exception e) {
            log.error("Error while changing Premium Status");
            e.printStackTrace();
            throw new Exception("Error while changing Premium Status");
        }
    }

    public String getEscalationStatus() throws Exception {
        try {
            log.info("Getting Escalation Status");
            return getAttribute(policyInformationPageObjects.getEscalationStatus(), "value");
        } catch (Exception e) {
            log.error("Error while changing Escalation Status.");
            e.printStackTrace();
            throw new Exception("Error while changing Escalation Status.");
        }
    }

    public void changeEscalationAction(String option) throws Exception {
        try {
            log.info("Selecting Escalation Action: " + option);
            selectMatchingDataLabel(policyInformationPageObjects.getEscalationActionSelect(), policyInformationPageObjects.getEscalationActionSelectItems(), option);
        } catch (Exception e) {
            log.error("Error while changing Escalation Action.");
            e.printStackTrace();
            throw new Exception("Error while changing Escalation Action.");
        }
    }

    public String getPremiumStatusValue() throws Exception {
        try {
            WebElement premiumStatusValue = policyInformationPageObjects.getPremiumStatusValue();
            return premiumStatusValue.getText();
        } catch (Exception e) {
            log.error("Error while getting premium status value.");
            throw new Exception("Error while getting premium status value.");
        }
    }
}
