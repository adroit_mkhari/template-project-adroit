package co.za.fnb.flow.tester.policy_details;

import co.za.fnb.flow.models.policy_details.PremiumStatus;
import co.za.fnb.flow.pages.CreateMultipleMintItemTable;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import co.za.fnb.flow.pages.ErrorHandle;
import co.za.fnb.flow.pages.TestHandle;
import co.za.fnb.flow.pages.policy_details.information.BankInformation;
import co.za.fnb.flow.pages.policy_details.information.PolicyInformation;

public class ChangePremiumStatus {
    private Logger log  = LogManager.getLogger(ChangePremiumStatus.class);
    WebDriver driver;
    PremiumStatus premiumStatus;
    PolicyInformation policyInformation;
    BankInformation bankInformation;
    private String comment;
    private boolean valid;
    private ErrorHandle errorHandle = new ErrorHandle();
    private TestHandle testHandle = new TestHandle();

    public ChangePremiumStatus(WebDriver driver, PremiumStatus premiumStatus, ScenarioOperator scenarioOperator) {
        this.driver = driver;
        this.premiumStatus = premiumStatus;
        policyInformation = new PolicyInformation(driver, scenarioOperator);
        bankInformation = new BankInformation(driver, scenarioOperator);
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public void setExpectedResults() {
        policyInformation.setExpectedResults(premiumStatus.getExpectResult());
    }

    public void change() throws Exception {
        String option = premiumStatus.getPremiumStatus();

        if (!option.isEmpty()) {
            log.info("Changing Premium Status");
            policyInformation.changePremiumStatus(option);
        }
    }

    public void save() throws Exception {
        try {
            log.info("Saving Premium Status.");
            String bankName = premiumStatus.getBankName();
            String debitOrderDate = premiumStatus.getDebitOrderDate();
            String nextDueDate = premiumStatus.getNextDueDate();

            // Note: Should our premium status change to have more of these popup values we need to update these.
            String[] popUpWorkType = {premiumStatus.getPopupWorkTypeZero()};
            String[] popUpStatus = {premiumStatus.getPopupStatusZero()};
            String[] popUpQueue = {premiumStatus.getPopupQueueZero()};
            // TODO: Check if we have any of these values for Premium Status ? For now we don't but still to confirm with @Shirley if we may possibly have at some point.
            String commentCategoryValue = "";
            String commentValue = "";

            if (!bankName.isEmpty()) {
                bankInformation.changeBankName(bankName);
            }

            if (!debitOrderDate.isEmpty()) {
                bankInformation.changeDebitOrderDate(debitOrderDate);
            }

            if (!nextDueDate.isEmpty()) {
                bankInformation.changeNextDueDate(nextDueDate);
            }

            if (commentCategoryValue.isEmpty() && commentValue.isEmpty()) {
                policyInformation.save(popUpWorkType, popUpStatus, popUpQueue);
            } else {
                policyInformation.save(popUpWorkType, popUpStatus, popUpQueue, commentCategoryValue, commentValue);
            }

            testHandle = policyInformation.getTestHandle();
            errorHandle = policyInformation.getErrorHandle();
        } catch (Exception e) {
            testHandle.setSuccess(false);
            errorHandle.setError(e.getMessage());
            throw new Exception("ERROR on Save: " + e.getMessage());
        }
    }

    public CreateMultipleMintItemTable getCreateMultipleMintItemTable() {
        return policyInformation.getCreateMultipleMintItemTable();
    }

    public void validate() {
        try {
            String premiumStatusValue = policyInformation.getPremiumStatusValue();
            if (premiumStatusValue.equalsIgnoreCase(premiumStatus.getPremiumStatus())) {
                setValid(true);
            } else {
                setValid(false);
                setComment("Failed to change premium status. Expected \"" + premiumStatus.getPremiumStatus() + "\" but got \"" + premiumStatusValue + "\"");
            }
        } catch (Exception e) {
            setValid(false);
            setComment(e.getMessage());
        }
    }

    public ErrorHandle getErrorHandle() {
        return this.errorHandle;
    }

    public void setErrorHandle(ErrorHandle errorHandle) {
        this.errorHandle = errorHandle;
    }

    public TestHandle getTestHandle() {
        return this.testHandle;
    }

    public void setTestHandle(TestHandle testHandle) {
        this.testHandle = testHandle;
    }

}