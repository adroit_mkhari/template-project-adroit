package co.za.fnb.flow.pages.policy_details.page_factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class DeleteCommentPageObjects {
    @FindBy(id = "deleteCommentsView:deleteComments")
    private WebElement deleteComments;

    @FindBy(id = "deleteCommentsView:j_idt12")
    private WebElement saveComment;

    @FindBy(id = "createSearchItemView:mainTabView:quoteInfoTable:Remove")
    private WebElement remove;

    @FindBy(id = "createSearchItemView:mainTabView:save")
    private WebElement save;

    public DeleteCommentPageObjects(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public WebElement getDeleteComments() {
        return deleteComments;
    }

    public WebElement getSaveComment() {
        return saveComment;
    }

    public WebElement getRemove() {
        return remove;
    }

    public WebElement getSave() {
        return save;
    }
}
