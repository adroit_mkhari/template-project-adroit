package co.za.fnb.flow.models.custom_exceptions;

public class ServiceException extends Exception {
    public ServiceException(String serviceError) {
        super(serviceError);
    }
}
