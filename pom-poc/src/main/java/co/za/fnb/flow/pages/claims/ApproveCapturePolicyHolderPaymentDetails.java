package co.za.fnb.flow.pages.claims;

import co.za.fnb.flow.pages.BasePage;
import co.za.fnb.flow.pages.claims.page_factory.ApproveCapturePolicyHolderPaymentPageObjects;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;

public class ApproveCapturePolicyHolderPaymentDetails extends BasePage {
    private Logger log  = LogManager.getLogger(ApproveCapturePolicyHolderPaymentDetails.class);
    ApproveCapturePolicyHolderPaymentPageObjects approveCapturePolicyHolderPaymentPageObjects = new ApproveCapturePolicyHolderPaymentPageObjects(driver);

    public ApproveCapturePolicyHolderPaymentDetails(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
    }

    public void clickApprove() throws Exception {
        try {
            click(approveCapturePolicyHolderPaymentPageObjects.getBtnMakePolicyHolderPaymentApprove());
        } catch (Exception e) {
            log.error("Error while clicking on Btn Make Policy Holder Payment Approve.");
            throw new Exception("Error while clicking on Btn Make Policy Holder Payment Approve.");
        }
    }

    public void clickFail() throws Exception {
        try {
            click(approveCapturePolicyHolderPaymentPageObjects.getBtnMakePolicyHolderPaymentDecline());
        } catch (Exception e) {
            log.error("Error while clicking on Btn Make Policy Holder Payment Fail.");
            throw new Exception("Error while clicking on Btn Make Policy Holder Payment Fail.");
        }
    }

    public void clickYesApprove() throws Exception {
        try {
            click(approveCapturePolicyHolderPaymentPageObjects.getBtnConfirmPolicyHolderMakePaymentApproveYes());
        } catch (Exception e) {
            log.error("Error while clicking on Yes Approve.");
            throw new Exception("Error while clicking on Yes Approve.");
        }
    }

    public void clickNoDoNotApprove() throws Exception {
        try {
            click(approveCapturePolicyHolderPaymentPageObjects.getBtnConfirmPolicyHolderMakePaymentApproveNo());
        } catch (Exception e) {
            log.error("Error while clicking on No Do Not Approve.");
            throw new Exception("Error while clicking on No Do Not Approve.");
        }
    }

}
