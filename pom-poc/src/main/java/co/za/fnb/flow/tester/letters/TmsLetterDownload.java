package co.za.fnb.flow.tester.letters;

import co.za.fnb.flow.pages.ErrorHandle;
import co.za.fnb.flow.pages.TestHandle;
import co.za.fnb.flow.pages.tms.*;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;

public class TmsLetterDownload {
    private Logger log  = LogManager.getLogger(TmsLetterDownload.class);
    private WebDriver driver;
    private String comment;
    private boolean valid;
    private ErrorHandle errorHandle = new ErrorHandle();
    private TestHandle testHandle = new TestHandle();
    private String expectedResults;
    private TmsHomePage tmsHomePage;
    private TmsLoginPage tmsLoginPage;
    private TmsDistributionPage tmsDistributionPage;
    private TmsLettersTablePage tmsLettersTablePage;
    private TmsContentPage tmsContentPage;
    private ScenarioOperator scenarioOperator;

    public TmsLetterDownload(WebDriver driver, ScenarioOperator scenarioOperator) {
        this.driver = driver;
        this.scenarioOperator = scenarioOperator;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public String getExpectedResults() {
        return expectedResults;
    }

    public void setExpectedResults(String expectedResults) {
        this.expectedResults = expectedResults;
    }

    private String getExpectedDoubleNumberFormat(String premiumAmountDataValue) {
        if (!premiumAmountDataValue.isEmpty()) {
            if (!premiumAmountDataValue.contains(".")) {
                if (premiumAmountDataValue.contains(",")) {
                    return premiumAmountDataValue.replace(",", ".");
                } else {
                    return premiumAmountDataValue + ".00";
                }
            }
        } else {
            log.error("Got an Empty String Value" );
            premiumAmountDataValue = "0.00";
        }
        return premiumAmountDataValue;
    }

    private int getIntegerValue(String value) {
        try {
            return !value.isEmpty() ? Integer.valueOf(value) : 1;
        } catch (Exception e) {
            log.debug(e.getMessage());
            try {
                if (value.contains(".")) {
                    String[] numberProps = value.split("\\.");
                    return Integer.valueOf(numberProps[0]);
                }
            } catch (Exception ex) {
                return 1;
            }
        }
        return -1;
    }

    public void login(String fNumber, String password) throws Exception {
        log.info("Logging in to TMS");
        tmsLoginPage.inputFNumber(fNumber);
        tmsLoginPage.inputPassword(password);
        tmsHomePage = tmsLoginPage.clickSignIn();
        Thread.sleep(5000);
        log.info("Done with TMS Login logic");
    }

    public void maximizeWindow(int windowIndex) throws Exception {
        tmsLoginPage = new TmsLoginPage(driver, scenarioOperator);
        log.info("Maximizing TMS Window.");
        tmsLoginPage.maximizeWindow(windowIndex);
        log.info("Done Maximizing TMS Window.");
    }

    public void searchTargetLetters(String documentDescription) throws Exception {
        log.info("Handling TMS Letter Searching logic");
        tmsDistributionPage = tmsHomePage.clickQADistribution();
        Thread.sleep(3000);
        tmsDistributionPage.clickBusinessUnitOwner();
        Thread.sleep(1000);
        tmsDistributionPage.clickBusinessUnitOwnerOption();
        Thread.sleep(1000);
        tmsDistributionPage.clickCompanyCode();
        Thread.sleep(1000);
        tmsDistributionPage.clickCompanyCodeOption();
        Thread.sleep(1000);
        tmsLettersTablePage = tmsDistributionPage.clickSearch();
        Thread.sleep(6000);

        int lettersTableRowsList = tmsLettersTablePage.getLettersTableRowsList(), traceCount = 5;
        while (lettersTableRowsList < 1 && traceCount-- > 0) {
            Thread.sleep(2000);
            lettersTableRowsList = tmsLettersTablePage.getLettersTableRowsList();
        }

        tmsLettersTablePage.typeSearchInput(documentDescription);
        log.info("Done Handling TMS Letter Searching logic");
    }

    public void download(String targetDocumentName) throws Exception {
        log.info("Handling TMS Letter Download logic");
        int lettersTableRowsList;

        lettersTableRowsList = tmsLettersTablePage.getLettersTableRowsList();
        if (lettersTableRowsList != 0) {
            String documentName;
            for (int i = 1; i <= lettersTableRowsList; i++) {
                documentName = tmsLettersTablePage.getDocumentName(i);
                if (!documentName.isEmpty()) {
                    if (documentName.toLowerCase().contains(targetDocumentName.toLowerCase())) {
                        tmsContentPage = tmsLettersTablePage.clickDocumentDescription(i);

                        boolean checkPdfJs;
                        try {
                            tmsContentPage.clickContentOne();
                            checkPdfJs = tmsContentPage.checkPdfJs();

                            if (!checkPdfJs) {
                                tmsContentPage.clickContentTwo();
                                checkPdfJs = tmsContentPage.checkPdfJs();
                            }
                        } catch (Exception e) {
                            log.error("Error while looking pdf content.\n" + e.getMessage());
                            throw new Exception("Error while looking pdf content.\n" + e.getMessage());
                        }

                        Thread.sleep(3000);
                        if (checkPdfJs) {
                            tmsContentPage.clickDownload();
                            Thread.sleep(5000);
                            break;
                        }
                    }
                }
            }
        } else {
            driver.close();
            log.error("No Matching Letter For Search Text: " + targetDocumentName);
            throw new Exception("No Matching Letter For Search Text: " + targetDocumentName);
        }
        log.info("Done Handling TMS Letter Download logic");
    }

    public void sendAll(String targetDocumentName, String targeSendAddress) throws Exception {
        log.info("Handling TMS Letter Sending logic");
        int lettersTableRowsList;

        lettersTableRowsList = tmsLettersTablePage.getLettersTableRowsList();
        if (lettersTableRowsList != 0) {
            String documentName;
            String sendAddress;
            for (int i = 1; i <= lettersTableRowsList; i++) {
                documentName = tmsLettersTablePage.getDocumentName(i);
                sendAddress = tmsLettersTablePage.getSendAddress(i);
                if (!documentName.isEmpty()) {
                    if (documentName.toLowerCase().contains(targetDocumentName.toLowerCase())
                        && sendAddress.toLowerCase().equalsIgnoreCase(targeSendAddress)) {
                        tmsLettersTablePage.clickLettersTableCheckBox(i);
                        // TODO: Ask If We Want All Or Only Matching Ones.
                    }
                }
            }
            // TODO: Click Send All
            tmsDistributionPage.clickSendAll();
        } else {
            driver.close();
            log.error("No Matching Letter For Search Text: " + targetDocumentName);
            throw new Exception("No Matching Letter For Search Text: " + targetDocumentName);
        }
        log.info("Done Handling TMS Letter Sending logic");
    }

    public void save() throws Exception {
        try {
            log.info("Saving TMS Downloaded Letter.");

            // TODO: Save TMS Downloaded Letter
            // Note: Configured Selenium To Save To The Project Resource Download Folder By Default.

            // TODO: testHandle = policyInformation.getTestHandle();
            // TODO: errorHandle = policyInformation.getErrorHandle();
        } catch (Exception e) {
            testHandle.setSuccess(false);
            errorHandle.setError(e.getMessage());
            throw new Exception("ERROR on Save: " + e.getMessage());
        }
    }

    public void validate() {
        // TODO: Implement Validation Logic
        setValid(true);
    }

    public ErrorHandle getErrorHandle() {
        return this.errorHandle;
    }

    public void setErrorHandle(ErrorHandle errorHandle) {
        this.errorHandle = errorHandle;
    }

    public TestHandle getTestHandle() {
        return this.testHandle;
    }

    public void setTestHandle(TestHandle testHandle) {
        this.testHandle = testHandle;
    }

}