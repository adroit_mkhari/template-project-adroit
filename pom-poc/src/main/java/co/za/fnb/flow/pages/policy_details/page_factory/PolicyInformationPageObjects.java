package co.za.fnb.flow.pages.policy_details.page_factory;

import co.za.fnb.flow.pages.policy_details.PolicyDetails;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PolicyInformationPageObjects {
    @FindBy(id = "createSearchItemView:mainTabView:coverAmount")
    private WebElement coverAmount;

    @FindBy(id = "createSearchItemView:mainTabView:totalPremiumAmount")
    private WebElement totalPremiumAmount;

    @FindBy(id = "createSearchItemView:mainTabView:arrearsAmount")
    private WebElement arrearsAmount;

    @FindBy(id = "createSearchItemView:mainTabView:planType")
    private WebElement planType;

    @FindBy(id = "createSearchItemView:mainTabView:escalationStatus")
    private WebElement escalationStatus;

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:policyInformationPanel')]//div[contains (@id, 'createSearchItemView:mainTabView:policyInformationPanel_content')]//tbody//tr//td//div[contains (@id, 'createSearchItemView:mainTabView:premiumstatus')]//div[contains (@class, 'ui-selectonemenu-trigger ui-state-default ui-corner-right')]")
    private WebElement premiumStatusSelect;

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:premiumstatus_panel')]//div//ul[contains (@id, 'createSearchItemView:mainTabView:premiumstatus_items')]")
    private WebElement premiumStatusSelectItems;

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:policyInformationPanel')]//div[contains (@id, 'createSearchItemView:mainTabView:policyInformationPanel_content')]//tbody//tr//td//label[contains (@id, 'createSearchItemView:mainTabView:premiumstatus_label')]")
    private WebElement premiumStatusValue;

    @FindBy(id = "createSearchItemView:mainTabView:escalationAction")
    private WebElement escalationAction;

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:policyInformationPanel')]//div[contains (@id, 'createSearchItemView:mainTabView:policyInformationPanel_content')]//tbody//tr//td//div[contains (@id, 'createSearchItemView:mainTabView:escalationAction')]//div[contains (@class, 'ui-selectonemenu-trigger ui-state-default ui-corner-right')]")
    private WebElement escalationActionSelect;

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:escalationAction_panel')]//div//ul[contains (@id, 'createSearchItemView:mainTabView:escalationAction_items')]")
    private WebElement escalationActionSelectItems;

    public PolicyInformationPageObjects(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public WebElement getCoverAmount() {
        return coverAmount;
    }

    public WebElement getTotalPremiumAmount() {
        return totalPremiumAmount;
    }

    public WebElement getArrearsAmount() {
        return arrearsAmount;
    }

    public WebElement getPlanType() {
        return planType;
    }

    public WebElement getEscalationStatus() {
        return escalationStatus;
    }

    public WebElement getPremiumStatusSelect() {
        return premiumStatusSelect;
    }

    public WebElement getPremiumStatusSelectItems() {
        return premiumStatusSelectItems;
    }

    public WebElement getPremiumStatusValue() {
        return premiumStatusValue;
    }

    public WebElement getEscalationAction() {
        return escalationAction;
    }

    public WebElement getEscalationActionSelect() {
        return escalationActionSelect;
    }

    public WebElement getEscalationActionSelectItems() {
        return escalationActionSelectItems;
    }
}
