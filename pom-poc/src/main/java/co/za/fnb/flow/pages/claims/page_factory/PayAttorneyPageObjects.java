package co.za.fnb.flow.pages.claims.page_factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PayAttorneyPageObjects {

    // region Supplier
    @FindBy(id = "frmAddClaimPaymentConfirmation:Suplier:0")
    WebElement supplierZero;
    @FindBy(id = "frmAddClaimPaymentConfirmation:Suplier:1")
    WebElement supplierOne;
    @FindBy(id = "frmAddClaimPaymentConfirmation:Suplier:2")
    WebElement supplierTwo;
    @FindBy(id = "frmAddClaimPaymentConfirmation:Suplier:3")
    WebElement supplierThree;
    @FindBy(id = "frmAddClaimPaymentConfirmation:Suplier:4")
    WebElement supplierFour;
    @FindBy(id = "frmAddClaimPaymentConfirmation:Suplier:5")
    WebElement supplierFive;
    // endregion

    // region VAT
    @FindBy(id = "frmAddClaimPaymentConfirmation:vatConfirmation:0")
    WebElement vatConfirmationZero;
    @FindBy(id = "frmAddClaimPaymentConfirmation:vatConfirmation:1")
    WebElement vatConfirmationOne;
    @FindBy(id = "frmAddClaimPaymentConfirmation:vatConfirmation:2")
    WebElement vatConfirmationTwo;
    // endregion

    // region FRS
    @FindBy(id = "frmAddClaimPaymentConfirmation:fsr:0")
    WebElement frsZero;
    @FindBy(id = "frmAddClaimPaymentConfirmation:fsr:1")
    WebElement frsOne;
    @FindBy(id = "frmAddClaimPaymentConfirmation:fsr:2")
    WebElement frsTwo;
    // endregion

    // region AmountExclusiveRadio
    @FindBy(id = "frmAddClaimPaymentConfirmation:AmountExclusiveRadio:0")
    WebElement amountExclusiveRadioZero;
    @FindBy(id = "frmAddClaimPaymentConfirmation:AmountExclusiveRadio:1")
    WebElement amountExclusiveRadioOne;
    // endregion

    @FindBy(id = "frmAddClaimPaymentConfirmation:j_idt326_input")
    WebElement idt326;

    @FindBy(id = "frmAddClaimPaymentConfirmation:j_idt327_input")
    WebElement idt327;

    @FindBy(id = "frmAddClaimPaymentConfirmation:j_idt328_input")
    WebElement idt328;

    @FindBy(id = "btnMakePaymentConfirmationCancel")
    WebElement confirmationCancel;

    @FindBy(id = "btnMakePaymentSubmitConfirmation")
    WebElement submitConfirmation;

    public PayAttorneyPageObjects(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public WebElement getSupplierZero() {
        return supplierZero;
    }

    public WebElement getSupplierOne() {
        return supplierOne;
    }

    public WebElement getSupplierTwo() {
        return supplierTwo;
    }

    public WebElement getSupplierThree() {
        return supplierThree;
    }

    public WebElement getSupplierFour() {
        return supplierFour;
    }

    public WebElement getSupplierFive() {
        return supplierFive;
    }

    public WebElement getVatConfirmationZero() {
        return vatConfirmationZero;
    }

    public WebElement getVatConfirmationOne() {
        return vatConfirmationOne;
    }

    public WebElement getVatConfirmationTwo() {
        return vatConfirmationTwo;
    }

    public WebElement getFrsZero() {
        return frsZero;
    }

    public WebElement getFrsOne() {
        return frsOne;
    }

    public WebElement getFrsTwo() {
        return frsTwo;
    }

    public WebElement getAmountExclusiveRadioZero() {
        return amountExclusiveRadioZero;
    }

    public WebElement getAmountExclusiveRadioOne() {
        return amountExclusiveRadioOne;
    }

    public WebElement getIdt326() {
        return idt326;
    }

    public WebElement getIdt327() {
        return idt327;
    }

    public WebElement getIdt328() {
        return idt328;
    }

    public WebElement getConfirmationCancel() {
        return confirmationCancel;
    }

    public WebElement getSubmitConfirmation() {
        return submitConfirmation;
    }
}
