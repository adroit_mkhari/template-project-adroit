package co.za.fnb.flow.pages.generic_letter.page_factory;

import co.za.fnb.flow.pages.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class GenericLetterPageObjects {

    @FindBy(id = "createSearchItemView:mainTabView:letterType")
    private WebElement letterType;

    @FindBy(id = "createSearchItemView:mainTabView:letterType_label")
    private WebElement letterTypeSelect;

    @FindBy(id = "createSearchItemView:mainTabView:letterType_items")
    private WebElement letterTypeSelectItems;

    @FindBy(id = "createSearchItemView:mainTabView:letterDescription")
    private WebElement letterDescription;

    @FindBy(id = "createSearchItemView:mainTabView:preferredCorr")
    private WebElement preferredCorrespondence;

    @FindBy(id = "createSearchItemView:mainTabView:preferredCorr_label")
    private WebElement preferredCorrespondenceSelect;

    @FindBy(id = "createSearchItemView:mainTabView:preferredCorr_items")
    private WebElement preferredCorrespondenceSelectItems;

    @FindBy(id = "createSearchItemView:mainTabView:attachPolicySchedule")
    private WebElement attachPolicySchedule;

    @FindBy(id = "createSearchItemView:mainTabView:letterContent")
    private WebElement letterContent;

    @FindBy(id = "createSearchItemView:mainTabView:sendGenericLetter")
    private WebElement sendGenericLetter;

    public GenericLetterPageObjects(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public WebElement getLetterType() {
        return letterType;
    }

    public WebElement getLetterTypeSelect() {
        return letterTypeSelect;
    }

    public WebElement getLetterTypeSelectItems() {
        return letterTypeSelectItems;
    }

    public WebElement getLetterDescription() {
        return letterDescription;
    }

    public WebElement getPreferredCorrespondence() {
        return preferredCorrespondence;
    }

    public WebElement getPreferredCorrespondenceSelect() {
        return preferredCorrespondenceSelect;
    }

    public WebElement getPreferredCorrespondenceSelectItems() {
        return preferredCorrespondenceSelectItems;
    }

    public WebElement getAttachPolicySchedule() {
        return attachPolicySchedule;
    }

    public WebElement getLetterContent() {
        return letterContent;
    }

    public WebElement getSendGenericLetter() {
        return sendGenericLetter;
    }
}
