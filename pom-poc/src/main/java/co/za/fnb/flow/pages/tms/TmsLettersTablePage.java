package co.za.fnb.flow.pages.tms;

import co.za.fnb.flow.pages.BasePage;
import co.za.fnb.flow.pages.tms.page_factory.TmsLettersTablePageFactory;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;

public class TmsLettersTablePage extends BasePage {
    private Logger log  = LogManager.getLogger(TmsLettersTablePage.class);
    private TmsLettersTablePageFactory tmsLettersTablePageFactory = new TmsLettersTablePageFactory(driver, scenarioOperator);
    private Actions actions = new Actions(driver);

    public TmsLettersTablePage(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
        PageFactory.initElements(driver, this);
    }

    public void typeSearchInput(String searchTextInput) throws Exception {
        try {
            type(tmsLettersTablePageFactory.getSearchTextInputField(), searchTextInput);
        } catch (Exception e) {
            log.error("Error while typing serach text: " + searchTextInput);
            throw new Exception("Error while typing serach text: " + searchTextInput);
        }
    }

    public int getLettersTableRowsList() throws Exception {
        try {
            String lettersTableCellLocatorTemplateXpath = tmsLettersTablePageFactory.getLettersTableRowsList();
            int letterTableRowsList = getElementsList(lettersTableCellLocatorTemplateXpath);
            return letterTableRowsList;
        } catch (Exception e) {
            log.error("Error while getting Letters Table Rows List.");
            throw new Exception("Error while getting Letters Table Rows List.");
        }
    }

    public String getLetterTableCell(int row, int column) throws Exception {
        try {
            String lettersTableCellLocatorTemplateXpath = tmsLettersTablePageFactory.getLettersTableCellLocatorTemplateXpath();
            String locatorForTableRowWithTrTd = getLocatorForTableRowWithTrTd(lettersTableCellLocatorTemplateXpath, row, column);
            WebElement letterTableCell = driver.findElement(By.xpath(locatorForTableRowWithTrTd));
            return getText(letterTableCell);
        } catch (Exception e) {
            log.error("Error while getting Letter Table Cell: " + row + "->" + column);
            throw new Exception("Error while getting Letter Table Cell: " + row + "->" + column);
        }
    }

    public String getLetterTableRow(int index) throws Exception {
        try {
            String rowLocatorTemplateXpath = tmsLettersTablePageFactory.getRowLocatorTemplateXpath();
            String matchPrefix = "tr[", matchPostfix = "]", targetTextRegex = "tr\\[1\\]";
            String rowLocatorForIndex = getLocatorForIndex(rowLocatorTemplateXpath, targetTextRegex, matchPrefix, matchPostfix, index);
            WebElement letterTableRow = driver.findElement(By.xpath(rowLocatorForIndex));
            return getText(letterTableRow);
        } catch (Exception e) {
            log.error("Error while getting Letter Table Row: " + index);
            throw new Exception("Error while getting Letter Table Row: " + index);
        }
    }

    public void clickLettersTableCheckBox(int index) throws Exception {
        try {
            String rowLocatorTemplateXpath = tmsLettersTablePageFactory.getCheckBoxLocatorTemplateXpath();
            String matchPrefix = "tr[", matchPostfix = "]", targetTextRegex = "tr\\[1\\]";
            String rowLocatorForIndex = getLocatorForIndex(rowLocatorTemplateXpath, targetTextRegex, matchPrefix, matchPostfix, index);
            WebElement letterTableRow = driver.findElement(By.xpath(rowLocatorForIndex));
            click(letterTableRow);
        } catch (Exception e) {
            log.error("Error while getting Letter Table Check Box: " + index);
            throw new Exception("Error while getting Letter Table Check Box: " + index);
        }
    }

    public String getDocumentDescription(int index) throws Exception {
        try {
            String rowLocatorTemplateXpath = tmsLettersTablePageFactory.getDocumentDescriptionLocatorTemplateXpath();
            String matchPrefix = "-", matchPostfix = "", targetTextRegex = "-1";
            String cellLocatorForIndex = getLocatorForIndex(rowLocatorTemplateXpath, targetTextRegex, matchPrefix, matchPostfix, index);
            WebElement letterTableCell = driver.findElement(By.xpath(cellLocatorForIndex));
            return getText(letterTableCell);
        } catch (Exception e) {
            log.error("Error while clicking Letter Table Document Description: " + index);
            throw new Exception("Error while clicking Letter Table Document Description: " + index);
        }
    }

    public TmsContentPage clickDocumentDescription(int index) throws Exception {
        try {
            String rowLocatorTemplateXpath = tmsLettersTablePageFactory.getDocumentDescriptionLocatorTemplateXpath();
            String matchPrefix = "-", matchPostfix = "", targetTextRegex = "-1";
            String rowLocatorForIndex = getLocatorForIndex(rowLocatorTemplateXpath, targetTextRegex, matchPrefix, matchPostfix, index);
            WebElement letterTableRow = driver.findElement(By.xpath(rowLocatorForIndex));
            actions.doubleClick(letterTableRow).build().perform();
            Thread.sleep(5000);
            return new TmsContentPage(driver, scenarioOperator);
        } catch (Exception e) {
            log.error("Error while clicking Letter Table Document Description: " + index);
            throw new Exception("Error while clicking Letter Table Document Description: " + index);
        }
    }

    public String getCompanyCode(int index) throws Exception {
        try {
            String rowLocatorTemplateXpath = tmsLettersTablePageFactory.getCompanyCodeLocatorTemplateXpath();
            String matchPrefix = "-", matchPostfix = "", targetTextRegex = "-1";
            String cellLocatorForIndex = getLocatorForIndex(rowLocatorTemplateXpath, targetTextRegex, matchPrefix, matchPostfix, index);
            WebElement letterTableCell = driver.findElement(By.xpath(cellLocatorForIndex));
            return getText(letterTableCell);
        } catch (Exception e) {
            log.error("Error while clicking Letter Table Company Code: " + index);
            throw new Exception("Error while clicking Letter Table Company Code: " + index);
        }
    }

    public String getDocumentCode(int index) throws Exception {
        try {
            String rowLocatorTemplateXpath = tmsLettersTablePageFactory.getDocumentCodeLocatorTemplateXpath();
            String matchPrefix = "-", matchPostfix = "", targetTextRegex = "-1";
            String cellLocatorForIndex = getLocatorForIndex(rowLocatorTemplateXpath, targetTextRegex, matchPrefix, matchPostfix, index);
            WebElement letterTableCell = driver.findElement(By.xpath(cellLocatorForIndex));
            return getText(letterTableCell);
        } catch (Exception e) {
            log.error("Error while clicking Letter Table Document Code: " + index);
            throw new Exception("Error while clicking Letter Table Document Code: " + index);
        }
    }

    public String getDocumentName(int index) throws Exception {
        try {
            String rowLocatorTemplateXpath = tmsLettersTablePageFactory.getDocumentNameLocatorTemplateXpath();
            String matchPrefix = "-", matchPostfix = "", targetTextRegex = "-1";
            String cellLocatorForIndex = getLocatorForIndex(rowLocatorTemplateXpath, targetTextRegex, matchPrefix, matchPostfix, index);
            WebElement letterTableCell = driver.findElement(By.xpath(cellLocatorForIndex));
            return getText(letterTableCell);
        } catch (Exception e) {
            log.error("Error while clicking Letter Table Document Name: " + index);
            throw new Exception("Error while clicking Letter Table Document Name: " + index);
        }
    }

    public String getDistributionMethod(int index) throws Exception {
        try {
            String rowLocatorTemplateXpath = tmsLettersTablePageFactory.getDistributionMethodLocatorTemplateXpath();
            String matchPrefix = "-", matchPostfix = "", targetTextRegex = "-1";
            String cellLocatorForIndex = getLocatorForIndex(rowLocatorTemplateXpath, targetTextRegex, matchPrefix, matchPostfix, index);
            WebElement letterTableCell = driver.findElement(By.xpath(cellLocatorForIndex));
            return getText(letterTableCell);
        } catch (Exception e) {
            log.error("Error while clicking Letter Table Distribution Method: " + index);
            throw new Exception("Error while clicking Letter Table Distribution Method: " + index);
        }
    }

    public String getLastModifiedUser(int index) throws Exception {
        try {
            String rowLocatorTemplateXpath = tmsLettersTablePageFactory.getLastModifiedUserLocatorTemplateXpath();
            String matchPrefix = "-", matchPostfix = "", targetTextRegex = "-1";
            String cellLocatorForIndex = getLocatorForIndex(rowLocatorTemplateXpath, targetTextRegex, matchPrefix, matchPostfix, index);
            WebElement letterTableCell = driver.findElement(By.xpath(cellLocatorForIndex));
            return getText(letterTableCell);
        } catch (Exception e) {
            log.error("Error while clicking Letter Table Last Modified User: " + index);
            throw new Exception("Error while clicking Letter Table Last Modified User: " + index);
        }
    }

    public String getLastModifiedDate(int index) throws Exception {
        try {
            String rowLocatorTemplateXpath = tmsLettersTablePageFactory.getLastModifiedDateLocatorTemplateXpath();
            String matchPrefix = "-", matchPostfix = "", targetTextRegex = "-1";
            String cellLocatorForIndex = getLocatorForIndex(rowLocatorTemplateXpath, targetTextRegex, matchPrefix, matchPostfix, index);
            WebElement letterTableCell = driver.findElement(By.xpath(cellLocatorForIndex));
            return getText(letterTableCell);
        } catch (Exception e) {
            log.error("Error while clicking Letter Table Last Modified Date: " + index);
            throw new Exception("Error while clicking Letter Table Last Modified Date: " + index);
        }
    }

    public String getSendAddress(int index) throws Exception {
        try {
            String rowLocatorTemplateXpath = tmsLettersTablePageFactory.getSendAddressLocatorTemplateXpath();
            String matchPrefix = "-", matchPostfix = "", targetTextRegex = "-1";
            String cellLocatorForIndex = getLocatorForIndex(rowLocatorTemplateXpath, targetTextRegex, matchPrefix, matchPostfix, index);
            WebElement letterTableCell = driver.findElement(By.xpath(cellLocatorForIndex));
            return getText(letterTableCell);
        } catch (Exception e) {
            log.error("Error while clicking Letter Table Send Address: " + index);
            throw new Exception("Error while clicking Letter Table Send Address: " + index);
        }
    }

    public String getTheme(int index) throws Exception {
        try {
            String rowLocatorTemplateXpath = tmsLettersTablePageFactory.getThemeLocatorTemplateXpath();
            String matchPrefix = "-", matchPostfix = "", targetTextRegex = "-1";
            String cellLocatorForIndex = getLocatorForIndex(rowLocatorTemplateXpath, targetTextRegex, matchPrefix, matchPostfix, index);
            WebElement letterTableCell = driver.findElement(By.xpath(cellLocatorForIndex));
            return getText(letterTableCell);
        } catch (Exception e) {
            log.error("Error while clicking Letter Table Theme: " + index);
            throw new Exception("Error while clicking Letter Table Theme: " + index);
        }
    }

}
