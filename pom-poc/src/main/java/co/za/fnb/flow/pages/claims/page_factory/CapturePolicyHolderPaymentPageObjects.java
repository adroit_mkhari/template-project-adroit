package co.za.fnb.flow.pages.claims.page_factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CapturePolicyHolderPaymentPageObjects {

    // region Group Information
    @FindBy(id = "frmAddClaimPaymentPolicyHolder:policyHolder")
    WebElement policyHolder; // Has attribute value.

    @FindBy(id = "frmAddClaimPaymentPolicyHolder:policyHolderBankName")
    WebElement policyHolderBankName; // Has attribute value.

    @FindBy(id = "frmAddClaimPaymentPolicyHolder:policyHolderBranchCode")
    WebElement policyHolderBranchCode; // Has attribute value.

    @FindBy(id = "frmAddClaimPaymentPolicyHolder:policyHolderAccountType")
    WebElement policyHolderAccountType; // Has attribute value.

    @FindBy(id = "frmAddClaimPaymentPolicyHolder:policyHolderAccountNumber")
    WebElement policyHolderAccountNumber; // Has attribute value.

    @FindBy(id = "frmAddClaimPaymentPolicyHolder:policyHolderPolicyNumber")
    WebElement policyHolderPolicyNumber; // Has attribute value.

    @FindBy(id = "frmAddClaimPaymentPolicyHolder:policyHolderClaimRefNo")
    WebElement policyHolderClaimRefNo; // Has attribute value.

    @FindBy(id = "frmAddClaimPaymentPolicyHolder:policyHolderClaimStatus")
    WebElement policyHolderClaimStatus; // Has attribute value.

    @FindBy(id = "frmAddClaimPaymentPolicyHolder:policyHolderremainCaseLimit")
    WebElement policyHolderRemainCaseLimit; // Has attribute value.
    // endregion

    @FindBy(id = "frmAddClaimPaymentPolicyHolder:receviedDatePH_input")
    WebElement receivedDatePolicyHolderInput;

    @FindBy(id = "frmAddClaimPaymentPolicyHolder:aolAmount_input")
    WebElement aolAmountInput;

    @FindBy(id = "frmAddClaimPaymentPolicyHolder:paymentStagePolicyHolder:0")
    WebElement paymentStageInterimPolicyHolder;

    @FindBy(id = "frmAddClaimPaymentPolicyHolder:paymentStagePolicyHolder:1")
    WebElement paymentStageFinalPolicyHolder;

    @FindBy(id = "btnMakePolicyHolderPaymentSubmit")
    WebElement btnMakePolicyHolderPaymentSubmit;

    @FindBy(id = "btnMakePolicyHolderPaymentCancle")
    WebElement btnMakePolicyHolderPaymentCancel;

    public CapturePolicyHolderPaymentPageObjects(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public WebElement getPolicyHolder() {
        return policyHolder;
    }

    public WebElement getPolicyHolderBankName() {
        return policyHolderBankName;
    }

    public WebElement getPolicyHolderBranchCode() {
        return policyHolderBranchCode;
    }

    public WebElement getPolicyHolderAccountType() {
        return policyHolderAccountType;
    }

    public WebElement getPolicyHolderAccountNumber() {
        return policyHolderAccountNumber;
    }

    public WebElement getPolicyHolderPolicyNumber() {
        return policyHolderPolicyNumber;
    }

    public WebElement getPolicyHolderClaimRefNo() {
        return policyHolderClaimRefNo;
    }

    public WebElement getPolicyHolderClaimStatus() {
        return policyHolderClaimStatus;
    }

    public WebElement getPolicyHolderRemainCaseLimit() {
        return policyHolderRemainCaseLimit;
    }

    public WebElement getReceivedDatePolicyHolderInput() {
        return receivedDatePolicyHolderInput;
    }

    public WebElement getAolAmountInput() {
        return aolAmountInput;
    }

    public WebElement getPaymentStageInterimPolicyHolder() {
        return paymentStageInterimPolicyHolder;
    }

    public WebElement getPaymentStageFinalPolicyHolder() {
        return paymentStageFinalPolicyHolder;
    }

    public WebElement getBtnMakePolicyHolderPaymentSubmit() {
        return btnMakePolicyHolderPaymentSubmit;
    }

    public WebElement getBtnMakePolicyHolderPaymentCancel() {
        return btnMakePolicyHolderPaymentCancel;
    }
}
