package co.za.fnb.flow.pages.audit_trail;

import co.za.fnb.flow.pages.ErrorHandle;
import co.za.fnb.flow.pages.TestHandle;
import co.za.fnb.flow.pages.audit_trail.page_factory.AuditTrailPageObjects;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import co.za.fnb.flow.pages.BasePage;

import java.util.Arrays;
import java.util.List;

public class AuditTrail extends BasePage {
    private Logger log  = LogManager.getLogger(AuditTrail.class);
    AuditTrailPageObjects auditTrailPageObjects = new AuditTrailPageObjects(driver);

    private ErrorHandle errorHandle = new ErrorHandle();
    private TestHandle testHandle = new TestHandle();

    public AuditTrail(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
        PageFactory.initElements(driver, this);
    }

    public void audit(String expectedAuditTrail) throws Exception {
        String[] _audit = expectedAuditTrail.split(",");
        int numberOfExpectedAuditTrailItems = _audit.length;
        String[] auditTrailItems = new String[numberOfExpectedAuditTrailItems];

        String workItem;
        for (int j = 0; j < numberOfExpectedAuditTrailItems; j++) {
            Thread.sleep(2000);
            WebElement auditTrailEvent = driver.findElement(By.id(getLocatorForIndex(auditTrailPageObjects.getAuditTrailEventLocator(), j)));
            workItem = auditTrailEvent.getText();
            Thread.sleep(500);
            auditTrailItems[j] = workItem;
        }

        boolean containsFlag = true;
        for (int j = 0; j < numberOfExpectedAuditTrailItems; j++) {
            workItem = _audit[j];
            List<String> auditTrailItemsList = Arrays.asList(auditTrailItems);

            if (!auditTrailItemsList.contains(workItem)) {
                containsFlag = false;
            }
        }

        if (containsFlag) {
            testHandle.setSuccess(true);
            log.info("Audit Trail Items Matched.");
        } else {
            testHandle.setSuccess(false);

            StringBuilder actualAuditTrail = new StringBuilder();
            for (int j = 0; j < numberOfExpectedAuditTrailItems; j++) {
                if (j == 0) {
                    actualAuditTrail.append(auditTrailItems[j]);
                } else {
                    actualAuditTrail.append(",").append(auditTrailItems[j]);
                }
            }

            String errorLog = "Failed Audit Trail: \n Expected: " + expectedAuditTrail + "\n Actual: " + actualAuditTrail;
            errorHandle.setError(errorLog);
            log.info(errorLog);
        }
    }

    public void check(String expectAuditTrail) throws Exception {
        try {
            int auditTrailDataTableSize = driver.findElements(auditTrailPageObjects.getAuditTrailTableDataBy()).size();
            String[] expectAuditTrailItems = expectAuditTrail.split(",");
            StringBuilder stringBuilder = new StringBuilder();

            boolean isPresent = false;
            String auditTrailEventValue;
            for (int i = 0; i < auditTrailDataTableSize; i++) {
                // TODO: implement Audit Trail Check Logic
                WebElement auditTrailEvent = driver.findElement(By.id(getLocatorForIndex(auditTrailPageObjects.getAuditTrailEventLocator(), i)));
                auditTrailEventValue = auditTrailEvent.getText().trim();

                for (String expectAuditTrailItem: expectAuditTrailItems) {
                    if (expectAuditTrailItem.trim().equalsIgnoreCase(auditTrailEventValue)) {
                        isPresent = true;
                    } else {
                        stringBuilder.append("\"").append(expectAuditTrailItem).append("\" not equal to \"").append(auditTrailEventValue).append("\"").append("\n");
                    }
                }
            }

            if (!isPresent) {
                testHandle.setSuccess(false);
                errorHandle.setError("Expected audit trail not present on audit trail list table:\n\n" + stringBuilder.toString());
            } else  {
                testHandle.setSuccess(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while processing audit trail.");
            throw new Exception("Error while processing audit trail.");
        }
    }

    @Override
    public ErrorHandle getErrorHandle() {
        return this.errorHandle;
    }

    @Override
    public void setErrorHandle(ErrorHandle errorHandle) {
        this.errorHandle = errorHandle;
    }

    @Override
    public TestHandle getTestHandle() {
        return this.testHandle;
    }

    @Override
    public void setTestHandle(TestHandle testHandle) {
        this.testHandle = testHandle;
    }

}
