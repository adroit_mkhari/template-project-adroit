package co.za.fnb.flow.pages.tms;

import co.za.fnb.flow.pages.BasePage;
import co.za.fnb.flow.pages.tms.page_factory.TmsWorkQueuePageFactory;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class TmsWorkQueuePage extends BasePage {
    private Logger log  = LogManager.getLogger(TmsWorkQueuePage.class);
    private TmsWorkQueuePageFactory tmsWorkQueuePageFactory = new TmsWorkQueuePageFactory(driver, scenarioOperator);

    public TmsWorkQueuePage(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
        PageFactory.initElements(driver, this);
    }

    public TmsHomePage clickHome() throws Exception {
        try {
            click(tmsWorkQueuePageFactory.getHome());
            return new TmsHomePage(driver, scenarioOperator);
        } catch (Exception e) {
            log.error("Error while clicking Home.");
            throw new Exception("Error while clicking Home.");
        }
    }

    public void typeSearchWorkQueueInput(String searchInput) throws Exception {
        try {
            type(tmsWorkQueuePageFactory.getSearchWorkQueue(), searchInput);
        } catch (Exception e) {
            log.error("Error while Typing Search Work Queue Input.");
            throw new Exception("Error while Typing Search Work Queue Input.");
        }
    }

    public void clickSearch() throws Exception {
        try {
            click(tmsWorkQueuePageFactory.getSearch());
        } catch (Exception e) {
            log.error("Error while clicking Search Work Queue Search.");
            throw new Exception("Error while clicking Search Work Queue Search.");
        }
    }

    public void clickClear() throws Exception {
        try {
            click(tmsWorkQueuePageFactory.getClear());
        } catch (Exception e) {
            log.error("Error while clicking Search Work Queue Clear.");
            throw new Exception("Error while clicking Search Work Queue Clear.");
        }
    }

}
