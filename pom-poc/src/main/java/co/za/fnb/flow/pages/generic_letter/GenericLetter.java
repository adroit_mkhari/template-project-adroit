package co.za.fnb.flow.pages.generic_letter;

import co.za.fnb.flow.pages.generic_letter.page_factory.GenericLetterPageObjects;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import co.za.fnb.flow.pages.BasePage;

public class GenericLetter extends BasePage {
    GenericLetterPageObjects genericLetterPageObjects = new GenericLetterPageObjects(driver);

    public GenericLetter(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
        PageFactory.initElements(driver, this);
    }
}
