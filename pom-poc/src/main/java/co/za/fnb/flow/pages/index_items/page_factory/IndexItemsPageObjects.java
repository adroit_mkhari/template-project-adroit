package co.za.fnb.flow.pages.index_items.page_factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class IndexItemsPageObjects {
    // @xpath = //*[@id="indexItemsForm:tabViewId"]/ul/li/a
    // e.g., Claims, Underwriting
    @FindBy(xpath = "//*[@id=\"indexItemsForm:tabViewId\"]/ul/li/a")
    private WebElement indexItemsForm;

    // Items To Index
    // @xapth = //*[@id="indexItemsForm:tabViewId:0:itemsIndexPanel_header"]/span
    @FindBy(xpath = "//*[@id=\"indexItemsForm:tabViewId:0:itemsIndexPanel_header\"]/span")
    private WebElement itemsToIndex;

    // @id = indexItemsForm:tabViewId:0:emailId
    // @xapth = //*[@id="indexItemsForm:tabViewId:0:emailId"]
    @FindBy(xpath = "//*[@id=\"indexItemsForm:tabViewId:0:emailId\"]")
    private WebElement emailId;

    // @id = indexItemsForm:tabViewId:0:subject
    // @xpath = //*[@id="indexItemsForm:tabViewId:0:subject"]
    @FindBy(xpath = "//*[@id=\"indexItemsForm:tabViewId:0:subject\"]")
    private WebElement subject;

    // @id = indexItemsForm:tabViewId:0:fromDate_input
    // @xpath = //*[@id="indexItemsForm:tabViewId:0:fromDate_input"]
    @FindBy(xpath = "//*[@id=\"indexItemsForm:tabViewId:0:fromDate_input\"]")
    private WebElement fromDateInput;

    // @id = indexItemsForm:tabViewId:0:toDate_input
    // @xpath = //*[@id="indexItemsForm:tabViewId:0:toDate_input"]
    @FindBy(xpath = "//*[@id=\"indexItemsForm:tabViewId:0:toDate_input\"]")
    private WebElement toDateInput;

    // Filter
    // @xpath = //*[@id="indexItemsForm:tabViewId:0:Filter"]/span[2]
    @FindBy(xpath = "//*[@id=\"indexItemsForm:tabViewId:0:Filter\"]/span[2]")
    private WebElement filter;

    // Clear
    // @xpath = //*[@id="indexItemsForm:tabViewId:0:Clear"]/span[2]
    @FindBy(xpath = "//*[@id=\"indexItemsForm:tabViewId:0:Clear\"]/span[2]")
    private WebElement clear;

    @FindBy(id = "indexItemsForm:tabViewId:0:emailItemsTable_data")
    private WebElement emailItemsTableData;

    @FindBy(xpath = "//*[@id=\"indexItemsForm:tabViewId:0:emailItemsTable_data\"]/tr/td")
    private WebElement emailItemsTableDataNoItems;

    // //*[@id="indexItemsForm:tabViewId:0:emailItemsTable:0:indexDocumenthash"]
    private String indexDocumentHashTemplateXpath = "//*[@id=\"indexItemsForm:tabViewId:0:emailItemsTable:0:indexDocumenthash\"]";

    // //*[@id="indexItemsForm:tabViewId:0:emailItemsTable:0:indexDocumentDateandTime"]
    private String indexDocumentDateAndTimeTemplateXpath = "//*[@id=\"indexItemsForm:tabViewId:0:emailItemsTable:0:indexDocumentDateandTime\"]";

    // //*[@id="indexItemsForm:tabViewId:0:emailItemsTable:0:indexDocumentEmailAddress"]
    private String indexDocumentEmailAddressTemplateXpath = "//*[@id=\"indexItemsForm:tabViewId:0:emailItemsTable:0:indexDocumentEmailAddress\"]";

    // //*[@id="indexItemsForm:tabViewId:0:emailItemsTable:0:indexDocumentSubject"]
    private String indexDocumentSubjectTemplateXpath = "//*[@id=\"indexItemsForm:tabViewId:0:emailItemsTable:0:indexDocumentSubject\"]";

    // //*[@id="indexItemsForm:tabViewId:0:emailItemsTable_data"]/tr[1]/td[5]/div/div[2]/span
    // //*[@id="indexItemsForm:tabViewId:0:emailItemsTable_data"]/tr[2]/td[5]/div/div[2]/span
    // //*[@id="indexItemsForm:tabViewId:0:emailItemsTable_data"]/tr[4]/td[5]/div/div[2]/span
    // //*[@id="indexItemsForm:tabViewId:0:emailItemsTable_data"]/tr[5]/td[5]/div/div[2]/span
    // //*[@id="indexItemsForm:tabViewId:0:emailItemsTable_data"]/tr[6]/td[5]/div/div[2]/span
    private String emailItemsTableDataSelectTemplateXpath = "//*[@id=\"indexItemsForm:tabViewId:0:emailItemsTable_data\"]/tr[1]/td[5]/div/div[2]/span";

    // //*[@id="indexItemsForm:tabViewId:0:emailItemsTable:indexDocumentSelectAll"]/div/div[2]/span
    @FindBy(xpath = "//*[@id=\"indexItemsForm:tabViewId:0:emailItemsTable:indexDocumentSelectAll\"]/div/div[2]/span")
    private WebElement indexDocumentSelectAll;

    public IndexItemsPageObjects(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public WebElement getIndexItemsForm() {
        return indexItemsForm;
    }

    public WebElement getItemsToIndex() {
        return itemsToIndex;
    }

    public WebElement getEmailId() {
        return emailId;
    }

    public WebElement getSubject() {
        return subject;
    }

    public WebElement getFromDateInput() {
        return fromDateInput;
    }

    public WebElement getToDateInput() {
        return toDateInput;
    }

    public WebElement getFilter() {
        return filter;
    }

    public WebElement getClear() {
        return clear;
    }

    public WebElement getEmailItemsTableData() {
        return emailItemsTableData;
    }

    public WebElement getEmailItemsTableDataNoItems() {
        return emailItemsTableDataNoItems;
    }

    public String getIndexDocumentHashTemplateXpath() {
        return indexDocumentHashTemplateXpath;
    }

    public String getIndexDocumentDateAndTimeTemplateXpath() {
        return indexDocumentDateAndTimeTemplateXpath;
    }

    public String getIndexDocumentEmailAddressTemplateXpath() {
        return indexDocumentEmailAddressTemplateXpath;
    }

    public String getIndexDocumentSubjectTemplateXpath() {
        return indexDocumentSubjectTemplateXpath;
    }

    public String getEmailItemsTableDataSelectTemplateXpath() {
        return emailItemsTableDataSelectTemplateXpath;
    }

    public WebElement getIndexDocumentSelectAll() {
        return indexDocumentSelectAll;
    }
}
