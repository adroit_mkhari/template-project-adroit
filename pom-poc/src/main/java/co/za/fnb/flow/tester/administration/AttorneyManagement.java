package co.za.fnb.flow.tester.administration;

import co.za.fnb.flow.models.policy_details.AttorneyDetails;
import co.za.fnb.flow.pages.CreateMultipleMintItemTable;
import co.za.fnb.flow.pages.ErrorHandle;
import co.za.fnb.flow.pages.TestHandle;
import co.za.fnb.flow.pages.administration.AddAttorneyPage;
import co.za.fnb.flow.pages.administration.ApproveOrRejectAttorneyPage;
import co.za.fnb.flow.pages.administration.LegalAdvisorAdminPage;
import co.za.fnb.flow.pages.policy_details.information.BankInformation;
import co.za.fnb.flow.pages.policy_details.information.BeneficiaryInformation;
import co.za.fnb.flow.pages.policy_details.information.PolicyInformation;
import co.za.fnb.flow.pages.policy_details.information.QuoteInformation;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;

public class AttorneyManagement {
    private Logger log  = LogManager.getLogger(AttorneyManagement.class);
    WebDriver driver;
    AttorneyDetails attorneyDetails;
    PolicyInformation policyInformation;
    LegalAdvisorAdminPage legalAdvisorAdminPage;
    AddAttorneyPage addAttorneyPage;
    BeneficiaryInformation beneficiaryInformation;
    QuoteInformation quoteInformation;
    BankInformation bankInformation;
    private String comment;
    private boolean valid;
    private ErrorHandle errorHandle = new ErrorHandle();
    private TestHandle testHandle = new TestHandle();

    public AttorneyManagement(WebDriver driver, AttorneyDetails attorneyDetails, ScenarioOperator scenarioOperator) {
        this.driver = driver;
        this.attorneyDetails = attorneyDetails;
        policyInformation = new PolicyInformation(driver, scenarioOperator);
        beneficiaryInformation = new BeneficiaryInformation(driver, scenarioOperator);
        bankInformation = new BankInformation(driver, scenarioOperator);
        quoteInformation = new QuoteInformation(driver, scenarioOperator);
        legalAdvisorAdminPage = new LegalAdvisorAdminPage(driver, scenarioOperator);
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public void setExpectedResults() {
        policyInformation.setExpectedResults(attorneyDetails.getExpectResult());
    }

    public void addAttorney() throws Exception {
        log.info("Handling Attorney Management logic: Add Attorney");
        boolean isLawOnCall = attorneyDetails.getProductName().contains("LOC");
        log.info("This is a Law On Call Product: " + isLawOnCall);

        addAttorneyPage = legalAdvisorAdminPage.clickAddUser();
        Thread.sleep(1000);
        addAttorneyPage.switchToAnfMaximizeServiceWindow(0);
        Thread.sleep(1000);
        if (!attorneyDetails.getCompanyName().isEmpty()) {
            addAttorneyPage.updateComapanyName(attorneyDetails.getCompanyName());
            Thread.sleep(1000);
        }

        if (!attorneyDetails.getVatRegistered().isEmpty()) {
            if (attorneyDetails.getVatRegistered().equalsIgnoreCase("Yes")) {
                addAttorneyPage.clickVatFlag();
                Thread.sleep(1000);
                if (!attorneyDetails.getVatRegistrationNumber().isEmpty()) {
                    addAttorneyPage.updateVatRegNo(attorneyDetails.getVatRegistrationNumber());
                    Thread.sleep(1000);
                }
            }
        }

        if (!attorneyDetails.getEmailAddress().isEmpty()) {
            addAttorneyPage.updateEmail(attorneyDetails.getEmailAddress());
            Thread.sleep(1000);
        }

        if (!attorneyDetails.getCellNumber().isEmpty()) {
            addAttorneyPage.updateCellNo(attorneyDetails.getCellNumber());
            Thread.sleep(1000);
        }

        if (!attorneyDetails.getWorkNumber().isEmpty()) {
            addAttorneyPage.updateWorkNo(attorneyDetails.getWorkNumber());
            Thread.sleep(1000);
        }

        if (!attorneyDetails.getAttorneyType().isEmpty()) {
            addAttorneyPage.selectAttorneyType(attorneyDetails.getAttorneyType());
            Thread.sleep(1000);
        }

        if (!attorneyDetails.getAddressType().isEmpty()) {
            addAttorneyPage.selectAddressType(attorneyDetails.getAddressType());
            Thread.sleep(1000);
        }

        if (!attorneyDetails.getAddressLineOne().isEmpty()) {
            addAttorneyPage.updateAddressLineOne(attorneyDetails.getAddressLineOne());
            Thread.sleep(1000);
        }

        if (!attorneyDetails.getAddressLineTwo().isEmpty()) {
            addAttorneyPage.updateAddressLineTwo(attorneyDetails.getAddressLineTwo());
            Thread.sleep(1000);
        }

        if (!attorneyDetails.getSuburb().isEmpty()) {
            addAttorneyPage.updateSuburb(attorneyDetails.getSuburb());
            Thread.sleep(1000);
        }

        if (!attorneyDetails.getBankName().isEmpty()) {
            addAttorneyPage.selectBankName(attorneyDetails.getBankName());
            Thread.sleep(1000);
        }

        if (!attorneyDetails.getBranchCode().isEmpty()) {
            addAttorneyPage.updateBranchCode(attorneyDetails.getBranchCode());
            Thread.sleep(1000);
        }

        if (!attorneyDetails.getAccountType().isEmpty()) {
            addAttorneyPage.selectAccountType(attorneyDetails.getAccountType());
            Thread.sleep(1000);
        }

        if (!attorneyDetails.getAccountNumber().isEmpty()) {
            addAttorneyPage.updateAccountNumber(attorneyDetails.getAccountNumber());
            Thread.sleep(1000);
        }

        if (!attorneyDetails.getAccountHolder().isEmpty()) {
            addAttorneyPage.updateAccountHolder(attorneyDetails.getAccountHolder());
            Thread.sleep(1000);
        }

        if (!attorneyDetails.getProvince().isEmpty()) {
            addAttorneyPage.selectProvince(attorneyDetails.getProvince());
            Thread.sleep(1000);
        }

        if (!attorneyDetails.getCity().isEmpty()) {
            addAttorneyPage.clickCity();
            Thread.sleep(1000);
            if (!attorneyDetails.getCity().equalsIgnoreCase("All")) {
                addAttorneyPage.clickFirstCity();
                Thread.sleep(1000);
            } else {
                addAttorneyPage.clickAllCities();
                Thread.sleep(1000);
            }
            addAttorneyPage.clickCity();
            Thread.sleep(1000);
        }

        addAttorneyPage.clickAdd();
        Thread.sleep(1000);
        log.info("Done Handling Attorney Management: Add Attorney logic");
    }

    public String getAttorneyReference() throws Exception {
        log.info("Handling Attorney Management logic: Get Attorney Reference");

        int attorneyListTableSize = legalAdvisorAdminPage.getAttorneyListTableSize();
        Thread.sleep(1000);
        String companyName, status, type, attorneyReference = null;
        boolean gotMatchingAttorney = false;
        for (int i = 0; i < attorneyListTableSize; i++) {
            companyName = legalAdvisorAdminPage.getCompanyName(i);
            status = legalAdvisorAdminPage.getAttorneyStatus(i);
            type = legalAdvisorAdminPage.getAttorneyType(i);

            if (companyName.equalsIgnoreCase(attorneyDetails.getCompanyName())
                && status.equalsIgnoreCase("INACTIVE")
                && type.equalsIgnoreCase(attorneyDetails.getAttorneyType())) {
                attorneyReference = legalAdvisorAdminPage.getAttorneyReference(i);
                gotMatchingAttorney = true;
                break;
            }
        }

        if (!gotMatchingAttorney) {
            log.error("No matching newly created attorney found.");
            throw new Exception("No matching newly created attorney found.");
        }

        log.info("Done Handling Attorney Management: Get Attorney Reference " + attorneyReference);
        return attorneyReference;

    }

    public void approveAttorney(ScenarioOperator scenarioOperator) throws Exception {
        log.info("Handling Attorney Management logic: Approve Attorney.");

        int attorneyListTableSize = legalAdvisorAdminPage.getAttorneyListTableSize();
        String companyName, status, type, attorneyReference = null;
        boolean gotMatchingAttorney = false;
        int attorneyIndex = -1;
        for (int i = 0; i < attorneyListTableSize; i++) {
            attorneyReference = legalAdvisorAdminPage.getAttorneyReference(i);
            companyName = legalAdvisorAdminPage.getCompanyName(i);
            status = legalAdvisorAdminPage.getAttorneyStatus(i);
            type = legalAdvisorAdminPage.getAttorneyType(i);

            if (attorneyReference.equalsIgnoreCase(attorneyDetails.getPolicyNumber())
                && companyName.equalsIgnoreCase(attorneyDetails.getCompanyName())
                && status.equalsIgnoreCase("INACTIVE")
                && type.equalsIgnoreCase(attorneyDetails.getAttorneyType())) {
                legalAdvisorAdminPage.clickAttorneyReference(i);
                attorneyIndex = i;
                gotMatchingAttorney = true;
                break;
            }
        }

        if (gotMatchingAttorney) {
            legalAdvisorAdminPage.rightClickAttorneyReference(attorneyIndex);
            legalAdvisorAdminPage.clickApproveOrReject();
            ApproveOrRejectAttorneyPage approveOrRejectAttorneyPage
                    = new ApproveOrRejectAttorneyPage(driver, scenarioOperator);

            if (attorneyDetails.getApprove().equalsIgnoreCase("Yes")) {
                approveOrRejectAttorneyPage.clickApprove();
                if (attorneyDetails.getYesApprove().equalsIgnoreCase("Yes")) {
                    approveOrRejectAttorneyPage.clickYesApprove();
                    approve();

                    boolean gotMatchingApprovedAttorney = false;
                    for (int i = 0; i < attorneyListTableSize; i++) {
                        attorneyReference = legalAdvisorAdminPage.getAttorneyReference(i);
                        companyName = legalAdvisorAdminPage.getCompanyName(i);
                        status = legalAdvisorAdminPage.getAttorneyStatus(i);
                        type = legalAdvisorAdminPage.getAttorneyType(i);

                        if (attorneyReference.equalsIgnoreCase(attorneyDetails.getPolicyNumber())
                                && companyName.equalsIgnoreCase(attorneyDetails.getCompanyName())
                                && status.equalsIgnoreCase("ACTIVE")
                                && type.equalsIgnoreCase(attorneyDetails.getAttorneyType())) {
                            gotMatchingApprovedAttorney = true;
                            break;
                        }
                    }

                    if (!gotMatchingApprovedAttorney) {
                        log.error("No matching Approved attorney found.");
                        throw new Exception("No matching Approved attorney found.");
                    }
                } else {
                    approveOrRejectAttorneyPage.clickNoDoNotApprove();
                }
            } else {
                approveOrRejectAttorneyPage.clickFail();
                // TODO: Implement the rest of the  logic.
            }
        } else  {
            log.error("No matching newly created attorney found.");
            throw new Exception("No matching newly created attorney found.");
        }

        log.info("Done Handling Attorney Management: Approve Attorney " + attorneyReference);
    }

    private String getExpectedDoubleNumberFormat(String premiumAmountDataValue) {
        if (!premiumAmountDataValue.isEmpty()) {
            if (!premiumAmountDataValue.contains(".")) {
                if (premiumAmountDataValue.contains(",")) {
                    return premiumAmountDataValue.replace(",", ".");
                } else {
                    return premiumAmountDataValue + ".00";
                }
            }
        } else {
            log.error("Got an Empty String Value" );
            premiumAmountDataValue = "0.00";
        }
        return premiumAmountDataValue;
    }

    private int getIntegerValue(String value) {
        try {
            return !value.isEmpty() ? Integer.valueOf(value) : 1;
        } catch (Exception e) {
            log.debug(e.getMessage());
            try {
                if (value.contains(".")) {
                    String[] numberProps = value.split("\\.");
                    return Integer.valueOf(numberProps[0]);
                }
            } catch (Exception ex) {
                return 1;
            }
        }
        return -1;
    }

    public void save() throws Exception {
        try {
            log.info("Submitting Attorney.");
            addAttorneyPage.clickSubmit();
            Thread.sleep(1000);

            // Note: Should our premium status change to have more of these popup values we need to update these.
            String[] popUpWorkType = {attorneyDetails.getAddAttorneyPopUpWorkType()};
            String[] popUpStatus = {attorneyDetails.getAddAttorneyPopUpStatus()};
            String[] popUpQueue = {attorneyDetails.getAddAttorneyPopUpQueue()};

            String commentCategoryValue = "";
            String commentValue = "";

            if (commentCategoryValue.isEmpty() && commentValue.isEmpty()) {
                policyInformation.proceedAfterClickingSubmit(popUpWorkType, popUpStatus, popUpQueue);
            } else {
                // TODO: Add Logic For This Case.
                // policyInformation.proceedAfterClickingSubmit(popUpWorkType, popUpStatus, popUpQueue, commentCategoryValue, commentValue);
            }

            testHandle = policyInformation.getTestHandle();
            errorHandle = policyInformation.getErrorHandle();
        } catch (Exception e) {
            testHandle.setSuccess(false);
            errorHandle.setError(e.getMessage());
            throw new Exception("ERROR on Save: " + e.getMessage());
        }
    }

    public void approve() throws Exception {
        try {
            log.info("Approve Attorney.");
            // Note: Should our premium status change to have more of these popup values we need to update these.
            String[] popUpWorkType = {attorneyDetails.getApprovePopUpWorkType()};
            String[] popUpStatus = {attorneyDetails.getApprovePopUpStatus()};
            String[] popUpQueue = {attorneyDetails.getApprovePopUpQueue()};

            String commentCategoryValue = "";
            String commentValue = "";

            if (commentCategoryValue.isEmpty() && commentValue.isEmpty()) {
                policyInformation.proceedAfterClickingSubmit(popUpWorkType, popUpStatus, popUpQueue);
            } else {
                // TODO: Add Logic For This Case.
                // policyInformation.proceedAfterClickingSubmit(popUpWorkType, popUpStatus, popUpQueue, commentCategoryValue, commentValue);
            }

            testHandle = policyInformation.getTestHandle();
            errorHandle = policyInformation.getErrorHandle();
        } catch (Exception e) {
            testHandle.setSuccess(false);
            errorHandle.setError(e.getMessage());
            throw new Exception("ERROR on Save: " + e.getMessage());
        }
    }

    public CreateMultipleMintItemTable getCreateMultipleMintItemTable() {
        return policyInformation.getCreateMultipleMintItemTable();
    }

    public void validate() {
        // TODO: Implement Validation Logic
        setValid(true);
    }

    public ErrorHandle getErrorHandle() {
        return this.errorHandle;
    }

    public void setErrorHandle(ErrorHandle errorHandle) {
        this.errorHandle = errorHandle;
    }

    public TestHandle getTestHandle() {
        return this.testHandle;
    }

    public void setTestHandle(TestHandle testHandle) {
        this.testHandle = testHandle;
    }

}