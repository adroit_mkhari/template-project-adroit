package co.za.fnb.flow.handlers;

import generated.ResponseErrors;

import javax.xml.bind.*;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.io.StringWriter;

public class JAXBHelper {
    private JAXBContext jaxbContext;
    private Marshaller marshaller;
    private Unmarshaller unmarshaller;
    private JAXBElement<Object> dataObject;
    private String inputXml;
    private File outputFile;
    private String result;
    private Object unMarshalResult;
    private StringWriter stringWriter = new StringWriter();

    public JAXBHelper() {
    }

    public JAXBHelper(JAXBContext jaxbContext, Marshaller marshaller) {
        this.jaxbContext = jaxbContext;
        this.marshaller = marshaller;
    }

    public JAXBHelper(JAXBContext jaxbContext, Marshaller marshaller, JAXBElement<Object> dataObject) {
        this.jaxbContext = jaxbContext;
        this.marshaller = marshaller;
        this.dataObject = dataObject;
    }

    public JAXBHelper(JAXBContext jaxbContext, Marshaller marshaller, Unmarshaller unmarshaller, JAXBElement<Object> dataObject) {
        this.jaxbContext = jaxbContext;
        this.marshaller = marshaller;
        this.unmarshaller = unmarshaller;
        this.dataObject = dataObject;
    }

    public JAXBHelper(JAXBContext jaxbContext, Marshaller marshaller, JAXBElement<Object> dataObject, File outputFile) {
        this.jaxbContext = jaxbContext;
        this.marshaller = marshaller;
        this.dataObject = dataObject;
        this.outputFile = outputFile;
    }

    public JAXBHelper(JAXBContext jaxbContext, Unmarshaller unmarshaller, String inputXml) {
        this.jaxbContext = jaxbContext;
        this.unmarshaller = unmarshaller;
        this.inputXml = inputXml;
    }

    public JAXBHelper(JAXBContext jaxbContext, Marshaller marshaller, Unmarshaller unmarshaller, JAXBElement<Object> dataObject, File outputFile) {
        this.jaxbContext = jaxbContext;
        this.marshaller = marshaller;
        this.unmarshaller = unmarshaller;
        this.dataObject = dataObject;
        this.outputFile = outputFile;
    }

    public JAXBHelper(JAXBContext jaxbContext, Marshaller marshaller, Unmarshaller unmarshaller, JAXBElement<Object> dataObject, String inputXml, File outputFile) {
        this.jaxbContext = jaxbContext;
        this.marshaller = marshaller;
        this.unmarshaller = unmarshaller;
        this.dataObject = dataObject;
        this.inputXml = inputXml;
        this.outputFile = outputFile;
    }

    public void setJaxbContext(JAXBContext jaxbContext) {
        this.jaxbContext = jaxbContext;
    }

    public void setCustomJaxbContext(Class<?> objectType) throws JAXBException {
        jaxbContext = JAXBContext.newInstance(objectType);
        marshaller = jaxbContext.createMarshaller();
        unmarshaller = jaxbContext.createUnmarshaller();
    }

    public void setMarshaller(Marshaller marshaller) {
        this.marshaller = marshaller;
    }

    public void setUnmarshaller(Unmarshaller unmarshaller) {
        this.unmarshaller = unmarshaller;
    }

    public void setDataObject(JAXBElement<Object> dataObject) {
        this.dataObject = dataObject;
    }

    public String getInputXml() {
        return inputXml;
    }

    public void setInputXml(String inputXml) {
        this.inputXml = inputXml;
    }

    public File getOutputFile() {
        return outputFile;
    }

    public void setOutputFile(File outputFile) {
        this.outputFile = outputFile;
    }

    public String getResult() {
        return result;
    }

    public Object getUnMarshalResult() {
        return unMarshalResult;
    }

    public String formatInputXml(Class<?> objectType) {
        return formatInputXml(inputXml, objectType);
    }

    public String formatOutputXml(Class<?> objectType) {
        return formatOutputXml(inputXml, objectType);
    }

    public String formatResponseErrorXml() {
        return formatResponseErrorXml(inputXml);
    }

    public String formatInputXml(String stringXml, Class<?> objectType) {
        String formattedStringOutput = stringXml.replaceAll("&lt;", "<").replaceAll("&gt;", ">").replace("<output>", "").replace("</output>", "");
        System.out.println(formattedStringOutput);
        inputXml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" +
                "<input xsi:type=\"" + objectType.getSimpleName() + "\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">" +
                formattedStringOutput +
                "</input>";
        return inputXml;
    }

    public String formatOutputXml(String stringXml, Class<?> objectType) {
        if (!(stringXml.contains("<errors>") || stringXml.contains("<error>")) && !(stringXml.contains("&lt;errors&gt;") || stringXml.contains("&lt;error&gt;"))) {
            String formattedStringOutput = stringXml.replaceAll("&lt;", "<").replaceAll("&gt;", ">").replace("<output>", "").replace("</output>", "");
            System.out.println(formattedStringOutput);
            inputXml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" +
                    "<output xsi:type=\"" + objectType.getSimpleName() + "\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">" +
                    formattedStringOutput +
                    "</output>";
            return inputXml;
        } else {
            try {
                setCustomJaxbContext(ResponseErrors.class);
            } catch (JAXBException e) {
                e.printStackTrace();
            }
            return formatResponseErrorXml(stringXml);
        }
    }

    public String formatResponseErrorXml(String stringXml) {
        String formattedStringOutput = stringXml.replaceAll("&lt;", "<").replaceAll("&gt;", ">");
        System.out.println(formattedStringOutput);
        inputXml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" +
                "<errors xsi:type=\"ResponseErrors\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">" +
                formattedStringOutput +
                "</errors>";
        return inputXml;
    }

    public String marshal(boolean saveOutputToFile) throws Exception {
        try {
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            if (saveOutputToFile) {
                marshaller.marshal(dataObject, outputFile);
                marshaller.marshal(dataObject, stringWriter);
                result = stringWriter.toString();
            } else {
                marshaller.marshal(dataObject, stringWriter);
                result = stringWriter.toString();
            }
            return result;
        } catch (Exception e) {
            throw new Exception("Error while marshalling object: " + e.getMessage());
        }
    }

    public Object unMarshal() throws Exception {
        try {
            byte[] bytes = inputXml.getBytes();
            InputStream ts = new ByteArrayInputStream(bytes);
            Object unMarshal = unmarshaller.unmarshal(ts);
            unMarshalResult = JAXBIntrospector.getValue(unMarshal);
            return unMarshalResult;
        } catch (Exception e) {
            throw new Exception("Error while un-marshalling object: " + e.getMessage());
        }
    }

    public Object unMarshal(String inputXml) throws Exception {
        try {
            byte[] bytes = inputXml.getBytes();
            InputStream ts = new ByteArrayInputStream(bytes);
            Object unMarshal = unmarshaller.unmarshal(ts);
            unMarshalResult = JAXBIntrospector.getValue(unMarshal);
            return unMarshalResult;
        } catch (Exception e) {
            throw new Exception("Error while un-marshalling object: " + e.getMessage());
        }
    }

}
