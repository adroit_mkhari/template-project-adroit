package co.za.fnb.flow.tester.services.functions;

import co.za.fnb.flow.models.custom_exceptions.ServiceException;
import co.za.fnb.flow.models.custom_exceptions.ValidationException;
import co.za.fnb.flow.tester.ScenarioOperator;
import co.za.fnb.flow.tester.ScenarioTester;
import co.za.fnb.flow.tester.services.GetPolicyDetailsTester;
import co.za.fnb.flow.tester.services.QuoteTester;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import generated.*;
import generated.Error;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Quote {
    private Logger log  = LogManager.getLogger(Quote.class);
    private QuoteAddRoleRequestInput quoteAddRoleRequestInput;
    private QuoteDeleteRoleRequestInput quoteDeleteRoleRequestInput;
    private QuoteUpdateRoleRequestInput quoteUpdateRoleRequestInput;
    private GetPolicyDetailRequestInput getPolicyDetailRequestInput;
    private QuoteAddRoleResponseOutput quoteAddRoleResponseOutput;
    private QuoteDeleteRoleResponseOutput quoteDeleteRoleResponseOutput;
    private QuoteUpdateRoleResponseOutput quoteUpdateRoleResponseOutput;
    private GetPolicyDetailResponseOutput getPolicyDetailResponseOutput;
    private ResponsePolicyRoleType targetRole;
    private ResponseErrors responseErrors;
    private ScenarioTester scenarioTester;
    private ScenarioOperator scenarioOperator;
    private String username;

    public Quote(ScenarioTester scenarioTester) {
        this.scenarioTester = scenarioTester;
    }

    public void setQuoteAddRoleRequestInput(QuoteAddRoleRequestInput quoteAddRoleRequestInput) {
        this.quoteAddRoleRequestInput = quoteAddRoleRequestInput;
    }

    public void setQuoteDeleteRoleRequestInput(QuoteDeleteRoleRequestInput quoteDeleteRoleRequestInput) {
        this.quoteDeleteRoleRequestInput = quoteDeleteRoleRequestInput;
    }

    public void setQuoteUpdateRoleRequestInput(QuoteUpdateRoleRequestInput quoteUpdateRoleRequestInput) {
        this.quoteUpdateRoleRequestInput = quoteUpdateRoleRequestInput;
    }

    public QuoteAddRoleRequestInput getQuoteAddRoleRequestInput() {
        return quoteAddRoleRequestInput;
    }

    public QuoteDeleteRoleRequestInput getQuoteDeleteRoleRequestInput() {
        return quoteDeleteRoleRequestInput;
    }

    public QuoteUpdateRoleRequestInput getQuoteUpdateRoleRequestInput() {
        return quoteUpdateRoleRequestInput;
    }

    public QuoteAddRoleResponseOutput getQuoteAddRoleResponseOutput() {
        return quoteAddRoleResponseOutput;
    }

    public QuoteDeleteRoleResponseOutput getQuoteDeleteRoleResponseOutput() {
        return quoteDeleteRoleResponseOutput;
    }

    public QuoteUpdateRoleResponseOutput getQuoteUpdateRoleResponseOutput() {
        return quoteUpdateRoleResponseOutput;
    }

    public void sendRequest() throws Exception {
        scenarioOperator = scenarioTester.getScenarioOperator();
        String function = scenarioOperator.getFunction();

        if (function.equalsIgnoreCase("Quote Add Role")) {
            quoteAddRole();
        } else if (function.equalsIgnoreCase("Quote Update Role")) {
            quoteUpdateRole();
        } else if (function.equalsIgnoreCase("Quote Delete Role")) {
            quoteDeleteRole();
        }
    }

    public void performValidations() throws Exception {
        scenarioOperator = scenarioTester.getScenarioOperator();
        try {
            if (scenarioOperator != null) {
                String function = scenarioOperator.getFunction();
                if (function.equalsIgnoreCase("Quote Add Role")) {
                    quoteAddRoleValidations();
                } else if (function.equalsIgnoreCase("Quote Update Role")) {
                    quoteUpdateRoleValidations();
                } else if (function.equalsIgnoreCase("Quote Delete Role")) {
                    quoteDeleteRoleValidations();
                } else {
                    log.error("No Matching ScenarioOperator Function For Validations.");
                    throw new Exception("No Matching ScenarioOperator Function For Validations.");
                }
            }  else {
                log.error("No ScenarioOperator Set For Validations.");
                throw new Exception("No ScenarioOperator Set For Validations.");
            }
        } catch (Exception e) {
            String exceptionMessage = e.getMessage();
            handleValidationException(e, exceptionMessage);
        }
    }

    public GetPolicyDetailResponseOutput getPolicyDetails(String policyNumber, String username) throws ServiceException {
        if (getPolicyDetailRequestInput == null) {
            getPolicyDetailRequestInput = new GetPolicyDetailRequestInput();
            getPolicyDetailRequestInput.setPolicy(policyNumber);
            getPolicyDetailRequestInput.setUsername(username);
        }
        GetPolicyDetailsTester getPolicyDetailsTester = new GetPolicyDetailsTester(getPolicyDetailRequestInput);

        StringBuilder getPolicyDetailsErrorStringBuilder = new StringBuilder();
        try {
            GetPolicyDetailResponseOutput getPolicyDetailResponseOutput = getPolicyDetailsTester.sendGetPolicyDetailsRequest();
            return getPolicyDetailResponseOutput;
        } catch (Exception e) {
            responseErrors = getPolicyDetailsTester.getResponseErrors();
            Errors errors = responseErrors.getErrors();
            List<Error> errorList = errors.getError();
            for (Error error : errorList) {
                getPolicyDetailsErrorStringBuilder.append("Code: ").append(error.getCode())
                        .append(", Subject: ").append(error.getSubject())
                        .append(", Description: ").append(error.getDescription())
                        .append(", Adjunct: ").append(error.getAdjunct())
                        .append("\n");
            }
            throw new ServiceException(getPolicyDetailsErrorStringBuilder.toString());
        }
    }

    private ResponsePolicyRoleType getTargetRole(String policyNumber, String roleId) throws Exception {
        ResponsePolicyRoleType targetRole = null;

        String targetRelationship = scenarioTester.getCellValue("Target Relationship");
        String targetName = scenarioTester.getCellValue("Target Name");
        String targetBirthDate = scenarioTester.getCellValue("Target Birth Date");
        String targetGenderCode = scenarioTester.getCellValue("Target Gender Code");

        getPolicyDetailResponseOutput = getPolicyDetails(policyNumber, username);

        List<ResponsePolicyRoleType> policyRoleList = null;
        if (getPolicyDetailResponseOutput != null) {
            ResponsePolicy policy = getPolicyDetailResponseOutput.getPolicy();
            ResponseRoles policyRoles = policy.getRoles();
            policyRoleList = policyRoles.getRole();
        }

        Multimap<String, ResponsePolicyRoleType> matchingTargetTypeRoles = ArrayListMultimap.create();
        if (policyRoleList != null) {
            for (ResponsePolicyRoleType roleBefore : policyRoleList) {
                if (roleBefore.getRoletype().equalsIgnoreCase(targetRelationship)
                        && roleBefore.getStatus().equalsIgnoreCase("ACTIVE")
                ) {
                    String currentRoleId = roleBefore.getRoleid();
                    matchingTargetTypeRoles.put(currentRoleId, roleBefore);
                }
            }
        }

        if (!matchingTargetTypeRoles.isEmpty()) {
            if (!roleId.isEmpty()) {
                Collection<ResponsePolicyRoleType> matchingRoles = matchingTargetTypeRoles.get(roleId);
                if (!matchingRoles.isEmpty()) {
                    /**
                     * This is under the assumption that only one roleId will match.
                     */
                    for (ResponsePolicyRoleType role: matchingRoles) {
                        targetRole = role;
                    }
                } else {
                    // TODO: What To Do When There Are No Matching roleId.
                }
            } else {
                /**
                 * Get The Policy With Matching Target Values from {@code matchingTargetTypeRoles}
                 */
                Collection<ResponsePolicyRoleType> allTargetTypeMatchingRoles = matchingTargetTypeRoles.values();
                for (ResponsePolicyRoleType role: allTargetTypeMatchingRoles) {
                    // TODO: Compare With Target Values And Get The First Matching One And Break Out.
                    String roleFullName = role.getFullname();
                    String roleBirthDate = role.getBirthdate();
                    String roleGenderCode = role.getGendercode();

                    if (roleFullName.equalsIgnoreCase(targetName)
                            && roleBirthDate.equalsIgnoreCase(targetBirthDate)
                            && roleGenderCode.equalsIgnoreCase(targetGenderCode)) {
                        targetRole = role;
                        break;
                    }
                }
            }
        } else {
            // TODO: What To Do When There Are No Matching Role Types.
        }
        return targetRole;
    }

    private void quoteAddRole() throws Exception {
        if (quoteAddRoleRequestInput == null) {
            username = scenarioTester.getCellValue("Username");
            String policyNumber = scenarioOperator.getPolicyNumber();

            String roleId = scenarioTester.getCellValue("Role Id");
            String action = scenarioTester.getCellValue("Action");
            String roleType = scenarioTester.getCellValue("Role Type");
            String fullName = scenarioTester.getCellValue("Full Name");
            String middleName = scenarioTester.getCellValue("Middle Name");
            String idNumber = scenarioTester.getCellValue("Id Number");
            String birthDate = scenarioTester.getCellValue("Birth Date");
            String genderCode = scenarioTester.getCellValue("Gender Code");
            String coverAmount = scenarioTester.getCellValue("Cover Amount");
            String email = scenarioTester.getCellValue("Email");
            String contactNumber = scenarioTester.getCellValue("Contact Number");

            /** Get Policy Before Proceeding To Adding A Role, To Make It Easier To Identify Added Role */
            getPolicyDetailResponseOutput = getPolicyDetails(policyNumber, username);

            quoteAddRoleRequestInput = new QuoteAddRoleRequestInput();
            quoteAddRoleRequestInput.setUsername(username);
            quoteAddRoleRequestInput.setPolicy(policyNumber);

            List<QuoteAddRole> roles = quoteAddRoleRequestInput.getRoles();

            QuoteAddRole quoteAddRole = new QuoteAddRole();
            QuoteAddRoleType quoteAddRoleType = new QuoteAddRoleType();
            quoteAddRoleType.setRoleid(roleId);
            quoteAddRoleType.setAction(action);
            quoteAddRoleType.setRoletype(roleType);
            quoteAddRoleType.setFullname(fullName);
            quoteAddRoleType.setMiddlename(middleName);
            quoteAddRoleType.setIdnumber(idNumber);
            quoteAddRoleType.setBirthdate(birthDate);
            quoteAddRoleType.setGendercode(genderCode);
            quoteAddRoleType.setCoveramnt(coverAmount);
            quoteAddRoleType.setEmail(email);
            quoteAddRoleType.setContactno(contactNumber);
            quoteAddRole.setRole(quoteAddRoleType);

            roles.add(quoteAddRole);
        }

        QuoteTester quoteTester = new QuoteTester(quoteAddRoleRequestInput);
        quoteAddRoleResponseOutput = quoteTester.sendQuoteAddRoleRequest();
    }

    private void quoteUpdateRole() throws Exception {
        if (quoteUpdateRoleRequestInput == null) {
            username = scenarioTester.getCellValue("Username");
            String policyNumber = scenarioOperator.getPolicyNumber();

            String roleId = scenarioTester.getCellValue("Role Id");
            String action = scenarioTester.getCellValue("Action");
            String roleType = scenarioTester.getCellValue("Role Type");
            String fullName = scenarioTester.getCellValue("Full Name");
            String middleName = scenarioTester.getCellValue("Middle Name");
            String idNumber = scenarioTester.getCellValue("Id Number");
            String birthDate = scenarioTester.getCellValue("Birth Date");
            String genderCode = scenarioTester.getCellValue("Gender Code");
            String coverAmount = scenarioTester.getCellValue("Cover Amount");
            String income = scenarioTester.getCellValue("Income");

            targetRole = getTargetRole(policyNumber, roleId);

            if (targetRole != null) {
                String targetRoleRoleId = targetRole.getRoleid();
                // TODO: Get Existing Role Values / Current Role Values.
                quoteUpdateRoleRequestInput = new QuoteUpdateRoleRequestInput();

                quoteUpdateRoleRequestInput.setUsername(username);
                quoteUpdateRoleRequestInput.setPolicy(policyNumber);

                QuoteUpdateRole quoteUpdateRole = new QuoteUpdateRole();
                quoteUpdateRole.setRoleid(targetRoleRoleId);
                quoteUpdateRole.setAction(action);
                quoteUpdateRole.setRoletype(roleType);
                quoteUpdateRole.setFullname(fullName);
                quoteUpdateRole.setMiddlename(middleName);
                quoteUpdateRole.setIdnumber(idNumber);
                quoteUpdateRole.setBirthdate(birthDate);
                quoteUpdateRole.setGender(genderCode);
                quoteUpdateRole.setCoveramnt(coverAmount);
                quoteUpdateRole.setIncome(income);
                quoteUpdateRoleRequestInput.setRole(quoteUpdateRole);
            } else {
                log.error("No Target Role To Quote Update On.");
                throw new Exception("No Target Role To Quote Update On.");
            }
        }
        QuoteTester quoteTester = new QuoteTester(quoteUpdateRoleRequestInput);
        quoteUpdateRoleResponseOutput = quoteTester.sendQuoteUpdateRoleRequest();
    }

    private void quoteDeleteRole() throws Exception {
        // TODO: Implement Quote Delete Role Logic Here
        if (quoteDeleteRoleRequestInput == null) {

            username = scenarioTester.getCellValue("Username");
            String policyNumber = scenarioOperator.getPolicyNumber();

            String roleId = scenarioTester.getCellValue("Role Id");

            ResponsePolicyRoleType targetRole = null;
            if (roleId.isEmpty()) {
                targetRole = getTargetRole(policyNumber, roleId);
            }

            String targetRoleRoleId = null;
            if (targetRole != null) {
                targetRoleRoleId = targetRole.getRoleid();
            }

            if (targetRoleRoleId == null) {
                targetRoleRoleId = roleId;
            }

            quoteDeleteRoleRequestInput = new QuoteDeleteRoleRequestInput();

            quoteDeleteRoleRequestInput.setUsername(username);
            quoteDeleteRoleRequestInput.setPolicy(policyNumber);
            quoteDeleteRoleRequestInput.setRoleid(targetRoleRoleId); // TODO: Get The Actual Existing Role Id If Empty
        }

        QuoteTester quoteTester = new QuoteTester(quoteDeleteRoleRequestInput);
        quoteDeleteRoleResponseOutput = quoteTester.sendQuoteDeleteRoleRequest();
    }

    public void quoteAddRoleValidations() throws Exception {
        try {
            String expectedCoverAmount = scenarioTester.getCellValue("Expected Cover Amount");
            String expectedPremium = scenarioTester.getCellValue("Expected Premium");
            String expectedRoleCoverAmount = scenarioTester.getCellValue("Expected Role Cover Amount");
            String expectedRolePremium = scenarioTester.getCellValue("Expected Role Premium");
            String checkExpectedCoverCount = scenarioTester.getCellValue("Check Expected Cover Count");
            String expectedCoverCount = scenarioTester.getCellValue("Expected Cover Count");

            if (quoteAddRoleResponseOutput != null) {
                List<QuoteAddRoleRole> roles = quoteAddRoleResponseOutput.getRole();
                QuoteAddRoleResponsePolicy policy = quoteAddRoleResponseOutput.getPolicy();
                String coverAmount = policy.getCoveramount();
                String premiumAmount = policy.getPremium();

                List<String> roleIdsBefore = new ArrayList<>();
                if (getPolicyDetailResponseOutput != null) {
                    ResponsePolicy policyBefore = getPolicyDetailResponseOutput.getPolicy();
                    ResponseRoles policyBeforeRoles = policyBefore.getRoles();
                    List<ResponsePolicyRoleType> policyBeforeRolesList = policyBeforeRoles.getRole();

                    for (ResponsePolicyRoleType role : policyBeforeRolesList) {
                        String roleId = role.getRoleid();
                        roleIdsBefore.add(roleId);
                    }
                }

                QuoteAddRoleRole targetRole = null;
                for (QuoteAddRoleRole role: roles) {
                    String roleId = role.getRoleid();
                    if (!roleIdsBefore.contains(roleId)) {
                        targetRole = role;
                        break;
                    }
                }

                String targetRoleCoverAmnt = "00";
                String targetRolePremium = "00";
                List<Cover> covers = null;
                if (targetRole != null) {
                    targetRoleCoverAmnt = targetRole.getCoveramnt();
                    targetRolePremium = targetRole.getPremium();
                    CoverDetail coverDetail = targetRole.getCoverdetail();
                    covers = coverDetail.getCover();
                } else {
                    String exceptionMessage = "No Quote Target Added Role To Validate.";
                    handleValidationException(new ValidationException(exceptionMessage), exceptionMessage);
                }

                if (!expectedCoverAmount.isEmpty()) {
                    Double expectedCoverAmountValue = Double.valueOf(expectedCoverAmount);
                    Double coverAmountValue = Double.valueOf(coverAmount);

                    if (!coverAmountValue.equals(expectedCoverAmountValue)) {
                        log.error("Expected Cover Amount Not Correct. Expected: " + expectedCoverAmount + " -> Found: " + coverAmount);
                        throw new ValidationException("Expected Cover Amount Not Correct. Expected: " + expectedCoverAmount + " -> Found: " + coverAmount);
                    }
                }

                if (!expectedPremium.isEmpty()) {
                    Double expectedPremiumValue = Double.valueOf(expectedPremium);
                    Double premiumValue = Double.valueOf(premiumAmount);

                    if (!premiumValue.equals(expectedPremiumValue)) {
                        log.error("Expected Premium Not Correct. Expected: " + expectedPremium + " -> Found: " + premiumAmount);
                        throw new ValidationException("Expected Premium Not Correct. Expected: " + expectedPremium + " -> Found: " + premiumAmount);
                    }
                }

                if (!expectedRoleCoverAmount.isEmpty()) {
                    Double expectedRoleCoverAmountValue = Double.valueOf(expectedRoleCoverAmount);
                    Double roleCoverAmntValue = Double.valueOf(targetRoleCoverAmnt);

                    if (!roleCoverAmntValue.equals(expectedRoleCoverAmountValue)) {
                        log.error("Expected Role Cover Amount Not Correct. Expected: " + expectedRoleCoverAmount + " -> Found: " + targetRoleCoverAmnt);
                        throw new ValidationException("Expected Role Cover Amount Not Correct. Expected: " + expectedRoleCoverAmount + " -> Found: " + targetRoleCoverAmnt);
                    }
                }

                if (!expectedRolePremium.isEmpty()) {
                    Double expectedRolePremiumValue = Double.valueOf(expectedRolePremium);
                    Double rolePremiumValue = Double.valueOf(targetRolePremium);

                    if (!rolePremiumValue.equals(expectedRolePremiumValue)) {
                        log.error("Expected Role Premium Not Correct. Expected: " + expectedRolePremium + " -> Found: " + targetRolePremium);
                        throw new ValidationException("Expected Role Premium Not Correct. Expected: " + expectedRolePremium + " -> Found: " + targetRolePremium);
                    }
                }

                if (checkExpectedCoverCount.equalsIgnoreCase("Yes")) {
                    Integer numberOfCovers = 0;
                    if (covers != null) {
                        numberOfCovers = covers.size();
                    }
                    Integer expectedCoverCountValue = Integer.valueOf(expectedCoverCount);
                    if (!numberOfCovers.equals(expectedCoverCountValue)) {
                        log.error("Expected Number Of Covers Not Correct. Expected: " + expectedCoverCount + " -> Found: " + numberOfCovers);
                        throw new ValidationException("Expected Number Of Covers Not Correct. Expected: " + expectedCoverCount + " -> Found: " + numberOfCovers);
                    }
                }
            } else {
                String exceptionMessage = "No Quote Add Role Response Output To Validate.";
                handleValidationException(new ValidationException(exceptionMessage), exceptionMessage);
            }
        } catch (Exception e) {
            String exceptionMessage = "Error While Validating Quote Add Role. " + e.getMessage();
            handleValidationException(e, exceptionMessage);
        }
    }

    public void quoteUpdateRoleValidations() throws Exception {
        try {
            String expectedCoverAmount = scenarioTester.getCellValue("Expected Cover Amount");
            String expectedPremium = scenarioTester.getCellValue("Expected Premium");
            String expectedRoleCoverAmount = scenarioTester.getCellValue("Expected Role Cover Amount");
            String expectedRolePremium = scenarioTester.getCellValue("Expected Role Premium");
            String checkExpectedCoverCount = scenarioTester.getCellValue("Check Expected Cover Count");
            String expectedCoverCount = scenarioTester.getCellValue("Expected Cover Count");

            if (quoteUpdateRoleResponseOutput != null) {
                QuoteUpdateResponseRole role = quoteUpdateRoleResponseOutput.getRole();
                QuoteUpdateRoleResponsePolicy policy = quoteUpdateRoleResponseOutput.getPolicy();
                String coverAmount = policy.getCoveramount();
                String premium = policy.getPremium();

                String roleCoverAmnt = role.getCoveramnt();
                String rolePremium = role.getPremium();

                if (!expectedCoverAmount.isEmpty()) {
                    Double expectedCoverAmountValue = Double.valueOf(expectedCoverAmount);
                    Double coverAmountValue = Double.valueOf(coverAmount);

                    if (!coverAmountValue.equals(expectedCoverAmountValue)) {
                        log.error("Expected Cover Amount Not Correct. Expected: " + expectedCoverAmount + " -> Found: " + coverAmount);
                        throw new ValidationException("Expected Cover Amount Not Correct. Expected: " + expectedCoverAmount + " -> Found: " + coverAmount);
                    }
                }

                if (!expectedPremium.isEmpty()) {
                    Double expectedPremiumValue = Double.valueOf(expectedPremium);
                    Double premiumValue = Double.valueOf(premium);

                    if (!premiumValue.equals(expectedPremiumValue)) {
                        log.error("Expected Premium Not Correct. Expected: " + expectedPremium + " -> Found: " + premium);
                        throw new ValidationException("Expected Premium Not Correct. Expected: " + expectedPremium + " -> Found: " + premium);
                    }
                }

                if (!expectedRoleCoverAmount.isEmpty()) {
                    Double expectedRoleCoverAmountValue = Double.valueOf(expectedRoleCoverAmount);
                    Double roleCoverAmntValue = Double.valueOf(roleCoverAmnt);

                    if (!roleCoverAmntValue.equals(expectedRoleCoverAmountValue)) {
                        log.error("Expected Role Cover Amount Not Correct. Expected: " + expectedRoleCoverAmount + " -> Found: " + roleCoverAmnt);
                        throw new ValidationException("Expected Role Cover Amount Not Correct. Expected: " + expectedRoleCoverAmount + " -> Found: " + roleCoverAmnt);
                    }
                }

                if (!expectedRolePremium.isEmpty()) {
                    Double expectedRolePremiumValue = Double.valueOf(expectedRolePremium);
                    Double rolePremiumValue = Double.valueOf(rolePremium);

                    if (!rolePremiumValue.equals(expectedRolePremiumValue)) {
                        log.error("Expected Role Premium Not Correct. Expected: " + expectedRolePremium + " -> Found: " + rolePremium);
                        throw new ValidationException("Expected Role Premium Not Correct. Expected: " + expectedRolePremium + " -> Found: " + rolePremium);
                    }
                }

                if (checkExpectedCoverCount.equalsIgnoreCase("Yes")) {
                    CoverDetail roleCoverDetail = role.getCoverdetail();
                    List<Cover> covers = roleCoverDetail.getCover();
                    Integer numberOfCovers = covers.size();

                    if (targetRole != null) {
                        String targetRoleCoverAmnt = targetRole.getCoveramnt();
                        String targetRolePremium = targetRole.getPremium();
                        ResponseCoverDetail targetRoleCoverDetail = targetRole.getCoverdetail();
                        String targetRoleCoverCount = targetRoleCoverDetail.getCount();

                        Double targetRoleCoverAmntValue = Double.valueOf(targetRoleCoverAmnt);
                        Double roleCoverAmntValue = Double.valueOf(roleCoverAmnt);

                        Double targetRolePremiumValue = Double.valueOf(targetRolePremium);
                        Double rolePremiumValue = Double.valueOf(rolePremium);

                        Integer targetRoleCoverCountValue = Integer.valueOf(targetRoleCoverCount);
                        if (targetRoleCoverAmntValue.equals(roleCoverAmntValue)
                            && targetRolePremiumValue.equals(rolePremiumValue)) {

                            if (!numberOfCovers.equals(targetRoleCoverCountValue)) {
                                log.error("Cover Amount And Premium Amount Did Not Change But Cover Count Changed. Old: " + targetRoleCoverCountValue + " -> New: " + numberOfCovers);
                                throw new ValidationException("Cover Amount And Premium Amount Did Not Change But Cover Count Changed. Old: " + targetRoleCoverCountValue + " -> New: " + numberOfCovers);
                            }
                        } else {
                            int coverCountDifference = numberOfCovers - targetRoleCoverCountValue;

                            if (coverCountDifference != 1) {
                                log.error("Cover Difference Not Equal To 1. Old: " + targetRoleCoverCountValue + " -> New: " + numberOfCovers + ". Cover Difference: " + coverCountDifference);
                                throw new ValidationException("Cover Difference Not Equal To 1. Old: " + targetRoleCoverCountValue + " -> New: " + numberOfCovers + ". Cover Difference: " + coverCountDifference);
                            }
                        }
                    }
                }
            } else {
                String exceptionMessage = "No Quote Update Role Response Output To Validate.";
                handleValidationException(new ValidationException(exceptionMessage), exceptionMessage);
            }
        } catch (Exception e) {
            String exceptionMessage = "Error While Validating Quote Update Role. " + e.getMessage();
            handleValidationException(e, exceptionMessage);
        }
    }

    public void quoteDeleteRoleValidations() throws Exception {
        try {
            String expectedCoverAmount = scenarioTester.getCellValue("Expected Cover Amount");
            String expectedPremium = scenarioTester.getCellValue("Expected Premium");
            if (quoteDeleteRoleResponseOutput != null) {
                String coverAmnt = quoteDeleteRoleResponseOutput.getCoveramnt();
                String premium = quoteDeleteRoleResponseOutput.getPremium();

                if (!expectedCoverAmount.isEmpty()) {
                    Double expectedCoverAmountValue = Double.valueOf(expectedCoverAmount);
                    Double coverAmntValue = Double.valueOf(coverAmnt);

                    if (!coverAmntValue.equals(expectedCoverAmountValue)) {
                        log.error("Expected Cover Amount Not Correct. Expected: " + expectedCoverAmount + " -> Found: " + coverAmnt);
                        throw new ValidationException("Expected Cover Amount Not Correct. Expected: " + expectedCoverAmount + " -> Found: " + coverAmnt);
                    }
                }

                if (!expectedPremium.isEmpty()) {
                    Double expectedPremiumValue = Double.valueOf(expectedPremium);
                    Double premiumValue = Double.valueOf(premium);

                    if (!premiumValue.equals(expectedPremiumValue)) {
                        log.error("Expected Premium Not Correct. Expected: " + expectedPremium + " -> Found: " + premium);
                        throw new ValidationException("Expected Premium Not Correct. Expected: " + expectedPremium + " -> Found: " + premium);
                    }
                }
            } else {
                String exceptionMessage = "No Quote Delete Role Response Output To Validate.";
                handleValidationException(new ValidationException(exceptionMessage), exceptionMessage);
            }
        } catch (Exception e) {
            String exceptionMessage = "Error While Validating Quote Delete Role. " + e.getMessage();
            handleValidationException(e, exceptionMessage);
        }
    }

    private void handleValidationException(Exception e, String exceptionMessage) throws Exception {
        log.error(exceptionMessage);
        if (e instanceof ValidationException) {
            throw new ValidationException(exceptionMessage);
        } else {
            throw new Exception(exceptionMessage);
        }
    }

}
