package co.za.fnb.flow.models.custom_exceptions;

public class ValidationException extends Exception {
    public ValidationException(String validationError) {
        super(validationError);
    }
}
