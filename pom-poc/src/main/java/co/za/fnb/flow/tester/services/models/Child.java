package co.za.fnb.flow.tester.services.models;

public class Child {
    private String relationship;
    private String name;
    private String middleName;
    private String birthDate;
    private String idType;
    private String idNumber;
    private String gender;
    private String coverAmount;
    private String premium;

    public Child() {
    }

    public Child(String relationship, String name, String middleName, String birthDate, String idType, String idNumber, String gender, String coverAmount, String premium) {
        this.relationship = relationship;
        this.name = name;
        this.middleName = middleName;
        this.birthDate = birthDate;
        this.idType = idType;
        this.idNumber = idNumber;
        this.gender = gender;
        this.coverAmount = coverAmount;
        this.premium = premium;
    }

    public String getRelationship() {
        return relationship;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getIdType() {
        return idType;
    }

    public void setIdType(String idType) {
        this.idType = idType;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCoverAmount() {
        return coverAmount;
    }

    public void setCoverAmount(String coverAmount) {
        this.coverAmount = coverAmount;
    }

    public String getPremium() {
        return premium;
    }

    public void setPremium(String premium) {
        this.premium = premium;
    }
}
