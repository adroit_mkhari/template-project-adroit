package co.za.fnb.flow.tester;

import co.za.fnb.flow.handlers.database.QueryHandler;
import co.za.fnb.flow.models.Query;
import co.za.fnb.flow.models.QueryFactory;
import co.za.fnb.flow.models.custom_exceptions.FicaStatusException;
import co.za.fnb.flow.models.custom_exceptions.NegativeTestException;
import co.za.fnb.flow.models.custom_exceptions.ServicePageException;
import co.za.fnb.flow.models.policy_details.*;
import co.za.fnb.flow.models.work_items.FlowWorkItem;
import co.za.fnb.flow.pages.*;
import co.za.fnb.flow.pages.administration.LegalAdvisorAdminPage;
import co.za.fnb.flow.pages.audit_trail.AuditTrail;
import co.za.fnb.flow.pages.claims.Claims;
import co.za.fnb.flow.pages.generic_maintanence.GenericMaintenance;
import co.za.fnb.flow.pages.policy_details.PolicyDetails;
import co.za.fnb.flow.pages.principal_member_details.PrincipalMemberDetails;
import co.za.fnb.flow.pages.work_items.WorkItems;
import co.za.fnb.flow.setup.TestResultReportFlag;
import co.za.fnb.flow.tester.policy_details.*;
import co.za.fnb.flow.tester.work_items.WorkItem;
import cucumber.api.DataTable;
import gherkin.formatter.model.Row;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.DateUtil;
import org.openqa.selenium.WebDriver;

import java.io.IOException;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class WorkItemsRunner {
    private Logger log  = LogManager.getLogger(WorkItemsRunner.class);
    QueryHandler queryHandler = new QueryHandler();
    WebDriver driver;
    DataTable dataTable;
    HomePage homePage;
    PolicyDetails policyDetails;
    PrincipalMemberDetails principalMemberDetails;
    Claims claims;
    GenericMaintenance genericMaintenance;
    CreateSearchItemPage createSearchItemPage;
    LegalAdvisorAdminPage legalAdvisorAdminPage;
    CreateMultipleMintItemTable createMultipleMintItemtable;
    CreateOrUpdateWorkItem createOrUpdateWorkItem;
    WorkItems workItems;
    AuditTrail auditTrail;

    public WorkItemsRunner() {
    }

    public WorkItemsRunner(WebDriver driver) {
        this.driver = driver;
    }

    public WorkItemsRunner(DataTable dataTable) {
        this.dataTable = dataTable;
    }

    public WorkItemsRunner(WebDriver driver, DataTable dataTable) {
        this.dataTable = dataTable;
        this.driver = driver;
    }

    public WebDriver getDriver() {
        return driver;
    }

    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }

    public PolicyDetails getPolicyDetails() {
        return policyDetails;
    }

    public void setPolicyDetails(PolicyDetails policyDetails) {
        this.policyDetails = policyDetails;
    }

    public int getHeaderIndex(Object[] headers, String header) {
        return Arrays.asList(headers).indexOf(header);
    }

    private String getCellValue(Object[] headers, List<String> dataEntry, String field) {
        return getHeaderIndex(headers, field) != -1 ? dataEntry.get(getHeaderIndex(headers, field)) : "";
    }

    private void updateFicaStatus(ScenarioOperator scenarioOperator, String dbPolicyNumber, String risk, String sanction, String edd, String kyc, String ficaStatus, String updatedTe) throws Exception {
        if (!scenarioOperator.getProductName().toUpperCase().contains("LOC")) {
            queryHandler.updateFicaStatus(dbPolicyNumber,
                    risk,
                    sanction,
                    edd,
                    kyc,
                    ficaStatus,
                    updatedTe);

            ResultSet ficaUpdatedPolicy = queryHandler.queryForPolicyNumberOnFicaTable(dbPolicyNumber);
            // TODO: Handle errors thrown here.
            if (ficaUpdatedPolicy.next()) {
                String policy = ficaUpdatedPolicy.getString("POLICYNO").trim();
                String status = ficaUpdatedPolicy.getString("STATUS").trim();
                // responseTrace.append("FICA ERROR: " + status + "\n");
                log.debug("POLICYNO: " + policy + " STATUS: " + status);
                if (status.equalsIgnoreCase("FICA ERROR")) {
                    throw new FicaStatusException("FICA ERROR status on policy Number: " + policy);
                }
            } else {
                throw new FicaStatusException("FICA ERROR: SQL Failed to retrieve policy with the policy number: " + scenarioOperator.getPolicyNumber());
            }
        }
    }

    public void run() throws IOException, InterruptedException {
        log.info("Starting ScenarioOperator Tests.");
        String[] reportableFields = {"Test Case Number", "Scenario Description", "Function", "Product Name", "Policy Number", "Result", "Failure Reason"};
        ScenarioOperator scenarioOperator = new ScenarioOperator(reportableFields);
        scenarioOperator.createReport();
        log.info("Reportable Fields: " + Arrays.asList(reportableFields));
        log.info("Report Path: " + scenarioOperator.getReportPath());

        Object[] headers = dataTable.getGherkinRows().get(0).getCells().toArray();
        List<String> dataEntry;
        String run, testFunction, scenarioDescription, testCaseNumber, productName, policyNumberQuery, policyCode, policyNumber;

        if (Arrays.asList(headers).contains("Test Case No")) {
            for (Row row : dataTable.getGherkinRows()) {
                if (row.getLine() != 1) {
                    dataEntry = row.getCells();
                    run = getCellValue(headers, dataEntry,"Run");
                    if (run.equalsIgnoreCase("YES")) {
                        testCaseNumber = getCellValue(headers, dataEntry,"Test Case No");
                        scenarioDescription = getCellValue(headers, dataEntry,"Scenario Description");
                        testFunction = getCellValue(headers, dataEntry,"Function");
                        productName = getCellValue(headers, dataEntry,"Product Name");
                        scenarioOperator.setTestCaseNumber(testCaseNumber);
                        scenarioOperator.setScenarioDescription(scenarioDescription);
                        scenarioOperator.setFunction(testFunction);
                        scenarioOperator.setProductName(productName);
                        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Test Case Number"), scenarioOperator.getTestCaseNumber(), TestResultReportFlag.DEFAULT);
                        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Scenario Description"), scenarioOperator.getScenarioDescription(), TestResultReportFlag.WARNING);
                        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Function"), scenarioOperator.getFunction(), TestResultReportFlag.WARNING);
                        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Product Name"), scenarioOperator.getProductName(), TestResultReportFlag.WARNING);

                        scenarioOperator.setScreenShotFileName(scenarioOperator.getFunction() + " " + scenarioOperator.getTestCaseNumber() + " " + scenarioOperator.getScenarioDescription());
                        // BasePage.setScenarioOperator(scenarioOperator);

                        try {
                            policyNumber = getCellValue(headers, dataEntry,"Policy Number");
                            policyNumberQuery = getCellValue(headers, dataEntry,"Policy Number Query");
                            policyCode = getCellValue(headers, dataEntry,"Policy No");
                            if (policyNumber.isEmpty()) {
                                try {
                                    if (policyNumberQuery.isEmpty()) {
                                        if (policyCode.isEmpty()) {
                                            policyNumber = "No Policy Number";
                                            scenarioOperator.setPolicyNumber(policyNumber);
                                        } else {
                                            Query query = QueryFactory.getQuery(policyCode);
                                            ResultSet policyNumberQueryResults = queryHandler.getPolicyNumberQueryResults(query.getQuery());
                                            if (policyNumberQueryResults.next()) {
                                                policyNumber = policyNumberQueryResults.getString("POLICYNO").trim();
                                            } else {
                                                policyCode = query.getPolicyCode();
                                                policyNumberQuery = query.getQuery();
                                                policyNumber = null;
                                            }

                                            if (policyNumber == null) {
                                                throw new Exception("Error getting policy number for policy code: " + policyCode);
                                            }
                                            queryHandler.setDbPolicyNumber(policyNumber);
                                            policyNumber = getFlowPolicyNumberFormat(productName, policyNumber);
                                            scenarioOperator.setPolicyNumber(policyNumber);
                                        }
                                    } else {
                                        ResultSet policyNumberQueryResults = queryHandler.getPolicyNumberQueryResults(policyNumberQuery);
                                        if (policyNumberQueryResults.next()) {
                                            policyNumber = policyNumberQueryResults.getString("POLICYNO").trim();
                                            queryHandler.setDbPolicyNumber(policyNumber);
                                            policyNumber = getFlowPolicyNumberFormat(productName, policyNumber);
                                            scenarioOperator.setPolicyNumber(policyNumber);
                                        }
                                    }
                                } catch (Exception e) {
                                    throw new Exception("Error getting policy number. Policy Code: " + policyCode + " Query: " + policyNumberQuery);
                                }
                            } else {
                                scenarioOperator.setPolicyNumber(policyNumber);
                                queryHandler.setDbPolicyNumber(policyNumber);
                            }

                            log.info("===========================================================================");
                            log.info("Running Test Case Number: " + testCaseNumber + ", Function: " + testFunction);
                            log.info("---------------------------------------------------------------------------");

                            if (testFunction.equalsIgnoreCase("Search Policy")) {
                                searchPolicy(reportableFields, scenarioOperator, headers, dataEntry);
                                scenarioOperator.increamentReportRowIndex();
                            } else if (testFunction.equalsIgnoreCase("Create")
                                       || testFunction.equalsIgnoreCase("Update")
                                       || testFunction.equalsIgnoreCase("Clone")) {
                                workItem(reportableFields, scenarioOperator, headers, dataEntry);
                                scenarioOperator.increamentReportRowIndex();
                            }
                            log.info("End Of Test Case Number: " + testCaseNumber + ", Function: " + testFunction);
                        } catch (Exception e) {
                            log.error(e.getMessage());
                            e.printStackTrace();
                            reportFailure(reportableFields, scenarioOperator, e.getMessage());
                            scenarioOperator.increamentReportRowIndex();
                        } finally {
                            if (driver.getWindowHandles().toArray().length > 1) {
                                driver.close();
                            }
                        }
                    }
                }
            }
        }

        log.info("Saving Report.");
        scenarioOperator.saveReport();
        log.info("===========================================================================");
    }

    private void searchPolicy(String[] reportableFields, ScenarioOperator scenarioOperator, Object[] headers, List<String> dataEntry) throws Exception {
        log.info("Running Search Policy Logic");
        // TODO: Fix Date Of Birth Format
        int testCaseNumber = getIntegerValue(getCellValue(headers, dataEntry, "Test Case No")) != -1 ? getIntegerValue(getCellValue(headers, dataEntry, "Test Case No")) : 0;
        int debitOrderDate = getIntegerValue(getCellValue(headers, dataEntry, "Debit Order Date")) != -1 ? getIntegerValue(getCellValue(headers, dataEntry, "Debit Order Date")) : 1;
        int numberOfMembers = getIntegerValue(getCellValue(headers, dataEntry, "NoD")) != -1 ? getIntegerValue(getCellValue(headers, dataEntry, "NoD")) : 1;
        Member member = new Member(
                // region Add Member Model Fields
                String.valueOf(testCaseNumber),
                getCellValue(headers, dataEntry,"Product Name"),
                getCellValue(headers, dataEntry,"Scenario Description"),
                getCellValue(headers, dataEntry,"Policy No"),
                scenarioOperator.getPolicyNumber(),
                getCellValue(headers, dataEntry,"Function"),
                getCellValue(headers, dataEntry,"Run"),
                getCellValue(headers, dataEntry,"First Popup"),
                getCellValue(headers, dataEntry,"Second Popup"),
                String.valueOf(debitOrderDate),
                getDateStringWithFormat(getCellValue(headers, dataEntry,"Next Due Date"), "dd-MM-yyyy"),
                getCellValue(headers, dataEntry,"Bank Name"),
                getCellValue(headers, dataEntry,"Expected Result"),
                getCellValue(headers, dataEntry,"Work Type0"),
                getCellValue(headers, dataEntry,"Status0"),
                getCellValue(headers, dataEntry,"Queue0"),
                getCellValue(headers, dataEntry,"Work Type1"),
                getCellValue(headers, dataEntry,"Status1"),
                getCellValue(headers, dataEntry,"Queue1"),
                getCellValue(headers, dataEntry,"POP UP Work Type0"),
                getCellValue(headers, dataEntry,"POP UP Status0"),
                getCellValue(headers, dataEntry,"POP UP Queue0"),
                getCellValue(headers, dataEntry,"POP UP Work Type1"),
                getCellValue(headers, dataEntry,"POP UP Status1"),
                getCellValue(headers, dataEntry,"POP UP Queue1"),
                getCellValue(headers, dataEntry,"Audit Trail Events"),
                getCellValue(headers, dataEntry,"Updatedte"),
                getCellValue(headers, dataEntry,"Risk"),
                getCellValue(headers, dataEntry,"Sanction"),
                getCellValue(headers, dataEntry,"Edd"),
                getCellValue(headers, dataEntry,"Kyc"),
                getCellValue(headers, dataEntry,"Status"),
                getCellValue(headers, dataEntry,"Relationship"),
                getCellValue(headers, dataEntry,"Company Name / Full Name"),
                getCellValue(headers, dataEntry,"Trading as Name/ Middle Name"),
                getCellValue(headers, dataEntry,"Company Reg Number / ID Number"),
                getCellValue(headers, dataEntry,"DOB"),
                getCellValue(headers, dataEntry,"Gender"),
                getCellValue(headers, dataEntry,"Cover Amount"),
                getCellValue(headers, dataEntry,"Premium Amount"),
                getCellValue(headers, dataEntry,"Discount"),
                getCellValue(headers, dataEntry,"Email Address"),
                getCellValue(headers, dataEntry,"Cell Phone Number"),
                getCellValue(headers, dataEntry,"Bank Name"),
                String.valueOf(numberOfMembers),
                getCellValue(headers, dataEntry,"Beneficiary Name"),
                getCellValue(headers, dataEntry,"Beneficiary DOB"),
                getCellValue(headers, dataEntry,"Beneficiary ID Number"),
                getCellValue(headers, dataEntry,"Beneficiary Contact Number"),
                getCellValue(headers, dataEntry,"Beneficiary Email Address"),
                getCellValue(headers, dataEntry, "Family Bundle").equalsIgnoreCase("Yes")
                // endregion
        );

        homePage = new HomePage(driver, scenarioOperator);
        createSearchItemPage = homePage.selectPersonalQueueCreateOrSearchItem();

        scenarioOperator.setPolicyNumber(member.getPolicyNumber());
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Policy Number"), scenarioOperator.getPolicyNumber(), TestResultReportFlag.DEFAULT);

        VerifyClientDialog verifyClientDialog = new VerifyClientDialog(driver, scenarioOperator, member.getFirstPopup(), member.getSecondPopup());
        AddMember addMember = new AddMember(driver, member, scenarioOperator);

        TestHandle testHandle;
        ErrorHandle errorHandle;

        try {
            createSearchItemPage.searchPolicyNumber(scenarioOperator.getPolicyNumber(), true);
            Thread.sleep(3000);
            String policyHolder = createSearchItemPage.getPolicyHolder();

            if (!policyHolder.isEmpty()) {
                reportSuccess(reportableFields, scenarioOperator);
            } else {
                reportFailure(reportableFields, scenarioOperator, "Policy Not Found.");
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            if (e instanceof FicaStatusException) {
                reportFailure(reportableFields, scenarioOperator, e.getMessage());
            } else if (e instanceof ServicePageException) {
                reportFailure(reportableFields, scenarioOperator, e.getMessage());
            } else if (e instanceof NegativeTestException) {
                reportSuccess(reportableFields, scenarioOperator, e.getMessage());
            } else {
                reportFailure(reportableFields, scenarioOperator, e.getMessage());
            }
        }
        log.info("End Of Search Policy Logic");
    }

    private void workItem(String[] reportableFields, ScenarioOperator scenarioOperator, Object[] headers, List<String> dataEntry) throws Exception {
        log.info("Running Add Member Logic");
        // TODO: Fix Date Of Birth Format
        int testCaseNumber = getIntegerValue(getCellValue(headers, dataEntry, "Test Case No")) != -1 ? getIntegerValue(getCellValue(headers, dataEntry, "Test Case No")) : 0;
        FlowWorkItem flowWorkItem = new FlowWorkItem(
                // region Flow Work Item Model Fields
                String.valueOf(testCaseNumber),
                getCellValue(headers, dataEntry,"Product Name"),
                getCellValue(headers, dataEntry,"Scenario Description"),
                getCellValue(headers, dataEntry,"Policy No"),
                scenarioOperator.getPolicyNumber(),
                getCellValue(headers, dataEntry,"Function"),
                getCellValue(headers, dataEntry,"Run"),
                getCellValue(headers, dataEntry,"Expected Result"),
                getCellValue(headers, dataEntry,"Audit Trail Events"),
                getCellValue(headers, dataEntry,"Updatedte"),
                getCellValue(headers, dataEntry,"Risk"),
                getCellValue(headers, dataEntry,"Sanction"),
                getCellValue(headers, dataEntry,"Edd"),
                getCellValue(headers, dataEntry,"Kyc"),
                getCellValue(headers, dataEntry,"Status"),
                getCellValue(headers, dataEntry,"Reference Number"),
                getCellValue(headers, dataEntry,"Work Type"),
                getCellValue(headers, dataEntry,"Work Item Status"),
                getCellValue(headers, dataEntry,"Queue"),
                getCellValue(headers, dataEntry,"Search Status"),
                getCellValue(headers, dataEntry,"Record Dissatisfaction"),
                getCellValue(headers, dataEntry,"Voice Log Ref"),
                getCellValue(headers, dataEntry,"Created Date"),
                getCellValue(headers, dataEntry,"Locked By"),
                getCellValue(headers, dataEntry,"Assign To Me"),
                getCellValue(headers, dataEntry,"Load External Work Types"),
                getCellValue(headers, dataEntry,"Call From Transaction?"),
                getCellValue(headers, dataEntry,"Number Of Clones"),
                getCellValue(headers, dataEntry,"Work Item Queue"),
                getCellValue(headers, dataEntry,"Comment"),
                getCellValue(headers, dataEntry,"Await Doc Duration Per Day"),
                getCellValue(headers, dataEntry,"Await Doc Comments"),
                getCellValue(headers, dataEntry,"Await Doc Reason"),
                getCellValue(headers, dataEntry,"Await Doc Submit"),
                getCellValue(headers, dataEntry,"Sms View To Address"),
                getCellValue(headers, dataEntry,"Sms View Template")
                // endregion
        );

        homePage = new HomePage(driver, scenarioOperator);
        createSearchItemPage = homePage.selectPersonalQueueCreateOrSearchItem();

        scenarioOperator.setPolicyNumber(flowWorkItem.getPolicyNumber());
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Policy Number"), scenarioOperator.getPolicyNumber(), TestResultReportFlag.DEFAULT);

        WorkItem workItem = new WorkItem(driver, flowWorkItem, scenarioOperator);

        TestHandle testHandle;
        ErrorHandle errorHandle;

        try {
            createSearchItemPage.searchPolicyNumber(scenarioOperator.getPolicyNumber(), true);

            String dbPolicyNumber = queryHandler.getDbPolicyNumber();
            if (dbPolicyNumber != null) {
                // region Update FICA Status
                // updateFicaStatus(scenarioOperator, dbPolicyNumber, flowWorkItem.getRisk(), flowWorkItem.getSanction(), flowWorkItem.getEdd(), flowWorkItem.getKyc(), flowWorkItem.getStatus(), flowWorkItem.getUpdatedTe());
                // endregion
            }

            workItem.setExpectedResults();
            log.info("Work Item, Policy Number: " + scenarioOperator.getPolicyNumber());

            String function = flowWorkItem.getFunction();
            if (function.equalsIgnoreCase("Create")) {
                workItem.create();
            } else if (function.equalsIgnoreCase("Clone")) {
                workItem.cloneWorkItem();
            } else if (function.equalsIgnoreCase("Update")) {
                workItem.update();
            }

            log.info("Saving Work Item(s).");
            workItem.save();

            // It's at this point that you would maybe want to check if the expected policy status is correct.
            // String policyStatus = createSearchItemPage.getPolicyStatus();
            // Then do all the checks against the expected.
            // if is not as expected then fail the test.

            // TODO: Check Both Change Premium Status Results Handles and Audit Trail Results Handles.
            testHandle = workItem.getTestHandle();
            errorHandle = workItem.getErrorHandle();
            if (testHandle.isSuccess()) {
                workItem.validate();
                if (!workItem.isValid()) {
                    reportFailure(reportableFields, scenarioOperator, workItem.getComment());
                } else {
                    createMultipleMintItemtable = workItem.getCreateMultipleMintItemTable();
                    int mintItemCommentCESize = createMultipleMintItemtable.getMintItemCommentCESize();
                    workItems = createSearchItemPage.openWorkItems(false);
                    workItems.getWorkItems(mintItemCommentCESize);
                    // workItems.compareWorkItems(flowWorkItem);

                    testHandle = workItems.getTestHandle();
                    errorHandle = workItems.getErrorHandle();
                    if (testHandle.isSuccess()) {
                        // Note: We don't necessarily need the comments. So we can comments the below line of code out.
                        // workItems.getComments(premiumStatus.productName, premiumStatus.testCaseNumber, premiumStatus.function, mintItemCommentCESize);
                        auditTrail = createSearchItemPage.openAuditTrail(false);
                        String expectAuditTrail = flowWorkItem.getAuditTrail();
                        auditTrail.audit(expectAuditTrail);

                        testHandle = auditTrail.getTestHandle();
                        errorHandle = auditTrail.getErrorHandle();
                        if (testHandle.isSuccess()) {
                            reportSuccess(reportableFields, scenarioOperator);
                        } else {
                            reportFailure(reportableFields, scenarioOperator, errorHandle.getError());
                        }
                    } else {
                        reportFailure(reportableFields, scenarioOperator, errorHandle.getError());
                    }
                }
            } else {
                reportFailure(reportableFields, scenarioOperator, errorHandle.getError());
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            if (e instanceof FicaStatusException) {
                reportFailure(reportableFields, scenarioOperator, e.getMessage());
            } else if (e instanceof ServicePageException) {
                reportFailure(reportableFields, scenarioOperator, e.getMessage());
            } else if (e instanceof NegativeTestException) {
                reportSuccess(reportableFields, scenarioOperator, e.getMessage());
            } else {
                reportFailure(reportableFields, scenarioOperator, e.getMessage());
            }
        }
        log.info("End Of Add Member Logic");
    }

    private String getFlowPolicyNumberFormat(String productName, String policyNumber) throws Exception {
        String actualPolicyNumber;
        log.debug("Formatting Policy Number");
        try {
            log.debug("db Policy Number: " + policyNumber);

            String prefix ="";
            String suffix="";
            String middleValue="";

            if (productName.equalsIgnoreCase("Funeral Insurance")) {
                prefix = policyNumber.substring(0, 2);
                suffix = policyNumber.substring(2, 9);
                log.info("Prefix: " + prefix);
                log.info("Suffix: " + suffix);
                middleValue = "0000";
            } else if (productName.equalsIgnoreCase("Health Cash Plan") || productName.equalsIgnoreCase("Health Cash")) {
                int size = policyNumber.length();
                int diff = 13 - size;

                if (policyNumber.toUpperCase().contains("HCP")) {
                    prefix =  policyNumber.substring(0, 3);
                    suffix = policyNumber.substring(3, size);
                    log.info("Prefix: " + prefix);
                    log.info("Suffix: " + suffix);
                    for (int i = 0; i < diff ; i++) {
                        middleValue += "0";
                    }
                } else if (policyNumber.toUpperCase().contains("HC")) {
                    prefix =  policyNumber.substring(0, 2);
                    suffix = policyNumber.substring(2, size);
                    log.info("Prefix: " + prefix);
                    log.info("Suffix: " + suffix);
                    for (int i = 0; i < diff ; i++) {
                        middleValue += "0";
                    }
                }
            } else if (productName.contains("LOC")) {
                prefix = "LC";
                suffix = policyNumber;
                int size = policyNumber.length();
                int diff = 13 - size;

                for (int i = 0; i < diff; i++){
                    middleValue += "0";
                }
            } else if (productName.toLowerCase().contains("Personal Accident".toLowerCase())) {
                prefix = "CP";
                suffix = policyNumber;
                int size = policyNumber.length();
                int diff = 11 - size;

                for (int i = 0; i < diff; i++){
                    middleValue += "0";
                }
            } else if (productName.equalsIgnoreCase("Pay Protect")) {
                int size = policyNumber.length();
                int diff = 13 - size;
                prefix =  policyNumber.substring(0, 2);
                suffix = policyNumber.substring(2, size);
                log.info("Prefix: " + prefix);
                log.info("Suffix: " + suffix);

                for (int i = 0; i < diff; i++){
                    middleValue += "0";
                }
            } else if (productName.equalsIgnoreCase("Cover For Life")) {
                int size = policyNumber.length();
                int diff = 13 - size;
                prefix =  policyNumber.substring(0, 2);
                suffix = policyNumber.substring(2, size);
                log.info("Prefix: " + prefix);
                log.info("Suffix: " + suffix);

                for (int i = 0; i < diff; i++){
                    middleValue += "0";
                }
            }

            actualPolicyNumber = prefix + middleValue + suffix;
            log.debug("Policy Number: " + actualPolicyNumber);
            return actualPolicyNumber;
        } catch (Exception e) {
            log.error("Error while getting flow policy number format.");
            throw new Exception("Error while getting flow policy number format.");
        }
    }

    private int getIntegerValue(String value) {
        try {
            return !value.isEmpty() ? Integer.valueOf(value) : 1;
        } catch (Exception e) {
            log.debug(e.getMessage());
            try {
                if (value.contains(".")) {
                    String[] numberProps = value.split("\\.");
                    return Integer.valueOf(numberProps[0]);
                }
            } catch (Exception ex) {
                return 1;
            }
        }
        return -1;
    }

    private Double stringToDouble(String policyHolderCoverAmount) throws Exception {
        try {
            return Double.valueOf(policyHolderCoverAmount);
        } catch (Exception e) {
            log.error("Error while getting double value: " + e.getMessage());
            throw new Exception("Error while getting double value: " + e.getMessage());
        }
    }

    private String getDoubleString(String numberString) {
        // TODO: Try to get the double value first.
        if (numberString.isEmpty()) {
            numberString = "0.00";
        }

        if (!numberString.contains(".")) {
            return numberString + ".00";
        } else {
            return numberString;
        }
    }

    private String dateToString(Date date, String dateFormat) {
        return new SimpleDateFormat(dateFormat).format(date);
    }

    private String doubleToDateString(String dateDoubleValue) {
        Double excelDateMilliSeconds = Double.valueOf(dateDoubleValue);
        Date javaDate = DateUtil.getJavaDate(excelDateMilliSeconds);
        return dateToString(javaDate, "dd-MM-YYYY");
    }

    private Date formatDate(String dateValue) throws Exception {
        try {
            if (!dateValue.contains("-")) {
               dateValue = doubleToDateString(dateValue);
            }
            String[] debitOderDateAttributes = dateValue.split("-");
            if (debitOderDateAttributes[0].length() == 4) {
                return new SimpleDateFormat("yyyy-MM-dd").parse(dateValue);
            } else if (debitOderDateAttributes[2].length() == 4) {
                return new SimpleDateFormat("dd-MM-yyyy").parse(dateValue);
            }
        } catch (Exception e) {
            throw new Exception("Error while formatting date.");
        }
        return null;
    }

    public String getDateStringWithFormat(String dateString, String dateFormat) throws Exception {
        try {
            return !dateString.isEmpty() ? dateToString(formatDate(dateString), dateFormat) : "";
        } catch (Exception e) {
            throw new Exception("Error while getting date format from date value: " + dateString);
        }
    }

    private void reportSuccess(String[] reportableFields, ScenarioOperator scenarioOperator) {
        scenarioOperator.setResult("Pass");
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Result"), scenarioOperator.getResult(), TestResultReportFlag.SUCCESS);
    }

    private void reportSuccess(String[] reportableFields, ScenarioOperator scenarioOperator, String negativeResult) {
        scenarioOperator.setResult("Pass");
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Result"), scenarioOperator.getResult(), TestResultReportFlag.SUCCESS);
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Failure Reason"), negativeResult, TestResultReportFlag.WARNING);
    }

    private void reportFailure(String[] reportableFields, ScenarioOperator scenarioOperator, String error) {
        scenarioOperator.setResult("Fail");
        scenarioOperator.setFailureReason(error);
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Result"), scenarioOperator.getResult(), TestResultReportFlag.FAIL);
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Failure Reason"), scenarioOperator.getFailureReason(), TestResultReportFlag.WARNING);
    }
}
