package co.za.fnb.flow.pages.principal_member_details.page_factory;

import co.za.fnb.flow.pages.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PrincipalMemberDetailsPageObjects {

    @FindBy(id = "createSearchItemView:mainTabView:personalInformationPanel")
    private WebElement personalInformation;

    @FindBy(id = "createSearchItemView:mainTabView:residentialInformationPanel")
    private WebElement residentialInformation;

    @FindBy(id = "createSearchItemView:mainTabView:contactInformationPanel")
    private WebElement contactInformation;

    @FindBy(id = "createSearchItemView:mainTabView:postalInformationPanel")
    private WebElement postalInformation;

    @FindBy(xpath = "//*//button[contains (@id, 'createSearchItemView:mainTabView:Save')]")
    private WebElement save;

    @FindBy(id = "createSearchItemView:mainTabView:showPrincipalMemberPoliciesDlg")
    private WebElement showPrincipalMemberPoliciesDlg;

    @FindBy(id = "createSearchItemView:mainTabView:showPrincipalMemberPoliciesView:principalMemberLinkedPoliciesSave")
    private WebElement principalMemberLinkedPoliciesSave;

    public PrincipalMemberDetailsPageObjects(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public WebElement getPersonalInformation() {
        return personalInformation;
    }

    public WebElement getResidentialInformation() {
        return residentialInformation;
    }

    public WebElement getContactInformation() {
        return contactInformation;
    }

    public WebElement getPostalInformation() {
        return postalInformation;
    }

    public WebElement getSave() {
        return save;
    }

    public WebElement getShowPrincipalMemberPoliciesDlg() {
        return showPrincipalMemberPoliciesDlg;
    }

    public WebElement getPrincipalMemberLinkedPoliciesSave() {
        return principalMemberLinkedPoliciesSave;
    }
}
