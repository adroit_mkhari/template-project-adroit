package co.za.fnb.flow.tester.services;

import co.za.fnb.flow.handlers.JAXBHelper;
import co.za.fnb.flow.setup.PropertiesSetup;
import generated.*;
import generated.ObjectFactory;
import iseries.wsbeans.gsd00030pr.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.util.Properties;

public class GetAuditTrailTester {
    private Logger log  = LogManager.getLogger(GetAuditTrailTester.class);
    private GetAuditTrailRequestInput getAuditTrailRequestInput;
    private JAXBHelper marshallerJaxbHelper = new JAXBHelper();
    private JAXBHelper unMarshallerJaxbHelper = new JAXBHelper();
    private JAXBElement<Object> input;
    private GetAuditTrailResponseOutput getAuditTrailResponseOutput;
    private ResponseErrors responseErrors;
    private Properties properties = null;
    private String environment = "";

    public GetAuditTrailTester(GetAuditTrailRequestInput getAuditTrailRequestInput) {
        this.getAuditTrailRequestInput = getAuditTrailRequestInput;
        try {
            // Setup The Marshalling Context
            marshallerJaxbHelper.setCustomJaxbContext(GetAuditTrailRequestInput.class);
            ObjectFactory objectFactory = new ObjectFactory();
            input = objectFactory.createInput(getAuditTrailRequestInput);
            marshallerJaxbHelper.setDataObject(input);

            // Setup The Expected Output UnMarshalling Context
            unMarshallerJaxbHelper.setCustomJaxbContext(GetAuditTrailResponseOutput.class);

            log.info("Loading Properties");
            PropertiesSetup propertiesSetup = new PropertiesSetup();
            propertiesSetup.loadProperties();
            properties = propertiesSetup.getProperties();
            environment = properties.getProperty("ENVIROMENT");
            log.info("Done Loading Properties");
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    public GetAuditTrailRequestInput getGetAuditTrailRequestInput() {
        return getAuditTrailRequestInput;
    }

    public JAXBHelper getMarshallerJaxbHelper() {
        return marshallerJaxbHelper;
    }

    public JAXBHelper getUnMarshallerJaxbHelper() {
        return unMarshallerJaxbHelper;
    }

    public JAXBElement<Object> getInput() {
        return input;
    }

    public GetAuditTrailResponseOutput getGetAuditTrailResponseOutput() {
        return getAuditTrailResponseOutput;
    }

    public ResponseErrors getResponseErrors() {
        return responseErrors;
    }

    public GetAuditTrailResponseOutput sendGetAuditTrailRequest() throws Exception {
        String marshalResult = marshallerJaxbHelper.marshal(false);

        GSD00030PRPre gsd00030PRPre = new GSD00030PRPre();
        GSD00030PRTst gsd00030PRTst = new GSD00030PRTst();
        GSD00030PRServices gsd00030PRServicesPort = null;

        if (!environment.isEmpty()) {
            if (environment.equalsIgnoreCase("pre")) {
                gsd00030PRServicesPort = gsd00030PRPre.getGSD00030PRServicesPort();
            } else {
                gsd00030PRServicesPort = gsd00030PRTst.getGSD00030PRServicesPort();
            }
        } else {
            log.error("Error: No Target Environment Specified.");
            throw new Exception("Error: No Target Environment Specified.");
        }

        iseries.wsbeans.gsd00030pr.ObjectFactory serviceObjectFactory = new iseries.wsbeans.gsd00030pr.ObjectFactory();
        GensrvGETAUDITTRAILInput gensrvGETAUDITTRAILInput = serviceObjectFactory.createGensrvGETAUDITTRAILInput();
        PXMLIN pxmlin = serviceObjectFactory.createPXMLIN();
        pxmlin.setLength(10000);
        pxmlin.setString(marshalResult);
        gensrvGETAUDITTRAILInput.setPXMLIN(pxmlin);
        GensrvGETAUDITTRAILResult gensrvGETAUDITTRAILResult = gsd00030PRServicesPort.gensrvGetaudittrail(gensrvGETAUDITTRAILInput);
        PXMLOUT getAuditTrailResponseOutput = gensrvGETAUDITTRAILResult.getPXMLOUT();
        String pXmlOutString = getAuditTrailResponseOutput.getString();

        unMarshallerJaxbHelper.formatOutputXml(pXmlOutString, GetAuditTrailResponseOutput.class);
        Object unMarshal = unMarshallerJaxbHelper.unMarshal();
         if (!(unMarshal instanceof ResponseErrors)) {
             this.getAuditTrailResponseOutput = (GetAuditTrailResponseOutput) unMarshal;
             return this.getAuditTrailResponseOutput;
         } else {
             responseErrors = (ResponseErrors) unMarshal;
             throw new Exception("Error while getting GetAuditTrailResponseOutput: " + ((ResponseErrors) unMarshal).getErrors());
         }
    }
}
