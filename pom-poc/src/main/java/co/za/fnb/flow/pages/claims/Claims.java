package co.za.fnb.flow.pages.claims;

import co.za.fnb.flow.pages.BasePage;
import co.za.fnb.flow.pages.claims.page_factory.ClaimsPageObjects;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Claims extends BasePage {
    private Logger log  = LogManager.getLogger(Claims.class);
    ClaimsPageObjects claimsPageObjects = new ClaimsPageObjects(driver);

    public Claims(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
    }

    public CaptureNewClaim create() throws Exception {
        try {
            WebDriverWait webDriverWait = new WebDriverWait(driver, 60);
            webDriverWait.until(ExpectedConditions.visibilityOf(claimsPageObjects.getCreate()));
            log.info("Create Claim Element Is Visible.");
            click(claimsPageObjects.getCreate());
            return new CaptureNewClaim(driver, scenarioOperator);
        } catch (Exception e) {
            log.error("Error while clicking create.");
            throw new Exception("Error while clicking create.");
        }
    }
}
