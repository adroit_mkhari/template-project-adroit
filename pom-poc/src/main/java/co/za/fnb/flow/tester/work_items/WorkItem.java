package co.za.fnb.flow.tester.work_items;

import co.za.fnb.flow.models.work_items.FlowWorkItem;
import co.za.fnb.flow.pages.*;
import co.za.fnb.flow.pages.claims.AwaitDocOrDiarizeItem;
import co.za.fnb.flow.pages.policy_details.information.PolicyInformation;
import co.za.fnb.flow.pages.work_items.WorkItems;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;

public class WorkItem {
    private Logger log  = LogManager.getLogger(WorkItem.class);
    WebDriver driver;
    FlowWorkItem flowWorkItem;
    PolicyInformation policyInformation;
    private String comment;
    private boolean valid;
    private ErrorHandle errorHandle = new ErrorHandle();
    private TestHandle testHandle = new TestHandle();
    private CreateOrUpdateWorkItem createOrUpdateWorkItem;
    private WorkItems workItems;
    private CreateSearchItemPage createSearchItemPage;
    private AwaitDocOrDiarizeItem awaitDocOrDiarizeItem;

    public WorkItem(WebDriver driver, FlowWorkItem flowWorkItem, ScenarioOperator scenarioOperator) {
        this.driver = driver;
        this.flowWorkItem = flowWorkItem;
        policyInformation = new PolicyInformation(driver, scenarioOperator);
        createOrUpdateWorkItem = new CreateOrUpdateWorkItem(driver, scenarioOperator);
        workItems = new WorkItems(driver, scenarioOperator);
        createSearchItemPage = new CreateSearchItemPage(driver, scenarioOperator);
        awaitDocOrDiarizeItem = new AwaitDocOrDiarizeItem(driver, scenarioOperator);
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public void setExpectedResults() {
        policyInformation.setExpectedResults(flowWorkItem.getExpectResult());
    }

    public void create() throws Exception {
        log.info("Handling Create Work Item logic");

        // TODO: Implement Logic
        String referenceNumber = flowWorkItem.getReferenceNumber();
        String workType = flowWorkItem.getWorkType();
        String status = flowWorkItem.getStatus();
        String searchStatus = flowWorkItem.getSearchStatus();
        String itemWorkItemStatus = flowWorkItem.getWorkItemstatus();
        String queue = flowWorkItem.getQueue();
        String recordDissatisfaction = flowWorkItem.getRecordDissatisfaction();
        String voiceLogRef = flowWorkItem.getVoiceLogRef();
        String createdDate = flowWorkItem.getCreatedDate();
        String lockedBy = flowWorkItem.getLockedBy();
        String assignToMe = flowWorkItem.getAssignToMe();
        String loadExternalWorkTypes = flowWorkItem.getLoadExternalWorkTypes();
        String comment = flowWorkItem.getComment();
        String awaitDocDurationPerDay = flowWorkItem.getAwaitDocDurationPerDay();
        String awaitDocComments = flowWorkItem.getAwaitDocComments();
        String detailsAwaitDocReason = flowWorkItem.getAwaitDocReason();
        String awaitDocSubmit = flowWorkItem.getAwaitDocSubmit();
        String callFromTransaction = flowWorkItem.getCallFromTransaction();

        if (!referenceNumber.isEmpty()) {
            createOrUpdateWorkItem.inputReferenceNumber(referenceNumber);
            Thread.sleep(1000);
        }

        if (!workType.isEmpty()) {
            createOrUpdateWorkItem.selectWorkType(workType);
            Thread.sleep(1000);
        }

        if (!itemWorkItemStatus.isEmpty()) {
            createOrUpdateWorkItem.selectStatus(itemWorkItemStatus);
            Thread.sleep(1000);
        }

        if (!queue.isEmpty()) {
            createOrUpdateWorkItem.selectQueue(queue);
            Thread.sleep(1000);
        }

        if (!assignToMe.isEmpty()) {
            if (assignToMe.equalsIgnoreCase("YES")) {
                createOrUpdateWorkItem.clickAssignToMe();
                Thread.sleep(1000);
            }
        }

        if (!comment.isEmpty()) {
            createOrUpdateWorkItem.clickAddComments();
            Thread.sleep(1000);
            createOrUpdateWorkItem.inputComment(comment);
            Thread.sleep(1000);
            createOrUpdateWorkItem.clickAddCommentsAdd();
            Thread.sleep(1000);
            policyInformation.getResponses(false);
            Thread.sleep(1000);
        }

        createSearchItemPage.clickCreate();
        Thread.sleep(3000);

        driver.switchTo().defaultContent();
        driver.switchTo().frame(0);

        String confirmMessage = createOrUpdateWorkItem.getConfirmMessage();
        log.info("Confirm Message: " + confirmMessage);

        if (confirmMessage.toLowerCase().contains("was a call made from this transaction")) {
            if (!callFromTransaction.isEmpty()) {
                if (callFromTransaction.equalsIgnoreCase("NO")) {
                    createOrUpdateWorkItem.clickConfirmNo();
                    Thread.sleep(1000);
                } else {
                    createOrUpdateWorkItem.clickConfirmYes();
                    Thread.sleep(2000);

                    if (!voiceLogRef.isEmpty()) {
                        createOrUpdateWorkItem.inputVoiceLogRef(voiceLogRef);
                        Thread.sleep(1000);
                    }
                    // createSearchItemPage.clickCreate();
                }
            }
        } else {
            createOrUpdateWorkItem.clickConfirmYes();
            Thread.sleep(3000);
            driver.switchTo().frame(0);
            Thread.sleep(1000);

            confirmMessage = createOrUpdateWorkItem.getConfirmMessage();
            log.info("Confirm Message: " + confirmMessage);

            if (confirmMessage.toLowerCase().contains("was a call made from this transaction")) {
                if (!callFromTransaction.isEmpty()) {
                    if (callFromTransaction.equalsIgnoreCase("NO")) {
                        createOrUpdateWorkItem.clickConfirmNo();
                        Thread.sleep(1000);
                    } else {
                        createOrUpdateWorkItem.clickConfirmYes();
                        Thread.sleep(2000);

                        if (!voiceLogRef.isEmpty()) {
                            createOrUpdateWorkItem.inputVoiceLogRef(voiceLogRef);
                            Thread.sleep(1000);
                        }
                        createSearchItemPage.clickCreate();
                    }
                }
            }
        }

        if (!searchStatus.isEmpty()) {
            if (searchStatus.equalsIgnoreCase("AWAITDOC")) {
                if (!awaitDocDurationPerDay.isEmpty()) {
                    awaitDocOrDiarizeItem.selectTime(awaitDocDurationPerDay);
                    Thread.sleep(1000);
                }

                if (!awaitDocComments.isEmpty()) {
                    awaitDocOrDiarizeItem.updateComments(awaitDocComments);
                    Thread.sleep(1000);
                }

                if (!detailsAwaitDocReason.isEmpty()) {
                    awaitDocOrDiarizeItem.selectReason(detailsAwaitDocReason);
                }

                if (awaitDocSubmit.equalsIgnoreCase("Yes")) {
                    awaitDocOrDiarizeItem.clickSubmit();
                    Thread.sleep(1000);
                } else {
                    awaitDocOrDiarizeItem.clickCancel();
                    Thread.sleep(1000);
                    // TODO: Handle the No Case.
                }

                createSearchItemPage.switchToWindowAndMaximize(1);
            }
        }

        policyInformation.getResponses(true);

        log.info("Done Create Work Item logic");
    }

    public void update() throws Exception {
        log.info("Handling Update Work Item logic");

        // TODO: Implement Logic
        String referenceNumber = flowWorkItem.getReferenceNumber();
        String workType = flowWorkItem.getWorkType();
        String status = flowWorkItem.getStatus();
        String queue = flowWorkItem.getQueue();
        String searchStatus = flowWorkItem.getSearchStatus();
        String recordDissatisfaction = flowWorkItem.getRecordDissatisfaction();
        String voiceLogRef = flowWorkItem.getVoiceLogRef();
        String createdDate = flowWorkItem.getCreatedDate();
        String lockedBy = flowWorkItem.getLockedBy();
        String assignToMe = flowWorkItem.getAssignToMe();
        String loadExternalWorkTypes = flowWorkItem.getLoadExternalWorkTypes();
        String comment = flowWorkItem.getComment();
        String awaitDocDurationPerDay = flowWorkItem.getAwaitDocDurationPerDay();
        String awaitDocComments = flowWorkItem.getAwaitDocComments();
        String detailsAwaitDocReason = flowWorkItem.getAwaitDocReason();
        String awaitDocSubmit = flowWorkItem.getAwaitDocSubmit();
        String callFromTransaction = flowWorkItem.getCallFromTransaction();

        int workItemTableSize = workItems.getWorkItemTableSize();
        workItems.getWorkItems(workItemTableSize);
        String[] workTypeList = workItems.getWorkType();
        String[] statusList = workItems.getStatus();
        String[] createdDateList = workItems.getCreatedDate();

        String workItemCreatedDate, workItemStatus;
        boolean presentFlag = false, receiveDoc = searchStatus.equalsIgnoreCase("RECEIVEDOC");
        if (!createdDate.isEmpty()) {
            for (int i = 0; i < workItemTableSize; i++) {
                workItemCreatedDate = createdDateList[i];
                if (createdDate.equalsIgnoreCase(workItemCreatedDate)) {
                    workItems.doubleClickWorkItem(i);
                    presentFlag = true;
                    break;
                }
            }

            if (!presentFlag) {
                log.error("No Work Item With The Created Date " + createdDate);
                throw new Exception("No Work Item With The Created Date " + createdDate);
            }
        } else {
            if (!searchStatus.isEmpty()) {
                if (searchStatus.equalsIgnoreCase("N/A")) {
                    workItems.doubleClickWorkItem(0);
                } else if (receiveDoc) {
                    for (int i = 0; i < workItemTableSize; i++) {
                        workItemStatus = statusList[i];
                        if (searchStatus.equalsIgnoreCase(workItemStatus)) {
                            workItems.doubleClickWorkItem(i);
                            presentFlag = true;
                            break;
                        }
                    }

                    if (!presentFlag) {
                        log.error("No Work Item With The Status " + searchStatus);
                        throw new Exception("No Work Item With The Status " + searchStatus);
                    }
                }
            }

            createSearchItemPage.clickDocuments();
            Thread.sleep(1000);
            createSearchItemPage.moveToAndDoubleClickTheFirstPolicyDocument();
            Thread.sleep(1000);

            createSearchItemPage.switchToWindowAndMaximize(2);
            Thread.sleep(1000);
            driver.close();
            Thread.sleep(1000);
            createSearchItemPage.switchToWindowAndMaximize(1);
            Thread.sleep(1000);
        }

        if (!referenceNumber.isEmpty()) {
            createOrUpdateWorkItem.inputReferenceNumber(referenceNumber);
            Thread.sleep(1000);
        }

        if (!voiceLogRef.isEmpty()) {
            createOrUpdateWorkItem.inputVoiceLogRef(voiceLogRef);
            Thread.sleep(1000);
        }

        if (!workType.isEmpty()) {
            createOrUpdateWorkItem.selectWorkType(workType);
            Thread.sleep(1000);
        }

        if (!status.isEmpty()) {
            createOrUpdateWorkItem.selectStatus(status);
            Thread.sleep(1000);
        }

        if (!assignToMe.isEmpty()) {
            if (assignToMe.equalsIgnoreCase("YES")) {
                createOrUpdateWorkItem.clickAssignToMe();
                Thread.sleep(1000);
            }
        }

        if (!comment.isEmpty()) {
            createOrUpdateWorkItem.clickAddComments();
            Thread.sleep(1000);
            createOrUpdateWorkItem.inputComment(comment);
            Thread.sleep(1000);
            createOrUpdateWorkItem.clickAddCommentsAdd();
            Thread.sleep(1000);
            policyInformation.getResponses(false);
            Thread.sleep(1000);
        }

        String statusValue = createOrUpdateWorkItem.getStatus();
        createSearchItemPage.clickUpdate();
        Thread.sleep(2000);

        if (!status.isEmpty()) {
            if (statusValue.equalsIgnoreCase("AWAITDOC")) {
                if (!awaitDocDurationPerDay.isEmpty()) {
                    awaitDocOrDiarizeItem.selectTime(awaitDocDurationPerDay);
                    Thread.sleep(1000);
                }

                if (!awaitDocComments.isEmpty()) {
                    awaitDocOrDiarizeItem.updateComments(awaitDocComments);
                    Thread.sleep(1000);
                }

                if (!detailsAwaitDocReason.isEmpty()) {
                    awaitDocOrDiarizeItem.selectReason(detailsAwaitDocReason);
                }

                if (awaitDocSubmit.equalsIgnoreCase("Yes")) {
                    awaitDocOrDiarizeItem.clickSubmit();
                    Thread.sleep(1000);
                } else {
                    awaitDocOrDiarizeItem.clickCancel();
                    Thread.sleep(1000);
                    // TODO: Handle the No Case.
                }

                createSearchItemPage.switchToWindowAndMaximize(1);
            }
        }
        policyInformation.getResponses(false);

        log.info("Done Update Work Item logic");
    }

    public void cloneWorkItem() throws Exception {
        log.info("Handling Clone Work Item logic");

        // TODO: Implement Logic
        String referenceNumber = flowWorkItem.getReferenceNumber();
        String workType = flowWorkItem.getWorkType();
        String status = flowWorkItem.getStatus();
        String queue = flowWorkItem.getQueue();
        String searchStatus = flowWorkItem.getSearchStatus();
        String recordDissatisfaction = flowWorkItem.getRecordDissatisfaction();
        String voiceLogRef = flowWorkItem.getVoiceLogRef();
        String createdDate = flowWorkItem.getCreatedDate();
        String lockedBy = flowWorkItem.getLockedBy();
        String assignToMe = flowWorkItem.getAssignToMe();
        String loadExternalWorkTypes = flowWorkItem.getLoadExternalWorkTypes();
        String comment = flowWorkItem.getComment();
        String awaitDocDurationPerDay = flowWorkItem.getAwaitDocDurationPerDay();
        String awaitDocComments = flowWorkItem.getAwaitDocComments();
        String detailsAwaitDocReason = flowWorkItem.getAwaitDocReason();
        String awaitDocSubmit = flowWorkItem.getAwaitDocSubmit();
        String callFromTransaction = flowWorkItem.getCallFromTransaction();
        String numberOfClones = flowWorkItem.getNumberOfClones();

        int workItemTableSize = workItems.getWorkItemTableSize();
        workItems.getWorkItems(workItemTableSize);
        String[] workTypeList = workItems.getWorkType();
        String[] statusList = workItems.getStatus();
        String[] createdDateList = workItems.getCreatedDate();

        String workItemCreatedDate, workItemStatus;
        boolean presentFlag = false, receiveDoc = searchStatus.equalsIgnoreCase("RECEIVEDOC");
        if (!createdDate.isEmpty()) {
            for (int i = 0; i < workItemTableSize; i++) {
                workItemCreatedDate = createdDateList[i];
                if (createdDate.equalsIgnoreCase(workItemCreatedDate)) {
                    workItems.doubleClickWorkItem(i);
                    presentFlag = true;
                    break;
                }
            }

            if (!presentFlag) {
                log.error("No Work Item With The Created Date " + createdDate);
                throw new Exception("No Work Item With The Created Date " + createdDate);
            }
        }

        createOrUpdateWorkItem.clickClone();
        Thread.sleep(1000);

        driver.switchTo().defaultContent();
        driver.switchTo().frame(0);

        createOrUpdateWorkItem.selectNumberOfClones(numberOfClones);
        Thread.sleep(1000);
        createOrUpdateWorkItem.clickViewClone();

        policyInformation.getResponses(false);

        log.info("Done Clone Work Item logic");
    }

    public void sendSms() throws Exception {
        log.info("Handling Send Sms logic");

        // TODO: Implement Logic
        String referenceNumber = flowWorkItem.getReferenceNumber();
        String workType = flowWorkItem.getWorkType();
        String status = flowWorkItem.getStatus();
        String queue = flowWorkItem.getQueue();
        String searchStatus = flowWorkItem.getSearchStatus();
        String recordDissatisfaction = flowWorkItem.getRecordDissatisfaction();
        String voiceLogRef = flowWorkItem.getVoiceLogRef();
        String createdDate = flowWorkItem.getCreatedDate();
        String lockedBy = flowWorkItem.getLockedBy();
        String assignToMe = flowWorkItem.getAssignToMe();
        String loadExternalWorkTypes = flowWorkItem.getLoadExternalWorkTypes();
        String comment = flowWorkItem.getComment();
        String awaitDocDurationPerDay = flowWorkItem.getAwaitDocDurationPerDay();
        String awaitDocComments = flowWorkItem.getAwaitDocComments();
        String detailsAwaitDocReason = flowWorkItem.getAwaitDocReason();
        String awaitDocSubmit = flowWorkItem.getAwaitDocSubmit();
        String callFromTransaction = flowWorkItem.getCallFromTransaction();

        String toAddress = flowWorkItem.getSmsViewToAddress();
        String template = flowWorkItem.getSmsViewTemplate();

        int workItemTableSize = workItems.getWorkItemTableSize();
        workItems.getWorkItems(workItemTableSize);
        String[] workTypeList = workItems.getWorkType();
        String[] statusList = workItems.getStatus();
        String[] createdDateList = workItems.getCreatedDate();

        String workItemCreatedDate, workItemStatus;
        boolean presentFlag = false, receiveDoc = searchStatus.equalsIgnoreCase("RECEIVEDOC");
        if (!createdDate.isEmpty()) {
            for (int i = 0; i < workItemTableSize; i++) {
                workItemCreatedDate = createdDateList[i];
                if (createdDate.equalsIgnoreCase(workItemCreatedDate)) {
                    workItems.doubleClickWorkItem(i);
                    presentFlag = true;
                    break;
                }
            }

            if (!presentFlag) {
                log.error("No Work Item With The Created Date " + createdDate);
                throw new Exception("No Work Item With The Created Date " + createdDate);
            }
        } else {
            if (!searchStatus.isEmpty()) {
                if (searchStatus.equalsIgnoreCase("N/A")) {
                    workItems.doubleClickWorkItem(0);
                } else if (receiveDoc) {
                    for (int i = 0; i < workItemTableSize; i++) {
                        workItemStatus = statusList[i];
                        if (searchStatus.equalsIgnoreCase(workItemStatus)) {
                            workItems.doubleClickWorkItem(i);
                            presentFlag = true;
                            break;
                        }
                    }

                    if (!presentFlag) {
                        log.error("No Work Item With The Status " + searchStatus);
                        throw new Exception("No Work Item With The Status " + searchStatus);
                    }
                }
            }

            createSearchItemPage.clickDocuments();
            Thread.sleep(1000);
            createSearchItemPage.moveToAndDoubleClickTheFirstPolicyDocument();
            Thread.sleep(1000);

            createSearchItemPage.switchToWindowAndMaximize(2);
            Thread.sleep(1000);
            driver.close();
            Thread.sleep(1000);
            createSearchItemPage.switchToWindowAndMaximize(1);
            Thread.sleep(1000);
        }

        createOrUpdateWorkItem.clickCorrespondence();
        Thread.sleep(1000);

        createOrUpdateWorkItem.clickCorrespondenceSms();
        Thread.sleep(1000);

        createOrUpdateWorkItem.clickOpenSms();
        Thread.sleep(3000);

        driver.switchTo().frame(0);
        Thread.sleep(1000);

        if (!toAddress.isEmpty()) {
            createOrUpdateWorkItem.inputSmsViewToAddress(toAddress);
            Thread.sleep(1000);
        }

        if (!template.isEmpty()) {
            createOrUpdateWorkItem.selectSmsViewTemplateSelect(template);
            Thread.sleep(1000);
        }

        policyInformation.getResponses(false);

        log.info("Done With Send Sms logic");
    }

    private String getExpectedDoubleNumberFormat(String premiumAmountDataValue) {
        if (!premiumAmountDataValue.isEmpty()) {
            if (!premiumAmountDataValue.contains(".")) {
                if (premiumAmountDataValue.contains(",")) {
                    return premiumAmountDataValue.replace(",", ".");
                } else {
                    return premiumAmountDataValue + ".00";
                }
            }
        } else {
            log.error("Got an Empty String Value" );
            premiumAmountDataValue = "0.00";
        }
        return premiumAmountDataValue;
    }

    private int getIntegerValue(String value) {
        try {
            return !value.isEmpty() ? Integer.valueOf(value) : 1;
        } catch (Exception e) {
            log.debug(e.getMessage());
            try {
                if (value.contains(".")) {
                    String[] numberProps = value.split("\\.");
                    return Integer.valueOf(numberProps[0]);
                }
            } catch (Exception ex) {
                return 1;
            }
        }
        return -1;
    }

    public void save() throws Exception {
        try {
            log.info("Saving Work Items.");

            // TODO: Implement Logic

            testHandle = policyInformation.getTestHandle();
            errorHandle = policyInformation.getErrorHandle();
        } catch (Exception e) {
            testHandle.setSuccess(false);
            errorHandle.setError(e.getMessage());
            throw new Exception("ERROR on Save: " + e.getMessage());
        }
    }

    public CreateMultipleMintItemTable getCreateMultipleMintItemTable() {
        return policyInformation.getCreateMultipleMintItemTable();
    }

    public void validate() {
        // TODO: Implement Validation Logic
        setValid(true);
    }

    public ErrorHandle getErrorHandle() {
        return this.errorHandle;
    }

    public void setErrorHandle(ErrorHandle errorHandle) {
        this.errorHandle = errorHandle;
    }

    public TestHandle getTestHandle() {
        return this.testHandle;
    }

    public void setTestHandle(TestHandle testHandle) {
        this.testHandle = testHandle;
    }

}