package co.za.fnb.flow.tester.letters.helpers;

public class PdfCompare {
    private boolean same;
    private String resultPath;

    public PdfCompare(boolean same, String resultPath) {
        this.same = same;
        this.resultPath = resultPath;
    }

    public boolean isSame() {
        return same;
    }

    public String getResultPath() {
        return resultPath;
    }
}
