package co.za.fnb.flow.models.policy_details;

public class DebitDateCheck {
    private String productName;
    private String policyNumber;
    private String action;
    private String debitOrderDate;
    private String nextDueDate;
    private String waitingPeriod;
    private String areas;

    public DebitDateCheck() {
    }

    public DebitDateCheck(String productName, String policyNumber, String action, String debitOrderDate, String nextDueDate, String waitingPeriod, String areas) {
        this.productName = productName;
        this.policyNumber = policyNumber;
        this.action = action;
        this.debitOrderDate = debitOrderDate;
        this.nextDueDate = nextDueDate;
        this.waitingPeriod = waitingPeriod;
        this.areas = areas;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getDebitOrderDate() {
        return debitOrderDate;
    }

    public void setDebitOrderDate(String debitOrderDate) {
        this.debitOrderDate = debitOrderDate;
    }

    public String getNextDueDate() {
        return nextDueDate;
    }

    public void setNextDueDate(String nextDueDate) {
        this.nextDueDate = nextDueDate;
    }

    public String getWaitingPeriod() {
        return waitingPeriod;
    }

    public void setWaitingPeriod(String waitingPeriod) {
        this.waitingPeriod = waitingPeriod;
    }

    public String getAreas() {
        return areas;
    }

    public void setAreas(String areas) {
        this.areas = areas;
    }

    @Override
    public String toString() {
        return "DebitDateCheck{" +
                "productName='" + productName + '\'' +
                ", policyNumber='" + policyNumber + '\'' +
                ", action='" + action + '\'' +
                ", debitOrderDate='" + debitOrderDate + '\'' +
                ", nextDueDate='" + nextDueDate + '\'' +
                ", waitingPeriod='" + waitingPeriod + '\'' +
                ", areas='" + areas + '\'' +
                '}';
    }
}
