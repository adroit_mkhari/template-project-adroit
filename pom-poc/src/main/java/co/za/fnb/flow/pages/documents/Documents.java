package co.za.fnb.flow.pages.documents;

import co.za.fnb.flow.pages.documents.page_factory.DocumentsPageObjects;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import co.za.fnb.flow.pages.BasePage;

public class Documents extends BasePage {
    DocumentsPageObjects documentsPageObjects = new DocumentsPageObjects(driver);

    public Documents(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
        PageFactory.initElements(driver, this);
    }
}
