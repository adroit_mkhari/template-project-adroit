package co.za.fnb.flow.handlers.database;

import co.za.fnb.flow.models.custom_exceptions.FicaStatusException;
import co.za.fnb.flow.setup.PropertiesSetup;
import co.za.fnb.flow.tester.ScenarioOperator;
import co.za.fnb.flow.tester.letters.helpers.CsvQueueItem;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.Properties;

public class QueryHandler {
    private Logger log = LogManager.getLogger(QueryHandler.class);
    private Connection connection;
    private String policyNumber;
    private String dbPolicyNumber;
    private Statement statement;
    private ResultSet resultSet;
    private String dbConnectionString;
    private String username;
    private String password;

    public QueryHandler() {

        try {
            Class.forName("com.ibm.as400.access.AS400JDBCDriver");

            PropertiesSetup propertiesSetup = new PropertiesSetup();
            propertiesSetup.loadProperties();
            Properties properties = propertiesSetup.getProperties();
            dbConnectionString = properties.getProperty("DB_CONNECTION_STRING");

            username = "FNBFLOW";
            password = "fnbflow";
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public QueryHandler(String policyNumber) {
        this();
        this.policyNumber = policyNumber;
    }

    public QueryHandler(String dbConnectionString, String username, String password) {
        this();
        this.dbConnectionString = dbConnectionString;
        this.username = username;
        this.password = password;
    }

    public QueryHandler(String policyNumber, String dbConnectionString, String username, String password) {
        this();
        this.policyNumber = policyNumber;
        this.dbConnectionString = dbConnectionString;
        this.username = username;
        this.password = password;
    }

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public Statement getStatement() {
        return statement;
    }

    public void setStatement(Statement statement) {
        this.statement = statement;
    }

    public ResultSet getResultSet() {
        return resultSet;
    }

    public void setResultSet(ResultSet resultSet) {
        this.resultSet = resultSet;
    }

    public String getDbConnectionString() {
        return dbConnectionString;
    }

    public void setDbConnectionString(String dbConnectionString) {
        this.dbConnectionString = dbConnectionString;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDbPolicyNumber() {
        return dbPolicyNumber;
    }

    public void setDbPolicyNumber(String dbPolicyNumber) {
        this.dbPolicyNumber = dbPolicyNumber;
    }

    public void connectToDatabase() {
        try {
            log.info("Connecting To Database");
            connection = DriverManager.getConnection(dbConnectionString, username, password);
        } catch (SQLException e) {
            e.printStackTrace();
            log.debug("Failed to connect to database.");
            log.error(e.getMessage());
        }
    }

    public void createStatement() {

        if (connection == null) {
            connectToDatabase();
        }

        try {
            log.info("Creating Query Statement");
            statement = connection.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
            log.debug("Failed to create query statement.");
            log.error(e.getMessage());
        }
    }

    private void closeDatabaseConnection() {
        if (connection != null){
            try{
                log.info("Closing Database Connection...");
                // connection.close();
                log.info("Done Closing Database Connection.");
            } catch (Exception e){
                log.error("Failed to close database connection");
            }
        }
    }

    public void computePolicyNumber(String policyCode, String productName) throws Exception {
        // Connect to the database if connection is null:
        log.info("Computing Policy Number.");
        if (connection == null) {
            connectToDatabase();
        }

        // TODO: Review the below logic, we can just use the same anyway because the policy codes tell us which one it is:
        if (productName.equalsIgnoreCase("Funeral Insurance")) {
            // TODO
            String policyNumber = queryForPolicyNumber(policyCode, productName);
            setPolicyNumber(policyNumber);
            if (this.policyNumber != null) {
                updateWorkFlowItemSet(this.policyNumber, productName);
            }
        } else if (productName.contains("LOC")){
            // TODO
            String policyNumber = queryForPolicyNumber(policyCode, productName);
            setPolicyNumber(policyNumber);
            if (this.policyNumber != null) {
                updateWorkFlowItemSet(this.policyNumber, productName);
            }
        } else if (productName.toLowerCase().contains("Personal Accident".toLowerCase())){
            // TODO
            String policyNumber = queryForPolicyNumber(policyCode, productName);
            setPolicyNumber(policyNumber);
            if (this.policyNumber != null) {
                updateWorkFlowItemSet(this.policyNumber, productName);
            }
        } else if (productName.equalsIgnoreCase("Health Cash") || productName.equalsIgnoreCase("Health Cash Plan")) {
            // TODO
            String policyNumber = queryForPolicyNumber(policyCode, productName);
            setPolicyNumber(policyNumber);
            if (this.policyNumber != null) {
                updateWorkFlowItemSet(this.policyNumber, productName);
            }
        } else if (productName.equalsIgnoreCase("Pay Protect")) {
            // TODO
            String policyNumber = queryForPolicyNumber(policyCode, productName);
            setPolicyNumber(policyNumber);
            if (this.policyNumber != null) {
                updateWorkFlowItemSet(this.policyNumber, productName);
            }
        } else if (productName.equalsIgnoreCase("Cover For Life")) {
            // TODO
            String policyNumber = queryForPolicyNumber(policyCode, productName);
            setPolicyNumber(policyNumber);
            if (this.policyNumber != null) {
                updateWorkFlowItemSet(this.policyNumber, productName);
            }
        }
    }

    private String queryForPolicyNumber(String policyCode, String productName) throws Exception {
        String policyNumber;

        createStatement();

        log.debug("Query Database For Policy Number");

        if (productName.contains("LOC")) {
            queryForLOCPolicyNumber(policyCode);
        } else if (productName.equalsIgnoreCase("Funeral Insurance")){
            queryForFuneralInsurancePolicyNumber(policyCode);
        } else if (productName.equalsIgnoreCase("Funeral")){
            queryForFuneralPolicyNumber(policyCode);
        } else if (productName.toLowerCase().contains("Personal Accident".toLowerCase())){
            queryForPersonalAccidentPolicyNumber(policyCode);
        } else if (productName.equalsIgnoreCase("Health Cash") || productName.equalsIgnoreCase("Health Cash Plan")) {
            queryForHospitalCashPlanPolicyNumber(policyCode);
        } else if (productName.equalsIgnoreCase("Pay Protect")) {
            queryForPayProtect(policyCode);
        } else if (productName.equalsIgnoreCase("Cover For Life")) {
            queryForCoverForLife(policyCode);
        }

        if (resultSet != null) {
            policyNumber = formatPolicyNumber(resultSet, productName);
        } else {
            return null;
        }

        if (policyNumber != null && policyNumber.isEmpty()) {
            return null;
        }

        return policyNumber;
    }

    private void queryForLOCPolicyNumber(String policyCode) throws Exception {
        try {
            log.debug("Running Query For " + policyCode);
            if (policyCode.equalsIgnoreCase("LOC1")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.LOC1));
            } else if (policyCode.equalsIgnoreCase("LOC2")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.LOC2));
            } else if (policyCode.equalsIgnoreCase("LOC3")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.LOC3));
            } else if (policyCode.equalsIgnoreCase("LOCB1")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.LOCB1));
            } else if (policyCode.equalsIgnoreCase("LOCB2")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.LOCB2));
            } else if (policyCode.equalsIgnoreCase("LOCB3")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.LOCB3));
            } else if (policyCode.equalsIgnoreCase("LOCB4")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.LOCB4));
            } else if (policyCode.equalsIgnoreCase("LOCB5")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.LOCB5));
            } else if (policyCode.equalsIgnoreCase("LOCB6")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.LOCB6));
            } else if (policyCode.equalsIgnoreCase("LOCB7")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.LOCB7));
            } else if (policyCode.equalsIgnoreCase("LOCB8")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.LOCB8));
            } else if (policyCode.equalsIgnoreCase("LOCB9")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.LOCB9));
            } else if (policyCode.equalsIgnoreCase("C_LOC1")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.C_LOC1));
            } else if (policyCode.equalsIgnoreCase("C_LOCB1")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.C_LOCB1));
            } else if (policyCode.equalsIgnoreCase("N_LOC1")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.N_LOC1));
            } else if (policyCode.equalsIgnoreCase("N_LOCB1")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.N_LOCB1));
            } else if (policyCode.equalsIgnoreCase("P_LOC1")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.P_LOC1));
            } else if (policyCode.equalsIgnoreCase("P_LOCB1")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.P_LOCB1));
            } else if (policyCode.equalsIgnoreCase("R_LOC1")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.R_LOC1));
            } else if (policyCode.equalsIgnoreCase("R_LOCB1")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.R_LOCB1));
            } else if (policyCode.equalsIgnoreCase("R_LOC2")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.R_LOC2));
            }
        } catch (Exception e) {
            log.debug("Error while querying for " + policyCode + " policy Number");
            log.error(e.getMessage());
            throw new Exception(e.getMessage());
        }
    }

    private void queryForFuneralInsurancePolicyNumber(String policyCode) throws Exception {
        try {
            log.debug("Running Query For " + policyCode);
            if (policyCode.equalsIgnoreCase("FI1")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.FI1));
            } else if (policyCode.equalsIgnoreCase("FI2")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.FI2));
            } else if (policyCode.equalsIgnoreCase("FI3")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.FI3));
            } else if (policyCode.equalsIgnoreCase("FI4")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.FI4));
            } else if (policyCode.equalsIgnoreCase("FI5")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.FI5));
            } else if (policyCode.equalsIgnoreCase("FI6")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.FI6));
            } else if (policyCode.equalsIgnoreCase("FI7")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.FI7));
            } else if (policyCode.equalsIgnoreCase("FI8")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.FI8));
            } else if (policyCode.equalsIgnoreCase("FI9")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.FI9));
            } else if (policyCode.equalsIgnoreCase("FI10")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.FI10));
            } else if (policyCode.equalsIgnoreCase("FI11")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.FI11));
            } else if (policyCode.equalsIgnoreCase("FI12")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.FI12));
            } else if (policyCode.equalsIgnoreCase("FI13")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.FI13));
            } else if (policyCode.equalsIgnoreCase("FI14")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.FI14));
            } else if (policyCode.equalsIgnoreCase("FI15")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.FI15));
            } else if (policyCode.equalsIgnoreCase("FI16")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.FI16));
            } else if (policyCode.equalsIgnoreCase("FI17")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.FI17));
            } else if (policyCode.equalsIgnoreCase("FI18")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.FI18));
            } else if (policyCode.equalsIgnoreCase("FI19")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.FI19));
            } else if (policyCode.equalsIgnoreCase("FI20")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.FI20));
            }

            // Lapesed Policies
            else if (policyCode.equalsIgnoreCase("R_FI1")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.R_FI1));
            } else if (policyCode.equalsIgnoreCase("C_FI1")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.C_FI1));
            }
        } catch (Exception e) {
            log.debug("Error while querying for " + policyCode + " policy Number");
            log.error(e.getMessage());
            throw new Exception(e.getMessage());
        }
    }

    private void queryForFuneralPolicyNumber(String policyCode) throws Exception {
        try {
            log.debug("Running Query For " + policyCode);
            if (policyCode.equalsIgnoreCase("FX")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.HCP1));
            }
        } catch (Exception e) {
            log.debug("Error while querying for " + policyCode + " policy Number");
            log.error(e.getMessage());
            throw new Exception(e.getMessage());
        }
    }

    private void queryForHospitalCashPlanPolicyNumber(String policyCode) throws Exception {
        try {
            log.debug("Running Query For " + policyCode);
            if (policyCode.equalsIgnoreCase("HCP1")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.HCP1));
            } else if (policyCode.equalsIgnoreCase("HCP2")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.HCP2));
            } else if (policyCode.equalsIgnoreCase("HCP3")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.HCP3));
            } else if (policyCode.equalsIgnoreCase("HCP4")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.HCP4));
            } else if (policyCode.equalsIgnoreCase("HCP5")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.HCP5));
            } else if (policyCode.equalsIgnoreCase("HCP6")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.HCP6));
            } else if (policyCode.equalsIgnoreCase("HCP7")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.HCP7));
            } else if (policyCode.equalsIgnoreCase("HCP8")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.HCP8));
            } else if (policyCode.equalsIgnoreCase("HCP9")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.HCP9));
            } else if (policyCode.equalsIgnoreCase("HCP10")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.HCP10));
            } else if (policyCode.equalsIgnoreCase("HCP11")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.HCP11));
            } else if (policyCode.equalsIgnoreCase("HCP12")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.HCP12));
            } else if (policyCode.equalsIgnoreCase("HCP13")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.HCP13));
            } else if (policyCode.equalsIgnoreCase("HCP14")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.HCP14));
            } else if (policyCode.equalsIgnoreCase("HCP15")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.HCP15));
            } else if (policyCode.equalsIgnoreCase("HCP16")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.HCP16));
            } else if (policyCode.equalsIgnoreCase("HC1")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.HC1));
            } else if (policyCode.equalsIgnoreCase("HC2")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.HC2));
            } else if (policyCode.equalsIgnoreCase("HC3")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.HC3));
            } else if (policyCode.equalsIgnoreCase("HC4")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.HC4));
            } else if (policyCode.equalsIgnoreCase("HC5")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.HC5));
            } else if (policyCode.equalsIgnoreCase("HC6")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.HC6));
            } else if (policyCode.equalsIgnoreCase("HC7")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.HC7));
            } else if (policyCode.equalsIgnoreCase("HC8")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.HC8));
            } else if (policyCode.equalsIgnoreCase("HC9")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.HC9));
            } else if (policyCode.equalsIgnoreCase("HC10")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.HC10));
            } else if (policyCode.equalsIgnoreCase("HC11")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.HC11));
            } else if (policyCode.equalsIgnoreCase("HC12")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.HC12));
            } else if (policyCode.equalsIgnoreCase("HC13")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.HC13));
            } else if (policyCode.equalsIgnoreCase("HC14")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.HC14));
            } else if (policyCode.equalsIgnoreCase("HC15")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.HC15));
            } else if (policyCode.equalsIgnoreCase("HC16")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.HC16));
            } else if (policyCode.equalsIgnoreCase("HC17")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.HC17));
            } else if (policyCode.equalsIgnoreCase("HC18")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.HC18));
            } else if (policyCode.equalsIgnoreCase("HC19")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.HC19));
            } else if (policyCode.equalsIgnoreCase("HC20")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.HC20));
            }

            // Reinstate
            else if (policyCode.equalsIgnoreCase("R_HCP1")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.R_HCP1));
            }
        } catch (Exception e) {
            log.debug("Error while querying for " + policyCode + " policy Number");
            log.error(e.getMessage());
            throw new Exception(e.getMessage());
        }
    }

    private void queryForPersonalAccidentPolicyNumber(String policyCode) throws Exception {
        try {
            log.debug("Running Query For " + policyCode);
            if (policyCode.equalsIgnoreCase("AD1")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD1));
            } else if (policyCode.equalsIgnoreCase("AD2")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD2));
            } else if (policyCode.equalsIgnoreCase("AD3")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD3));
            } else if (policyCode.equalsIgnoreCase("AD4")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD4));
            } else if (policyCode.equalsIgnoreCase("AD5")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD5));
            } else if (policyCode.equalsIgnoreCase("AD6")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD6));
            } else if (policyCode.equalsIgnoreCase("AD7")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD7));
            } else if (policyCode.equalsIgnoreCase("AD8")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD8));
            } else if (policyCode.equalsIgnoreCase("AD9")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD9));
            } else if (policyCode.equalsIgnoreCase("AD10")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD10));
            } else if (policyCode.equalsIgnoreCase("AD11")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD11));
            } else if (policyCode.equalsIgnoreCase("AD12")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD12));
            } else if (policyCode.equalsIgnoreCase("AD13")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD13));
            } else if (policyCode.equalsIgnoreCase("AD14")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD14));
            } else if (policyCode.equalsIgnoreCase("AD15")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD15));
            } else if (policyCode.equalsIgnoreCase("AD16")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD16));
            } else if (policyCode.equalsIgnoreCase("AD17")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD17));
            } else if (policyCode.equalsIgnoreCase("AD18")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD18));
            } else if (policyCode.equalsIgnoreCase("AD19")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD19));
            } else if (policyCode.equalsIgnoreCase("AD20")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD20));
            } else if (policyCode.equalsIgnoreCase("AD21")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD21));
            } else if (policyCode.equalsIgnoreCase("AD22")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD22));
            } else if (policyCode.equalsIgnoreCase("AD23")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD23));
            } else if (policyCode.equalsIgnoreCase("AD24")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD24));
            } else if (policyCode.equalsIgnoreCase("AD25")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD25));
            } else if (policyCode.equalsIgnoreCase("AD26")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD26));
            } else if (policyCode.equalsIgnoreCase("AD27")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD27));
            } else if (policyCode.equalsIgnoreCase("AD28")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD28));
            } else if (policyCode.equalsIgnoreCase("AD29")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD29));
            } else if (policyCode.equalsIgnoreCase("AD30")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD30));
            } else if (policyCode.equalsIgnoreCase("AD31")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD31));
            } else if (policyCode.equalsIgnoreCase("AD32")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD32));
            } else if (policyCode.equalsIgnoreCase("AD33")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD33));
            } else if (policyCode.equalsIgnoreCase("AD34")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD34));
            } else if (policyCode.equalsIgnoreCase("AD35")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD35));
            } else if (policyCode.equalsIgnoreCase("AD36")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD36));
            } else if (policyCode.equalsIgnoreCase("AD37")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD37));
            } else if (policyCode.equalsIgnoreCase("AD38")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD38));
            } else if (policyCode.equalsIgnoreCase("AD39")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD39));
            } else if (policyCode.equalsIgnoreCase("AD40")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD40));
            } else if (policyCode.equalsIgnoreCase("AD41")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD41));
            } else if (policyCode.equalsIgnoreCase("AD42")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD42));
            } else if (policyCode.equalsIgnoreCase("AD43")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD43));
            } else if (policyCode.equalsIgnoreCase("AD44")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD44));
            } else if (policyCode.equalsIgnoreCase("AD45")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD45));
            } else if (policyCode.equalsIgnoreCase("AD46")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD46));
            } else if (policyCode.equalsIgnoreCase("AD47")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD47));
            } else if (policyCode.equalsIgnoreCase("AD48")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD48));
            } else if (policyCode.equalsIgnoreCase("AD49")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD49));
            } else if (policyCode.equalsIgnoreCase("AD50")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD50));
            } else if (policyCode.equalsIgnoreCase("AD51")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD51));
            } else if (policyCode.equalsIgnoreCase("AD52")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD52));
            } else if (policyCode.equalsIgnoreCase("AD53")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD53));
            } else if (policyCode.equalsIgnoreCase("AD54")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD54));
            } else if (policyCode.equalsIgnoreCase("AD55")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD55));
            } else if (policyCode.equalsIgnoreCase("AD56")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD56));
            } else if (policyCode.equalsIgnoreCase("AD57")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD57));
            } else if (policyCode.equalsIgnoreCase("AD58")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD58));
            } else if (policyCode.equalsIgnoreCase("AD59")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD59));
            } else if (policyCode.equalsIgnoreCase("AD60")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD60));
            } else if (policyCode.equalsIgnoreCase("AD61")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD61));
            } else if (policyCode.equalsIgnoreCase("AD62")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD62));
            } else if (policyCode.equalsIgnoreCase("AD63")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD63));
            } else if (policyCode.equalsIgnoreCase("AD64")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD64));
            } else if (policyCode.equalsIgnoreCase("AD65")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD65));
            } else if (policyCode.equalsIgnoreCase("AD66")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD66));
            } else if (policyCode.equalsIgnoreCase("AD67")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD67));
            } else if (policyCode.equalsIgnoreCase("AD68")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.AD68));
            }
        } catch (Exception e) {
            log.debug("Error while querying for " + policyCode + " policy Number");
            log.error(e.getMessage());
            throw new Exception(e.getMessage());
        }
    }

    private void queryForPayProtect(String policyCode) throws Exception {
        try {
            log.debug("Running Query For " + policyCode);
            if (policyCode.equalsIgnoreCase("PP1")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.PP1));
            } else if (policyCode.equalsIgnoreCase("C_PP1")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.C_PP1));
            }
        } catch (Exception e) {
            log.debug("Error while querying for " + policyCode + " policy Number");
            log.error(e.getMessage());
            throw new Exception(e.getMessage());
        }
    }

    private void queryForCoverForLife(String policyCode) throws Exception {
        try {
            log.debug("Running Query For " + policyCode);
            if (policyCode.equalsIgnoreCase("C4L1")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.C4L1));
            } else if (policyCode.equalsIgnoreCase("C_C4L1")) {
                resultSet = statement.executeQuery(String.valueOf(Queries.C_C4L1));
            }
        } catch (Exception e) {
            log.debug("Error while querying for " + policyCode + " policy Number");
            log.error(e.getMessage());
            throw new Exception(e.getMessage());
        }
    }

    private String formatPolicyNumber(ResultSet resultSet, String productName) {
        String policyNumber;
        String actualPolicyNumber;
        log.debug("Formatting Policy Number");
        try {
            if (resultSet.next()) {
                policyNumber = resultSet.getString("policyno").trim();
                dbPolicyNumber = policyNumber;
                log.debug("db Policy Number: " + dbPolicyNumber);

                String prefix ="";
                String suffix="";
                String middleValue="";

                if (productName.equalsIgnoreCase("Funeral Insurance")) {
                    prefix = policyNumber.substring(0, 2);
                    suffix = policyNumber.substring(2, 9);
                    log.info("Prefix: " + prefix);
                    log.info("Suffix: " + suffix);
                    middleValue = "0000";
                } else if (productName.equalsIgnoreCase("Health Cash Plan") || productName.equalsIgnoreCase("Health Cash")) {
                    int size = policyNumber.length();
                    int diff = 13 - size;

                    if (policyNumber.toUpperCase().contains("HCP")) {
                        prefix =  policyNumber.substring(0, 3);
                        suffix = policyNumber.substring(3, size);
                        log.info("Prefix: " + prefix);
                        log.info("Suffix: " + suffix);
                        for (int i = 0; i < diff ; i++) {
                            middleValue += "0";
                        }
                    } else if (policyNumber.toUpperCase().contains("HC")) {
                        prefix =  policyNumber.substring(0, 2);
                        suffix = policyNumber.substring(2, size);
                        log.info("Prefix: " + prefix);
                        log.info("Suffix: " + suffix);
                        for (int i = 0; i < diff ; i++) {
                            middleValue += "0";
                        }
                    }
                } else if (productName.contains("LOC")) {
                    prefix = "LC";
                    suffix = policyNumber;
                    int size = policyNumber.length();
                    int diff = 13 - size;

                    for (int i = 0; i < diff; i++){
                        middleValue += "0";
                    }
                } else if (productName.toLowerCase().contains("Personal Accident".toLowerCase())) {
                    prefix = "CP";
                    suffix = policyNumber;
                    int size = policyNumber.length();
                    int diff = 11 - size;

                    for (int i = 0; i < diff; i++){
                        middleValue += "0";
                    }
                } else if (productName.equalsIgnoreCase("Pay Protect")) {
                    int size = policyNumber.length();
                    int diff = 13 - size;
                    prefix =  policyNumber.substring(0, 2);
                    suffix = policyNumber.substring(2, size);
                    log.info("Prefix: " + prefix);
                    log.info("Suffix: " + suffix);

                    for (int i = 0; i < diff; i++){
                        middleValue += "0";
                    }
                } else if (productName.equalsIgnoreCase("Cover For Life")) {
                    int size = policyNumber.length();
                    int diff = 13 - size;
                    prefix =  policyNumber.substring(0, 2);
                    suffix = policyNumber.substring(2, size);
                    log.info("Prefix: " + prefix);
                    log.info("Suffix: " + suffix);

                    for (int i = 0; i < diff; i++){
                        middleValue += "0";
                    }
                }

                actualPolicyNumber = prefix + middleValue + suffix;
                log.debug("Policy Number: " + actualPolicyNumber);
                return actualPolicyNumber;
            } else {
                throw new SQLException("No Policy Numbers Found on the database");
            }
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return null;
    }

    public void updateWorkFlowItemSet(String policyNumber, String productName) {
        log.debug("Updating Work Flow Item Set");
        try {
            if (productName.contains("Funeral")) {
                statement.executeUpdate("UPDATE WORKFLOW.WORKFLOW_ITEM SET POLICY_NUMBER = 'FN00000000002' WHERE POLICY_NUMBER = '" + policyNumber + "'");
            } else if (productName.contains("LOC")){
                statement.executeUpdate("UPDATE WORKFLOW.WORKFLOW_ITEM SET POLICY_NUMBER = 'LC0000001684923' WHERE POLICY_NUMBER = '" + policyNumber + "'");
            } else if (productName.equalsIgnoreCase("Personal Accident")) {
                statement.executeUpdate("UPDATE WORKFLOW.WORKFLOW_ITEM SET POLICY_NUMBER = 'CP00230434885' WHERE POLICY_NUMBER = '" + policyNumber + "'");
            } else if (productName.equalsIgnoreCase("Health Cash")) {
                statement.executeUpdate("UPDATE WORKFLOW.WORKFLOW_ITEM SET POLICY_NUMBER = 'HC00000000001' WHERE POLICY_NUMBER = '" + policyNumber + "'");
            } else if (productName.equalsIgnoreCase("Health Cash Plan")) {
                statement.executeUpdate("UPDATE WORKFLOW.WORKFLOW_ITEM SET POLICY_NUMBER = 'HC000000P2539' WHERE POLICY_NUMBER = '" + policyNumber + "'");
            } else if (productName.equalsIgnoreCase("Funeral Insurance")) {
                statement.executeUpdate("UPDATE WORKFLOW.WORKFLOW_ITEM SET POLICY_NUMBER = 'FI00000000001' WHERE POLICY_NUMBER = '" + policyNumber + "'");
            }
        } catch (SQLException e) {
            log.debug("Updating Work Flow Item Set: " + e.getMessage());
        }
    }

    public void updateFicaStatus(String dbPolicyNumber, String risk, String sanction, String edd, String kyc, String status, String updatedTe) {
        log.debug("Updating Policy using DB Number: " + dbPolicyNumber);

        connectToDatabase();
        createStatement();

        try {
            // Note: The value is read as a double fro the datasheet, so we want it to be a plain whole number.
            if (risk.contains(".")) {
                risk = risk.split("\\.")[0];
            }

            // The date is formatted the opposite way, so only if this is the case we rearrange it.
            String[] dateObjectArray = updatedTe.split("-");
            if (dateObjectArray[2].length() == 4) {
                updatedTe = dateObjectArray[2] + "-" + dateObjectArray[1] + "-" + dateObjectArray[0];
            }

            statement.executeUpdate("UPDATE genplib.gsdficsvpf SET RISK = '"+ risk +"', SANCTION = '"+ sanction +"', EDD = '"+ edd +"', KYC = '"+ kyc+"'" +
                    ", STATUS = '"+ status+"', UPDATEDTE = '"+updatedTe+"' WHERE POLICYNO = '" + dbPolicyNumber + "'");
        } catch (Exception e){
            e.getStackTrace();
            log.error("Failed to update table");
        } finally {
            closeDatabaseConnection();
        }
    }

    public void addFirstPremiumRecord() {
        log.debug("Adding First Premium");

        connectToDatabase();
        createStatement();

        try {
            log.debug("Adding Flag Records");
            resultSet = statement.executeQuery("SELECT * FROM PP_PLIB.PPPOLMPF\n" +
                    "WHERE POLICYNO NOT IN (\n" +
                    "\tSELECT DISTINCT POLICY.POLICYNO \n" +
                    "\tFROM PP_PLIB.PPPOLMPF POLICY\n" +
                    "\tINNER JOIN PP_PLIB.PPFLAGPF FLAGS\n" +
                    "\tON POLICY.ID = FLAGS.RECORD_ID\n" +
                    "\tINNER JOIN PP_PLIB.PPROLPOPF ROLE\n" +
                    "\tON POLICY.ID = ROLE.POLICY_ID\n" +
                    "\tINNER JOIN LEGALPLIB.LSDROLEPF ROLEPF\n" +
                    "\tON ROLE.ROLE_ID = ROLEPF.ID\n" +
                    "\tLIMIT 20000\n" +
                    ")\n" +
                    "AND STATUS_ID = '6' LIMIT 20000");

            int count = 0;
            while (resultSet.next()) {
                String recordId = resultSet.getString("ID");

                if (count++ == 0) {
                    createStatement();
                }
                statement.execute("INSERT INTO PP_PLIB.PPFLAGPF (RECORD_ID, CODES_ID, FLAG, USERCAPT) VALUES ('" + recordId + "', '98', 'Y', 'FNBTTEST5')");
            }

            log.debug("Adding First Premium");
            // createStatement();
            resultSet = statement.executeQuery("SELECT POLICY.POLICYNO, POLICY.ID, FLAGS.RECORD_ID, FLAGS.FLAG, FLAGS.CODES_ID\n" +
                    "FROM PP_PLIB.PPPOLMPF POLICY\n" +
                    "INNER JOIN PP_PLIB.PPFLAGPF FLAGS\n" +
                    "ON POLICY.ID = FLAGS.RECORD_ID\n" +
                    "WHERE POLICY.STATUS_ID = '6' AND FLAGS.FLAG = 'Y'");

            count = 0;
            while (resultSet.next()) {
                String policyNo = resultSet.getString("POLICYNO").trim();
                if (count++ == 0) {
                    createStatement();
                }
                statement.executeUpdate("INSERT INTO GENPLIB.GSDFIRSTPF (SYSTEM,POLICYNO,USERCAPT) VALUES ('PAYPROTECT','" + policyNo +"','FNBTTEST5')");
            }

        } catch (Exception e){
            e.getStackTrace();
            log.error("Failed to update table");
        } finally {
            closeDatabaseConnection();
        }
    }

    public String getPolicyId(String productName) throws Exception {

        connectToDatabase();
        createStatement();

        String policyId = null;
        try {
            log.debug("Querying For " + dbPolicyNumber);

            if (productName.contains("Funeral")) {
                resultSet = statement.executeQuery("SELECT * FROM FUNINS_P.FIPOLMPF WHERE POLICYNO = '" + dbPolicyNumber + "' LIMIT 1");
            } else if (productName.equalsIgnoreCase("Health Cash Plan") || productName.equalsIgnoreCase("Health Cash")) {
                resultSet = statement.executeQuery("SELECT * FROM HOSTCAPLIB.HCPPOLMPF WHERE POLICYNO = '" + dbPolicyNumber + "' LIMIT 1");
            } else if (productName.equalsIgnoreCase("Accidental Death") || productName.equalsIgnoreCase("Personal Accident")) {
                resultSet = statement.executeQuery("SELECT * FROM PA_PLIB.PAPOLMPF WHERE POLICYNO = '" + dbPolicyNumber + "' LIMIT 1");
            } else if (productName.equalsIgnoreCase("Pay Protect")) {
                resultSet = statement.executeQuery("SELECT * FROM PP_PLIB.PPPOLMPF WHERE POLICYNO = '" + dbPolicyNumber + "' LIMIT 1");
            } else if (productName.equalsIgnoreCase("Cover For Life")) {
                resultSet = statement.executeQuery("SELECT * FROM COV4LIFEP.C4LPOLMPF WHERE POLICYNO = '" + dbPolicyNumber + "' LIMIT 1");
            } else if (productName.contains("LIFESTYLE PROTECTION")) {
                resultSet = statement.executeQuery("SELECT * FROM LP_PLIB.LPPOLMPF WHERE POLICYNO = '" + dbPolicyNumber + "' LIMIT 1");
            }

            if (resultSet.next()) {
                policyId = resultSet.getString("ID").trim();
                log.debug("POLICY ID: " + policyId);
            } else {
                throw new FicaStatusException("FICA ERROR: SQL Failed to retrieve policy id for policy number: " + dbPolicyNumber);
            }
        } catch (Exception e) {
            log.debug("Error while getting Policy Id for " + dbPolicyNumber + " policy Number");
            log.error(e.getMessage());
            throw new Exception(e.getMessage());
        } finally {
            closeDatabaseConnection();
        }
        return policyId;
    }

    public void insertFicaRecord(String productName, String risk, String sanction, String edd, String kyc, String status, String updatedTe) throws Exception {
        log.debug("Inserting FICA Record Policy using DB Number: " + dbPolicyNumber);
        String policyId = getPolicyId(productName);

        connectToDatabase();
        createStatement();

        try {
            // Note: The value is read as a double fro the datasheet, so we want it to be a plain whole number.
            if (risk.contains(".")) {
                risk = risk.split("\\.")[0];
            }

            // The date is formatted the opposite way, so only if this is the case we rearrange it.
            String[] dateObjectArray = updatedTe.split("-");
            if (dateObjectArray[2].length() == 4) {
                updatedTe = dateObjectArray[2] + "-" + dateObjectArray[1] + "-" + dateObjectArray[0];
            }

            if (policyId != null) {
                if (productName.contains("Funeral")) {
                    statement.executeUpdate("DELETE FROM GENPLIB.GSDFICSVPF WHERE POLICYNO IN ('" + dbPolicyNumber + "')");
                    statement.executeUpdate("INSERT INTO GENPLIB.GSDFICSVPF (SYSTEM,POLICYNO,POLICYID,UPDATEDTE,RISK,SANCTION,EDD,KYC,STATUS) VALUES ('FUNINS','" + dbPolicyNumber + "'," + policyId + ",{d '" + updatedTe + "'},'" + risk + "','" + sanction + "','" + edd + "','" + kyc + "','" + status + "')");
                } else if (productName.equalsIgnoreCase("Health Cash Plan") || productName.equalsIgnoreCase("Health Cash")) {
                    statement.executeUpdate("DELETE FROM GENPLIB.GSDFICSVPF WHERE POLICYNO IN ('" + dbPolicyNumber + "')");
                    statement.executeUpdate("INSERT INTO GENPLIB.GSDFICSVPF (SYSTEM,POLICYNO,POLICYID,UPDATEDTE,RISK,SANCTION,EDD,KYC,STATUS) VALUES ('HOSPITAL CASH PLAN','" + dbPolicyNumber + "'," + policyId + ",{d '" + updatedTe + "'},'" + risk + "','" + sanction + "','" + edd + "','" + kyc + "','" + status + "')");
                } else if (productName.equalsIgnoreCase("Accidental Death") || productName.equalsIgnoreCase("Personal Accident")) {
                    statement.executeUpdate("DELETE FROM GENPLIB.GSDFICSVPF WHERE POLICYNO IN ('" + dbPolicyNumber + "')");
                    statement.executeUpdate("INSERT INTO GENPLIB.GSDFICSVPF (SYSTEM,POLICYNO,POLICYID,UPDATEDTE,RISK,SANCTION,EDD,KYC,STATUS) VALUES ('PERSONAL ACCIDENT','" + dbPolicyNumber + "'," + policyId + ",{d '" + updatedTe + "'},'" + risk + "','" + sanction + "','" + edd + "','" + kyc + "','" + status + "')");
                } else if (productName.equalsIgnoreCase("Pay Protect")) {
                    statement.executeUpdate("DELETE FROM GENPLIB.GSDFICSVPF WHERE POLICYNO IN ('" + dbPolicyNumber + "')");
                    statement.executeUpdate("INSERT INTO GENPLIB.GSDFICSVPF (SYSTEM,POLICYNO,POLICYID,UPDATEDTE,RISK,SANCTION,EDD,KYC,STATUS) VALUES ('PAYPROTECT','" + dbPolicyNumber + "'," + policyId + ",{d '" + updatedTe + "'},'" + risk + "','" + sanction + "','" + edd + "','" + kyc + "','" + status + "')");
                } else if (productName.equalsIgnoreCase("Cover For Life")) {
                    statement.executeUpdate("DELETE FROM GENPLIB.GSDFICSVPF WHERE POLICYNO IN ('" + dbPolicyNumber + "')");
                    statement.executeUpdate("INSERT INTO GENPLIB.GSDFICSVPF (SYSTEM,POLICYNO,POLICYID,UPDATEDTE,RISK,SANCTION,EDD,KYC,STATUS) VALUES ('COVER 4 LIFE','" + dbPolicyNumber + "'," + policyId + ",{d '" + updatedTe + "'},'" + risk + "','" + sanction + "','" + edd + "','" + kyc + "','" + status + "')");
                } else if (productName.contains("LIFESTYLE PROTECTION")) {
                    statement.executeUpdate("DELETE FROM GENPLIB.GSDFICSVPF WHERE POLICYNO IN ('" + dbPolicyNumber + "')");
                    statement.executeUpdate("INSERT INTO GENPLIB.GSDFICSVPF (SYSTEM,POLICYNO,POLICYID,UPDATEDTE,RISK,SANCTION,EDD,KYC,STATUS) VALUES ('LIFESTYLE PROTECTOR ','" + dbPolicyNumber + "'," + policyId + ",{d '" + updatedTe + "'},'" + risk + "','" + sanction + "','" + edd + "','" + kyc + "','" + status + "')");
                }
            }
        } catch (Exception e){
            e.getStackTrace();
            log.error("Failed Inserting FICA Record");
        } finally {
            closeDatabaseConnection();
        }
    }

    public void updateFicaStatus(String risk, String sanction, String edd, String kyc, String status, String updatedTe) {
        log.debug("Updating Policy using DB Number: " + dbPolicyNumber);

        connectToDatabase();
        createStatement();

        try {
            // Note: The value is read as a double fro the datasheet, so we want it to be a plain whole number.
            if (risk.contains(".")) {
                risk = risk.split("\\.")[0];
            }

            // The date is formatted the opposite way, so only if this is the case we rearrange it.
            String[] dateObjectArray = updatedTe.split("-");
            if (dateObjectArray[2].length() == 4) {
                updatedTe = dateObjectArray[2] + "-" + dateObjectArray[1] + "-" + dateObjectArray[0];
            }

            statement.executeUpdate("UPDATE genplib.gsdficsvpf SET RISK = '"+ risk +"', SANCTION = '"+ sanction +"', EDD = '"+ edd +"', KYC = '"+ kyc+"'" +
                    ", STATUS = '"+ status+"', UPDATEDTE = '"+updatedTe+"' WHERE POLICYNO = '" + dbPolicyNumber + "'");
        } catch (Exception e){
            e.getStackTrace();
            log.error("Failed to update table");
        } finally {
            closeDatabaseConnection();
        }
    }

    public void updateUCNNumber(String policyNumber, String roleId, String idNumber, String ucnNumber)  throws Exception {

        connectToDatabase();
        createStatement();

        try {
            log.debug("Updating UCN Number For Policy Number: " + policyNumber + " , Role Id: " + roleId + " to UCN Number: " + ucnNumber);
            statement.executeUpdate("UPDATE LEGALPLIB.LSDUCNPF SET UCNNO = '" + ucnNumber + "' WHERE ROLE_ID = '" + roleId + "' AND IDNUMBER = '" + idNumber + "'");
        } catch (Exception e) {
            log.error("Error while Updating UCN Number For Policy Number: " + policyNumber + " , Role Id: " + roleId + " to UCN Number: " + ucnNumber);
            throw new Exception("Error while Updating UCN Number For Policy Number: " + policyNumber + " , Role Id: " + roleId + " to UCN Number: " + ucnNumber);
        } finally {
            closeDatabaseConnection();
        }
    }

    public void addLeadNumber(String leadNumber, String accountNumber, String branchCode, String accountType, String userCapt, String ucnNumber)  throws Exception {

        connectToDatabase();
        createStatement();

        try {
            log.debug("Adding Lead Number: " + leadNumber);

            resultSet = statement.executeQuery("SELECT * FROM GENPLIB.GSD00080PF WHERE LEADSNO = '" + leadNumber + "'");

            if (!resultSet.next()) {
                statement.executeUpdate("INSERT INTO GENPLIB.GSD00080PF " +
                        "(LEADSNO,ACCNO,BRANCHCODE,ACCTYPE,USERCAPT,UCNNO)" +
                        "VALUES ('" + leadNumber + "','" + accountNumber + "','" + branchCode + "','" + accountType + "','" + userCapt + "','" + ucnNumber + "')");
            }
            Thread.sleep(3000);
        } catch (Exception e) {
            log.error("Error while Adding Lead Number: " + leadNumber);
            throw new Exception("Error while Adding Lead Number: " + leadNumber);
        } finally {
            closeDatabaseConnection();
            Thread.sleep(3000);
        }
    }

    ///Update date of birth

    public void updateDOBPrincipalMember(String policyNumber,String dateOfbirth,String gender)throws Exception{
        connectToDatabase();
        createStatement();
        try{
            log.debug("Updating Date of Birth and Gender for Policy Number:  "+policyNumber);
            statement.execute("UPDATE LEGALPLIB.LSDROLEPF\n" +
                    "SET BIRTHDTE = '"+dateOfbirth+"', GENDER = '"+gender+"'\n" +
                    "WHERE ID IN (\n" +
                    "    SELECT ROLE_ID FROM LEGALPLIB.LCPOLMPF POLICY\n" +
                    "    INNER JOIN LEGALPLIB.LCROLPOPF ROLE\n" +
                    "    ON POLICY.ID = ROLE.POLICY_ID\n" +
                    "    WHERE ROLE.ROLETYP_ID = '17'\n" +
                    "    AND POLICYNO = '"+policyNumber+"'\n" +
                    ")");
        }catch (Exception e){
            log.error("Updating Date of Birth and Gender for Policy Number"+policyNumber);
            throw  new Exception("Error while Updating Date of Birth and Gender for Policy Number:"+ policyNumber );
        }finally {
            closeDatabaseConnection();
        }

    }

    public ResultSet queryForPolicyNumberOnFicaTable(String policyNumber) throws Exception {

        connectToDatabase();
        createStatement();

        try {
            log.debug("Querying For " + policyNumber);
            resultSet = statement.executeQuery("SELECT * FROM genplib.gsdficsvpf WHERE POLICYNO = '" + policyNumber + "' LIMIT 1");
        } catch (Exception e) {
            log.debug("Error while querying for " + policyNumber + " policy Number");
            log.error(e.getMessage());
            throw new Exception(e.getMessage());
        } finally {
            closeDatabaseConnection();
        }
        return resultSet;
    }

    public ResultSet updateUCN(String policyNumber, String ucn, String productName) throws Exception {
        connectToDatabase();
        createStatement();

        try {
            if (productName.equalsIgnoreCase("Funeral Insurance")) {
                resultSet = statement.executeQuery("SELECT POLICY.ID, POLID, UCNPF.UCNNO, POLICYNO, POLNO, ROLE.ROLE_ID, AMOUNT, BANK.* FROM FUNINS_P.FIPOLMPF POLICY\n" +
                        "INNER JOIN GENPLIB.GSDARRSTPF ARR\n" +
                        "ON POLICY.POLICYNO = ARR.POLNO\n" +
                        "INNER JOIN FUNINS_P.FIROLPOPF ROLE\n" +
                        "ON POLICY.ID = ROLE.POLICY_ID\n" +
                        "INNER JOIN LEGALPLIB.LSDUCNPF UCNPF\n" +
                        "ON ROLE.ROLE_ID = UCNPF.ROLE_ID\n" +
                        "INNER JOIN LEGALPLIB.LSDBANKPF BANK\n" +
                        "ON POLICY.BANK_ID = BANK.ID\n" +
                        "WHERE POLICY.POLICYNO = '" + policyNumber + "'");
            } else if (productName.equalsIgnoreCase("Health Cash Plan") || productName.equalsIgnoreCase("Health Cash")) {
                resultSet = statement.executeQuery("SELECT POLICY.ID, POLID, UCNPF.UCNNO, POLICYNO, POLNO, ROLE.ROLE_ID, AMOUNT, BANK.* FROM HOSTCAPLIB.HCPPOLMPF POLICY\n" +
                        "INNER JOIN GENPLIB.GSDARRSTPF ARR\n" +
                        "ON POLICY.POLICYNO = ARR.POLNO\n" +
                        "INNER JOIN HOSTCAPLIB.HCPROLPOPF ROLE\n" +
                        "ON POLICY.ID = ROLE.POLICY_ID\n" +
                        "INNER JOIN LEGALPLIB.LSDUCNPF UCNPF\n" +
                        "ON ROLE.ROLE_ID = UCNPF.ROLE_ID\n" +
                        "INNER JOIN LEGALPLIB.LSDBANKPF BANK\n" +
                        "ON POLICY.BANK_ID = BANK.ID\n" +
                        "WHERE POLICY.POLICYNO = '" + policyNumber + "'");
            } else if (productName.equalsIgnoreCase("Pay Protect")) {
                resultSet = statement.executeQuery("SELECT POLICY.ID, POLID, UCNPF.UCNNO, POLICYNO, POLNO, ROLE.ROLE_ID, AMOUNT, BANK.* FROM PP_PLIB.PPPOLMPF POLICY\n" +
                        "INNER JOIN GENPLIB.GSDARRSTPF ARR\n" +
                        "ON POLICY.POLICYNO = ARR.POLNO\n" +
                        "INNER JOIN PP_PLIB.PPROLPOPF ROLE\n" +
                        "ON POLICY.ID = ROLE.POLICY_ID\n" +
                        "INNER JOIN LEGALPLIB.LSDUCNPF UCNPF\n" +
                        "ON ROLE.ROLE_ID = UCNPF.ROLE_ID\n" +
                        "INNER JOIN LEGALPLIB.LSDBANKPF BANK\n" +
                        "ON POLICY.BANK_ID = BANK.ID\n" +
                        "WHERE POLICY.POLICYNO = '" + policyNumber + "'");
            } else if (productName.contains("LIFESTYLE PROTECTION")) {
                resultSet = statement.executeQuery("SELECT POLICY.ID, POLID, UCNPF.UCNNO, POLICYNO, POLNO, ROLE.ROLE_ID, AMOUNT, BANK.* FROM PP_PLIB.PPPOLMPF POLICY\n" +
                        "INNER JOIN GENPLIB.GSDARRSTPF ARR\n" +
                        "ON POLICY.POLICYNO = ARR.POLNO\n" +
                        "INNER JOIN LP_PLIB.LPROLPOPF ROLE\n" +
                        "ON POLICY.ID = ROLE.POLICY_ID\n" +
                        "INNER JOIN LEGALPLIB.LSDUCNPF UCNPF\n" +
                        "ON ROLE.ROLE_ID = UCNPF.ROLE_ID\n" +
                        "INNER JOIN LEGALPLIB.LSDBANKPF BANK\n" +
                        "ON POLICY.BANK_ID = BANK.ID\n" +
                        "WHERE POLICY.POLICYNO = '" + policyNumber + "'");
            } else if (productName.equalsIgnoreCase("Cover For Life")) {
                resultSet = statement.executeQuery("SELECT POLICY.ID, POLID, UCNPF.UCNNO, POLICYNO, POLNO, ROLE.ROLE_ID, AMOUNT, BANK.* FROM COV4LIFEP.C4LPOLMPF POLICY\n" +
                        "INNER JOIN GENPLIB.GSDARRSTPF ARR\n" +
                        "ON POLICY.POLICYNO = ARR.POLNO\n" +
                        "INNER JOIN COV4LIFEP.C4LROLPOPF ROLE\n" +
                        "ON POLICY.ID = ROLE.POLICY_ID\n" +
                        "INNER JOIN LEGALPLIB.LSDUCNPF UCNPF\n" +
                        "ON ROLE.ROLE_ID = UCNPF.ROLE_ID\n" +
                        "INNER JOIN LEGALPLIB.LSDBANKPF BANK\n" +
                        "ON POLICY.BANK_ID = BANK.ID\n" +
                        "WHERE POLICY.POLICYNO = '" + policyNumber + "'");
            } else if (productName.equalsIgnoreCase("PERSONAL") || productName.equalsIgnoreCase("BUSINESS")) {
                resultSet = statement.executeQuery("SELECT POLICY.ID, POLID, UCNPF.UCNNO, POLICYNO, POLNO, ROLE.ROLE_ID, AMOUNT, BANK.* FROM LEGALPLIB.LCPOLMPF POLICY\n" +
                        "INNER JOIN GENPLIB.GSDARRSTPF ARR\n" +
                        "ON POLICY.POLICYNO = ARR.POLNO\n" +
                        "INNER JOIN LEGALPLIB.LCROLPOPF ROLE\n" +
                        "ON POLICY.ID = ROLE.POLICY_ID\n" +
                        "INNER JOIN LEGALPLIB.LSDUCNPF UCNPF\n" +
                        "ON ROLE.ROLE_ID = UCNPF.ROLE_ID\n" +
                        "INNER JOIN LEGALPLIB.LSDBANKPF BANK\n" +
                        "ON POLICY.BANK_ID = BANK.ID\n" +
                        "WHERE POLICY.POLICYNO = '" + policyNumber + "'");
            } else if (productName.equalsIgnoreCase("Accidental Death") || productName.equalsIgnoreCase("Personal Accident")) {
                // TODO: Logic to insert and update UCN records
                log.info("Policy Number: " + policyNumber);
                if (policyNumber.contains("CP")) {
                    policyNumber = policyNumber.replace("CP", "");
                    log.info("Policy Number: " + policyNumber);
                }

                resultSet = statement.executeQuery("SELECT * FROM GENPLIB.GSDSYNC1PF WHERE POLICYNO = '" + policyNumber + "'");

                if (resultSet.next()) {
                    statement.executeUpdate("UPDATE GENPLIB.GSDSYNC1PF SET UCN = '" + ucn + "' WHERE POLICYNO = '" + policyNumber + "'");
                } else {
                    resultSet = statement.executeQuery("SELECT * FROM PA_PLIB.PAPOLMPF WHERE POLICYNO = '" + policyNumber + "'");
                    String id = "", statusID = "";
                    if (resultSet.next()) {
                        id = resultSet.getString("ID");
                        statusID = resultSet.getString("STATUS_ID");
                    }
                    statement.executeUpdate("INSERT INTO GENPLIB.GSDSYNC1PF \n" +
                            "(ACTION,SYSTEM,POLICYNO,POLICY_ID,POLSTAT_ID,POLSTATCDE,PREMIUM,INCEPTDTE,SENDTOBANK," +
                            "PRDCDE,SUBPRDCDE,PRODUCT_ID,USERCAPT,IDNUMBER,UCN,CISREFNO,MSG,TOTALCOVER) \n" +
                            "VALUES ('B','PERSONAL ACCIDENT','" + policyNumber + "','" + id + "','" + statusID + "','C',132.00," +
                            "{d '2021-02-22'},'N','FIS','PA',2,'F4993667','','" + ucn + "','','SENT TO CIS',0.00)");
                }
                return resultSet;
            }

            StringBuilder stringBuilder = new StringBuilder();
            while (resultSet.next()) {
                String roleId = resultSet.getString("ROLE_ID").trim();
                log.debug("ROLE_ID: " + roleId );
                stringBuilder.append("'").append(roleId).append("',");
            }
            String string = stringBuilder.toString();
            int length = string.length();
            if (length > 0) {
                stringBuilder.deleteCharAt(length - 1);
                String roleIds = stringBuilder.toString();
                log.info("Role Ids: " + roleIds);

                createStatement();

                statement.executeUpdate("UPDATE LEGALPLIB.LSDUCNPF SET UCNNO = '" + ucn + "' WHERE ROLE_ID IN (" + roleIds + ")");

            }
        } catch (Exception e) {
            log.debug("Error While Updating UCN For Policy Number: " + policyNumber + " :#: " + e.getMessage());
            throw new Exception("Error While Updating UCN For Policy Number: " + policyNumber + " :#: " + e.getMessage());
        } finally {
            closeDatabaseConnection();
        }
        return resultSet;
    }

    public ResultSet queryForPolicyNumberOnFicaTable() throws Exception {

        connectToDatabase();
        createStatement();

        try {
            log.debug("Querying For " + dbPolicyNumber);
            resultSet = statement.executeQuery("SELECT * FROM genplib.gsdficsvpf WHERE POLICYNO = '" + dbPolicyNumber + "' LIMIT 1");
        } catch (Exception e) {
            log.debug("Error while querying for " + dbPolicyNumber + " policy Number");
            log.error(e.getMessage());
            throw new Exception(e.getMessage());
        } finally {
            closeDatabaseConnection();
        }
        return resultSet;
    }

    public ResultSet getPolicyNumberQueryResults(String stringSqlQuery) throws Exception {

        connectToDatabase();
        createStatement();

        try {
            log.debug("Running Policy Number Query From Database");
            resultSet = statement.executeQuery(stringSqlQuery);
        } catch (Exception e) {
            log.debug("Error while Running Policy Number Query");
            log.error(e.getMessage());
            throw new Exception(e.getMessage());
        } finally {
            closeDatabaseConnection();
        }
        return resultSet;
    }

    public ResultSet queryForPolicyInformation(String policyNumber) throws Exception {

        connectToDatabase();
        createStatement();

        try {
            log.debug("Querying For " + policyNumber);
            resultSet = statement.executeQuery("SELECT * FROM GENPLIB.GSDESC30PF WHERE POLICYNO  = '" + policyNumber + "'");
        } catch (Exception e) {
            log.debug("Error while querying for " + policyNumber + " policy information");
            log.error(e.getMessage());
            throw new Exception(e.getMessage());
        } finally {
            closeDatabaseConnection();
        }
        return resultSet;
    }

    public boolean checkPolicyLetterFlag(String productName, String flagTypId, String policyNumber) throws Exception {

        connectToDatabase();
        createStatement();

        String recordId = null;
        try {
            log.debug("Checking Policy Flag for Policy Number: " + policyNumber + ", and FLAGTYP_ID: " + flagTypId);
            ResultSet resultSet = null;

            if (!productName.isEmpty()) {
                // 2021 Letters Products:
                if (productName.equalsIgnoreCase("Funeral Insurance")) {
                    resultSet = statement.executeQuery("SELECT POLICY.POLICYNO, POLICY.ID, FLAGS.RECORD_ID, FLAGS.FLAG, FLAGS.CODES_ID " +
                            "FROM FUNINS_P.FIPOLMPF POLICY\n" +
                            "INNER JOIN FUNINS_P.FIFLAGPF FLAGS\n" +
                            "ON POLICY.ID = FLAGS.RECORD_ID\n" +
                            "WHERE POLICY.POLICYNO = '" + policyNumber + "'");

                    recordId = "";
                    if (resultSet.next()) {
                        recordId = resultSet.getString("RECORD_ID");
                    }

                    if (!recordId.isEmpty()) {
                        resultSet = statement.executeQuery("SELECT * FROM FUNINS_P.FIFLAGPF WHERE RECORD_ID = '" + recordId + "' AND CODES_ID = '" + flagTypId + "'");
                    }
                } else if (productName.equalsIgnoreCase("Accidental Death")) {
                    resultSet = statement.executeQuery("SELECT POLICY.POLICYNO, POLICY.ID, FLAGS.RECORD_ID, FLAGS.FLAG, FLAGS.FLAGTYP_ID " +
                            "FROM PA_PLIB.PAPOLMPF POLICY\n" +
                            "INNER JOIN PANEWPLIB.OSDFLAGPF FLAGS\n" +
                            "ON POLICY.ID = FLAGS.RECORD_ID\n" +
                            "WHERE POLICY.POLICYNO = '" + policyNumber + "'");

                    recordId = "";
                    if (resultSet.next()) {
                        recordId = resultSet.getString("RECORD_ID");
                    }

                    if (!recordId.isEmpty()) {
                        resultSet = statement.executeQuery("SELECT * FROM PANEWPLIB.OSDFLAGPF WHERE RECORD_ID = '" + recordId + "' AND FLAGTYP_ID = '" + flagTypId + "'");
                    }
                } else if (productName.equalsIgnoreCase("Health Cash")) {
                    resultSet = statement.executeQuery("SELECT POLICY.POLICYNO, POLICY.ID, FLAGS.RECORD_ID, FLAGS.FLAG, FLAGS.CODES_ID " +
                            "FROM HOSTCAPLIB.HCPPOLMPF POLICY\n" +
                            "INNER JOIN HOSTCAPLIB.HCPFLAGPF FLAGS\n" +
                            "ON POLICY.ID = FLAGS.RECORD_ID\n" +
                            "WHERE POLICY.POLICYNO = '" + policyNumber + "'");

                    recordId = "";
                    if (resultSet.next()) {
                        recordId = resultSet.getString("RECORD_ID");
                    }

                    if (!recordId.isEmpty()) {
                        resultSet = statement.executeQuery("SELECT * FROM HOSTCAPLIB.HCPFLAGPF WHERE RECORD_ID = '" + recordId + "' AND CODES_ID = '" + flagTypId + "'");
                    }
                } else if (productName.equalsIgnoreCase("Hospital Cash 1")) {
                    resultSet = statement.executeQuery("SELECT POLICY.POLICYNO, POLICY.ID, FLAGS.RECORD_ID, FLAGS.FLAG, FLAGS.CODES_ID " +
                            "FROM HOSTCAPLIB.HCPPOLMPF POLICY\n" +
                            "INNER JOIN HOSTCAPLIB.HCPFLAGPF FLAGS\n" +
                            "ON POLICY.ID = FLAGS.RECORD_ID\n" +
                            "WHERE POLICY.POLICYNO = '" + policyNumber + "'");

                    recordId = "";
                    if (resultSet.next()) {
                        recordId = resultSet.getString("RECORD_ID");
                    }

                    if (!recordId.isEmpty()) {
                        resultSet = statement.executeQuery("SELECT * FROM HOSTCAPLIB.HCPFLAGPF WHERE RECORD_ID = '" + recordId + "' AND CODES_ID = '" + flagTypId + "'");
                    }
                    // TODO: Annual Statement
                } else if (productName.equalsIgnoreCase("Hospital Cash 2")) {
                    resultSet = statement.executeQuery("SELECT POLICY.POLICYNO, POLICY.ID, FLAGS.RECORD_ID, FLAGS.FLAG, FLAGS.CODES_ID " +
                            "FROM HOSTCAPLIB.HCPPOLMPF POLICY\n" +
                            "INNER JOIN HOSTCAPLIB.HCPFLAGPF FLAGS\n" +
                            "ON POLICY.ID = FLAGS.RECORD_ID\n" +
                            "WHERE POLICY.POLICYNO = '" + policyNumber + "'");

                    recordId = "";
                    if (resultSet.next()) {
                        recordId = resultSet.getString("RECORD_ID");
                    }

                    if (!recordId.isEmpty()) {
                        resultSet = statement.executeQuery("SELECT * FROM HOSTCAPLIB.HCPFLAGPF WHERE RECORD_ID = '" + recordId + "' AND CODES_ID = '" + flagTypId + "'");
                    }
                    // TODO: Annual Statement
                } else if (productName.equalsIgnoreCase("Lifestyle Protection")) {
                    resultSet = statement.executeQuery("SELECT POLICY.POLICYNO, POLICY.ID, FLAGS.RECORD_ID, FLAGS.FLAG, FLAGS.CODES_ID " +
                            "FROM LP_PLIB.LPPOLMPF POLICY\n" +
                            "INNER JOIN LP_PLIB.LPFLAGPF FLAGS\n" +
                            "ON POLICY.ID = FLAGS.RECORD_ID\n" +
                            "WHERE POLICY.POLICYNO = '" + policyNumber + "'");

                    recordId = "";
                    if (resultSet.next()) {
                        recordId = resultSet.getString("RECORD_ID");
                    }

                    if (!recordId.isEmpty()) {
                        resultSet = statement.executeQuery("SELECT * FROM LP_PLIB.LPFLAGPF WHERE RECORD_ID = '" + recordId + "' AND CODES_ID = '" + flagTypId + "'");
                    }
                } else if (productName.equalsIgnoreCase("Pay Protect")) {
                    resultSet = statement.executeQuery("SELECT POLICY.POLICYNO, POLICY.ID, FLAGS.RECORD_ID, FLAGS.FLAG, FLAGS.CODES_ID " +
                            "FROM PP_PLIB.PPPOLMPF POLICY\n" +
                            "INNER JOIN GENPLIB.GSDFLAGPF FLAGS\n" +
                            "ON POLICY.ID = FLAGS.RECORD_ID\n" +
                            "WHERE POLICY.POLICYNO = '" + policyNumber + "'");

                    recordId = "";
                    if (resultSet.next()) {
                        recordId = resultSet.getString("RECORD_ID");
                    }

                    if (!recordId.isEmpty()) {
                        resultSet = statement.executeQuery("SELECT * FROM GENPLIB.GSDFLAGPF WHERE RECORD_ID = '" + recordId + "' AND CODES_ID = '" + flagTypId + "'");
                    }
                } else if (productName.equalsIgnoreCase("Smart Cover")) {
                    resultSet = statement.executeQuery("SELECT POLICY.POLICYNO, POLICY.ID, FLAGS.RECORD_ID, FLAGS.FLAG, FLAGS.FLAGTYP_ID " +
                            "FROM PERACCPLIB.PSDPOLMPF POLICY\n" +
                            "INNER JOIN PERACCPLIB.PSDFLAGPF FLAGS\n" +
                            "ON POLICY.ID = FLAGS.RECORD_ID\n" +
                            "WHERE POLICY.POLICYNO = '" + policyNumber + "'");

                    recordId = "";
                    if (resultSet.next()) {
                        recordId = resultSet.getString("RECORD_ID");
                    }

                    if (!recordId.isEmpty()) {
                        resultSet = statement.executeQuery("SELECT * FROM PERACCPLIB.PSDFLAGPF WHERE RECORD_ID = '" + recordId + "' AND FLAGTYP_ID = '" + flagTypId + "'");
                    }
                    // TODO: Annual Statement
                } else if (productName.equalsIgnoreCase("Cover4Life")) {
                    resultSet = statement.executeQuery("SELECT POLICY.POLICYNO, POLICY.ID, FLAGS.RECORD_ID, FLAGS.FLAG, FLAGS.CODES_ID " +
                            "FROM COV4LIFEP.C4LPOLMPF POLICY\n" +
                            "INNER JOIN COV4LIFEP.C4LFLAGPF FLAGS\n" +
                            "ON POLICY.ID = FLAGS.RECORD_ID\n" +
                            "WHERE POLICY.POLICYNO = '" + policyNumber + "'");

                    recordId = "";
                    if (resultSet.next()) {
                        recordId = resultSet.getString("RECORD_ID");
                    }

                    if (!recordId.isEmpty()) {
                        resultSet = statement.executeQuery("SELECT * FROM COV4LIFEP.C4LFLAGPF WHERE RECORD_ID = '" + recordId + "' AND CODES_ID = '" + flagTypId + "'");
                    }
                    // TODO: Annual Statement
                } else if (productName.equalsIgnoreCase("FNB Life Cover (Online Life)")) {
                    resultSet = statement.executeQuery("SELECT POLICY.POLICYNO, POLICY.ID, FLAGS.RECORD_ID, FLAGS.FLAG, FLAGS.CODES_ID " +
                            "FROM OLIFE_PLIB.OLPOLMPF POLICY\n" +
                            "INNER JOIN OLIFE_PLIB.OLFLAGPF FLAGS\n" +
                            "ON POLICY.ID = FLAGS.RECORD_ID\n" +
                            "WHERE POLICY.POLICYNO = '" + policyNumber + "'");

                    recordId = "";
                    if (resultSet.next()) {
                        recordId = resultSet.getString("RECORD_ID");
                    }

                    if (!recordId.isEmpty()) {
                        resultSet = statement.executeQuery("SELECT * FROM OLIFE_PLIB.OLFLAGPF WHERE RECORD_ID = '" + recordId + "' AND CODES_ID = '" + flagTypId + "'");
                    }
                    // TODO: Annual Statement
                } else if (productName.equalsIgnoreCase("Old Funeral")) {
                    resultSet = statement.executeQuery("SELECT POLICY.POLICYNO, POLICY.ID, FLAGS.RECORD_ID, FLAGS.FLAG, FLAGS.CODES_ID " +
                            "FROM FUNERPLIB.FSDPOLMPF POLICY\n" +
                            "INNER JOIN FUNERPLIB.FSDFLAGPF FLAGS\n" +
                            "ON POLICY.ID = FLAGS.RECORD_ID\n" +
                            "WHERE POLICY.POLICYNO = '" + policyNumber + "'");

                    recordId = "";
                    if (resultSet.next()) {
                        recordId = resultSet.getString("RECORD_ID");
                    }

                    if (!recordId.isEmpty()) {
                        resultSet = statement.executeQuery("SELECT * FROM FUNERPLIB.FSDFLAGPF WHERE RECORD_ID = '" + recordId + "' AND CODES_ID = '" + flagTypId + "'");
                    }
                    // TODO: Annual Statement
                }
            } else {
                throw new Exception("No Product Name Specified.");
            }

            if (resultSet != null) {
                if (resultSet.next()) {
                    return true;
                } else {
                    return false;
                }
            } else {
                throw new Exception("Policy Number: " + policyNumber + ", Record_Id: " + recordId + " and FLAGTYP_ID: " + flagTypId);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while Checking Policy Flag for Policy Number: " + policyNumber + ", Record_Id: " + recordId + " and FLAGTYP_ID: " + flagTypId);
            throw new Exception("Error while Checking Policy Flag for Policy Number: " + policyNumber + ", Record_Id: " + recordId + " and FLAGTYP_ID: " + flagTypId);
        } finally {
            closeDatabaseConnection();
        }
    }

    public void insertLetterFlag(String recordId, String productName, String flagTypId, String flag, String emailAddr, String userCapt, String policyNumber) throws Exception {

        connectToDatabase();
        createStatement();

        try {
            log.debug("Adding Policy Flag For Letters.");

            if (!productName.isEmpty()) {
                // 2021 Letters Products:
                if (productName.equalsIgnoreCase("Funeral Insurance")) {
                    resultSet = statement.executeQuery("SELECT POLICY.POLICYNO, POLICY.ID, FLAGS.RECORD_ID, FLAGS.FLAG, FLAGS.CODES_ID " +
                            "FROM FUNINS_P.FIPOLMPF POLICY\n" +
                            "INNER JOIN FUNINS_P.FIFLAGPF FLAGS\n" +
                            "ON POLICY.ID = FLAGS.RECORD_ID\n" +
                            "WHERE POLICY.POLICYNO = '" + policyNumber + "'");

                    if (resultSet.next()) {
                        recordId = resultSet.getString("RECORD_ID");
                    }

                    if (!recordId.isEmpty()) {
                        statement.execute("INSERT INTO FUNINS_P.FIFLAGPF (RECORD_ID, CODES_ID, FLAG, USERCAPT) VALUES ('" + recordId + "', " + flagTypId + ", '" + flag + "', '" + userCapt + "')");
                    }
                } else if (productName.equalsIgnoreCase("Accidental Death")) {
                    resultSet = statement.executeQuery("SELECT POLICY.POLICYNO, POLICY.ID, FLAGS.RECORD_ID, FLAGS.FLAG, FLAGS.FLAGTYP_ID " +
                            "FROM PA_PLIB.PAPOLMPF POLICY\n" +
                            "INNER JOIN PANEWPLIB.OSDFLAGPF FLAGS\n" +
                            "ON POLICY.ID = FLAGS.RECORD_ID\n" +
                            "WHERE POLICY.POLICYNO = '" + policyNumber + "'");

                    if (resultSet.next()) {
                        recordId = resultSet.getString("RECORD_ID");
                    }

                    if (!recordId.isEmpty()) {
                        statement.execute("INSERT INTO PANEWPLIB.OSDFLAGPF (RECORD_ID, FLAGTYP_ID, FLAG, USERCAPT) VALUES ('" + recordId + "', " + flagTypId + ", '" + flag + "', '" + userCapt + "')");
                    }
                } else if (productName.equalsIgnoreCase("Health Cash")) {
                    resultSet = statement.executeQuery("SELECT POLICY.POLICYNO, POLICY.ID, FLAGS.RECORD_ID, FLAGS.FLAG, FLAGS.CODES_ID " +
                            "FROM HOSTCAPLIB.HCPPOLMPF POLICY\n" +
                            "INNER JOIN HOSTCAPLIB.HCPFLAGPF FLAGS\n" +
                            "ON POLICY.ID = FLAGS.RECORD_ID\n" +
                            "WHERE POLICY.POLICYNO = '" + policyNumber + "'");

                    if (resultSet.next()) {
                        recordId = resultSet.getString("RECORD_ID");
                    }

                    if (!recordId.isEmpty()) {
                        statement.execute("INSERT INTO HOSTCAPLIB.HCPFLAGPF (RECORD_ID, CODES_ID, FLAG, USERCAPT) VALUES ('" + recordId + "', " + flagTypId + ", '" + flag + "', '" + userCapt + "')");
                    }
                } else if (productName.equalsIgnoreCase("Hospital Cash 1")) {
                    resultSet = statement.executeQuery("SELECT POLICY.POLICYNO, POLICY.ID, FLAGS.RECORD_ID, FLAGS.FLAG, FLAGS.CODES_ID " +
                            "FROM HOSTCAPLIB.HCPPOLMPF POLICY\n" +
                            "INNER JOIN HOSTCAPLIB.HCPFLAGPF FLAGS\n" +
                            "ON POLICY.ID = FLAGS.RECORD_ID\n" +
                            "WHERE POLICY.POLICYNO = '" + policyNumber + "'");

                    if (resultSet.next()) {
                        recordId = resultSet.getString("RECORD_ID");
                    }

                    if (!recordId.isEmpty()) {
                        statement.execute("INSERT INTO HOSTCAPLIB.HCPFLAGPF (RECORD_ID, CODES_ID, FLAG, USERCAPT) VALUES ('" + recordId + "', " + flagTypId + ", '" + flag + "', '" + userCapt + "')");
                    }
                    // TODO: Annual Statement
                } else if (productName.equalsIgnoreCase("Hospital Cash 2")) {
                    resultSet = statement.executeQuery("SELECT POLICY.POLICYNO, POLICY.ID, FLAGS.RECORD_ID, FLAGS.FLAG, FLAGS.CODES_ID " +
                            "FROM HOSTCAPLIB.HCPPOLMPF POLICY\n" +
                            "INNER JOIN HOSTCAPLIB.HCPFLAGPF FLAGS\n" +
                            "ON POLICY.ID = FLAGS.RECORD_ID\n" +
                            "WHERE POLICY.POLICYNO = '" + policyNumber + "'");

                    if (resultSet.next()) {
                        recordId = resultSet.getString("RECORD_ID");
                    }

                    if (!recordId.isEmpty()) {
                        statement.execute("INSERT INTO HOSTCAPLIB.HCPFLAGPF (RECORD_ID, CODES_ID, FLAG, USERCAPT) VALUES ('" + recordId + "', " + flagTypId + ", '" + flag + "', '" + userCapt + "')");
                    }
                    // TODO: Annual Statement
                } else if (productName.equalsIgnoreCase("Lifestyle Protection")) {
                    resultSet = statement.executeQuery("SELECT POLICY.POLICYNO, POLICY.ID, FLAGS.RECORD_ID, FLAGS.FLAG, FLAGS.CODES_ID " +
                            "FROM LP_PLIB.LPPOLMPF POLICY\n" +
                            "INNER JOIN LP_PLIB.LPFLAGPF FLAGS\n" +
                            "ON POLICY.ID = FLAGS.RECORD_ID\n" +
                            "WHERE POLICY.POLICYNO = '" + policyNumber + "'");

                    if (resultSet.next()) {
                        recordId = resultSet.getString("RECORD_ID");
                    }

                    if (!recordId.isEmpty()) {
                        statement.execute("INSERT INTO LP_PLIB.LPFLAGPF (RECORD_ID, CODES_ID, FLAG, USERCAPT) VALUES ('" + recordId + "', " + flagTypId + ", '" + flag + "', '" + userCapt + "')");
                    }
                } else if (productName.equalsIgnoreCase("Pay Protect")) {
                    resultSet = statement.executeQuery("SELECT POLICY.POLICYNO, POLICY.ID, FLAGS.RECORD_ID, FLAGS.FLAG, FLAGS.CODES_ID " +
                            "FROM PP_PLIB.PPPOLMPF POLICY\n" +
                            "INNER JOIN GENPLIB.GSDFLAGPF FLAGS\n" +
                            "ON POLICY.ID = FLAGS.RECORD_ID\n" +
                            "WHERE POLICY.POLICYNO = '" + policyNumber + "'");

                    if (resultSet.next()) {
                        recordId = resultSet.getString("RECORD_ID");
                    }

                    if (!recordId.isEmpty()) {
                        statement.execute("INSERT INTO GENPLIB.GSDFLAGPF (RECORD_ID, CODES_ID, FLAG, USERCAPT) VALUES ('" + recordId + "', " + flagTypId + ", '" + flag + "', '" + userCapt + "')");
                    }
                } else if (productName.equalsIgnoreCase("Smart Cover")) {
                    resultSet = statement.executeQuery("SELECT POLICY.POLICYNO, POLICY.ID, FLAGS.RECORD_ID, FLAGS.FLAG, FLAGS.FLAGTYP_ID " +
                            "FROM PERACCPLIB.PSDPOLMPF POLICY\n" +
                            "INNER JOIN PERACCPLIB.PSDFLAGPF FLAGS\n" +
                            "ON POLICY.ID = FLAGS.RECORD_ID\n" +
                            "WHERE POLICY.POLICYNO = '" + policyNumber + "'");

                    if (resultSet.next()) {
                        recordId = resultSet.getString("RECORD_ID");
                    }

                    if (!recordId.isEmpty()) {
                        log.info("INSERT INTO PERACCPLIB.PSDFLAGPF (FLAGTYP_ID,FLAG,USERCAPT) VALUES (" + flagTypId + ",'" + flag + "','" + userCapt + "')");
                        statement.execute("INSERT INTO PERACCPLIB.PSDFLAGPF (FLAGTYP_ID,FLAG,USERCAPT) VALUES (" + flagTypId + ",'" + flag + "','" + userCapt + "')");
                    }
                    // TODO: Annual Statement
                } else if (productName.equalsIgnoreCase("Cover4Life")) {
                    resultSet = statement.executeQuery("SELECT POLICY.POLICYNO, POLICY.ID, FLAGS.RECORD_ID, FLAGS.FLAG, FLAGS.CODES_ID " +
                            "FROM COV4LIFEP.C4LPOLMPF POLICY\n" +
                            "INNER JOIN COV4LIFEP.C4LFLAGPF FLAGS\n" +
                            "ON POLICY.ID = FLAGS.RECORD_ID\n" +
                            "WHERE POLICY.POLICYNO = '" + policyNumber + "'");

                    if (resultSet.next()) {
                        recordId = resultSet.getString("RECORD_ID");
                    }

                    if (!recordId.isEmpty()) {
                        statement.execute("INSERT INTO COV4LIFEP.C4LFLAGPF (RECORD_ID, CODES_ID, FLAG, USERCAPT) VALUES ('" + recordId + "', " + flagTypId + ", '" + flag + "', '" + userCapt + "')");
                    }
                    // TODO: Annual Statement
                } else if (productName.equalsIgnoreCase("FNB Life Cover (Online Life)")) {
                    resultSet = statement.executeQuery("SELECT POLICY.POLICYNO, POLICY.ID, FLAGS.RECORD_ID, FLAGS.FLAG, FLAGS.CODES_ID " +
                            "FROM OLIFE_PLIB.OLPOLMPF POLICY\n" +
                            "INNER JOIN OLIFE_PLIB.OLFLAGPF FLAGS\n" +
                            "ON POLICY.ID = FLAGS.RECORD_ID\n" +
                            "WHERE POLICY.POLICYNO = '" + policyNumber + "'");

                    if (resultSet.next()) {
                        recordId = resultSet.getString("RECORD_ID");
                    }

                    if (!recordId.isEmpty()) {
                        statement.execute("INSERT INTO OLIFE_PLIB.OLFLAGPF (RECORD_ID, CODES_ID, FLAG, USERCAPT) VALUES ('" + recordId + "', " + flagTypId + ", '" + flag + "', '" + userCapt + "')");
                    }
                    // TODO: Annual Statement
                } else if (productName.equalsIgnoreCase("Old Funeral")) {
                    resultSet = statement.executeQuery("SELECT POLICY.POLICYNO, POLICY.ID, FLAGS.RECORD_ID, FLAGS.FLAG, FLAGS.CODES_ID " +
                            "FROM FUNERPLIB.FSDPOLMPF POLICY\n" +
                            "INNER JOIN FUNERPLIB.FSDFLAGPF FLAGS\n" +
                            "ON POLICY.ID = FLAGS.RECORD_ID\n" +
                            "WHERE POLICY.POLICYNO = '" + policyNumber + "'");

                    if (resultSet.next()) {
                        recordId = resultSet.getString("RECORD_ID");
                    }

                    if (!recordId.isEmpty()) {
                        statement.execute("INSERT INTO FUNERPLIB.FSDFLAGPF (RECORD_ID, CODES_ID, FLAG, USERCAPT) VALUES ('" + recordId + "', " + flagTypId + ", '" + flag + "', '" + userCapt + "')");
                    }
                    // TODO: Annual Statement
                }
            } else {
                throw new Exception("No Product Name Specified.");
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while inserting flag letters: " + e.getMessage());
            throw new Exception("Error while inserting flag letters: " + e.getMessage());
        } finally {
            closeDatabaseConnection();
        }
    }

    public void unflagAllCurrentlyFlaggedPolicyForLetters(String productName) throws Exception {

        connectToDatabase();
        createStatement();

        try {
            log.debug("Unflagging all flagged policies for letters.");

            if (!productName.isEmpty()) {
                // 2021 Letters Products:
                if (productName.equalsIgnoreCase("Funeral Insurance")) {
                    statement.executeUpdate("UPDATE FUNINS_P.FIFLAGPF SET Flag='N' WHERE Flag='Y'");
                } else if (productName.equalsIgnoreCase("Accidental Death")) {
                    statement.executeUpdate("UPDATE PANEWPLIB.OSDFLAGPF SET Flag='N' WHERE Flag='Y'");
                } else if (productName.equalsIgnoreCase("Health Cash")) {
                    statement.executeUpdate("UPDATE HOSTCAPLIB.HCPFLAGPF SET Flag='N' WHERE Flag='Y'");
                } else if (productName.equalsIgnoreCase("Hospital Cash 1")) {
                    statement.executeUpdate("UPDATE HOSTCAPLIB.HCPFLAGPF SET Flag='N' WHERE Flag='Y'");
                    // TODO: Annual Statement
                    /*if (letterType.equalsIgnoreCase("Annual Statement")) {
                        statement.executeUpdate("UPDATE GENPLIB.GSDSTMTSPF SET Flag='N' WHERE Flag='Y'");
                    }*/
                } else if (productName.equalsIgnoreCase("Hospital Cash 2")) {
                    statement.executeUpdate("UPDATE HOSTCAPLIB.HCPFLAGPF SET Flag='N' WHERE Flag='Y'");
                    // TODO: Annual Statement
                    /*if (letterType.equalsIgnoreCase("Annual Statement")) {
                        statement.executeUpdate("GENPLIB.GSDSTMTSPF SET Flag='N' WHERE Flag='Y'");
                    }*/
                } else if (productName.equalsIgnoreCase("Lifestyle Protection")) {
                    statement.executeUpdate("UPDATE LP_PLIB.LPFLAGPF SET Flag='N' WHERE Flag='Y'");
                } else if (productName.equalsIgnoreCase("Pay Protect")) {
                    statement.executeUpdate("UPDATE GENPLIB.GSDFLAGPF SET Flag='N' WHERE Flag='Y'");
                } else if (productName.equalsIgnoreCase("Smart Cover")) {
                    statement.executeUpdate("UPDATE PERACCPLIB.PSDFLAGPF SET Flag='N' WHERE Flag='Y'");
                    // TODO: Annual Statement
                    /*if (letterType.equalsIgnoreCase("Annual Statement")) {
                        statement.executeUpdate("UPDATE GENPLIB.GSDSTMTSPF SET Flag='N' WHERE Flag='Y'");
                    }*/
                } else if (productName.equalsIgnoreCase("Cover4Life")) {
                    statement.executeUpdate("UPDATE COV4LIFEP.C4LFLAGPF SET Flag='N' WHERE Flag='Y'");
                    // TODO: Annual Statement
                    /*if (letterType.equalsIgnoreCase("Annual Statement")) {
                        statement.executeUpdate("UPDATE GENPLIB.GSDSTMTSPF SET Flag='N' WHERE Flag='Y'");
                    }*/
                } else if (productName.equalsIgnoreCase("FNB Life Cover (Online Life)")) {
                    statement.executeUpdate("UPDATE OLIFE_PLIB.OLFLAGPF SET Flag='N' WHERE Flag='Y'");
                    // TODO: Annual Statement
                    /*if (letterType.equalsIgnoreCase("Annual Statement")) {
                        statement.executeUpdate("UPDATE GENPLIB.GSDSTMTSPF SET Flag='N' WHERE Flag='Y'");
                    }*/
                } else if (productName.equalsIgnoreCase("Old Funeral")) {
                    statement.executeUpdate("UPDATE FUNERPLIB.FSDFLAGPF SET Flag='N' WHERE Flag='Y'");
                    // TODO: Annual Statement
                    /*if (letterType.equalsIgnoreCase("Annual Statement")) {
                        statement.executeUpdate("UPDATE GENPLIB.GSDSTMTSPF SET Flag='N' WHERE Flag='Y'");
                    }*/
                }
            } else {
                throw new Exception("No Product Name Specified.");
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while Unflagging all flagged policies for letters: " + e.getMessage());
            throw new Exception("Error while Unflagging all flagged policies for letters: " + e.getMessage());
        } finally {
            closeDatabaseConnection();
        }
    }

    public String flagPolicyForLettersOld(String productName, String policyNumber) throws Exception {

        connectToDatabase();
        createStatement();

        String recordId = null;
        try {
            log.debug("Getting the Record_Id for Policy: " + policyNumber);

            if (!productName.isEmpty()) {
                if (productName.equalsIgnoreCase("Funeral Insurance")) {
                    ResultSet resultSet = statement.executeQuery("SELECT A.ID, B.ID, B.Record_Id, B.Codes_Id, B.Flag " +
                            "FROM funins_p.fipolmpf A INNER JOIN funins_p.fiflagpf B " +
                            "ON A.ID = B.Record_Id WHERE A.POLICYNO  = '" + policyNumber + "'");

                    if (resultSet.next()) {
                        recordId = resultSet.getString("Record_Id");
                        log.debug("Flagging policy: " + policyNumber + " Record_Id: " + recordId + " for letters.");
                        statement.executeUpdate("UPDATE FUNINS_P.FIFLAGPF SET FLAG='Y' WHERE record_id  = '" + recordId + "'");
                    } else {
                        recordId = "Record Id Not Found.";
                        throw new Exception(recordId);
                    }
                } else if (productName.equalsIgnoreCase("Health Cash Plan") || productName.equalsIgnoreCase("Health Cash")) {
                    ResultSet resultSet = statement.executeQuery("SELECT A.ID, B.ID, B.Record_Id, B.Codes_Id, B.Flag " +
                            "FROM hostcaplib.HCPPOLMPF A INNER JOIN hostcaplib.HCPFLAGPF B " +
                            "ON A.ID = B.Record_Id WHERE A.POLICYNO  = '" + policyNumber + "'");

                    if (resultSet.next()) {
                        recordId = resultSet.getString("Record_Id");
                        log.debug("Flagging policy: " + policyNumber + " Record_Id: " + recordId + " for letters.");
                        statement.executeUpdate("UPDATE hostcaplib.HCPFLAGPF SET FLAG='Y' WHERE record_id  = '" + recordId + "'");
                    } else {
                        recordId = "Record Id Not Found.";
                        throw new Exception(recordId);
                    }
                } else if (productName.contains("LOC")) {
                    ResultSet resultSet = statement.executeQuery("SELECT A.ID, B.ID, B.Record_Id, B.Codes_Id, B.Flag " +
                            "FROM LEGALPLIB.LCPOLMPF A INNER JOIN LEGALPLIB.LCFLAGPF B " +
                            "ON A.ID = B.Record_Id WHERE A.POLICYNO  = '" + policyNumber + "'");

                    if (resultSet.next()) {
                        recordId = resultSet.getString("Record_Id");
                        log.debug("Flagging policy: " + policyNumber + " Record_Id: " + recordId + " for letters.");
                        statement.executeUpdate("UPDATE LEGALPLIB.LCFLAGPF SET FLAG='Y' WHERE record_id  = '" + recordId + "'");
                    } else {
                        recordId = "Record Id Not Found.";
                        throw new Exception(recordId);
                    }
                } else if (productName.toLowerCase().contains("Personal Accident".toLowerCase())) {
                    // TODO: Execute Query On The Product Table.
                    ResultSet resultSet = statement.executeQuery("SELECT A.ID, B.ID, B.Record_Id, B.FLAGTYP_ID, B.Flag " +
                            "FROM PA_PLIB.PAPOLMPF A INNER JOIN PANEWPLIB.OSDFLAGPF B " +
                            "ON A.ID = B.Record_Id WHERE A.POLICYNO  = '" + policyNumber + "'");

                    if (resultSet.next()) {
                        recordId = resultSet.getString("Record_Id");
                        log.debug("Flagging policy: " + policyNumber + " Record_Id: " + recordId + " for letters.");
                        statement.executeUpdate("UPDATE PA_PLIB.PAFLAGPF SET FLAG='Y' WHERE record_id  = '" + recordId + "'");
                    } else {
                        recordId = "Record Id Not Found.";
                        throw new Exception(recordId);
                    }
                } else if (productName.equalsIgnoreCase("Pay Protect")) {
                    // TODO: Execute Query On The Product Table.
                    ResultSet resultSet = statement.executeQuery("SELECT A.ID, B.ID, B.Record_Id, B.Codes_Id, B.Flag " +
                            "FROM PP_PLIB.PPPOLMPF A INNER JOIN PP_PLIB.PPFLAGPF B " +
                            "ON A.ID = B.Record_Id WHERE A.POLICYNO  = '" + policyNumber + "'");

                    if (resultSet.next()) {
                        recordId = resultSet.getString("Record_Id");
                        log.debug("Flagging policy: " + policyNumber + " Record_Id: " + recordId + " for letters.");
                        statement.executeUpdate("UPDATE PP_PLIB.PPFLAGPF SET FLAG='Y' WHERE record_id  = '" + recordId + "'");
                    } else {
                        recordId = "Record Id Not Found.";
                        throw new Exception(recordId);
                    }
                } else if (productName.equalsIgnoreCase("Cover For Life")) {
                    // TODO: Execute Query On The Product Table.
                    ResultSet resultSet = statement.executeQuery("SELECT A.ID, B.ID, B.Record_Id, B.Codes_Id, B.Flag " +
                            "FROM COV4LIFEP.C4LPOLMPF A INNER JOIN COV4LIFEP.C4LFLAGPF B " +
                            "ON A.ID = B.Record_Id WHERE A.POLICYNO  = '" + policyNumber + "'");

                    if (resultSet.next()) {
                        recordId = resultSet.getString("Record_Id");
                        log.debug("Flagging policy: " + policyNumber + " Record_Id: " + recordId + " for letters.");
                        statement.executeUpdate("UPDATE COV4LIFEP.C4LFLAGPF SET FLAG='Y' WHERE record_id  = '" + recordId + "'");
                    } else {
                        recordId = "Record Id Not Found.";
                        throw new Exception(recordId);
                    }
                }
                // TODO: Ensure that we catered for all products.
            } else {
                throw new Exception("No Product Name Specified.");
            }
            return recordId;
        } catch (Exception e) {
            log.error("Error while Flagging policy: " + policyNumber + " Record_Id: " + recordId + " for letters: " + e.getMessage());
            throw new Exception("Error while Flagging policy: " + policyNumber + " Record_Id: " + recordId + " for letters: " + e.getMessage());
        } finally {
            closeDatabaseConnection();
        }
    }

    public void flagPolicyForLetters(String productName, String flagTypId, String policyNumber)  throws Exception {

        connectToDatabase();
        createStatement();

        try {
            log.debug("Getting the Record_Id for Policy: " + policyNumber);

            if (!productName.isEmpty()) {
                // 2021 Letters Products:
                if (productName.equalsIgnoreCase("Funeral Insurance")) {
                    resultSet = statement.executeQuery("SELECT POLICY.POLICYNO, POLICY.ID, FLAGS.RECORD_ID, FLAGS.FLAG, FLAGS.CODES_ID " +
                            "FROM FUNINS_P.FIPOLMPF POLICY\n" +
                            "INNER JOIN FUNINS_P.FIFLAGPF FLAGS\n" +
                            "ON POLICY.ID = FLAGS.RECORD_ID\n" +
                            "WHERE POLICY.POLICYNO = '" + policyNumber + "'");

                    String recordId = "";
                    if (resultSet.next()) {
                        recordId = resultSet.getString("RECORD_ID");
                    }

                    if (!recordId.isEmpty()) {
                        statement.executeUpdate("UPDATE FUNINS_P.FIFLAGPF SET FLAG='Y' WHERE RECORD_ID = '" + recordId + "' AND CODES_ID = '" + flagTypId + "'");
                    }
                } else if (productName.equalsIgnoreCase("Accidental Death")) {
                    resultSet = statement.executeQuery("SELECT POLICY.POLICYNO, POLICY.ID, FLAGS.RECORD_ID, FLAGS.FLAG, FLAGS.FLAGTYP_ID " +
                            "FROM PA_PLIB.PAPOLMPF POLICY\n" +
                            "INNER JOIN PANEWPLIB.OSDFLAGPF FLAGS\n" +
                            "ON POLICY.ID = FLAGS.RECORD_ID\n" +
                            "WHERE POLICY.POLICYNO = '" + policyNumber + "'");

                    String recordId = "";
                    if (resultSet.next()) {
                        recordId = resultSet.getString("RECORD_ID");
                    }

                    if (!recordId.isEmpty()) {
                        statement.executeUpdate("UPDATE PANEWPLIB.OSDFLAGPF SET FLAG='Y' WHERE RECORD_ID = '" + recordId + "' AND FLAGTYP_ID = '" + flagTypId + "'");
                    }
                } else if (productName.equalsIgnoreCase("Health Cash")) {
                    resultSet = statement.executeQuery("SELECT POLICY.POLICYNO, POLICY.ID, FLAGS.RECORD_ID, FLAGS.FLAG, FLAGS.CODES_ID " +
                            "FROM HOSTCAPLIB.HCPPOLMPF POLICY\n" +
                            "INNER JOIN HOSTCAPLIB.HCPFLAGPF FLAGS\n" +
                            "ON POLICY.ID = FLAGS.RECORD_ID\n" +
                            "WHERE POLICY.POLICYNO = '" + policyNumber + "'");

                    String recordId = "";
                    if (resultSet.next()) {
                        recordId = resultSet.getString("RECORD_ID");
                    }

                    if (!recordId.isEmpty()) {
                        statement.executeUpdate("UPDATE HOSTCAPLIB.HCPFLAGPF SET FLAG='Y' WHERE RECORD_ID = '" + recordId + "' AND CODES_ID = '" + flagTypId + "'");
                    }
                } else if (productName.equalsIgnoreCase("Hospital Cash 1")) {
                    resultSet = statement.executeQuery("SELECT POLICY.POLICYNO, POLICY.ID, FLAGS.RECORD_ID, FLAGS.FLAG, FLAGS.CODES_ID " +
                            "FROM HOSTCAPLIB.HCPPOLMPF POLICY\n" +
                            "INNER JOIN HOSTCAPLIB.HCPFLAGPF FLAGS\n" +
                            "ON POLICY.ID = FLAGS.RECORD_ID\n" +
                            "WHERE POLICY.POLICYNO = '" + policyNumber + "'");

                    String recordId = "";
                    if (resultSet.next()) {
                        recordId = resultSet.getString("RECORD_ID");
                    }

                    if (!recordId.isEmpty()) {
                        statement.executeUpdate("UPDATE HOSTCAPLIB.HCPFLAGPF SET FLAG='Y' WHERE RECORD_ID = '" + recordId + "' AND CODES_ID = '" + flagTypId + "'");
                    }
                    // TODO: Annual Statement
                } else if (productName.equalsIgnoreCase("Hospital Cash 2")) {
                    resultSet = statement.executeQuery("SELECT POLICY.POLICYNO, POLICY.ID, FLAGS.RECORD_ID, FLAGS.FLAG, FLAGS.CODES_ID " +
                            "FROM HOSTCAPLIB.HCPPOLMPF POLICY\n" +
                            "INNER JOIN HOSTCAPLIB.HCPFLAGPF FLAGS\n" +
                            "ON POLICY.ID = FLAGS.RECORD_ID\n" +
                            "WHERE POLICY.POLICYNO = '" + policyNumber + "'");

                    String recordId = "";
                    if (resultSet.next()) {
                        recordId = resultSet.getString("RECORD_ID");
                    }

                    if (!recordId.isEmpty()) {
                        statement.executeUpdate("UPDATE HOSTCAPLIB.HCPFLAGPF SET FLAG='Y' WHERE RECORD_ID = '" + recordId + "' AND CODES_ID = '" + flagTypId + "'");
                    }
                    // TODO: Annual Statement
                } else if (productName.equalsIgnoreCase("Lifestyle Protection")) {
                    resultSet = statement.executeQuery("SELECT POLICY.POLICYNO, POLICY.ID, FLAGS.RECORD_ID, FLAGS.FLAG, FLAGS.CODES_ID " +
                            "FROM LP_PLIB.LPPOLMPF POLICY\n" +
                            "INNER JOIN LP_PLIB.LPFLAGPF FLAGS\n" +
                            "ON POLICY.ID = FLAGS.RECORD_ID\n" +
                            "WHERE POLICY.POLICYNO = '" + policyNumber + "'");

                    String recordId = "";
                    if (resultSet.next()) {
                        recordId = resultSet.getString("RECORD_ID");
                    }

                    if (!recordId.isEmpty()) {
                        statement.executeUpdate("UPDATE LP_PLIB.LPFLAGPF SET FLAG='Y' WHERE RECORD_ID = '" + recordId + "' AND CODES_ID = '" + flagTypId + "'");
                    }
                } else if (productName.equalsIgnoreCase("Pay Protect")) {
                    resultSet = statement.executeQuery("SELECT POLICY.POLICYNO, POLICY.ID, FLAGS.RECORD_ID, FLAGS.FLAG, FLAGS.CODES_ID " +
                            "FROM PP_PLIB.PPPOLMPF POLICY\n" +
                            "INNER JOIN GENPLIB.GSDFLAGPF FLAGS\n" +
                            "ON POLICY.ID = FLAGS.RECORD_ID\n" +
                            "WHERE POLICY.POLICYNO = '" + policyNumber + "'");

                    String recordId = "";
                    if (resultSet.next()) {
                        recordId = resultSet.getString("RECORD_ID");
                    }

                    if (!recordId.isEmpty()) {
                        statement.executeUpdate("UPDATE GENPLIB.GSDFLAGPF SET FLAG='Y' WHERE RECORD_ID = '" + recordId + "' AND CODES_ID = '" + flagTypId + "'");
                    }
                } else if (productName.equalsIgnoreCase("Smart Cover")) {
                    resultSet = statement.executeQuery("SELECT POLICY.POLICYNO, POLICY.ID, FLAGS.RECORD_ID, FLAGS.FLAG, FLAGS.FLAGTYP_ID " +
                            "FROM PERACCPLIB.PSDPOLMPF POLICY\n" +
                            "INNER JOIN PERACCPLIB.PSDFLAGPF FLAGS\n" +
                            "ON POLICY.ID = FLAGS.RECORD_ID\n" +
                            "WHERE POLICY.POLICYNO = '" + policyNumber + "'");

                    String recordId = "";
                    if (resultSet.next()) {
                        recordId = resultSet.getString("RECORD_ID");
                    }

                    if (!recordId.isEmpty()) {
                        statement.executeUpdate("UPDATE PERACCPLIB.PSDFLAGPF SET FLAG='Y' WHERE RECORD_ID = '" + recordId + "' AND FLAGTYP_ID = '" + flagTypId + "'");
                    }
                    // TODO: Annual Statement
                } else if (productName.equalsIgnoreCase("Cover4Life")) {
                    resultSet = statement.executeQuery("SELECT POLICY.POLICYNO, POLICY.ID, FLAGS.RECORD_ID, FLAGS.FLAG, FLAGS.CODES_ID " +
                            "FROM COV4LIFEP.C4LPOLMPF POLICY\n" +
                            "INNER JOIN COV4LIFEP.C4LFLAGPF FLAGS\n" +
                            "ON POLICY.ID = FLAGS.RECORD_ID\n" +
                            "WHERE POLICY.POLICYNO = '" + policyNumber + "'");

                    String recordId = "";
                    if (resultSet.next()) {
                        recordId = resultSet.getString("RECORD_ID");
                    }

                    if (!recordId.isEmpty()) {
                        statement.executeUpdate("UPDATE COV4LIFEP.C4LFLAGPF SET FLAG='Y' WHERE RECORD_ID = '" + recordId + "' AND CODES_ID = '" + flagTypId + "'");
                    }
                    // TODO: Annual Statement
                } else if (productName.equalsIgnoreCase("FNB Life Cover (Online Life)")) {
                    resultSet = statement.executeQuery("SELECT POLICY.POLICYNO, POLICY.ID, FLAGS.RECORD_ID, FLAGS.FLAG, FLAGS.CODES_ID " +
                            "FROM OLIFE_PLIB.OLPOLMPF POLICY\n" +
                            "INNER JOIN OLIFE_PLIB.OLFLAGPF FLAGS\n" +
                            "ON POLICY.ID = FLAGS.RECORD_ID\n" +
                            "WHERE POLICY.POLICYNO = '" + policyNumber + "'");

                    String recordId = "";
                    if (resultSet.next()) {
                        recordId = resultSet.getString("RECORD_ID");
                    }

                    if (!recordId.isEmpty()) {
                        statement.executeUpdate("UPDATE OLIFE_PLIB.OLFLAGPF SET FLAG='Y' WHERE RECORD_ID = '" + recordId + "' AND CODES_ID = '" + flagTypId + "'");
                    }
                    // TODO: Annual Statement
                } else if (productName.equalsIgnoreCase("Old Funeral")) {
                    resultSet = statement.executeQuery("SELECT POLICY.POLICYNO, POLICY.ID, FLAGS.RECORD_ID, FLAGS.FLAG, FLAGS.CODES_ID " +
                            "FROM FUNERPLIB.FSDPOLMPF POLICY\n" +
                            "INNER JOIN FUNERPLIB.FSDFLAGPF FLAGS\n" +
                            "ON POLICY.ID = FLAGS.RECORD_ID\n" +
                            "WHERE POLICY.POLICYNO = '" + policyNumber + "'");

                    String recordId = "";
                    if (resultSet.next()) {
                        recordId = resultSet.getString("RECORD_ID");
                    }

                    if (!recordId.isEmpty()) {
                        statement.executeUpdate("UPDATE FUNERPLIB.FSDFLAGPF SET FLAG='Y' WHERE RECORD_ID = '" + recordId + "' AND CODES_ID = '" + flagTypId + "'");
                    }
                    // TODO: Annual Statement
                }
            } else {
                throw new Exception("No Product Name Specified.");
            }
        } catch (Exception e) {
            log.error("Error while Flagging policy: " + policyNumber + " flagTypId: " + flagTypId + " for letters: " + e.getMessage());
            throw new Exception("Error while Flagging policy: " + policyNumber + " flagTypId: " + flagTypId + " for letters: " + e.getMessage());
        } finally {
            closeDatabaseConnection();
        }
    }

    public boolean checkPolicyFlagForLetters(String productName, String flagTypId, String policyNumber) throws Exception {

        connectToDatabase();
        createStatement();

        try {
            log.debug("Checking Policy Flag for Policy Number: " + policyNumber + " and flagTypId: " + flagTypId);
            ResultSet resultSet = null;

            if (!productName.isEmpty()) {
                // 2021 Letters Products:
                if (productName.equalsIgnoreCase("Funeral Insurance")) {
                    resultSet = statement.executeQuery("SELECT POLICY.POLICYNO, POLICY.ID, FLAGS.RECORD_ID, FLAGS.FLAG, FLAGS.CODES_ID " +
                            "FROM FUNINS_P.FIPOLMPF POLICY\n" +
                            "INNER JOIN FUNINS_P.FIFLAGPF FLAGS\n" +
                            "ON POLICY.ID = FLAGS.RECORD_ID\n" +
                            "WHERE POLICY.POLICYNO = '" + policyNumber + "'");

                    String recordId = "";
                    if (resultSet.next()) {
                        recordId = resultSet.getString("RECORD_ID");
                    }

                    if (!recordId.isEmpty()) {
                        resultSet = statement.executeQuery("SELECT * FROM FUNINS_P.FIFLAGPF WHERE RECORD_ID = '" + recordId + "' AND CODES_ID = '" + flagTypId + "'");
                    }
                } else if (productName.equalsIgnoreCase("Accidental Death")) {
                    resultSet = statement.executeQuery("SELECT POLICY.POLICYNO, POLICY.ID, FLAGS.RECORD_ID, FLAGS.FLAG, FLAGS.FLAGTYP_ID " +
                            "FROM PA_PLIB.PAPOLMPF POLICY\n" +
                            "INNER JOIN PANEWPLIB.OSDFLAGPF FLAGS\n" +
                            "ON POLICY.ID = FLAGS.RECORD_ID\n" +
                            "WHERE POLICY.POLICYNO = '" + policyNumber + "'");

                    String recordId = "";
                    if (resultSet.next()) {
                        recordId = resultSet.getString("RECORD_ID");
                    }

                    if (!recordId.isEmpty()) {
                        resultSet = statement.executeQuery("SELECT * FROM PANEWPLIB.OSDFLAGPF WHERE RECORD_ID = '" + recordId + "' AND FLAGTYP_ID = '" + flagTypId + "'");
                    }
                } else if (productName.equalsIgnoreCase("Health Cash")) {
                    resultSet = statement.executeQuery("SELECT POLICY.POLICYNO, POLICY.ID, FLAGS.RECORD_ID, FLAGS.FLAG, FLAGS.CODES_ID " +
                            "FROM HOSTCAPLIB.HCPPOLMPF POLICY\n" +
                            "INNER JOIN HOSTCAPLIB.HCPFLAGPF FLAGS\n" +
                            "ON POLICY.ID = FLAGS.RECORD_ID\n" +
                            "WHERE POLICY.POLICYNO = '" + policyNumber + "'");

                    String recordId = "";
                    if (resultSet.next()) {
                        recordId = resultSet.getString("RECORD_ID");
                    }

                    if (!recordId.isEmpty()) {
                        resultSet = statement.executeQuery("SELECT * FROM HOSTCAPLIB.HCPFLAGPF WHERE RECORD_ID = '" + recordId + "' AND CODES_ID = '" + flagTypId + "'");
                    }
                } else if (productName.equalsIgnoreCase("Hospital Cash 1")) {
                    resultSet = statement.executeQuery("SELECT POLICY.POLICYNO, POLICY.ID, FLAGS.RECORD_ID, FLAGS.FLAG, FLAGS.CODES_ID " +
                            "FROM HOSTCAPLIB.HCPPOLMPF POLICY\n" +
                            "INNER JOIN HOSTCAPLIB.HCPFLAGPF FLAGS\n" +
                            "ON POLICY.ID = FLAGS.RECORD_ID\n" +
                            "WHERE POLICY.POLICYNO = '" + policyNumber + "'");

                    String recordId = "";
                    if (resultSet.next()) {
                        recordId = resultSet.getString("RECORD_ID");
                    }

                    if (!recordId.isEmpty()) {
                        resultSet = statement.executeQuery("SELECT * FROM HOSTCAPLIB.HCPFLAGPF WHERE RECORD_ID = '" + recordId + "' AND CODES_ID = '" + flagTypId + "'");
                    }
                    // TODO: Annual Statement
                } else if (productName.equalsIgnoreCase("Hospital Cash 2")) {
                    resultSet = statement.executeQuery("SELECT POLICY.POLICYNO, POLICY.ID, FLAGS.RECORD_ID, FLAGS.FLAG, FLAGS.CODES_ID " +
                            "FROM HOSTCAPLIB.HCPPOLMPF POLICY\n" +
                            "INNER JOIN HOSTCAPLIB.HCPFLAGPF FLAGS\n" +
                            "ON POLICY.ID = FLAGS.RECORD_ID\n" +
                            "WHERE POLICY.POLICYNO = '" + policyNumber + "'");

                    String recordId = "";
                    if (resultSet.next()) {
                        recordId = resultSet.getString("RECORD_ID");
                    }

                    if (!recordId.isEmpty()) {
                        resultSet = statement.executeQuery("SELECT * FROM HOSTCAPLIB.HCPFLAGPF WHERE RECORD_ID = '" + recordId + "' AND CODES_ID = '" + flagTypId + "'");
                    }
                    // TODO: Annual Statement
                } else if (productName.equalsIgnoreCase("Lifestyle Protection")) {
                    resultSet = statement.executeQuery("SELECT POLICY.POLICYNO, POLICY.ID, FLAGS.RECORD_ID, FLAGS.FLAG, FLAGS.CODES_ID " +
                            "FROM LP_PLIB.LPPOLMPF POLICY\n" +
                            "INNER JOIN LP_PLIB.LPFLAGPF FLAGS\n" +
                            "ON POLICY.ID = FLAGS.RECORD_ID\n" +
                            "WHERE POLICY.POLICYNO = '" + policyNumber + "'");

                    String recordId = "";
                    if (resultSet.next()) {
                        recordId = resultSet.getString("RECORD_ID");
                    }

                    if (!recordId.isEmpty()) {
                        resultSet = statement.executeQuery("SELECT * FROM LP_PLIB.LPFLAGPF WHERE RECORD_ID = '" + recordId + "' AND CODES_ID = '" + flagTypId + "'");
                    }
                } else if (productName.equalsIgnoreCase("Pay Protect")) {
                    resultSet = statement.executeQuery("SELECT POLICY.POLICYNO, POLICY.ID, FLAGS.RECORD_ID, FLAGS.FLAG, FLAGS.CODES_ID " +
                            "FROM PP_PLIB.PPPOLMPF POLICY\n" +
                            "INNER JOIN GENPLIB.GSDFLAGPF FLAGS\n" +
                            "ON POLICY.ID = FLAGS.RECORD_ID\n" +
                            "WHERE POLICY.POLICYNO = '" + policyNumber + "'");

                    String recordId = "";
                    if (resultSet.next()) {
                        recordId = resultSet.getString("RECORD_ID");
                    }

                    if (!recordId.isEmpty()) {
                        resultSet = statement.executeQuery("SELECT * FROM GENPLIB.GSDFLAGPF WHERE RECORD_ID = '" + recordId + "' AND CODES_ID = '" + flagTypId + "'");
                    }
                } else if (productName.equalsIgnoreCase("Smart Cover")) {
                    resultSet = statement.executeQuery("SELECT POLICY.POLICYNO, POLICY.ID, FLAGS.RECORD_ID, FLAGS.FLAG, FLAGS.FLAGTYP_ID " +
                            "FROM PERACCPLIB.PSDPOLMPF POLICY\n" +
                            "INNER JOIN PERACCPLIB.PSDFLAGPF FLAGS\n" +
                            "ON POLICY.ID = FLAGS.RECORD_ID\n" +
                            "WHERE POLICY.POLICYNO = '" + policyNumber + "'");

                    String recordId = "";
                    if (resultSet.next()) {
                        recordId = resultSet.getString("RECORD_ID");
                    }

                    if (!recordId.isEmpty()) {
                        resultSet = statement.executeQuery("SELECT * FROM PERACCPLIB.PSDFLAGPF WHERE RECORD_ID = '" + recordId + "' AND FLAGTYP_ID = '" + flagTypId + "'");
                    }
                    // TODO: Annual Statement
                } else if (productName.equalsIgnoreCase("Cover4Life")) {
                    resultSet = statement.executeQuery("SELECT POLICY.POLICYNO, POLICY.ID, FLAGS.RECORD_ID, FLAGS.FLAG, FLAGS.CODES_ID " +
                            "FROM COV4LIFEP.C4LPOLMPF POLICY\n" +
                            "INNER JOIN COV4LIFEP.C4LFLAGPF FLAGS\n" +
                            "ON POLICY.ID = FLAGS.RECORD_ID\n" +
                            "WHERE POLICY.POLICYNO = '" + policyNumber + "'");

                    String recordId = "";
                    if (resultSet.next()) {
                        recordId = resultSet.getString("RECORD_ID");
                    }

                    if (!recordId.isEmpty()) {
                        resultSet = statement.executeQuery("SELECT * FROM COV4LIFEP.C4LFLAGPF WHERE RECORD_ID = '" + recordId + "' AND CODES_ID = '" + flagTypId + "'");
                    }
                    // TODO: Annual Statement
                } else if (productName.equalsIgnoreCase("FNB Life Cover (Online Life)")) {
                    resultSet = statement.executeQuery("SELECT POLICY.POLICYNO, POLICY.ID, FLAGS.RECORD_ID, FLAGS.FLAG, FLAGS.CODES_ID " +
                            "FROM OLIFE_PLIB.OLPOLMPF POLICY\n" +
                            "INNER JOIN OLIFE_PLIB.OLFLAGPF FLAGS\n" +
                            "ON POLICY.ID = FLAGS.RECORD_ID\n" +
                            "WHERE POLICY.POLICYNO = '" + policyNumber + "'");

                    String recordId = "";
                    if (resultSet.next()) {
                        recordId = resultSet.getString("RECORD_ID");
                    }

                    if (!recordId.isEmpty()) {
                        resultSet = statement.executeQuery("SELECT * FROM OLIFE_PLIB.OLFLAGPF WHERE RECORD_ID = '" + recordId + "' AND CODES_ID = '" + flagTypId + "'");
                    }
                    // TODO: Annual Statement
                } else if (productName.equalsIgnoreCase("Old Funeral")) {
                    resultSet = statement.executeQuery("SELECT POLICY.POLICYNO, POLICY.ID, FLAGS.RECORD_ID, FLAGS.FLAG, FLAGS.CODES_ID " +
                            "FROM FUNERPLIB.FSDPOLMPF POLICY\n" +
                            "INNER JOIN FUNERPLIB.FSDFLAGPF FLAGS\n" +
                            "ON POLICY.ID = FLAGS.RECORD_ID\n" +
                            "WHERE POLICY.POLICYNO = '" + policyNumber + "'");

                    String recordId = "";
                    if (resultSet.next()) {
                        recordId = resultSet.getString("RECORD_ID");
                    }

                    if (!recordId.isEmpty()) {
                        resultSet = statement.executeQuery("SELECT * FROM FUNERPLIB.FSDFLAGPF WHERE RECORD_ID = '" + recordId + "' AND CODES_ID = '" + flagTypId + "'");
                    }
                    // TODO: Annual Statement
                }
            } else {
                throw new Exception("No Product Name Specified.");
            }

            if (resultSet != null) {
                if (resultSet.next()) {
                    String flag = resultSet.getString("Flag");
                    if (flag != null) {
                        if (flag.equalsIgnoreCase("Y")) {
                            return true;
                        } else {
                            return false;
                        }
                    } else {
                        log.error("Flag not found.");
                        // throw new Exception("Flag not found.");
                        return false;
                    }

                } else {
                    // throw new Exception("Record Id Not Found.");
                    log.error("Record Id Not Found.");
                    return false;
                }
            } else {
                log.error("Record Id Not Found -> Result Set is NULL");
                // throw new Exception("Record Id Not Found -> Result Set is NULL");
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while Checking Policy Flag for Policy Number: " + policyNumber + " and flagTypId: " + flagTypId);
            throw new Exception("Error while Checking Policy Flag for Policy Number: " + policyNumber + " and flagTypId: " + flagTypId);
        } finally {
            closeDatabaseConnection();
        }
    }

    public String getLastSentRequestId() throws Exception {

        connectToDatabase();
        createStatement();

        try {
            log.debug("Getting Last Sent Request Id.");
            ResultSet resultSet = statement.executeQuery("SELECT * FROM GENPLIB.GSDLOG26PF WHERE XMLIN LIKE '%TMSLetterResponse%' ORDER BY TIMECAPTI DESC");

            if (resultSet.next()) {
                String id = resultSet.getString("ID");
                if (id != null) {
                    return id;
                } else {
                    log.error("Id not found.");
                    throw new Exception("Id not found.");
                }

            } else {
                throw new Exception("No Records Found.");
            }
        } catch (Exception e) {
            log.error("Error while Getting Last Sent Request Id.");
            throw new Exception("Error while Getting Last Sent Request Id.");
        } finally {
            closeDatabaseConnection();
        }
    }

    public CsvQueueItem getLastSentCsvQueueItem() throws Exception {

        connectToDatabase();
        createStatement();

        try {
            log.debug("Getting Last Sent Csv Queue Id.");
            ResultSet resultSet = statement.executeQuery("SELECT * FROM GENPLIB.GSDPATPF ORDER BY PROCDATE DESC");

            if (resultSet.next()) {
                String id = resultSet.getString("ID");
                String fileName= resultSet.getString("FILENAME");
                String blobfld= resultSet.getString("BLOBFLD");
                String type= resultSet.getString("TYPE");
                String procFlag= resultSet.getString("PROCFLAG");
                String procDate= resultSet.getString("PROCDATE");
                String userCapt= resultSet.getString("USERCAPT");
                String timeCapt= resultSet.getString("TIMECAPT");
                String chgStamp= resultSet.getString("CHGSTAMP");

                return new CsvQueueItem(id, fileName, blobfld, type, procFlag, procDate, userCapt, timeCapt, chgStamp);

            } else {
                throw new Exception("No Records Found For Csv Queue Item.");
            }
        } catch (Exception e) {
            log.error("Error while Getting Last Sent Csv Queue Id.");
            throw new Exception("Error while Getting Last Sent Csv Queue Id.");
        } finally {
            closeDatabaseConnection();
        }
    }

    public CsvQueueItem getCsvQueueItem(String itemId) throws Exception {

        connectToDatabase();
        createStatement();

        try {
            log.debug("Getting Last Sent Csv Queue Id.");
            ResultSet resultSet = statement.executeQuery("SELECT * FROM GENPLIB.GSDPATPF WHERE ID = '" + itemId + "' ORDER BY PROCDATE DESC");

            if (resultSet.next()) {
                String id = resultSet.getString("ID");
                String fileName= resultSet.getString("FILENAME");
                String blobfld= resultSet.getString("BLOBFLD");
                String type= resultSet.getString("TYPE");
                String procFlag= resultSet.getString("PROCFLAG");
                String procDate= resultSet.getString("PROCDATE");
                String userCapt= resultSet.getString("USERCAPT");
                String timeCapt= resultSet.getString("TIMECAPT");
                String chgStamp= resultSet.getString("CHGSTAMP");

                return new CsvQueueItem(id, fileName, blobfld, type, procFlag, procDate, userCapt, timeCapt, chgStamp);

            } else {
                throw new Exception("No Records Found For Csv Queue Item.");
            }
        } catch (Exception e) {
            log.error("Error while Getting Last Sent Csv Queue Id.");
            throw new Exception("Error while Getting Last Sent Csv Queue Id.");
        } finally {
            closeDatabaseConnection();
        }
    }

    public ResultSet getSentRequest(String id) throws Exception {

        connectToDatabase();
        createStatement();

        try {
            log.debug("Getting Last Sent Request Id.");
            return statement.executeQuery("SELECT * FROM GENPLIB.GSDLOG26PF WHERE ID = '" + id + "' AND XMLIN LIKE '%TMSLetterResponse%' ORDER BY TIMECAPTI DESC");
        } catch (Exception e) {
            log.error("Error while Getting Last Sent Request Id.");
            throw new Exception("Error while Getting Last Sent Request Id.");
        } finally {
            closeDatabaseConnection();
        }
    }

    public void changePolicyStatus(String policyNumber, String statusId) throws Exception {

        connectToDatabase();
        createStatement();

        try {
            log.debug("Changing Policy Status On: " + policyNumber);
            // TODO: Add Conditionals For Other Products
            statement.executeUpdate("UPDATE funins_p.fipolmpf SET STATUS_ID = '" + statusId + "' WHERE POLICYNO  = '" + policyNumber + "'");
        } catch (Exception e) {
            log.error("Error while Changing Policy Status On: " + policyNumber);
            throw new Exception("Error while Changing Policy Status On: " + policyNumber);
        } finally {
            closeDatabaseConnection();
        }
    }

    public void changePolicyStatus(ScenarioOperator scenarioOperator, String statusId) throws Exception {

        connectToDatabase();
        createStatement();

        try {
            if (dbPolicyNumber != null) {
                log.debug("Changing Policy Status On: " + dbPolicyNumber);
                // TODO: Add Conditionals For Other Products
                String productName = scenarioOperator.getProductName();
                if (productName.equalsIgnoreCase("Funeral Insurance")) {
                    statement.executeUpdate("UPDATE funins_p.fipolmpf SET STATUS_ID = '" + statusId + "' WHERE POLICYNO  = '" + dbPolicyNumber + "'");
                } else if (productName.equalsIgnoreCase("Accidental Death") || productName.equalsIgnoreCase("Personal Accident")) {
                    statement.executeUpdate("UPDATE PA_PLIB.PAPOLMPF SET STATUS_ID = '" + statusId + "' WHERE POLICYNO  = '" + dbPolicyNumber + "'");
                } else if (productName.equalsIgnoreCase("Health Cash Plan") || productName.equalsIgnoreCase("Health Cash")) {
                    statement.executeUpdate("UPDATE HOSTCAPLIB.HCPPOLMPF SET STATUS_ID = '" + statusId + "' WHERE POLICYNO  = '" + dbPolicyNumber + "'");
                } else if (productName.equalsIgnoreCase("Pay Protect")) {
                    statement.executeUpdate("UPDATE PP_PLIB.PPPOLMPF SET STATUS_ID = '" + statusId + "' WHERE POLICYNO  = '" + dbPolicyNumber + "'");
                } else if (productName.equalsIgnoreCase("Cover For Life")) {
                    statement.executeUpdate("UPDATE COV4LIFEP.C4LPOLMPF SET STATUS_ID = '" + statusId + "' WHERE POLICYNO  = '" + dbPolicyNumber + "'");
                } else if (productName.equalsIgnoreCase("LOC-PERSONAL") || productName.equalsIgnoreCase("PERSONAL")) {
                    statement.executeUpdate("UPDATE LEGALPLIB.LCPOLMPF SET STATUS_ID = '" + statusId + "' WHERE POLICYNO  = '" + dbPolicyNumber + "'");
                }else if (productName.equalsIgnoreCase("LOC-BUSINESS") || productName.equalsIgnoreCase("BUSINESS")) {
                    statement.executeUpdate("UPDATE LEGALPLIB.LCPOLMPF SET STATUS_ID = '" + statusId + "' WHERE POLICYNO  = '" + dbPolicyNumber + "'");
                }
            } else {
                log.error("DB Policy Number Is Null. Please set a DB Policy Number to use.");
                throw new Exception("DB Policy Number Is Null. Please set a DB Policy Number to use.");
            }
        } catch (Exception e) {
            log.error("Error while Changing Policy Status On: " + dbPolicyNumber);
            throw new Exception("Error while Changing Policy Status On: " + dbPolicyNumber);
        } finally {
            closeDatabaseConnection();
        }
    }

    public ResultSet getSentRequest(String policyNumber, String lastRequestId) throws Exception {

        connectToDatabase();
        createStatement();

        try {
            log.debug("Getting Last Sent Request For " + policyNumber + " After The Record Id: " + lastRequestId);
            return statement.executeQuery("SELECT * FROM GENPLIB.GSDLOG26PF WHERE XMLIN LIKE '%" + policyNumber + "</policy>%' AND ID > '" + lastRequestId + "'  ORDER BY TIMECAPTI DESC");
        } catch (Exception e) {
            log.error("Error while Getting Last Sent Request Id.");
            throw new Exception("Error while Getting Last Sent Request Id.");
        } finally {
            closeDatabaseConnection();
        }
    }

    public void selectFinancialInsurancePoliciesAndChangeStatusDateAndLapse(String date, String statusId, int numberOfPolicies) throws Exception {

        connectToDatabase();
        createStatement();

        try {
            // DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            // LocalDateTime now = LocalDateTime.now();
            // LocalDateTime nowSixMonthsBefore = now.minusMonths(6L);
            // String systemTime = format.format(nowSixMonthsBefore);
            log.debug("Querying For FI Policies And Changing Premium Status Date To " + date);
            resultSet = statement.executeQuery("SELECT * FROM FUNINS_P.FIPOLMPF");
            String[] listOfPolicies = new String[numberOfPolicies];

            while (resultSet.next() && --numberOfPolicies >= 0) {
                String policyno = resultSet.getString("POLICYNO").trim();
                listOfPolicies[numberOfPolicies] = policyno;
            }

            for (String policyno: listOfPolicies) {
                try {
                    statement.executeUpdate("UPDATE FUNINS_P.FIPOLMPF SET STATUS_ID = '" + statusId + "' WHERE POLICYNO = '" + policyno + "'");
                    statement.executeUpdate("UPDATE FUNINS_P.FIPOLMPF SET STATUSDTE = '" + date + "' WHERE POLICYNO = '" + policyno + "'");

                    Thread.sleep(2000);
                    resultSet = statement.executeQuery("SELECT * FROM FUNINS_P.FIPOLMPF WHERE POLICYNO = '" + policyno + "'");

                    if (resultSet.next()) {
                        policyno = resultSet.getString("POLICYNO");
                        String newStatusId = resultSet.getString("STATUS_ID");
                        String newStatusDate = resultSet.getString("STATUSDTE");
                        log.info("Policy Number: " + policyno + " , STATUS_ID: " + newStatusId + " , STATUSDTE: " + newStatusDate);
                    }
                } catch (Exception e) {
                    log.error("Error while updating for " + policyno + " , Error: " + e.getMessage());
                }
            }
        } catch (Exception e) {
            log.debug("Error while querying for " + policyNumber + " policy Number");
            log.error(e.getMessage());
            throw new Exception(e.getMessage());
        } finally {
            closeDatabaseConnection();
        }
    }

    public void selectFinancialInsurancePoliciesAndChangeStatusDateAndLapse(String date, String statusId, String[] listOfPolicies) throws Exception {

        connectToDatabase();
        createStatement();

        try {
            log.debug("Querying For FI Policies And Changing Premium Status Date To " + date);

            for (String policyno: listOfPolicies) {
                try {
                    statement.executeUpdate("UPDATE FUNINS_P.FIPOLMPF SET STATUS_ID = '" + statusId + "' WHERE POLICYNO = '" + policyno + "'");
                    statement.executeUpdate("UPDATE FUNINS_P.FIPOLMPF SET STATUSDTE = '" + date + "' WHERE POLICYNO = '" + policyno + "'");

                    Thread.sleep(2000);
                    resultSet = statement.executeQuery("SELECT * FROM FUNINS_P.FIPOLMPF WHERE POLICYNO = '" + policyno + "'");

                    if (resultSet.next()) {
                        policyno = resultSet.getString("POLICYNO");
                        String newStatusId = resultSet.getString("STATUS_ID");
                        String newStatusDate = resultSet.getString("STATUSDTE");
                        log.info("Policy Number: " + policyno + " , STATUS_ID: " + newStatusId + " , STATUSDTE: " + newStatusDate);
                    }
                } catch (Exception e) {
                    log.error("Error while updating for " + policyno + " , Error: " + e.getMessage());
                }
            }
        } catch (Exception e) {
            log.debug("Error while querying for " + policyNumber + " policy Number");
            log.error(e.getMessage());
            throw new Exception(e.getMessage());
        } finally {
            closeDatabaseConnection();
        }
    }

    public void selectHospitalCashPlanPoliciesAndChangeStatusDateAndActive(String date, String statusId, String[] listOfPolicies) throws Exception {

        connectToDatabase();
        createStatement();

        try {
            log.debug("Querying For HCP Policies And Changing Status Date To " + date);

            for (String policyno: listOfPolicies) {
                try {
                    statement.executeUpdate("UPDATE HOSTCAPLIB.HCPPOLMPF SET STATUS_ID = '" + statusId + "' WHERE POLICYNO = '" + policyno + "'");
                    statement.executeUpdate("UPDATE HOSTCAPLIB.HCPPOLMPF SET STATUSDTE = '" + date + "' WHERE POLICYNO = '" + policyno + "'");

                    Thread.sleep(2000);
                    resultSet = statement.executeQuery("SELECT * FROM HOSTCAPLIB.HCPPOLMPF WHERE POLICYNO = '" + policyno + "'");

                    if (resultSet.next()) {
                        policyno = resultSet.getString("POLICYNO");
                        String newStatusId = resultSet.getString("STATUS_ID");
                        String newStatusDate = resultSet.getString("STATUSDTE");
                        log.info("Policy Number: " + policyno + " , STATUS_ID: " + newStatusId + " , STATUSDTE: " + newStatusDate);
                    }
                } catch (Exception e) {
                    log.error("Error while updating for " + policyno + " , Error: " + e.getMessage());
                }
            }
        } catch (Exception e) {
            log.debug("Error while querying for " + policyNumber + " policy Number");
            log.error(e.getMessage());
            throw new Exception(e.getMessage());
        } finally {
            closeDatabaseConnection();
        }
    }

    public void selectOldFinancialInsurancePoliciesAndChangeStatusAndPremiumStatus(String statusId, String premStatus, String[] listOfPolicies) throws Exception {
        // TODO: Test These Once Network Is Back.
        connectToDatabase();
        createStatement();

        try {
            log.debug("Querying For FI Policies And Changing Status and Premium Status");

            for (String policyno: listOfPolicies) {
                try {
                    statement.executeUpdate("UPDATE FUNERPLIB.FSDPOLMPF SET STATUS_ID = '" + statusId + "' WHERE POLICYNO = '" + policyno + "'");
                    statement.executeUpdate("UPDATE FUNERPLIB.FSDPOLMPF SET PREMSTATUS = '" + premStatus + "' WHERE POLICYNO = '" + policyno + "'");

                    Thread.sleep(2000);
                    resultSet = statement.executeQuery("SELECT * FROM FUNERPLIB.FSDPOLMPF WHERE POLICYNO = '" + policyno + "'");

                    if (resultSet.next()) {
                        policyno = resultSet.getString("POLICYNO");
                        String newStatusId = resultSet.getString("STATUS_ID");
                        String newPremStatus = resultSet.getString("PREMSTATUS");
                        log.info("Policy Number: " + policyno + " , STATUS_ID: " + newStatusId + " , PREMSTATUS: " + newPremStatus);
                    }
                } catch (Exception e) {
                    log.error("Error while updating for " + policyno + " , Error: " + e.getMessage());
                }
            }
        } catch (Exception e) {
            log.debug("Error while querying for " + policyNumber + " policy Number");
            log.error(e.getMessage());
            throw new Exception(e.getMessage());
        } finally {
            closeDatabaseConnection();
        }
    }

    public void selectLawOnCallPoliciesAndChangeStatusDateAndLapse(String date, String statusId, String[] listOfPolicies) throws Exception {

        connectToDatabase();
        createStatement();

        try {
            log.debug("Querying For LC Policies And Changing Premium Status Date To " + date);

            for (String policyno: listOfPolicies) {
                try {
                    statement.executeUpdate("UPDATE LEGALPLIB.LCPOLMPF SET STATUS_ID = '" + statusId + "' WHERE POLICYNO = '" + policyno + "'");
                    statement.executeUpdate("UPDATE LEGALPLIB.LCPOLMPF SET STATUSDTE = '" + date + "' WHERE POLICYNO = '" + policyno + "'");

                    Thread.sleep(2000);
                    resultSet = statement.executeQuery("SELECT * FROM LEGALPLIB.LCPOLMPF WHERE POLICYNO = '" + policyno + "'");

                    if (resultSet.next()) {
                        policyno = resultSet.getString("POLICYNO");
                        String newStatusId = resultSet.getString("STATUS_ID");
                        String newStatusDate = resultSet.getString("STATUSDTE");
                        log.info("Policy Number: " + policyno + " , STATUS_ID: " + newStatusId + " , STATUSDTE: " + newStatusDate);
                    }
                } catch (Exception e) {
                    log.error("Error while updating for " + policyno + " , Error: " + e.getMessage());
                }
            }
        } catch (Exception e) {
            log.debug("Error while querying for " + policyNumber + " policy Number");
            log.error(e.getMessage());
            throw new Exception(e.getMessage());
        } finally {
            closeDatabaseConnection();
        }
    }

    public void selectPersonalAccidentPoliciesAndChangeStatusDateAndLapse(String date, String statusId, String[] listOfPolicies) throws Exception {

        connectToDatabase();
        createStatement();

        try {
            log.debug("Querying For AD Policies And Changing Premium Status Date To " + date);

            for (String policyno: listOfPolicies) {
                try {
                    statement.executeUpdate("UPDATE PA_PLIB.PAPOLMPF SET STATUS_ID = '" + statusId + "' WHERE POLICYNO = '" + policyno + "'");
                    statement.executeUpdate("UPDATE PA_PLIB.PAPOLMPF SET STATUSDTE = '" + date + "' WHERE POLICYNO = '" + policyno + "'");

                    Thread.sleep(2000);
                    resultSet = statement.executeQuery("SELECT * FROM PA_PLIB.PAPOLMPF WHERE POLICYNO = '" + policyno + "'");

                    if (resultSet.next()) {
                        policyno = resultSet.getString("POLICYNO");
                        String newStatusId = resultSet.getString("STATUS_ID");
                        String newStatusDate = resultSet.getString("STATUSDTE");
                        log.info("Policy Number: " + policyno + " , STATUS_ID: " + newStatusId + " , STATUSDTE: " + newStatusDate);
                    }
                } catch (Exception e) {
                    log.error("Error while updating for " + policyno + " , Error: " + e.getMessage());
                }
            }
        } catch (Exception e) {
            log.debug("Error while querying for " + policyNumber + " policy Number");
            log.error(e.getMessage());
            throw new Exception(e.getMessage());
        } finally {
            closeDatabaseConnection();
        }
    }

    public void selectPayProtectPoliciesAndChangeStatusDateAndLapse(String date, String statusId, String[] listOfPolicies) throws Exception {

        connectToDatabase();
        createStatement();

        try {
            log.debug("Querying For pp Policies And Changing Premium Status Date To " + date);

            for (String policyno: listOfPolicies) {
                try {
                    statement.executeUpdate("UPDATE PP_PLIB.PPPOLMPF SET STATUS_ID = '" + statusId + "' WHERE POLICYNO = '" + policyno + "'");
                    statement.executeUpdate("UPDATE PP_PLIB.PPPOLMPF SET STATUSDTE = '" + date + "' WHERE POLICYNO = '" + policyno + "'");

                    Thread.sleep(2000);
                    resultSet = statement.executeQuery("SELECT * FROM PP_PLIB.PPPOLMPF WHERE POLICYNO = '" + policyno + "'");

                    if (resultSet.next()) {
                        policyno = resultSet.getString("POLICYNO");
                        String newStatusId = resultSet.getString("STATUS_ID");
                        String newStatusDate = resultSet.getString("STATUSDTE");
                        log.info("Policy Number: " + policyno + " , STATUS_ID: " + newStatusId + " , STATUSDTE: " + newStatusDate);
                    }
                } catch (Exception e) {
                    log.error("Error while updating for " + policyno + " , Error: " + e.getMessage());
                }
            }
        } catch (Exception e) {
            log.debug("Error while querying for " + policyNumber + " policy Number");
            log.error(e.getMessage());
            throw new Exception(e.getMessage());
        } finally {
            closeDatabaseConnection();
        }
    }

    public void selectHospitalCashPlanPoliciesAndChangeStatusDateAndLapse(String date, String statusId, String[] listOfPolicies) throws Exception {

        connectToDatabase();
        createStatement();

        try {
            log.debug("Querying For HPC Policies And Changing Premium Status Date To " + date);

            for (String policyno: listOfPolicies) {
                try {
                    statement.executeUpdate("UPDATE HOSTCAPLIB.HCPPOLMPF SET STATUS_ID = '" + statusId + "' WHERE POLICYNO = '" + policyno + "'");
                    statement.executeUpdate("UPDATE HOSTCAPLIB.HCPPOLMPF SET STATUSDTE = '" + date + "' WHERE POLICYNO = '" + policyno + "'");

                    Thread.sleep(2000);
                    resultSet = statement.executeQuery("SELECT * FROM HOSTCAPLIB.HCPPOLMPF WHERE POLICYNO = '" + policyno + "'");

                    if (resultSet.next()) {
                        policyno = resultSet.getString("POLICYNO");
                        String newStatusId = resultSet.getString("STATUS_ID");
                        String newStatusDate = resultSet.getString("STATUSDTE");
                        log.info("Policy Number: " + policyno + " , STATUS_ID: " + newStatusId + " , STATUSDTE: " + newStatusDate);
                    }
                } catch (Exception e) {
                    log.error("Error while updating for " + policyno + " , Error: " + e.getMessage());
                }
            }
        } catch (Exception e) {
            log.debug("Error while querying for " + policyNumber + " policy Number");
            log.error(e.getMessage());
            throw new Exception(e.getMessage());
        } finally {
            closeDatabaseConnection();
        }
    }

}
