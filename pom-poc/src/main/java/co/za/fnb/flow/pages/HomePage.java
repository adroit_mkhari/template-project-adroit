package co.za.fnb.flow.pages;

import co.za.fnb.flow.pages.administration.LegalAdvisorAdminPage;
import co.za.fnb.flow.pages.create_bulk_items.CreateBulkItems;
import co.za.fnb.flow.pages.create_bulk_items.page_factory.CreateBulkItemsPageObjects;
import co.za.fnb.flow.pages.index_items.page_factory.IndexItemsPageObjects;
import co.za.fnb.flow.pages.root_page_factory.CreateSearchItemPageObjects;
import co.za.fnb.flow.pages.root_page_factory.HomePageObjects;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage extends BasePage {
    private Logger log  = LogManager.getLogger(HomePage.class);
    HomePageObjects homePageObjects = new HomePageObjects(driver);
    CreateSearchItemPageObjects createSearchItemPageObjects = new CreateSearchItemPageObjects(driver, scenarioOperator);
    IndexItemsPageObjects indexItemsPageObjects = new IndexItemsPageObjects(driver);
    CreateBulkItemsPageObjects createBulkItemsPageObjects = new CreateBulkItemsPageObjects(driver);

    Actions actions = new Actions(driver);

    public HomePage(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
    }

    public CreateSearchItemPage selectPersonalQueueCreateOrSearchItem() throws Exception {
        log.info("Selecting Personal Queue Create / SearchItem.");
        try {
            Thread.sleep(2000);
            actions = new Actions(driver);
            Object currentWindow = driver.getWindowHandles().toArray()[0];
            driver.switchTo().window((String) currentWindow);
            actions.moveToElement(homePageObjects.getPersonalQueue()).build().perform();
            Thread.sleep(2000);
            homePageObjects.getCreateOrSearchItem().click();
            Thread.sleep(2000);

            int waitCount = 0, length = driver.getWindowHandles().toArray().length;
            while (length == 1 && waitCount++ <= 5) {
                Thread.sleep(2000);
                length = driver.getWindowHandles().toArray().length;
            }

            maximizeServiceWindow();
            Thread.sleep(5000);
            WebDriverWait webDriverWait = new WebDriverWait(driver, 120);
            webDriverWait.until(ExpectedConditions.visibilityOf(createSearchItemPageObjects.getSearchPolicyNumber()));
            return new CreateSearchItemPage(driver, scenarioOperator);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while selecting Personal Queue.");
            throw new Exception("Error while selecting Personal Queue.");
        }
    }

    public CreateBulkItems selectCreateBulkItems() throws Exception {
        log.info("Selecting Create Bulk Items.");
        try {
            Thread.sleep(2000);
            actions = new Actions(driver);
            Object currentWindow = driver.getWindowHandles().toArray()[0];
            driver.switchTo().window((String) currentWindow);
            actions.moveToElement(homePageObjects.getPersonalQueue()).build().perform();
            Thread.sleep(2000);
            WebElement createBulkItems = homePageObjects.getCreateBulkItems();
            createBulkItems.click();
            Thread.sleep(5000);
            WebDriverWait webDriverWait = new WebDriverWait(driver, 120);
            webDriverWait.until(ExpectedConditions.visibilityOf(createBulkItemsPageObjects.getUploadItems()));
            return new CreateBulkItems(driver, scenarioOperator);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while selecting Create Bulk Items");
            throw new Exception("Error while selecting  Create Bulk Items");
        }
    }

    public CreateSearchItemPage goToIndexWork() throws Exception {
        log.info("Going To Index Work.");
        try {
            Thread.sleep(2000);
            actions = new Actions(driver);
            Object currentWindow = driver.getWindowHandles().toArray()[0];
            driver.switchTo().window((String) currentWindow);
            driver.manage().window().maximize();
            actions.moveToElement(homePageObjects.getIndexWork()).build().perform();
            Thread.sleep(2000);

            log.info("Clicking Index Items.");
            try {
                homePageObjects.getIndexItems().click();
            } catch (Exception e) {
                log.error("Error while clicking Index Items.");
                throw new Exception("Error while clicking Index Items.");
            }

            Thread.sleep(2000);
            WebDriverWait webDriverWait = new WebDriverWait(driver, 60);
            webDriverWait.until(ExpectedConditions.visibilityOf(indexItemsPageObjects.getIndexItemsForm()));
            return new CreateSearchItemPage(driver, scenarioOperator);
        } catch (Exception e) {
            log.error("Error while selecting Index Work.");
            throw new Exception("Error while selecting Index Work.");
        }
    }

    public LegalAdvisorAdminPage selectLegalAdvisorOrAdmin() throws Exception {
        log.info("Selecting Legal Advisor Admin.");
        try {
            Thread.sleep(2000);
            actions = new Actions(driver);
            Object currentWindow = driver.getWindowHandles().toArray()[0];
            driver.switchTo().window((String) currentWindow);
            actions.moveToElement(homePageObjects.getAdministration()).build().perform();
            Thread.sleep(2000);
            homePageObjects.getLegalAdvisorAdmin().click();
            Thread.sleep(2000);
            return new LegalAdvisorAdminPage(driver, scenarioOperator);
        } catch (Exception e) {
            log.error("Error while selecting Legal Advisor Admin.");
            throw new Exception("Error while selecting Legal Advisor Admin.");
        }
    }

    public void maximizeServiceWindow() {
        actions = new Actions(driver);
        Object currentWindow = driver.getWindowHandles().toArray()[1];
        driver.switchTo().window((String) currentWindow);
        driver.manage().window().maximize();
    }

    public void closeOpenWindow(int index) throws Exception {
        log.info("Closing Open Window At Index " + index);
        try {
            switchToAndCloseServiceWindow(index);
        } catch (Exception e) {
            log.error("Error while closing Open Window At Index " + index);
            throw new Exception("Error while closing Open Window At Index " + index);
        }
    }

}
