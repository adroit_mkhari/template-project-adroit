package co.za.fnb.flow.pages.principal_member_details.page_factory;

import co.za.fnb.flow.tester.ScenarioOperator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import co.za.fnb.flow.pages.principal_member_details.PrincipalMemberDetails;

public class PrincipalMemberPostalInformationPageObjects extends PrincipalMemberDetails {

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:postalInformationPanel_content')]//table//tbody//tr//td//div[contains (@id, 'createSearchItemView:mainTabView:copyAddress')]")
    private WebElement copyAddress;

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:postalInformationPanel_content')]//table//tbody//tr//td//input[contains (@id, 'createSearchItemView:mainTabView:postalLine1')]")
    private WebElement postalLine_1;

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:postalInformationPanel_content')]//table//tbody//tr//td//input[contains (@id, 'createSearchItemView:mainTabView:postalLine2')]")
    private WebElement postalLine_2;

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:postalInformationPanel_content')]//table//tbody//tr//td//input[contains (@id, 'createSearchItemView:mainTabView:postalSuburb')]")
    private WebElement postalSuburb;

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:postalInformationPanel_content')]//table//tbody//tr//td//input[contains (@id, 'createSearchItemView:mainTabView:postalCity')]")
    private WebElement postalCity;

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:postalInformationPanel_content')]//table//tbody//tr//td//table//tbody//tr//td//input[contains (@id, 'createSearchItemView:mainTabView:postalCode')]")
    private WebElement postalCode;

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:postalInformationPanel_content')]//table//tbody//tr//td//table//tbody//tr//td//button[contains (@id, 'createSearchItemView:mainTabView:searchPostalAddress')]")
    private WebElement searchPostalAddress;

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:postalInformationPanel_content')]//table//tbody//tr//td//div[contains (@id, 'createSearchItemView:mainTabView:cisCopyAddress')]")
    private WebElement cisCopyAddress;

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:postalInformationPanel_content')]//table//tbody//tr//td//input[contains (@id, 'createSearchItemView:mainTabView:cisPostalAddressLine1')]")
    private WebElement cisPostalAddressLine_1;

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:postalInformationPanel_content')]//table//tbody//tr//td//input[contains (@id, 'createSearchItemView:mainTabView:cisPostalAddressLine2')]")
    private WebElement cisPostalAddressLine_2;

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:postalInformationPanel_content')]//table//tbody//tr//td//input[contains (@id, 'createSearchItemView:mainTabView:cisPostalCity')]")
    private WebElement cisPostalCity;

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:postalInformationPanel_content')]//table//tbody//tr//td//table//tbody//tr//td//input[contains (@id, 'createSearchItemView:mainTabView:cisPostalCode')]")
    private WebElement cisPostalCode;

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:postalInformationPanel_content')]//table//tbody//tr//td//table//tbody//tr//td//button[contains (@id, 'createSearchItemView:mainTabView:cisSearchPostalAddress')]")
    private WebElement cisSearchPostalAddress;

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:postalInformationPanel_content')]//table//tbody//tr//td//input[contains (@id, 'createSearchItemView:mainTabView:cisPostalState')]")
    private WebElement cisPostalState;

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:postalInformationPanel_content')]//table//tbody//tr//td//input[contains (@id, 'createSearchItemView:mainTabView:cisPostalCountry')]")
    private WebElement cisPostalCountry;

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:postalInformationPanel_content')]//table//tbody//tr//td//table//tbody//tr//td//input[contains (@id, 'createSearchItemView:mainTabView:cisZipCode')]")
    private WebElement cisZipCode;

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:postalInformationPanel_content')]//table//tbody//tr//td//table//tbody//tr//td//button[contains (@id, 'createSearchItemView:mainTabView:cisSearchCisZipCode')]")
    private WebElement cisSearchCisZipCode;

    public PrincipalMemberPostalInformationPageObjects(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
        PageFactory.initElements(driver, this);
    }

    public WebElement getCopyAddress() {
        return copyAddress;
    }

    public WebElement getPostalLine_1() {
        return postalLine_1;
    }

    public WebElement getPostalLine_2() {
        return postalLine_2;
    }

    public WebElement getPostalSuburb() {
        return postalSuburb;
    }

    public WebElement getPostalCity() {
        return postalCity;
    }

    public WebElement getPostalCode() {
        return postalCode;
    }

    public WebElement getSearchPostalAddress() {
        return searchPostalAddress;
    }

    public WebElement getCisCopyAddress() {
        return cisCopyAddress;
    }

    public WebElement getCisPostalAddressLine_1() {
        return cisPostalAddressLine_1;
    }

    public WebElement getCisPostalAddressLine_2() {
        return cisPostalAddressLine_2;
    }

    public WebElement getCisPostalCity() {
        return cisPostalCity;
    }

    public WebElement getCisPostalCode() {
        return cisPostalCode;
    }

    public WebElement getCisSearchPostalAddress() {
        return cisSearchPostalAddress;
    }

    public WebElement getCisPostalState() {
        return cisPostalState;
    }

    public WebElement getCisPostalCountry() {
        return cisPostalCountry;
    }

    public WebElement getCisZipCode() {
        return cisZipCode;
    }

    public WebElement getCisSearchCisZipCode() {
        return cisSearchCisZipCode;
    }
}
