package co.za.fnb.flow.pages.tms;

import co.za.fnb.flow.pages.BasePage;
import co.za.fnb.flow.pages.tms.page_factory.TmsLettersContentPageFactory;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class TmsContentPage extends BasePage {
    private Logger log  = LogManager.getLogger(TmsContentPage.class);
    private TmsLettersContentPageFactory tmsLettersContentPageFactory = new TmsLettersContentPageFactory(driver, scenarioOperator);

    public TmsContentPage(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
        PageFactory.initElements(driver, this);
    }

    public void clickContentOne() throws Exception {
        try {
            click(tmsLettersContentPageFactory.getContentOne());
        } catch (Exception e) {
            log.error("Error while clicking Content 1.");
            throw new Exception("Error while clicking Content 1.");
        }
    }

    public void clickContentTwo() throws Exception {
        try {
            List<WebElement> buttons = tmsLettersContentPageFactory.getContentTwo();
            for (WebElement button : buttons) {
                String buttonText = button.getText();

                if (!buttonText.isEmpty()) {
                    if (buttonText.equalsIgnoreCase("Content2")) {
                        click(button);
                    }
                }
            }
        } catch (Exception e) {
            log.error("Error while clicking Content 2.");
            throw new Exception("Error while clicking Content 2.");
        }
    }

    public void clickDownload() throws Exception {
        try {
            click(tmsLettersContentPageFactory.getDownload());
        } catch (Exception e) {
            log.error("Error while clicking Download.");
            throw new Exception("Error while clicking Download.");
        }
    }

    public boolean checkToolbarContainer() throws Exception {
        try {
            WebElement toolbarContainer = tmsLettersContentPageFactory.getToolbarContainer();
            return toolbarContainer.isDisplayed();
        } catch (Exception e) {
            log.error("Error while checking ToolbarContainer display.");
            throw new Exception("Error while checking ToolbarContainer display.");
        }
    }

    public boolean checkPageNumberLabel() throws Exception {
        try {
            WebElement pageNumberLabel = tmsLettersContentPageFactory.getPageNumberLabel();
            return pageNumberLabel.isDisplayed();
        } catch (Exception e) {
            log.error("Error while checking PageNumberLabel display.");
            throw new Exception("Error while checking PageNumberLabel display.");
        }
    }

    public boolean checkPdfJs() throws Exception {
        try {
            WebElement pdfJs = tmsLettersContentPageFactory.getPdfJs();
            return pdfJs.isDisplayed();
        } catch (Exception e) {
            log.error("Error while checking Pdf Js display.");
            throw new Exception("Error while checking Pdf Js display.");
        }
    }

}
