package co.za.fnb.flow.pages.tms;

import co.za.fnb.flow.pages.BasePage;
import co.za.fnb.flow.pages.tms.page_factory.TmsDistributionPageFactory;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class TmsDistributionPage extends BasePage {
    private Logger log  = LogManager.getLogger(TmsDistributionPage.class);
    private TmsDistributionPageFactory tmsDistributionPageFactory = new TmsDistributionPageFactory(driver, scenarioOperator);

    public TmsDistributionPage(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
        PageFactory.initElements(driver, this);
    }

    public void clickBusinessUnitOwner() throws Exception {
        try {
            click(tmsDistributionPageFactory.getBusinessUnitOwner());
        } catch (Exception e) {
            log.error("Error while clicking Business Unit Owner.");
            throw new Exception("Error while clicking Business Unit Owner.");
        }
    }

    public void clickBusinessUnitOwnerOption() throws Exception {
        try {
            click(tmsDistributionPageFactory.getBusinessUnitOwnerOption());
        } catch (Exception e) {
            log.error("Error while clicking Business Unit Owner Option.");
            throw new Exception("Error while clicking Business Unit Owner Option.");
        }
    }

    public void clickCompanyCode() throws Exception {
        try {
            click(tmsDistributionPageFactory.getCompanyCode());
        } catch (Exception e) {
            log.error("Error while clicking Company Code.");
            throw new Exception("Error while clicking Company Code.");
        }
    }

    public void clickCompanyCodeOption() throws Exception {
        try {
            click(tmsDistributionPageFactory.getCompanyCodeOption());
        } catch (Exception e) {
            log.error("Error while clicking Company Code Option.");
            throw new Exception("Error while clicking Company Option.");
        }
    }

    public TmsLettersTablePage clickSearch() throws Exception {
        try {
            click(tmsDistributionPageFactory.getSearch());
            return new TmsLettersTablePage(driver, scenarioOperator);
        } catch (Exception e) {
            log.error("Error while clicking Search.");
            throw new Exception("Error while clicking Search.");
        }
    }

    public void clickSendAll() throws Exception {
        try {
            click(tmsDistributionPageFactory.getSendAll());
        } catch (Exception e) {
            log.error("Error while clicking Send All.");
            throw new Exception("Error while clicking Send All.");
        }
    }

}
