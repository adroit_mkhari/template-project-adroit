package co.za.fnb.flow.tester.services.models;

import cucumber.api.DataTable;
import gherkin.formatter.model.Row;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class TakeupScenarioFactory {
    static HashMap<String, TakeupScenario> takeupScenarios = new HashMap<>();

    private static int getHeaderIndex(Object[] headers, String header) {
        return Arrays.asList(headers).indexOf(header);
    }

    private static String getCellValue(List<String> dataEntry, Object[] headers, String header) {
        String cellValue = "";
        try {
            int headerIndex = getHeaderIndex(headers, header);
            if (headerIndex != -1) {
                cellValue = dataEntry.get(headerIndex);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cellValue;
    }

    public static void loadTakeupScenarios(DataTable queriesTable) {
        Object[] headers = queriesTable.getGherkinRows().get(0).getCells().toArray();
        List<String> dataEntry;
        TakeupScenario takeupScenario;
        for (
                Row row : queriesTable.getGherkinRows()) {
            if (row.getLine() != 1) {
                dataEntry = row.getCells();

                ContactDetails contactDetails = new ContactDetails();
                contactDetails.setEmail(getCellValue(dataEntry, headers, "Email"));
                contactDetails.setCellNumber(getCellValue(dataEntry, headers, "Cell Number"));
                contactDetails.setHomeNumber(getCellValue(dataEntry, headers, "Home Number"));
                contactDetails.setPostalAddressStreet(getCellValue(dataEntry, headers,"Postal Address Street"));
                contactDetails.setPostalAddressSuburb(getCellValue(dataEntry, headers,"Postal Address Suburb"));
                contactDetails.setPostalAddresCity(getCellValue(dataEntry, headers,"Postal Addres City"));
                contactDetails.setPostalAddressCode(getCellValue(dataEntry, headers,"Postal Address Code"));

                BankingDetails bankingDetails = new BankingDetails();
                bankingDetails.setBankName(getCellValue(dataEntry, headers,"Bank Name"));
                bankingDetails.setBankBranchName(getCellValue(dataEntry, headers,"Bank Branch Name"));
                bankingDetails.setBankBranchCode(getCellValue(dataEntry, headers,"Bank Branch Code"));
                bankingDetails.setBankAccountNumber(getCellValue(dataEntry, headers,"Bank Account Number"));
                bankingDetails.setPaymentDay(getCellValue(dataEntry, headers,"Payment Day"));
                bankingDetails.setDueDate(getCellValue(dataEntry, headers,"Due Date"));
                bankingDetails.setBankAccountCode(getCellValue(dataEntry, headers,"Bank Account Code"));
                bankingDetails.setBankAccountDesc(getCellValue(dataEntry, headers,"Bank Account Desc"));

                PolicyHolder policyHolder = new PolicyHolder();
                policyHolder.setRelationship(getCellValue(dataEntry, headers,"Policy Holder Relationship"));
                policyHolder.setTittle(getCellValue(dataEntry, headers,"Policy Holder Title"));
                policyHolder.setFirstname(getCellValue(dataEntry, headers,"Policy Holder First Name"));
                policyHolder.setName(getCellValue(dataEntry, headers,"Policy Holder Name"));
                policyHolder.setSurname(getCellValue(dataEntry, headers,"Policy Holder Surname"));
                policyHolder.setMiddleName(getCellValue(dataEntry, headers,"Policy Holder Middle Name"));
                policyHolder.setBirthDate(getCellValue(dataEntry, headers,"Policy Holder Birth Date"));
                policyHolder.setIdType(getCellValue(dataEntry, headers,"Policy Holder Id Type"));
                policyHolder.setIdNumber(getCellValue(dataEntry, headers,"Policy Holder Id Number"));
                policyHolder.setGender(getCellValue(dataEntry, headers,"Policy Holder Gender"));
                policyHolder.setCoverAmount(getCellValue(dataEntry, headers,"Policy Holder Cover Amount"));
                policyHolder.setPremium(getCellValue(dataEntry, headers,"Policy Holder Premium"));

                AuthorizedPerson authorizedPerson = new AuthorizedPerson();
                authorizedPerson.setRelationship(getCellValue(dataEntry, headers,"Authorized Person Relationship"));
                authorizedPerson.setName(getCellValue(dataEntry, headers,"Authorized Person Name"));
                authorizedPerson.setMiddleName(getCellValue(dataEntry, headers,"Authorized Person Middle Name"));
                authorizedPerson.setBirthDate(getCellValue(dataEntry, headers,"Authorized Person Birth Date"));
                authorizedPerson.setIdType(getCellValue(dataEntry, headers,"Authorized Person Id Type"));
                authorizedPerson.setIdNumber(getCellValue(dataEntry, headers,"Authorized Person Id Number"));
                authorizedPerson.setGender(getCellValue(dataEntry, headers,"Authorized Person Gender"));
                authorizedPerson.setCoverAmount(getCellValue(dataEntry, headers,"Authorized Person Cover Amount"));
                authorizedPerson.setPremium(getCellValue(dataEntry, headers,"Authorized Person Premium"));

                Spouse spouse = new Spouse();
                spouse.setAction(getCellValue(dataEntry, headers,"Spouse Action"));
                spouse.setRelationship(getCellValue(dataEntry, headers,"Spouse Relationship"));
                spouse.setName(getCellValue(dataEntry, headers,"Spouse Name"));
                spouse.setMiddleName(getCellValue(dataEntry, headers,"Spouse Middle Name"));
                spouse.setBirthDate(getCellValue(dataEntry, headers,"Spouse Birth Date"));
                spouse.setIdType(getCellValue(dataEntry, headers,"Spouse Id Type"));
                spouse.setIdNumber(getCellValue(dataEntry, headers,"Spouse Id Number"));
                spouse.setGender(getCellValue(dataEntry, headers,"Spouse Gender"));
                spouse.setCoverAmount(getCellValue(dataEntry, headers,"Spouse Cover Amount"));
                spouse.setPremium(getCellValue(dataEntry, headers,"Spouse Premium"));

                Child child = new Child();
                child.setRelationship(getCellValue(dataEntry, headers,"Child Relationship"));
                child.setName(getCellValue(dataEntry, headers,"Child Name"));
                child.setMiddleName(getCellValue(dataEntry, headers,"Child Middle Name"));
                child.setBirthDate(getCellValue(dataEntry, headers,"Child Birth Date"));
                child.setIdType(getCellValue(dataEntry, headers,"Child Id Type"));
                child.setIdNumber(getCellValue(dataEntry, headers,"Child Id Number"));
                child.setGender(getCellValue(dataEntry, headers,"Child Gender"));
                child.setCoverAmount(getCellValue(dataEntry, headers,"Child Cover Amount"));
                child.setPremium(getCellValue(dataEntry, headers,"Child Premium"));

                ParentExtendedFamily parentExtendedFamily = new ParentExtendedFamily();
                parentExtendedFamily.setRelationship(getCellValue(dataEntry, headers,"Parent/Extended Family Relationship"));
                parentExtendedFamily.setName(getCellValue(dataEntry, headers,"Parent/Extended Family Name"));
                parentExtendedFamily.setMiddleName(getCellValue(dataEntry, headers,"Parent/Extended Family Middle Name"));
                parentExtendedFamily.setBirthDate(getCellValue(dataEntry, headers,"Parent/Extended Family Birth Date"));
                parentExtendedFamily.setIdType(getCellValue(dataEntry, headers,"Parent/Extended Family Id Type"));
                parentExtendedFamily.setIdNumber(getCellValue(dataEntry, headers,"Parent/Extended Family Id Number"));
                parentExtendedFamily.setGender(getCellValue(dataEntry, headers,"Parent/Extended Family Gender"));
                parentExtendedFamily.setCoverAmount(getCellValue(dataEntry, headers,"Parent/Extended Family Cover Amount"));
                parentExtendedFamily.setPremium(getCellValue(dataEntry, headers,"Parent/Extended Family Premium"));

                Beneficiary beneficiary = new Beneficiary();
                beneficiary.setAdd(getCellValue(dataEntry, headers,"Add Beneficiary"));
                beneficiary.setRelationship(getCellValue(dataEntry, headers,"Beneficiary Relationship"));
                beneficiary.setName(getCellValue(dataEntry, headers,"Beneficiary Name"));
                beneficiary.setBirthDate(getCellValue(dataEntry, headers,"Beneficiary Birth Date"));
                beneficiary.setIdNumber(getCellValue(dataEntry, headers,"Beneficiary Id Number"));
                beneficiary.setGender(getCellValue(dataEntry, headers,"Beneficiary Gender"));
                beneficiary.setCellNumber(getCellValue(dataEntry, headers,"Beneficiary Cell Number"));
                beneficiary.setEmail(getCellValue(dataEntry, headers,"Beneficiary Email"));

                takeupScenario = new TakeupScenario(
                        getCellValue(dataEntry, headers,"Takeup Code"),
                        getCellValue(dataEntry, headers,"Scenario Description"),
                        getCellValue(dataEntry, headers,"Product Name"),
                        getCellValue(dataEntry, headers,"Premium Status"),
                        getCellValue(dataEntry, headers,"Required Policy Status"),
                        getCellValue(dataEntry, headers,"Username"),
                        getCellValue(dataEntry, headers,"Update Fica Status"),
                        getCellValue(dataEntry, headers,"Request Type"),
                        getCellValue(dataEntry, headers,"Child Role Id(s) - Action(s)"),
                        getCellValue(dataEntry, headers,"Parent/Extended Family Role Id(s) - Action(s)"),
                        getCellValue(dataEntry, headers,"Reference Number"),
                        getCellValue(dataEntry, headers,"Sub Channel"),
                        getCellValue(dataEntry, headers,"Campaign Number"),
                        getCellValue(dataEntry, headers,"Leads Number"),
                        getCellValue(dataEntry, headers,"UCN Number"),
                        getCellValue(dataEntry, headers,"Product Code"),
                        getCellValue(dataEntry, headers,"Sub Prod Code"),
                        getCellValue(dataEntry, headers,"Sales Person"),
                        getCellValue(dataEntry, headers,"Sales Person ID"),
                        getCellValue(dataEntry, headers,"Sales Person Number"),
                        getCellValue(dataEntry, headers,"Captured By"),
                        getCellValue(dataEntry, headers,"Captured By No"),
                        getCellValue(dataEntry, headers,"Captured Date"),
                        getCellValue(dataEntry, headers,"Inception Date"),
                        getCellValue(dataEntry, headers,"Company Name"),
                        getCellValue(dataEntry, headers,"Trading Name"),
                        getCellValue(dataEntry, headers,"Business Vat No"),
                        getCellValue(dataEntry, headers,"Company Registration No"),
                        getCellValue(dataEntry, headers,"Gross Income"),
                        getCellValue(dataEntry, headers,"Cover Amount"),
                        contactDetails,
                        bankingDetails,
                        policyHolder,
                        authorizedPerson,
                        spouse,
                        child,
                        parentExtendedFamily,
                        beneficiary);

                takeupScenarios.put(takeupScenario.getTakeupCode(), takeupScenario);
            }
        }
    }

    public static TakeupScenario getTakeupScenario(String takeupCode) {
        return takeupScenarios.get(takeupCode);
    }

    public static HashMap<String, TakeupScenario> getTakeupScenarios() {
        return takeupScenarios;
    }
}
