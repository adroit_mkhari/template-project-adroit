package co.za.fnb.flow.pages.root_page_factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CreateOrUpdateWorkItemPageObjects {

    // TODO: Review these web elements locators.

    @FindBy(xpath = "//*[contains(@id, 'referenceNumber')]")
    private WebElement referenceNumber;

    @FindBy(id = "createSearchItemView:selectWorkTypes_label")
    private WebElement selectWorkTypeSelect;

    @FindBy(id = "createSearchItemView:selectWorkTypes_items")
    private WebElement selectWorkTypeSelectItems; // Uses @data-label

    @FindBy(xpath = "//*[contains(@id, 'selectWorkTypes_items')]")
    private WebElement selectWorkTypeTable;

    @FindBy(id = "createSearchItemView:selectStatus_label")
    private WebElement statusSelect;

    @FindBy(id = "createSearchItemView:selectStatus_items")
    private WebElement statusSelectItems;

    @FindBy(xpath = "createSearchItemView:selectQueues_label")
    private WebElement queue;

    @FindBy(id = "createSearchItemView:recordDissatisfaction")
    private WebElement recordDissatisfaction;

    @FindBy(id = "createSearchItemView:voiceLogRef")
    private WebElement voiceLogRef;

    @FindBy(id = "createSearchItemView:createdDate")
    private WebElement createdDate;

    @FindBy(id = "createSearchItemView:lockedBy")
    private WebElement lockedBy;

    @FindBy(id = "createSearchItemView:assignToMe")
    private WebElement assignToMe;

    @FindBy(id = "createSearchItemView:idNumber")
    private WebElement idNumber;

    @FindBy(id = "createSearchItemView:ucnNumber")
    private WebElement ucnNumber;

    @FindBy(id = "createSearchItemView:loadExtWorkTypes")
    private WebElement loadExtWorkTypes;

    @FindBy(id = "cloneView:noOfClones_label")
    private WebElement noOfClonesSelect;

    @FindBy(id = "cloneView:noOfClones_items")
    private WebElement noOfClonesSelectItems;

    @FindBy(id = "cloneView:queues_label")
    private WebElement cloneViewQueueSelect;

    @FindBy(id = "cloneView:queues_items")
    private WebElement cloneViewQueueItems;

    @FindBy(id = "cloneView:Clone")
    private WebElement cloneViewClone;

    @FindBy(id = "cloneView:Cancel")
    private WebElement cloneViewCancel;

    @FindBy(id = "createSearchItemView:mainTabView:commentstable:AddComments")
    private WebElement addComments;

    @FindBy(xpath = "//*[contains(@id,'addCommentsView')]")
    private WebElement addCommentsView;

    @FindBy(xpath = "//*[contains(@id,'addCommentsView:selectCategory')]")
    private WebElement addCommentsViewSelectCategory;

    @FindBy(xpath = "//*[contains(@id,'addCommentsView:selectCategory_items')]")
    private WebElement addCommentsViewSelectCategoryItems;

    @FindBy(xpath = "//*[contains(@id,'addCommentsView:selectComment')]")
    private WebElement commentSelect;

    @FindBy(xpath = "//*[contains(@id,'addCommentsView:selectComment_items')]")
    private WebElement commentSelectItems;

    @FindBy(xpath = "//*[contains(@id,'addCommentsView:j_idt17')]")
    private WebElement summitComment;

    @FindBy(id = "addCommentsView:addCommentsinputTextArea")
    private WebElement commentsInputTextArea;

    @FindBy(id = "addCommentsView:Add")
    private WebElement addComment;

    @FindBy(id = "addCommentsView:Clear")
    private WebElement clear;

    @FindBy(id = "addCommentsView:Cancel")
    private WebElement cancel;

    @FindBy(xpath = "//span[contains(@id,'workItemTablePolicyNumber')]")
    private WebElement workItemData;

    @FindBy(id = "createSearchItemView:mainTabView:policyDocumentsTable:0:documentsUploadedDate")
    private WebElement documentsData;

    @FindBy(linkText = "Correspondence")
    private WebElement correspondence;

    @FindBy(xpath = "//*[@id=\"createSearchItemView:mainTabView:itemTabView:j_idt80\"]/ul/li[1]/a")
    private WebElement correspondenceEmail;

    @FindBy(xpath = "//*[@id=\"createSearchItemView:mainTabView:itemTabView:j_idt80\"]/ul/li[2]/a")
    private WebElement correspondenceSms;

    @FindBy(id = "createSearchItemView:mainTabView:itemTabView:j_idt80:smsTable_data")
    private WebElement createSearchItemSmsTableData;

    @FindBy(id = "createSearchItemView:mainTabView:itemTabView:j_idt80:smsTable:CreateSearchItemOpenSms")
    private WebElement createSearchItemOpenSms;

    @FindBy(id = "createSearchItemView:mainTabView:itemTabView:j_idt80:smsTable:CreateSearchItemNewSms")
    private WebElement createSearchItemNewSms;

    @FindBy(id = "smsView:toAddress")
    private WebElement smsViewToAddress;

    @FindBy(id = "smsView:template_label")
    private WebElement smsViewTemplateSelect;

    @FindBy(id = "smsView:template_items")
    private WebElement smsViewTemplateSelectItems;

    @FindBy(id = "smsView:message")
    private WebElement smsViewMessage;

    @FindBy(id = "smsView:selectQueues_label")
    private WebElement smsViewQueueSelect;

    @FindBy(id = "smsView:selectQueues_items")
    private WebElement smsViewQueueSelectItems;

    @FindBy(id = "smsView:Send")
    private WebElement smsViewSend;

    @FindBy(id = "//*[contains(@id,'CreateSearchItemNewEmail')]")
    private WebElement newEmail;

    @FindBy(id = "emailView:toAddress")
    private WebElement toEmailAddress;

    @FindBy(id = "emailView:ccAddress")
    private WebElement ccEmailAddress;

    @FindBy(id = "emailView:replyAddress_label")
    private WebElement replyAddressSelect;

    @FindBy(id = "emailView:replyAddress_items")
    private WebElement replyAddressSelectItems;

    @FindBy(id = "emailView:subject")
    private WebElement emailSubject;

    // TODO: Get The Correct Locator
    @FindBy(id = "createSearchItemView:mainTabView:workItemtable_data")
    private WebElement emailMessage;

    @FindBy(id = "emailView:signature")
    private WebElement emailSignature;

    @FindBy(id = "emailView:Send")
    private WebElement send;

    @FindBy(xpath = "//*[contains(@id,'workItemDocumentsBrowse_input')]")
    private WebElement documentsBrowse;

    @FindBy(xpath = "//button[span[contains(text(),'Upload')]]")
    private WebElement upload;

    @FindBy(id = "frmIndexData:policyNumber")
    private WebElement documentPolicyNumber;

    @FindBy(id = "ffrmIndexData:iDNumber")
    private WebElement documentIDNumber;

    @FindBy(id = "frmIndexData:UCNNumber")
    private WebElement documentUCNNumber;

    @FindBy(id = "frmIndexData:firstName")
    private WebElement documentFirstName;

    @FindBy(id = "frmIndexData:surname")
    private WebElement documentSurname;

    @FindBy(id = "frmIndexData:dateOfBirth_input")
    private WebElement documentDateOfBirth;

    @FindBy(id = "frmIndexData:selectDocType_label")
    private WebElement documentTypeSelect;

    @FindBy(id = "frmIndexData:selectDocType_items")
    private WebElement documentTypeSelectItems;

    @FindBy(id = "frmIndexData:selectDocStatus_label")
    private WebElement documentStatusSelect;

    @FindBy(id = "frmIndexData:selectDocStatus_items")
    private WebElement documentStatusSelectItems;

    @FindBy(id = "frmIndexData:selectWorkTypes_label")
    private WebElement documentWorkTypeSelect;

    @FindBy(id = "frmIndexData:selectWorkTypes_items")
    private WebElement documentWorkTypeSelectItems;

    @FindBy(id = "createSearchItemView:selectQueues_label")
    private WebElement documentWorkQueueSelect;

    @FindBy(id = "createSearchItemView:selectQueues_items")
    private WebElement documentWorkQueueSelectItems;

    @FindBy(id = "frmIndexData:itemPolicyNumber")
    private WebElement documentWorkItemPolicyNumber;

    @FindBy(id = "frmIndexData:AddItem")
    private WebElement documentAddItem;

    @FindBy(id = "frmIndexData:Index")
    private WebElement documentIndex;

    @FindBy(id = "frmDocumentView:SelectAll")
    private WebElement documentSelectAll;

    @FindBy(id = "createSearchItemView:paymentSeqNumber")
    private WebElement paymentSequenceNumber;

    @FindBy(xpath = "//*[@id=\"j_idt8\"]/label")
    private WebElement confirmMessage;

    @FindBy(id = "j_idt8:yes")
    private WebElement confirmYes;

    @FindBy(id = "j_idt8:no")
    private WebElement confirmNo;

    public CreateOrUpdateWorkItemPageObjects(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public WebElement getReferenceNumber() {
        return referenceNumber;
    }

    public WebElement getSelectWorkTypeSelect() {
        return selectWorkTypeSelect;
    }

    public WebElement getSelectWorkTypeSelectItems() {
        return selectWorkTypeSelectItems;
    }

    public WebElement getSelectWorkTypeTable() {
        return selectWorkTypeTable;
    }

    public WebElement getStatusSelect() {
        return statusSelect;
    }

    public WebElement getStatusSelectItems() {
        return statusSelectItems;
    }

    public WebElement getQueue() {
        return queue;
    }

    public WebElement getRecordDissatisfaction() {
        return recordDissatisfaction;
    }

    public WebElement getVoiceLogRef() {
        return voiceLogRef;
    }

    public WebElement getCreatedDate() {
        return createdDate;
    }

    public WebElement getLockedBy() {
        return lockedBy;
    }

    public WebElement getAssignToMe() {
        return assignToMe;
    }

    public WebElement getIdNumber() {
        return idNumber;
    }

    public WebElement getUcnNumber() {
        return ucnNumber;
    }

    public WebElement getLoadExtWorkTypes() {
        return loadExtWorkTypes;
    }

    public WebElement getNoOfClonesSelect() {
        return noOfClonesSelect;
    }

    public WebElement getNoOfClonesSelectItems() {
        return noOfClonesSelectItems;
    }

    public WebElement getCloneViewQueueSelect() {
        return cloneViewQueueSelect;
    }

    public WebElement getCloneViewQueueItems() {
        return cloneViewQueueItems;
    }

    public WebElement getCloneViewClone() {
        return cloneViewClone;
    }

    public WebElement getCloneViewCancel() {
        return cloneViewCancel;
    }

    public WebElement getAddComments() {
        return addComments;
    }

    public WebElement getAddCommentsView() {
        return addCommentsView;
    }

    public WebElement getAddCommentsViewSelectCategory() {
        return addCommentsViewSelectCategory;
    }

    public WebElement getAddCommentsViewSelectCategoryItems() {
        return addCommentsViewSelectCategoryItems;
    }

    public WebElement getCommentSelect() {
        return commentSelect;
    }

    public WebElement getCommentSelectItems() {
        return commentSelectItems;
    }

    public WebElement getSummitComment() {
        return summitComment;
    }

    public WebElement getCommentsInputTextArea() {
        return commentsInputTextArea;
    }

    public WebElement getAddComment() {
        return addComment;
    }

    public WebElement getClear() {
        return clear;
    }

    public WebElement getCancel() {
        return cancel;
    }

    public WebElement getWorkItemData() {
        return workItemData;
    }

    public WebElement getDocumentsData() {
        return documentsData;
    }

    public WebElement getCorrespondence() {
        return correspondence;
    }

    public WebElement getCorrespondenceEmail() {
        return correspondenceEmail;
    }

    public WebElement getCorrespondenceSms() {
        return correspondenceSms;
    }

    public WebElement getCreateSearchItemSmsTableData() {
        return createSearchItemSmsTableData;
    }

    public WebElement getCreateSearchItemOpenSms() {
        return createSearchItemOpenSms;
    }

    public WebElement getCreateSearchItemNewSms() {
        return createSearchItemNewSms;
    }

    public WebElement getSmsViewToAddress() {
        return smsViewToAddress;
    }

    public WebElement getSmsViewTemplateSelect() {
        return smsViewTemplateSelect;
    }

    public WebElement getSmsViewTemplateSelectItems() {
        return smsViewTemplateSelectItems;
    }

    public WebElement getSmsViewMessage() {
        return smsViewMessage;
    }

    public WebElement getSmsViewQueueSelect() {
        return smsViewQueueSelect;
    }

    public WebElement getSmsViewQueueSelectItems() {
        return smsViewQueueSelectItems;
    }

    public WebElement getSmsViewSend() {
        return smsViewSend;
    }

    public WebElement getNewEmail() {
        return newEmail;
    }

    public WebElement getToEmailAddress() {
        return toEmailAddress;
    }

    public WebElement getCcEmailAddress() {
        return ccEmailAddress;
    }

    public WebElement getReplyAddressSelect() {
        return replyAddressSelect;
    }

    public WebElement getReplyAddressSelectItems() {
        return replyAddressSelectItems;
    }

    public WebElement getEmailSubject() {
        return emailSubject;
    }

    public WebElement getEmailMessage() {
        return emailMessage;
    }

    public WebElement getEmailSignature() {
        return emailSignature;
    }

    public WebElement getSend() {
        return send;
    }

    public WebElement getDocumentsBrowse() {
        return documentsBrowse;
    }

    public WebElement getUpload() {
        return upload;
    }

    public WebElement getDocumentPolicyNumber() {
        return documentPolicyNumber;
    }

    public WebElement getDocumentIDNumber() {
        return documentIDNumber;
    }

    public WebElement getDocumentUCNNumber() {
        return documentUCNNumber;
    }

    public WebElement getDocumentFirstName() {
        return documentFirstName;
    }

    public WebElement getDocumentSurname() {
        return documentSurname;
    }

    public WebElement getDocumentDateOfBirth() {
        return documentDateOfBirth;
    }

    public WebElement getDocumentTypeSelect() {
        return documentTypeSelect;
    }

    public WebElement getDocumentTypeSelectItems() {
        return documentTypeSelectItems;
    }

    public WebElement getDocumentStatusSelect() {
        return documentStatusSelect;
    }

    public WebElement getDocumentStatusSelectItems() {
        return documentStatusSelectItems;
    }

    public WebElement getDocumentWorkTypeSelect() {
        return documentWorkTypeSelect;
    }

    public WebElement getDocumentWorkTypeSelectItems() {
        return documentWorkTypeSelectItems;
    }

    public WebElement getDocumentWorkQueueSelect() {
        return documentWorkQueueSelect;
    }

    public WebElement getDocumentWorkQueueSelectItems() {
        return documentWorkQueueSelectItems;
    }

    public WebElement getDocumentWorkItemPolicyNumber() {
        return documentWorkItemPolicyNumber;
    }

    public WebElement getDocumentAddItem() {
        return documentAddItem;
    }

    public WebElement getDocumentIndex() {
        return documentIndex;
    }

    public WebElement getDocumentSelectAll() {
        return documentSelectAll;
    }

    public WebElement getPaymentSequenceNumber() {
        return paymentSequenceNumber;
    }

    public WebElement getConfirmMessage() {
        return confirmMessage;
    }

    public WebElement getConfirmYes() {
        return confirmYes;
    }

    public WebElement getConfirmNo() {
        return confirmNo;
    }

}
