package co.za.fnb.flow.pages.policy_details.page_factory;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class MemberInformationPageObjects {

    private String policyDetailsHash = "createSearchItemView:mainTabView:memberInfoTable:0:policyDetailshash";
    private String policyDetailsRelationship = "createSearchItemView:mainTabView:memberInfoTable:0:policyDetailsRelationship";
    private String policyDetailsRelationshipSelect = "createSearchItemView:mainTabView:memberInfoTable:0:policyDetailsRelationship_label";
    private String policyDetailsRelationshipSelectItems = "createSearchItemView:mainTabView:memberInfoTable:0:policyDetailsRelationship_items";
    private String policyDetailsFullNameCE = "createSearchItemView:mainTabView:memberInfoTable:0:policyDetailsFullNameCE";
    private String policyDetailsFullName = "createSearchItemView:mainTabView:memberInfoTable:0:policyDetailsFullName";
    private String policyDetailsMiddleNameCE = "createSearchItemView:mainTabView:memberInfoTable:0:policyDetailsMiddleNameCE";
    private String policyDetailsMiddleName = "createSearchItemView:mainTabView:memberInfoTable:0:policyDetailsMiddleName";
    private String policyDetailsIDNumberCE = "createSearchItemView:mainTabView:memberInfoTable:0:policyDetailsIDNumberCE";
    private String policyDetailsIDNumber = "createSearchItemView:mainTabView:memberInfoTable:0:policyDetailsIDNumber";
    private String policyDetailsDOBCE = "createSearchItemView:mainTabView:memberInfoTable:0:policyDetailsDOBCE";
    private String policyDetailsDOB = "createSearchItemView:mainTabView:memberInfoTable:0:policyDetailsDOB";
    private String policyDetailsGenderCE = "createSearchItemView:mainTabView:memberInfoTable:0:policyDetailsGenderCE";
    private String policyDetailsGender = "createSearchItemView:mainTabView:memberInfoTable:0:policyDetailsGender";
    private String policyDetailsGenderSelect = "createSearchItemView:mainTabView:memberInfoTable:0:policyDetailsGender_label";
    private String policyDetailsGenderSelectItems = "createSearchItemView:mainTabView:memberInfoTable:0:policyDetailsGender_items";
    private String emailAddressCE = "createSearchItemView:mainTabView:memberInfoTable:0:emailAddressCE";
    private String emailAddress = "createSearchItemView:mainTabView:memberInfoTable:0:emailAddress";
    private String cellPhoneNumberCE = "createSearchItemView:mainTabView:memberInfoTable:0:cellPhoneNumberCE";
    private String cellPhoneNumber = "createSearchItemView:mainTabView:memberInfoTable:0:cellPhoneNumber";
    private String memberCoverAmountCE = "createSearchItemView:mainTabView:memberInfoTable:0:memberCoverAmountCE";
    private String memberCoverAmountID = "createSearchItemView:mainTabView:memberInfoTable:0:memberCoverAmountID";
    private String memberCoverAmountRefresh = "createSearchItemView:mainTabView:memberInfoTable:0:cai";
    private String policyDetailsPremium = "createSearchItemView:mainTabView:memberInfoTable:0:policyDetailsPremium";
    private String coverDetails = "createSearchItemView:mainTabView:memberInfoTable:0:CoverDetails";
    private By memberInformationTableItems = By.xpath("//*//tr//td/span[contains (@id,':policyDetailsPremium')]");
    private By numberOfExistingMembers = By.xpath("//table/tbody[@id='createSearchItemView:mainTabView:memberInfoTable_data']/tr");

    public MemberInformationPageObjects(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public String getPolicyDetailsHash() {
        return policyDetailsHash;
    }

    public String getPolicyDetailsRelationship() {
        return policyDetailsRelationship;
    }

    public String getPolicyDetailsRelationshipSelect() {
        return policyDetailsRelationshipSelect;
    }

    public String getPolicyDetailsRelationshipSelectItems() {
        return policyDetailsRelationshipSelectItems;
    }

    public String getPolicyDetailsFullNameCE() {
        return policyDetailsFullNameCE;
    }

    public String getPolicyDetailsFullName() {
        return policyDetailsFullName;
    }

    public String getPolicyDetailsMiddleNameCE() {
        return policyDetailsMiddleNameCE;
    }

    public String getPolicyDetailsMiddleName() {
        return policyDetailsMiddleName;
    }

    public String getPolicyDetailsIDNumberCE() {
        return policyDetailsIDNumberCE;
    }

    public String getPolicyDetailsIDNumber() {
        return policyDetailsIDNumber;
    }

    public String getPolicyDetailsDOBCE() {
        return policyDetailsDOBCE;
    }

    public String getPolicyDetailsDOB() {
        return policyDetailsDOB;
    }

    public String getPolicyDetailsGenderCE() {
        return policyDetailsGenderCE;
    }

    public String getPolicyDetailsGender() {
        return policyDetailsGender;
    }

    public String getPolicyDetailsGenderSelect() {
        return policyDetailsGenderSelect;
    }

    public String getPolicyDetailsGenderSelectItems() {
        return policyDetailsGenderSelectItems;
    }

    public String getEmailAddressCE() {
        return emailAddressCE;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public String getCellPhoneNumberCE() {
        return cellPhoneNumberCE;
    }

    public String getCellPhoneNumber() {
        return cellPhoneNumber;
    }

    public String getMemberCoverAmountCE() {
        return memberCoverAmountCE;
    }

    public String getMemberCoverAmountID() {
        return memberCoverAmountID;
    }

    public String getMemberCoverAmountRefresh() {
        return memberCoverAmountRefresh;
    }

    public String getPolicyDetailsPremium() {
        return policyDetailsPremium;
    }

    public String getCoverDetails() {
        return coverDetails;
    }

    public By getMemberInformationTableItems() {
        return memberInformationTableItems;
    }

    public By getNumberOfExistingMembers() {
        return numberOfExistingMembers;
    }
}
