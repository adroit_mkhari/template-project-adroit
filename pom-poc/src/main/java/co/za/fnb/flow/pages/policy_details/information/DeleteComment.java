package co.za.fnb.flow.pages.policy_details.information;

import co.za.fnb.flow.pages.policy_details.PolicyDetails;
import co.za.fnb.flow.pages.policy_details.page_factory.DeleteCommentPageObjects;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;

public class DeleteComment extends PolicyDetails {
    private Logger log  = LogManager.getLogger(DeleteComment.class);
    DeleteCommentPageObjects deleteCommentPageObjects = new DeleteCommentPageObjects(driver);

    public DeleteComment(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
    }

    public void writeComment(String comment) throws Exception {
        try {
            type(deleteCommentPageObjects.getDeleteComments(), comment);
        } catch (Exception e) {
            log.error("Error while writing comment.");
            throw new Exception("Error while writing comment.");
        }
    }

    public void saveComment() throws Exception {
        try {
            click(deleteCommentPageObjects.getSaveComment());
        } catch (Exception e) {
            log.error("Error while clicking save comment.");
            throw new Exception("Error while clicking save comment.");
        }
    }

    public void remove() throws Exception {
        try {
            click(deleteCommentPageObjects.getRemove());
        } catch (Exception e) {
            log.error("Error while clicking remove/confirm.");
            throw new Exception("Error while clicking remove/confirm.");
        }
    }

    public void save() throws Exception {
        try {
            click(deleteCommentPageObjects.getSave());
        } catch (Exception e) {
            log.error("Error while clicking save.");
            throw new Exception("Error while clicking save.");
        }
    }
}
