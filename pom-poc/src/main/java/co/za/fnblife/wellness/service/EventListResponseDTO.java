
package co.za.fnblife.wellness.service;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for eventListResponseDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="eventListResponseDTO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="responseHeader" type="{http://service.wellness.fnblife.za.co/}eventListResponseHeader" minOccurs="0"/&gt;
 *         &lt;element name="dataAreaRs" type="{http://service.wellness.fnblife.za.co/}eventListDataAreaRs" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "eventListResponseDTO", propOrder = {
    "responseHeader",
    "dataAreaRs"
})
public class EventListResponseDTO {

    protected EventListResponseHeader responseHeader;
    @XmlElement(nillable = true)
    protected List<EventListDataAreaRs> dataAreaRs;

    /**
     * Gets the value of the responseHeader property.
     * 
     * @return
     *     possible object is
     *     {@link EventListResponseHeader }
     *     
     */
    public EventListResponseHeader getResponseHeader() {
        return responseHeader;
    }

    /**
     * Sets the value of the responseHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link EventListResponseHeader }
     *     
     */
    public void setResponseHeader(EventListResponseHeader value) {
        this.responseHeader = value;
    }

    /**
     * Gets the value of the dataAreaRs property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dataAreaRs property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDataAreaRs().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EventListDataAreaRs }
     * 
     * 
     */
    public List<EventListDataAreaRs> getDataAreaRs() {
        if (dataAreaRs == null) {
            dataAreaRs = new ArrayList<EventListDataAreaRs>();
        }
        return this.dataAreaRs;
    }

}
