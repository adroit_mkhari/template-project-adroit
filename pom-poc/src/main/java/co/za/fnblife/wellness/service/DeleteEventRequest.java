
package co.za.fnblife.wellness.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for deleteEventRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="deleteEventRequest"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="header" type="{http://service.wellness.fnblife.za.co/}deleteEventRequestHeader" minOccurs="0"/&gt;
 *         &lt;element name="dataArea" type="{http://service.wellness.fnblife.za.co/}deleteEventRequestType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "deleteEventRequest", propOrder = {
    "header",
    "dataArea"
})
public class DeleteEventRequest {

    protected DeleteEventRequestHeader header;
    protected DeleteEventRequestType dataArea;

    /**
     * Gets the value of the header property.
     * 
     * @return
     *     possible object is
     *     {@link DeleteEventRequestHeader }
     *     
     */
    public DeleteEventRequestHeader getHeader() {
        return header;
    }

    /**
     * Sets the value of the header property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeleteEventRequestHeader }
     *     
     */
    public void setHeader(DeleteEventRequestHeader value) {
        this.header = value;
    }

    /**
     * Gets the value of the dataArea property.
     * 
     * @return
     *     possible object is
     *     {@link DeleteEventRequestType }
     *     
     */
    public DeleteEventRequestType getDataArea() {
        return dataArea;
    }

    /**
     * Sets the value of the dataArea property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeleteEventRequestType }
     *     
     */
    public void setDataArea(DeleteEventRequestType value) {
        this.dataArea = value;
    }

}
