
package co.za.fnblife.wellness.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for retrieveGoalsResponseDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="retrieveGoalsResponseDTO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="responseHeader" type="{http://service.wellness.fnblife.za.co/}retrieveGoalsResponseHeader" minOccurs="0"/&gt;
 *         &lt;element name="dataAreaRs" type="{http://service.wellness.fnblife.za.co/}retrieveGoalsDataAreaRs" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "retrieveGoalsResponseDTO", propOrder = {
    "responseHeader",
    "dataAreaRs"
})
public class RetrieveGoalsResponseDTO {

    protected RetrieveGoalsResponseHeader responseHeader;
    protected RetrieveGoalsDataAreaRs dataAreaRs;

    /**
     * Gets the value of the responseHeader property.
     * 
     * @return
     *     possible object is
     *     {@link RetrieveGoalsResponseHeader }
     *     
     */
    public RetrieveGoalsResponseHeader getResponseHeader() {
        return responseHeader;
    }

    /**
     * Sets the value of the responseHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link RetrieveGoalsResponseHeader }
     *     
     */
    public void setResponseHeader(RetrieveGoalsResponseHeader value) {
        this.responseHeader = value;
    }

    /**
     * Gets the value of the dataAreaRs property.
     * 
     * @return
     *     possible object is
     *     {@link RetrieveGoalsDataAreaRs }
     *     
     */
    public RetrieveGoalsDataAreaRs getDataAreaRs() {
        return dataAreaRs;
    }

    /**
     * Sets the value of the dataAreaRs property.
     * 
     * @param value
     *     allowed object is
     *     {@link RetrieveGoalsDataAreaRs }
     *     
     */
    public void setDataAreaRs(RetrieveGoalsDataAreaRs value) {
        this.dataAreaRs = value;
    }

}
