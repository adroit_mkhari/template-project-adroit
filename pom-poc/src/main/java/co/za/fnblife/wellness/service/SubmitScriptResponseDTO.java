
package co.za.fnblife.wellness.service;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for submitScriptResponseDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="submitScriptResponseDTO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="responseHeader" type="{http://service.wellness.fnblife.za.co/}submitScriptResponseHeader" minOccurs="0"/&gt;
 *         &lt;element name="orderNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="pharmacy" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="sessionKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="username" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="password" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="processDateTime" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" minOccurs="0"/&gt;
 *         &lt;element name="storeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="orderType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="deliveryAddress" type="{http://service.wellness.fnblife.za.co/}deliveryAddress" minOccurs="0"/&gt;
 *         &lt;element name="mainMember" type="{http://service.wellness.fnblife.za.co/}mainMember" minOccurs="0"/&gt;
 *         &lt;element name="dependant" type="{http://service.wellness.fnblife.za.co/}dependant" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="dispensedPatient" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="paymentType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="comments" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="originalScriptImage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="allergies" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="generics" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="repeatScript" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "submitScriptResponseDTO", propOrder = {
    "responseHeader",
    "orderNo",
    "status",
    "description",
    "pharmacy",
    "sessionKey",
    "username",
    "password",
    "processDateTime",
    "storeCode",
    "orderType",
    "deliveryAddress",
    "mainMember",
    "dependant",
    "dispensedPatient",
    "paymentType",
    "comments",
    "originalScriptImage",
    "allergies",
    "generics",
    "repeatScript"
})
public class SubmitScriptResponseDTO {

    protected SubmitScriptResponseHeader responseHeader;
    protected String orderNo;
    protected String status;
    protected String description;
    protected String pharmacy;
    protected String sessionKey;
    protected String username;
    protected String password;
    protected Object processDateTime;
    protected String storeCode;
    protected String orderType;
    protected DeliveryAddress deliveryAddress;
    protected MainMember mainMember;
    @XmlElement(nillable = true)
    protected List<Dependant> dependant;
    protected String dispensedPatient;
    protected String paymentType;
    protected String comments;
    protected String originalScriptImage;
    protected String allergies;
    protected String generics;
    protected String repeatScript;

    /**
     * Gets the value of the responseHeader property.
     * 
     * @return
     *     possible object is
     *     {@link SubmitScriptResponseHeader }
     *     
     */
    public SubmitScriptResponseHeader getResponseHeader() {
        return responseHeader;
    }

    /**
     * Sets the value of the responseHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link SubmitScriptResponseHeader }
     *     
     */
    public void setResponseHeader(SubmitScriptResponseHeader value) {
        this.responseHeader = value;
    }

    /**
     * Gets the value of the orderNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderNo() {
        return orderNo;
    }

    /**
     * Sets the value of the orderNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderNo(String value) {
        this.orderNo = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the pharmacy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPharmacy() {
        return pharmacy;
    }

    /**
     * Sets the value of the pharmacy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPharmacy(String value) {
        this.pharmacy = value;
    }

    /**
     * Gets the value of the sessionKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSessionKey() {
        return sessionKey;
    }

    /**
     * Sets the value of the sessionKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSessionKey(String value) {
        this.sessionKey = value;
    }

    /**
     * Gets the value of the username property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets the value of the username property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsername(String value) {
        this.username = value;
    }

    /**
     * Gets the value of the password property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the value of the password property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPassword(String value) {
        this.password = value;
    }

    /**
     * Gets the value of the processDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getProcessDateTime() {
        return processDateTime;
    }

    /**
     * Sets the value of the processDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setProcessDateTime(Object value) {
        this.processDateTime = value;
    }

    /**
     * Gets the value of the storeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStoreCode() {
        return storeCode;
    }

    /**
     * Sets the value of the storeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStoreCode(String value) {
        this.storeCode = value;
    }

    /**
     * Gets the value of the orderType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderType() {
        return orderType;
    }

    /**
     * Sets the value of the orderType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderType(String value) {
        this.orderType = value;
    }

    /**
     * Gets the value of the deliveryAddress property.
     * 
     * @return
     *     possible object is
     *     {@link DeliveryAddress }
     *     
     */
    public DeliveryAddress getDeliveryAddress() {
        return deliveryAddress;
    }

    /**
     * Sets the value of the deliveryAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeliveryAddress }
     *     
     */
    public void setDeliveryAddress(DeliveryAddress value) {
        this.deliveryAddress = value;
    }

    /**
     * Gets the value of the mainMember property.
     * 
     * @return
     *     possible object is
     *     {@link MainMember }
     *     
     */
    public MainMember getMainMember() {
        return mainMember;
    }

    /**
     * Sets the value of the mainMember property.
     * 
     * @param value
     *     allowed object is
     *     {@link MainMember }
     *     
     */
    public void setMainMember(MainMember value) {
        this.mainMember = value;
    }

    /**
     * Gets the value of the dependant property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dependant property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDependant().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Dependant }
     * 
     * 
     */
    public List<Dependant> getDependant() {
        if (dependant == null) {
            dependant = new ArrayList<Dependant>();
        }
        return this.dependant;
    }

    /**
     * Gets the value of the dispensedPatient property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDispensedPatient() {
        return dispensedPatient;
    }

    /**
     * Sets the value of the dispensedPatient property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDispensedPatient(String value) {
        this.dispensedPatient = value;
    }

    /**
     * Gets the value of the paymentType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentType() {
        return paymentType;
    }

    /**
     * Sets the value of the paymentType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentType(String value) {
        this.paymentType = value;
    }

    /**
     * Gets the value of the comments property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComments() {
        return comments;
    }

    /**
     * Sets the value of the comments property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComments(String value) {
        this.comments = value;
    }

    /**
     * Gets the value of the originalScriptImage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginalScriptImage() {
        return originalScriptImage;
    }

    /**
     * Sets the value of the originalScriptImage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginalScriptImage(String value) {
        this.originalScriptImage = value;
    }

    /**
     * Gets the value of the allergies property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAllergies() {
        return allergies;
    }

    /**
     * Sets the value of the allergies property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAllergies(String value) {
        this.allergies = value;
    }

    /**
     * Gets the value of the generics property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGenerics() {
        return generics;
    }

    /**
     * Sets the value of the generics property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGenerics(String value) {
        this.generics = value;
    }

    /**
     * Gets the value of the repeatScript property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRepeatScript() {
        return repeatScript;
    }

    /**
     * Sets the value of the repeatScript property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRepeatScript(String value) {
        this.repeatScript = value;
    }

}
