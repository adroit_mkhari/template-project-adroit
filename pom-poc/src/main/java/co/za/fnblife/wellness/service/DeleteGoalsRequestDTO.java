
package co.za.fnblife.wellness.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for deleteGoalsRequestDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="deleteGoalsRequestDTO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="header" type="{http://service.wellness.fnblife.za.co/}deleteGoalMessageHeader" minOccurs="0"/&gt;
 *         &lt;element name="dataAreaRq" type="{http://service.wellness.fnblife.za.co/}deleteGoalDataAreaRqDTO" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "deleteGoalsRequestDTO", propOrder = {
    "header",
    "dataAreaRq"
})
public class DeleteGoalsRequestDTO {

    protected DeleteGoalMessageHeader header;
    protected DeleteGoalDataAreaRqDTO dataAreaRq;

    /**
     * Gets the value of the header property.
     * 
     * @return
     *     possible object is
     *     {@link DeleteGoalMessageHeader }
     *     
     */
    public DeleteGoalMessageHeader getHeader() {
        return header;
    }

    /**
     * Sets the value of the header property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeleteGoalMessageHeader }
     *     
     */
    public void setHeader(DeleteGoalMessageHeader value) {
        this.header = value;
    }

    /**
     * Gets the value of the dataAreaRq property.
     * 
     * @return
     *     possible object is
     *     {@link DeleteGoalDataAreaRqDTO }
     *     
     */
    public DeleteGoalDataAreaRqDTO getDataAreaRq() {
        return dataAreaRq;
    }

    /**
     * Sets the value of the dataAreaRq property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeleteGoalDataAreaRqDTO }
     *     
     */
    public void setDataAreaRq(DeleteGoalDataAreaRqDTO value) {
        this.dataAreaRq = value;
    }

}
