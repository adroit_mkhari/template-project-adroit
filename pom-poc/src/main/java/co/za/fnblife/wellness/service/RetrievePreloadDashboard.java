
package co.za.fnblife.wellness.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for retrievePreloadDashboard complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="retrievePreloadDashboard"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="arg0" type="{http://service.wellness.fnblife.za.co/}retrievePreloadDashboardRequestDTO" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "retrievePreloadDashboard", propOrder = {
    "arg0"
})
public class RetrievePreloadDashboard {

    protected RetrievePreloadDashboardRequestDTO arg0;

    /**
     * Gets the value of the arg0 property.
     * 
     * @return
     *     possible object is
     *     {@link RetrievePreloadDashboardRequestDTO }
     *     
     */
    public RetrievePreloadDashboardRequestDTO getArg0() {
        return arg0;
    }

    /**
     * Sets the value of the arg0 property.
     * 
     * @param value
     *     allowed object is
     *     {@link RetrievePreloadDashboardRequestDTO }
     *     
     */
    public void setArg0(RetrievePreloadDashboardRequestDTO value) {
        this.arg0 = value;
    }

}
