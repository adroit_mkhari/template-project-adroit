
package co.za.fnblife.wellness.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for retrieveGoalsRequestDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="retrieveGoalsRequestDTO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="requestHeader" type="{http://service.wellness.fnblife.za.co/}retrieveGoalsRequestHeader" minOccurs="0"/&gt;
 *         &lt;element name="dataAreaRq" type="{http://service.wellness.fnblife.za.co/}retrieveGoalsDataAreaRq" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "retrieveGoalsRequestDTO", propOrder = {
    "requestHeader",
    "dataAreaRq"
})
public class RetrieveGoalsRequestDTO {

    protected RetrieveGoalsRequestHeader requestHeader;
    protected RetrieveGoalsDataAreaRq dataAreaRq;

    /**
     * Gets the value of the requestHeader property.
     * 
     * @return
     *     possible object is
     *     {@link RetrieveGoalsRequestHeader }
     *     
     */
    public RetrieveGoalsRequestHeader getRequestHeader() {
        return requestHeader;
    }

    /**
     * Sets the value of the requestHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link RetrieveGoalsRequestHeader }
     *     
     */
    public void setRequestHeader(RetrieveGoalsRequestHeader value) {
        this.requestHeader = value;
    }

    /**
     * Gets the value of the dataAreaRq property.
     * 
     * @return
     *     possible object is
     *     {@link RetrieveGoalsDataAreaRq }
     *     
     */
    public RetrieveGoalsDataAreaRq getDataAreaRq() {
        return dataAreaRq;
    }

    /**
     * Sets the value of the dataAreaRq property.
     * 
     * @param value
     *     allowed object is
     *     {@link RetrieveGoalsDataAreaRq }
     *     
     */
    public void setDataAreaRq(RetrieveGoalsDataAreaRq value) {
        this.dataAreaRq = value;
    }

}
