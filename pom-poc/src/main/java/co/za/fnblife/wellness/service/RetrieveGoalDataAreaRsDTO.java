
package co.za.fnblife.wellness.service;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for retrieveGoalDataAreaRsDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="retrieveGoalDataAreaRsDTO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="goal" type="{http://service.wellness.fnblife.za.co/}goalDTO" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="sessionkKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "retrieveGoalDataAreaRsDTO", propOrder = {
    "goal",
    "sessionkKey"
})
public class RetrieveGoalDataAreaRsDTO {

    @XmlElement(nillable = true)
    protected List<GoalDTO> goal;
    protected String sessionkKey;

    /**
     * Gets the value of the goal property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the goal property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGoal().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GoalDTO }
     * 
     * 
     */
    public List<GoalDTO> getGoal() {
        if (goal == null) {
            goal = new ArrayList<GoalDTO>();
        }
        return this.goal;
    }

    /**
     * Gets the value of the sessionkKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSessionkKey() {
        return sessionkKey;
    }

    /**
     * Sets the value of the sessionkKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSessionkKey(String value) {
        this.sessionkKey = value;
    }

}
