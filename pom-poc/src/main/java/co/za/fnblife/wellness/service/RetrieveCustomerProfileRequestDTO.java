
package co.za.fnblife.wellness.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for retrieveCustomerProfileRequestDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="retrieveCustomerProfileRequestDTO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="dataAreaRq" type="{http://service.wellness.fnblife.za.co/}RetrieveCustomerProfileDataAreaRq" minOccurs="0"/&gt;
 *         &lt;element name="requestHeader" type="{http://service.wellness.fnblife.za.co/}retrieveCustomerProfileRequestHeader" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "retrieveCustomerProfileRequestDTO", propOrder = {
    "dataAreaRq",
    "requestHeader"
})
public class RetrieveCustomerProfileRequestDTO {

    protected RetrieveCustomerProfileDataAreaRq dataAreaRq;
    protected RetrieveCustomerProfileRequestHeader requestHeader;

    /**
     * Gets the value of the dataAreaRq property.
     * 
     * @return
     *     possible object is
     *     {@link RetrieveCustomerProfileDataAreaRq }
     *     
     */
    public RetrieveCustomerProfileDataAreaRq getDataAreaRq() {
        return dataAreaRq;
    }

    /**
     * Sets the value of the dataAreaRq property.
     * 
     * @param value
     *     allowed object is
     *     {@link RetrieveCustomerProfileDataAreaRq }
     *     
     */
    public void setDataAreaRq(RetrieveCustomerProfileDataAreaRq value) {
        this.dataAreaRq = value;
    }

    /**
     * Gets the value of the requestHeader property.
     * 
     * @return
     *     possible object is
     *     {@link RetrieveCustomerProfileRequestHeader }
     *     
     */
    public RetrieveCustomerProfileRequestHeader getRequestHeader() {
        return requestHeader;
    }

    /**
     * Sets the value of the requestHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link RetrieveCustomerProfileRequestHeader }
     *     
     */
    public void setRequestHeader(RetrieveCustomerProfileRequestHeader value) {
        this.requestHeader = value;
    }

}
