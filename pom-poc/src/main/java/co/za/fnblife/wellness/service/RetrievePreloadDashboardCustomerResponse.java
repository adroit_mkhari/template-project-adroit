
package co.za.fnblife.wellness.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for retrievePreloadDashboardCustomerResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="retrievePreloadDashboardCustomerResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="responseID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="responseOption" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "retrievePreloadDashboardCustomerResponse", propOrder = {
    "responseID",
    "responseOption"
})
public class RetrievePreloadDashboardCustomerResponse {

    protected int responseID;
    protected int responseOption;

    /**
     * Gets the value of the responseID property.
     * 
     */
    public int getResponseID() {
        return responseID;
    }

    /**
     * Sets the value of the responseID property.
     * 
     */
    public void setResponseID(int value) {
        this.responseID = value;
    }

    /**
     * Gets the value of the responseOption property.
     * 
     */
    public int getResponseOption() {
        return responseOption;
    }

    /**
     * Sets the value of the responseOption property.
     * 
     */
    public void setResponseOption(int value) {
        this.responseOption = value;
    }

}
