
package co.za.fnblife.wellness.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for retrieveScriptRequestDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="retrieveScriptRequestDTO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="requestHeader" type="{http://service.wellness.fnblife.za.co/}retrieveScriptRequestHeader" minOccurs="0"/&gt;
 *         &lt;element name="dataAreaRq" type="{http://service.wellness.fnblife.za.co/}retrieveScriptDataAreaRq" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "retrieveScriptRequestDTO", propOrder = {
    "requestHeader",
    "dataAreaRq"
})
public class RetrieveScriptRequestDTO {

    protected RetrieveScriptRequestHeader requestHeader;
    protected RetrieveScriptDataAreaRq dataAreaRq;

    /**
     * Gets the value of the requestHeader property.
     * 
     * @return
     *     possible object is
     *     {@link RetrieveScriptRequestHeader }
     *     
     */
    public RetrieveScriptRequestHeader getRequestHeader() {
        return requestHeader;
    }

    /**
     * Sets the value of the requestHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link RetrieveScriptRequestHeader }
     *     
     */
    public void setRequestHeader(RetrieveScriptRequestHeader value) {
        this.requestHeader = value;
    }

    /**
     * Gets the value of the dataAreaRq property.
     * 
     * @return
     *     possible object is
     *     {@link RetrieveScriptDataAreaRq }
     *     
     */
    public RetrieveScriptDataAreaRq getDataAreaRq() {
        return dataAreaRq;
    }

    /**
     * Sets the value of the dataAreaRq property.
     * 
     * @param value
     *     allowed object is
     *     {@link RetrieveScriptDataAreaRq }
     *     
     */
    public void setDataAreaRq(RetrieveScriptDataAreaRq value) {
        this.dataAreaRq = value;
    }

}
