
package co.za.fnblife.wellness.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for retrieveQuestionSubmitAnswerResponseDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="retrieveQuestionSubmitAnswerResponseDTO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="responseHeader" type="{http://service.wellness.fnblife.za.co/}retrieveQuestionAnswerResponseHeader" minOccurs="0"/&gt;
 *         &lt;element name="dataAreaRs" type="{http://service.wellness.fnblife.za.co/}retrieveQuestionAnswerDataAreaRs" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "retrieveQuestionSubmitAnswerResponseDTO", propOrder = {
    "responseHeader",
    "dataAreaRs"
})
public class RetrieveQuestionSubmitAnswerResponseDTO {

    protected RetrieveQuestionAnswerResponseHeader responseHeader;
    protected RetrieveQuestionAnswerDataAreaRs dataAreaRs;

    /**
     * Gets the value of the responseHeader property.
     * 
     * @return
     *     possible object is
     *     {@link RetrieveQuestionAnswerResponseHeader }
     *     
     */
    public RetrieveQuestionAnswerResponseHeader getResponseHeader() {
        return responseHeader;
    }

    /**
     * Sets the value of the responseHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link RetrieveQuestionAnswerResponseHeader }
     *     
     */
    public void setResponseHeader(RetrieveQuestionAnswerResponseHeader value) {
        this.responseHeader = value;
    }

    /**
     * Gets the value of the dataAreaRs property.
     * 
     * @return
     *     possible object is
     *     {@link RetrieveQuestionAnswerDataAreaRs }
     *     
     */
    public RetrieveQuestionAnswerDataAreaRs getDataAreaRs() {
        return dataAreaRs;
    }

    /**
     * Sets the value of the dataAreaRs property.
     * 
     * @param value
     *     allowed object is
     *     {@link RetrieveQuestionAnswerDataAreaRs }
     *     
     */
    public void setDataAreaRs(RetrieveQuestionAnswerDataAreaRs value) {
        this.dataAreaRs = value;
    }

}
