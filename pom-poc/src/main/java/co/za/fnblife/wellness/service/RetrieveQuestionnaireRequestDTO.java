
package co.za.fnblife.wellness.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for retrieveQuestionnaireRequestDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="retrieveQuestionnaireRequestDTO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="requestHeader" type="{http://service.wellness.fnblife.za.co/}retrieveQuestionnaireRequestHeader" minOccurs="0"/&gt;
 *         &lt;element name="dataAreaRq" type="{http://service.wellness.fnblife.za.co/}retrieveQuestionnaireDataAreaRq" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "retrieveQuestionnaireRequestDTO", propOrder = {
    "requestHeader",
    "dataAreaRq"
})
public class RetrieveQuestionnaireRequestDTO {

    protected RetrieveQuestionnaireRequestHeader requestHeader;
    protected RetrieveQuestionnaireDataAreaRq dataAreaRq;

    /**
     * Gets the value of the requestHeader property.
     * 
     * @return
     *     possible object is
     *     {@link RetrieveQuestionnaireRequestHeader }
     *     
     */
    public RetrieveQuestionnaireRequestHeader getRequestHeader() {
        return requestHeader;
    }

    /**
     * Sets the value of the requestHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link RetrieveQuestionnaireRequestHeader }
     *     
     */
    public void setRequestHeader(RetrieveQuestionnaireRequestHeader value) {
        this.requestHeader = value;
    }

    /**
     * Gets the value of the dataAreaRq property.
     * 
     * @return
     *     possible object is
     *     {@link RetrieveQuestionnaireDataAreaRq }
     *     
     */
    public RetrieveQuestionnaireDataAreaRq getDataAreaRq() {
        return dataAreaRq;
    }

    /**
     * Sets the value of the dataAreaRq property.
     * 
     * @param value
     *     allowed object is
     *     {@link RetrieveQuestionnaireDataAreaRq }
     *     
     */
    public void setDataAreaRq(RetrieveQuestionnaireDataAreaRq value) {
        this.dataAreaRq = value;
    }

}
