
package co.za.fnblife.wellness.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for retrieveQuestionAnswerDataAreaRs complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="retrieveQuestionAnswerDataAreaRs"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="categoryID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="goTo" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="sessionKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="subCategory" type="{http://service.wellness.fnblife.za.co/}retrieveQuestionAnswerSubCategory" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "retrieveQuestionAnswerDataAreaRs", propOrder = {
    "categoryID",
    "goTo",
    "sessionKey",
    "subCategory"
})
public class RetrieveQuestionAnswerDataAreaRs {

    protected int categoryID;
    protected int goTo;
    protected String sessionKey;
    protected RetrieveQuestionAnswerSubCategory subCategory;

    /**
     * Gets the value of the categoryID property.
     * 
     */
    public int getCategoryID() {
        return categoryID;
    }

    /**
     * Sets the value of the categoryID property.
     * 
     */
    public void setCategoryID(int value) {
        this.categoryID = value;
    }

    /**
     * Gets the value of the goTo property.
     * 
     */
    public int getGoTo() {
        return goTo;
    }

    /**
     * Sets the value of the goTo property.
     * 
     */
    public void setGoTo(int value) {
        this.goTo = value;
    }

    /**
     * Gets the value of the sessionKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSessionKey() {
        return sessionKey;
    }

    /**
     * Sets the value of the sessionKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSessionKey(String value) {
        this.sessionKey = value;
    }

    /**
     * Gets the value of the subCategory property.
     * 
     * @return
     *     possible object is
     *     {@link RetrieveQuestionAnswerSubCategory }
     *     
     */
    public RetrieveQuestionAnswerSubCategory getSubCategory() {
        return subCategory;
    }

    /**
     * Sets the value of the subCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link RetrieveQuestionAnswerSubCategory }
     *     
     */
    public void setSubCategory(RetrieveQuestionAnswerSubCategory value) {
        this.subCategory = value;
    }

}
