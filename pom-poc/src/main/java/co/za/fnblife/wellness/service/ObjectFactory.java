
package co.za.fnblife.wellness.service;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.za.fnblife.wellness.service package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Exception_QNAME = new QName("http://service.wellness.fnblife.za.co/", "Exception");
    private final static QName _AddEvent_QNAME = new QName("http://service.wellness.fnblife.za.co/", "addEvent");
    private final static QName _AddEventRequest_QNAME = new QName("http://service.wellness.fnblife.za.co/", "addEventRequest");
    private final static QName _AddEventResponse_QNAME = new QName("http://service.wellness.fnblife.za.co/", "addEventResponse");
    private final static QName _AddEventResponseDTO_QNAME = new QName("http://service.wellness.fnblife.za.co/", "addEventResponseDTO");
    private final static QName _CancelScript_QNAME = new QName("http://service.wellness.fnblife.za.co/", "cancelScript");
    private final static QName _CancelScriptRequestDTO_QNAME = new QName("http://service.wellness.fnblife.za.co/", "cancelScriptRequestDTO");
    private final static QName _CancelScriptResponse_QNAME = new QName("http://service.wellness.fnblife.za.co/", "cancelScriptResponse");
    private final static QName _CancelScriptResponseDTO_QNAME = new QName("http://service.wellness.fnblife.za.co/", "cancelScriptResponseDTO");
    private final static QName _DeleteEvent_QNAME = new QName("http://service.wellness.fnblife.za.co/", "deleteEvent");
    private final static QName _DeleteEventRequest_QNAME = new QName("http://service.wellness.fnblife.za.co/", "deleteEventRequest");
    private final static QName _DeleteEventResponse_QNAME = new QName("http://service.wellness.fnblife.za.co/", "deleteEventResponse");
    private final static QName _DeleteEventResponseDTO_QNAME = new QName("http://service.wellness.fnblife.za.co/", "deleteEventResponseDTO");
    private final static QName _DeleteGoal_QNAME = new QName("http://service.wellness.fnblife.za.co/", "deleteGoal");
    private final static QName _DeleteGoalResponse_QNAME = new QName("http://service.wellness.fnblife.za.co/", "deleteGoalResponse");
    private final static QName _DeleteGoalsRequestDTO_QNAME = new QName("http://service.wellness.fnblife.za.co/", "deleteGoalsRequestDTO");
    private final static QName _DeleteGoalsResponseDTO_QNAME = new QName("http://service.wellness.fnblife.za.co/", "deleteGoalsResponseDTO");
    private final static QName _DischemPushNotification_QNAME = new QName("http://service.wellness.fnblife.za.co/", "dischemPushNotification");
    private final static QName _DischemPushNotificationResponse_QNAME = new QName("http://service.wellness.fnblife.za.co/", "dischemPushNotificationResponse");
    private final static QName _EventDetailsRequestDTO_QNAME = new QName("http://service.wellness.fnblife.za.co/", "eventDetailsRequestDTO");
    private final static QName _EventDetailsResponseDTO_QNAME = new QName("http://service.wellness.fnblife.za.co/", "eventDetailsResponseDTO");
    private final static QName _EventListRequestDTO_QNAME = new QName("http://service.wellness.fnblife.za.co/", "eventListRequestDTO");
    private final static QName _EventListResponseDTO_QNAME = new QName("http://service.wellness.fnblife.za.co/", "eventListResponseDTO");
    private final static QName _Init_QNAME = new QName("http://service.wellness.fnblife.za.co/", "init");
    private final static QName _InitResponse_QNAME = new QName("http://service.wellness.fnblife.za.co/", "initResponse");
    private final static QName _LoadCustomerProfile_QNAME = new QName("http://service.wellness.fnblife.za.co/", "loadCustomerProfile");
    private final static QName _LoadCustomerProfileRequestDTO_QNAME = new QName("http://service.wellness.fnblife.za.co/", "loadCustomerProfileRequestDTO");
    private final static QName _LoadCustomerProfileResponse_QNAME = new QName("http://service.wellness.fnblife.za.co/", "loadCustomerProfileResponse");
    private final static QName _LoadCustomerProfileResponseDTO_QNAME = new QName("http://service.wellness.fnblife.za.co/", "loadCustomerProfileResponseDTO");
    private final static QName _MaintainCustomerProfile_QNAME = new QName("http://service.wellness.fnblife.za.co/", "maintainCustomerProfile");
    private final static QName _MaintainCustomerProfileRequestDTO_QNAME = new QName("http://service.wellness.fnblife.za.co/", "maintainCustomerProfileRequestDTO");
    private final static QName _MaintainCustomerProfileResponse_QNAME = new QName("http://service.wellness.fnblife.za.co/", "maintainCustomerProfileResponse");
    private final static QName _MaintainCustomerProfileResponseDTO_QNAME = new QName("http://service.wellness.fnblife.za.co/", "maintainCustomerProfileResponseDTO");
    private final static QName _PushEventImage_QNAME = new QName("http://service.wellness.fnblife.za.co/", "pushEventImage");
    private final static QName _PushEventImageRequest_QNAME = new QName("http://service.wellness.fnblife.za.co/", "pushEventImageRequest");
    private final static QName _PushEventImageResponse_QNAME = new QName("http://service.wellness.fnblife.za.co/", "pushEventImageResponse");
    private final static QName _PushEventImageResponseDTO_QNAME = new QName("http://service.wellness.fnblife.za.co/", "pushEventImageResponseDTO");
    private final static QName _PushNotificationRequestDTO_QNAME = new QName("http://service.wellness.fnblife.za.co/", "pushNotificationRequestDTO");
    private final static QName _PushNotificationResponseDTO_QNAME = new QName("http://service.wellness.fnblife.za.co/", "pushNotificationResponseDTO");
    private final static QName _RetrieveALLEvents_QNAME = new QName("http://service.wellness.fnblife.za.co/", "retrieveALLEvents");
    private final static QName _RetrieveALLEventsResponse_QNAME = new QName("http://service.wellness.fnblife.za.co/", "retrieveALLEventsResponse");
    private final static QName _RetrieveCustomerProfile_QNAME = new QName("http://service.wellness.fnblife.za.co/", "retrieveCustomerProfile");
    private final static QName _RetrieveCustomerProfileRequestDTO_QNAME = new QName("http://service.wellness.fnblife.za.co/", "retrieveCustomerProfileRequestDTO");
    private final static QName _RetrieveCustomerProfileResponse_QNAME = new QName("http://service.wellness.fnblife.za.co/", "retrieveCustomerProfileResponse");
    private final static QName _RetrieveCustomerProfileResponseDTO_QNAME = new QName("http://service.wellness.fnblife.za.co/", "retrieveCustomerProfileResponseDTO");
    private final static QName _RetrieveDashboard_QNAME = new QName("http://service.wellness.fnblife.za.co/", "retrieveDashboard");
    private final static QName _RetrieveDashboardRequestDTO_QNAME = new QName("http://service.wellness.fnblife.za.co/", "retrieveDashboardRequestDTO");
    private final static QName _RetrieveDashboardResponse_QNAME = new QName("http://service.wellness.fnblife.za.co/", "retrieveDashboardResponse");
    private final static QName _RetrieveDashboardResponseDTO_QNAME = new QName("http://service.wellness.fnblife.za.co/", "retrieveDashboardResponseDTO");
    private final static QName _RetrieveEventDetails_QNAME = new QName("http://service.wellness.fnblife.za.co/", "retrieveEventDetails");
    private final static QName _RetrieveEventDetailsResponse_QNAME = new QName("http://service.wellness.fnblife.za.co/", "retrieveEventDetailsResponse");
    private final static QName _RetrieveEventRequestDTO_QNAME = new QName("http://service.wellness.fnblife.za.co/", "retrieveEventRequestDTO");
    private final static QName _RetrieveGoalList_QNAME = new QName("http://service.wellness.fnblife.za.co/", "retrieveGoalList");
    private final static QName _RetrieveGoalListRequestDTO_QNAME = new QName("http://service.wellness.fnblife.za.co/", "retrieveGoalListRequestDTO");
    private final static QName _RetrieveGoalListResponse_QNAME = new QName("http://service.wellness.fnblife.za.co/", "retrieveGoalListResponse");
    private final static QName _RetrieveGoalListResponseDTO_QNAME = new QName("http://service.wellness.fnblife.za.co/", "retrieveGoalListResponseDTO");
    private final static QName _RetrieveGoals_QNAME = new QName("http://service.wellness.fnblife.za.co/", "retrieveGoals");
    private final static QName _RetrieveGoalsRequestDTO_QNAME = new QName("http://service.wellness.fnblife.za.co/", "retrieveGoalsRequestDTO");
    private final static QName _RetrieveGoalsResponse_QNAME = new QName("http://service.wellness.fnblife.za.co/", "retrieveGoalsResponse");
    private final static QName _RetrieveGoalsResponseDTO_QNAME = new QName("http://service.wellness.fnblife.za.co/", "retrieveGoalsResponseDTO");
    private final static QName _RetrieveMyEvents_QNAME = new QName("http://service.wellness.fnblife.za.co/", "retrieveMyEvents");
    private final static QName _RetrieveMyEventsResponse_QNAME = new QName("http://service.wellness.fnblife.za.co/", "retrieveMyEventsResponse");
    private final static QName _RetrievePreloadDashboard_QNAME = new QName("http://service.wellness.fnblife.za.co/", "retrievePreloadDashboard");
    private final static QName _RetrievePreloadDashboardRequestDTO_QNAME = new QName("http://service.wellness.fnblife.za.co/", "retrievePreloadDashboardRequestDTO");
    private final static QName _RetrievePreloadDashboardResponse_QNAME = new QName("http://service.wellness.fnblife.za.co/", "retrievePreloadDashboardResponse");
    private final static QName _RetrievePreloadDashboardResponseDTO_QNAME = new QName("http://service.wellness.fnblife.za.co/", "retrievePreloadDashboardResponseDTO");
    private final static QName _RetrieveQuestionSubmitAnswer_QNAME = new QName("http://service.wellness.fnblife.za.co/", "retrieveQuestionSubmitAnswer");
    private final static QName _RetrieveQuestionSubmitAnswerRequestDTO_QNAME = new QName("http://service.wellness.fnblife.za.co/", "retrieveQuestionSubmitAnswerRequestDTO");
    private final static QName _RetrieveQuestionSubmitAnswerResponse_QNAME = new QName("http://service.wellness.fnblife.za.co/", "retrieveQuestionSubmitAnswerResponse");
    private final static QName _RetrieveQuestionSubmitAnswerResponseDTO_QNAME = new QName("http://service.wellness.fnblife.za.co/", "retrieveQuestionSubmitAnswerResponseDTO");
    private final static QName _RetrieveQuestionnaire_QNAME = new QName("http://service.wellness.fnblife.za.co/", "retrieveQuestionnaire");
    private final static QName _RetrieveQuestionnaireRequestDTO_QNAME = new QName("http://service.wellness.fnblife.za.co/", "retrieveQuestionnaireRequestDTO");
    private final static QName _RetrieveQuestionnaireResponse_QNAME = new QName("http://service.wellness.fnblife.za.co/", "retrieveQuestionnaireResponse");
    private final static QName _RetrieveQuestionnaireResponseDTO_QNAME = new QName("http://service.wellness.fnblife.za.co/", "retrieveQuestionnaireResponseDTO");
    private final static QName _RetrieveScript_QNAME = new QName("http://service.wellness.fnblife.za.co/", "retrieveScript");
    private final static QName _RetrieveScriptRequestDTO_QNAME = new QName("http://service.wellness.fnblife.za.co/", "retrieveScriptRequestDTO");
    private final static QName _RetrieveScriptResponse_QNAME = new QName("http://service.wellness.fnblife.za.co/", "retrieveScriptResponse");
    private final static QName _RetrieveScriptResponseDTO_QNAME = new QName("http://service.wellness.fnblife.za.co/", "retrieveScriptResponseDTO");
    private final static QName _RetrieveSuggestedEvents_QNAME = new QName("http://service.wellness.fnblife.za.co/", "retrieveSuggestedEvents");
    private final static QName _RetrieveSuggestedEventsResponse_QNAME = new QName("http://service.wellness.fnblife.za.co/", "retrieveSuggestedEventsResponse");
    private final static QName _SaveGoal_QNAME = new QName("http://service.wellness.fnblife.za.co/", "saveGoal");
    private final static QName _SaveGoalResponse_QNAME = new QName("http://service.wellness.fnblife.za.co/", "saveGoalResponse");
    private final static QName _SaveGoalsRequestDTO_QNAME = new QName("http://service.wellness.fnblife.za.co/", "saveGoalsRequestDTO");
    private final static QName _SaveGoalsResponseDTO_QNAME = new QName("http://service.wellness.fnblife.za.co/", "saveGoalsResponseDTO");
    private final static QName _SubmitScript_QNAME = new QName("http://service.wellness.fnblife.za.co/", "submitScript");
    private final static QName _SubmitScriptRequestDTO_QNAME = new QName("http://service.wellness.fnblife.za.co/", "submitScriptRequestDTO");
    private final static QName _SubmitScriptResponse_QNAME = new QName("http://service.wellness.fnblife.za.co/", "submitScriptResponse");
    private final static QName _SubmitScriptResponseDTO_QNAME = new QName("http://service.wellness.fnblife.za.co/", "submitScriptResponseDTO");
    private final static QName _WellnessRetrieveEvents_QNAME = new QName("http://service.wellness.fnblife.za.co/", "wellnessRetrieveEvents");
    private final static QName _WellnessRetrieveEventsResponse_QNAME = new QName("http://service.wellness.fnblife.za.co/", "wellnessRetrieveEventsResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.za.fnblife.wellness.service
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Exception }
     * 
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link AddEvent }
     * 
     */
    public AddEvent createAddEvent() {
        return new AddEvent();
    }

    /**
     * Create an instance of {@link AddEventRequest }
     * 
     */
    public AddEventRequest createAddEventRequest() {
        return new AddEventRequest();
    }

    /**
     * Create an instance of {@link AddEventResponse }
     * 
     */
    public AddEventResponse createAddEventResponse() {
        return new AddEventResponse();
    }

    /**
     * Create an instance of {@link AddEventResponseDTO }
     * 
     */
    public AddEventResponseDTO createAddEventResponseDTO() {
        return new AddEventResponseDTO();
    }

    /**
     * Create an instance of {@link CancelScript }
     * 
     */
    public CancelScript createCancelScript() {
        return new CancelScript();
    }

    /**
     * Create an instance of {@link CancelScriptRequestDTO }
     * 
     */
    public CancelScriptRequestDTO createCancelScriptRequestDTO() {
        return new CancelScriptRequestDTO();
    }

    /**
     * Create an instance of {@link CancelScriptResponse }
     * 
     */
    public CancelScriptResponse createCancelScriptResponse() {
        return new CancelScriptResponse();
    }

    /**
     * Create an instance of {@link CancelScriptResponseDTO }
     * 
     */
    public CancelScriptResponseDTO createCancelScriptResponseDTO() {
        return new CancelScriptResponseDTO();
    }

    /**
     * Create an instance of {@link DeleteEvent }
     * 
     */
    public DeleteEvent createDeleteEvent() {
        return new DeleteEvent();
    }

    /**
     * Create an instance of {@link DeleteEventRequest }
     * 
     */
    public DeleteEventRequest createDeleteEventRequest() {
        return new DeleteEventRequest();
    }

    /**
     * Create an instance of {@link DeleteEventResponse }
     * 
     */
    public DeleteEventResponse createDeleteEventResponse() {
        return new DeleteEventResponse();
    }

    /**
     * Create an instance of {@link DeleteEventResponseDTO }
     * 
     */
    public DeleteEventResponseDTO createDeleteEventResponseDTO() {
        return new DeleteEventResponseDTO();
    }

    /**
     * Create an instance of {@link DeleteGoal }
     * 
     */
    public DeleteGoal createDeleteGoal() {
        return new DeleteGoal();
    }

    /**
     * Create an instance of {@link DeleteGoalResponse }
     * 
     */
    public DeleteGoalResponse createDeleteGoalResponse() {
        return new DeleteGoalResponse();
    }

    /**
     * Create an instance of {@link DeleteGoalsRequestDTO }
     * 
     */
    public DeleteGoalsRequestDTO createDeleteGoalsRequestDTO() {
        return new DeleteGoalsRequestDTO();
    }

    /**
     * Create an instance of {@link DeleteGoalsResponseDTO }
     * 
     */
    public DeleteGoalsResponseDTO createDeleteGoalsResponseDTO() {
        return new DeleteGoalsResponseDTO();
    }

    /**
     * Create an instance of {@link DischemPushNotification }
     * 
     */
    public DischemPushNotification createDischemPushNotification() {
        return new DischemPushNotification();
    }

    /**
     * Create an instance of {@link DischemPushNotificationResponse }
     * 
     */
    public DischemPushNotificationResponse createDischemPushNotificationResponse() {
        return new DischemPushNotificationResponse();
    }

    /**
     * Create an instance of {@link EventDetailsRequestDTO }
     * 
     */
    public EventDetailsRequestDTO createEventDetailsRequestDTO() {
        return new EventDetailsRequestDTO();
    }

    /**
     * Create an instance of {@link EventDetailsResponseDTO }
     * 
     */
    public EventDetailsResponseDTO createEventDetailsResponseDTO() {
        return new EventDetailsResponseDTO();
    }

    /**
     * Create an instance of {@link EventListRequestDTO }
     * 
     */
    public EventListRequestDTO createEventListRequestDTO() {
        return new EventListRequestDTO();
    }

    /**
     * Create an instance of {@link EventListResponseDTO }
     * 
     */
    public EventListResponseDTO createEventListResponseDTO() {
        return new EventListResponseDTO();
    }

    /**
     * Create an instance of {@link Init }
     * 
     */
    public Init createInit() {
        return new Init();
    }

    /**
     * Create an instance of {@link InitResponse }
     * 
     */
    public InitResponse createInitResponse() {
        return new InitResponse();
    }

    /**
     * Create an instance of {@link LoadCustomerProfile }
     * 
     */
    public LoadCustomerProfile createLoadCustomerProfile() {
        return new LoadCustomerProfile();
    }

    /**
     * Create an instance of {@link LoadCustomerProfileRequestDTO }
     * 
     */
    public LoadCustomerProfileRequestDTO createLoadCustomerProfileRequestDTO() {
        return new LoadCustomerProfileRequestDTO();
    }

    /**
     * Create an instance of {@link LoadCustomerProfileResponse }
     * 
     */
    public LoadCustomerProfileResponse createLoadCustomerProfileResponse() {
        return new LoadCustomerProfileResponse();
    }

    /**
     * Create an instance of {@link LoadCustomerProfileResponseDTO }
     * 
     */
    public LoadCustomerProfileResponseDTO createLoadCustomerProfileResponseDTO() {
        return new LoadCustomerProfileResponseDTO();
    }

    /**
     * Create an instance of {@link MaintainCustomerProfile }
     * 
     */
    public MaintainCustomerProfile createMaintainCustomerProfile() {
        return new MaintainCustomerProfile();
    }

    /**
     * Create an instance of {@link MaintainCustomerProfileRequestDTO }
     * 
     */
    public MaintainCustomerProfileRequestDTO createMaintainCustomerProfileRequestDTO() {
        return new MaintainCustomerProfileRequestDTO();
    }

    /**
     * Create an instance of {@link MaintainCustomerProfileResponse }
     * 
     */
    public MaintainCustomerProfileResponse createMaintainCustomerProfileResponse() {
        return new MaintainCustomerProfileResponse();
    }

    /**
     * Create an instance of {@link MaintainCustomerProfileResponseDTO }
     * 
     */
    public MaintainCustomerProfileResponseDTO createMaintainCustomerProfileResponseDTO() {
        return new MaintainCustomerProfileResponseDTO();
    }

    /**
     * Create an instance of {@link PushEventImage }
     * 
     */
    public PushEventImage createPushEventImage() {
        return new PushEventImage();
    }

    /**
     * Create an instance of {@link PushEventImageRequest }
     * 
     */
    public PushEventImageRequest createPushEventImageRequest() {
        return new PushEventImageRequest();
    }

    /**
     * Create an instance of {@link PushEventImageResponse }
     * 
     */
    public PushEventImageResponse createPushEventImageResponse() {
        return new PushEventImageResponse();
    }

    /**
     * Create an instance of {@link PushEventImageResponseDTO }
     * 
     */
    public PushEventImageResponseDTO createPushEventImageResponseDTO() {
        return new PushEventImageResponseDTO();
    }

    /**
     * Create an instance of {@link PushNotificationRequestDTO }
     * 
     */
    public PushNotificationRequestDTO createPushNotificationRequestDTO() {
        return new PushNotificationRequestDTO();
    }

    /**
     * Create an instance of {@link PushNotificationResponseDTO }
     * 
     */
    public PushNotificationResponseDTO createPushNotificationResponseDTO() {
        return new PushNotificationResponseDTO();
    }

    /**
     * Create an instance of {@link RetrieveALLEvents }
     * 
     */
    public RetrieveALLEvents createRetrieveALLEvents() {
        return new RetrieveALLEvents();
    }

    /**
     * Create an instance of {@link RetrieveALLEventsResponse }
     * 
     */
    public RetrieveALLEventsResponse createRetrieveALLEventsResponse() {
        return new RetrieveALLEventsResponse();
    }

    /**
     * Create an instance of {@link RetrieveCustomerProfile }
     * 
     */
    public RetrieveCustomerProfile createRetrieveCustomerProfile() {
        return new RetrieveCustomerProfile();
    }

    /**
     * Create an instance of {@link RetrieveCustomerProfileRequestDTO }
     * 
     */
    public RetrieveCustomerProfileRequestDTO createRetrieveCustomerProfileRequestDTO() {
        return new RetrieveCustomerProfileRequestDTO();
    }

    /**
     * Create an instance of {@link RetrieveCustomerProfileResponse }
     * 
     */
    public RetrieveCustomerProfileResponse createRetrieveCustomerProfileResponse() {
        return new RetrieveCustomerProfileResponse();
    }

    /**
     * Create an instance of {@link RetrieveCustomerProfileResponseDTO }
     * 
     */
    public RetrieveCustomerProfileResponseDTO createRetrieveCustomerProfileResponseDTO() {
        return new RetrieveCustomerProfileResponseDTO();
    }

    /**
     * Create an instance of {@link RetrieveDashboard }
     * 
     */
    public RetrieveDashboard createRetrieveDashboard() {
        return new RetrieveDashboard();
    }

    /**
     * Create an instance of {@link RetrieveDashboardRequestDTO }
     * 
     */
    public RetrieveDashboardRequestDTO createRetrieveDashboardRequestDTO() {
        return new RetrieveDashboardRequestDTO();
    }

    /**
     * Create an instance of {@link RetrieveDashboardResponse }
     * 
     */
    public RetrieveDashboardResponse createRetrieveDashboardResponse() {
        return new RetrieveDashboardResponse();
    }

    /**
     * Create an instance of {@link RetrieveDashboardResponseDTO }
     * 
     */
    public RetrieveDashboardResponseDTO createRetrieveDashboardResponseDTO() {
        return new RetrieveDashboardResponseDTO();
    }

    /**
     * Create an instance of {@link RetrieveEventDetails }
     * 
     */
    public RetrieveEventDetails createRetrieveEventDetails() {
        return new RetrieveEventDetails();
    }

    /**
     * Create an instance of {@link RetrieveEventDetailsResponse }
     * 
     */
    public RetrieveEventDetailsResponse createRetrieveEventDetailsResponse() {
        return new RetrieveEventDetailsResponse();
    }

    /**
     * Create an instance of {@link RetrieveEventRequestDTO }
     * 
     */
    public RetrieveEventRequestDTO createRetrieveEventRequestDTO() {
        return new RetrieveEventRequestDTO();
    }

    /**
     * Create an instance of {@link RetrieveGoalList }
     * 
     */
    public RetrieveGoalList createRetrieveGoalList() {
        return new RetrieveGoalList();
    }

    /**
     * Create an instance of {@link RetrieveGoalListRequestDTO }
     * 
     */
    public RetrieveGoalListRequestDTO createRetrieveGoalListRequestDTO() {
        return new RetrieveGoalListRequestDTO();
    }

    /**
     * Create an instance of {@link RetrieveGoalListResponse }
     * 
     */
    public RetrieveGoalListResponse createRetrieveGoalListResponse() {
        return new RetrieveGoalListResponse();
    }

    /**
     * Create an instance of {@link RetrieveGoalListResponseDTO }
     * 
     */
    public RetrieveGoalListResponseDTO createRetrieveGoalListResponseDTO() {
        return new RetrieveGoalListResponseDTO();
    }

    /**
     * Create an instance of {@link RetrieveGoals }
     * 
     */
    public RetrieveGoals createRetrieveGoals() {
        return new RetrieveGoals();
    }

    /**
     * Create an instance of {@link RetrieveGoalsRequestDTO }
     * 
     */
    public RetrieveGoalsRequestDTO createRetrieveGoalsRequestDTO() {
        return new RetrieveGoalsRequestDTO();
    }

    /**
     * Create an instance of {@link RetrieveGoalsResponse }
     * 
     */
    public RetrieveGoalsResponse createRetrieveGoalsResponse() {
        return new RetrieveGoalsResponse();
    }

    /**
     * Create an instance of {@link RetrieveGoalsResponseDTO }
     * 
     */
    public RetrieveGoalsResponseDTO createRetrieveGoalsResponseDTO() {
        return new RetrieveGoalsResponseDTO();
    }

    /**
     * Create an instance of {@link RetrieveMyEvents }
     * 
     */
    public RetrieveMyEvents createRetrieveMyEvents() {
        return new RetrieveMyEvents();
    }

    /**
     * Create an instance of {@link RetrieveMyEventsResponse }
     * 
     */
    public RetrieveMyEventsResponse createRetrieveMyEventsResponse() {
        return new RetrieveMyEventsResponse();
    }

    /**
     * Create an instance of {@link RetrievePreloadDashboard }
     * 
     */
    public RetrievePreloadDashboard createRetrievePreloadDashboard() {
        return new RetrievePreloadDashboard();
    }

    /**
     * Create an instance of {@link RetrievePreloadDashboardRequestDTO }
     * 
     */
    public RetrievePreloadDashboardRequestDTO createRetrievePreloadDashboardRequestDTO() {
        return new RetrievePreloadDashboardRequestDTO();
    }

    /**
     * Create an instance of {@link RetrievePreloadDashboardResponse }
     * 
     */
    public RetrievePreloadDashboardResponse createRetrievePreloadDashboardResponse() {
        return new RetrievePreloadDashboardResponse();
    }

    /**
     * Create an instance of {@link RetrievePreloadDashboardResponseDTO }
     * 
     */
    public RetrievePreloadDashboardResponseDTO createRetrievePreloadDashboardResponseDTO() {
        return new RetrievePreloadDashboardResponseDTO();
    }

    /**
     * Create an instance of {@link RetrieveQuestionSubmitAnswer }
     * 
     */
    public RetrieveQuestionSubmitAnswer createRetrieveQuestionSubmitAnswer() {
        return new RetrieveQuestionSubmitAnswer();
    }

    /**
     * Create an instance of {@link RetrieveQuestionSubmitAnswerRequestDTO }
     * 
     */
    public RetrieveQuestionSubmitAnswerRequestDTO createRetrieveQuestionSubmitAnswerRequestDTO() {
        return new RetrieveQuestionSubmitAnswerRequestDTO();
    }

    /**
     * Create an instance of {@link RetrieveQuestionSubmitAnswerResponse }
     * 
     */
    public RetrieveQuestionSubmitAnswerResponse createRetrieveQuestionSubmitAnswerResponse() {
        return new RetrieveQuestionSubmitAnswerResponse();
    }

    /**
     * Create an instance of {@link RetrieveQuestionSubmitAnswerResponseDTO }
     * 
     */
    public RetrieveQuestionSubmitAnswerResponseDTO createRetrieveQuestionSubmitAnswerResponseDTO() {
        return new RetrieveQuestionSubmitAnswerResponseDTO();
    }

    /**
     * Create an instance of {@link RetrieveQuestionnaire }
     * 
     */
    public RetrieveQuestionnaire createRetrieveQuestionnaire() {
        return new RetrieveQuestionnaire();
    }

    /**
     * Create an instance of {@link RetrieveQuestionnaireRequestDTO }
     * 
     */
    public RetrieveQuestionnaireRequestDTO createRetrieveQuestionnaireRequestDTO() {
        return new RetrieveQuestionnaireRequestDTO();
    }

    /**
     * Create an instance of {@link RetrieveQuestionnaireResponse }
     * 
     */
    public RetrieveQuestionnaireResponse createRetrieveQuestionnaireResponse() {
        return new RetrieveQuestionnaireResponse();
    }

    /**
     * Create an instance of {@link RetrieveQuestionnaireResponseDTO }
     * 
     */
    public RetrieveQuestionnaireResponseDTO createRetrieveQuestionnaireResponseDTO() {
        return new RetrieveQuestionnaireResponseDTO();
    }

    /**
     * Create an instance of {@link RetrieveScript }
     * 
     */
    public RetrieveScript createRetrieveScript() {
        return new RetrieveScript();
    }

    /**
     * Create an instance of {@link RetrieveScriptRequestDTO }
     * 
     */
    public RetrieveScriptRequestDTO createRetrieveScriptRequestDTO() {
        return new RetrieveScriptRequestDTO();
    }

    /**
     * Create an instance of {@link RetrieveScriptResponse }
     * 
     */
    public RetrieveScriptResponse createRetrieveScriptResponse() {
        return new RetrieveScriptResponse();
    }

    /**
     * Create an instance of {@link RetrieveScriptResponseDTO }
     * 
     */
    public RetrieveScriptResponseDTO createRetrieveScriptResponseDTO() {
        return new RetrieveScriptResponseDTO();
    }

    /**
     * Create an instance of {@link RetrieveSuggestedEvents }
     * 
     */
    public RetrieveSuggestedEvents createRetrieveSuggestedEvents() {
        return new RetrieveSuggestedEvents();
    }

    /**
     * Create an instance of {@link RetrieveSuggestedEventsResponse }
     * 
     */
    public RetrieveSuggestedEventsResponse createRetrieveSuggestedEventsResponse() {
        return new RetrieveSuggestedEventsResponse();
    }

    /**
     * Create an instance of {@link SaveGoal }
     * 
     */
    public SaveGoal createSaveGoal() {
        return new SaveGoal();
    }

    /**
     * Create an instance of {@link SaveGoalResponse }
     * 
     */
    public SaveGoalResponse createSaveGoalResponse() {
        return new SaveGoalResponse();
    }

    /**
     * Create an instance of {@link SaveGoalsRequestDTO }
     * 
     */
    public SaveGoalsRequestDTO createSaveGoalsRequestDTO() {
        return new SaveGoalsRequestDTO();
    }

    /**
     * Create an instance of {@link SaveGoalsResponseDTO }
     * 
     */
    public SaveGoalsResponseDTO createSaveGoalsResponseDTO() {
        return new SaveGoalsResponseDTO();
    }

    /**
     * Create an instance of {@link SubmitScript }
     * 
     */
    public SubmitScript createSubmitScript() {
        return new SubmitScript();
    }

    /**
     * Create an instance of {@link SubmitScriptRequestDTO }
     * 
     */
    public SubmitScriptRequestDTO createSubmitScriptRequestDTO() {
        return new SubmitScriptRequestDTO();
    }

    /**
     * Create an instance of {@link SubmitScriptResponse }
     * 
     */
    public SubmitScriptResponse createSubmitScriptResponse() {
        return new SubmitScriptResponse();
    }

    /**
     * Create an instance of {@link SubmitScriptResponseDTO }
     * 
     */
    public SubmitScriptResponseDTO createSubmitScriptResponseDTO() {
        return new SubmitScriptResponseDTO();
    }

    /**
     * Create an instance of {@link WellnessRetrieveEvents }
     * 
     */
    public WellnessRetrieveEvents createWellnessRetrieveEvents() {
        return new WellnessRetrieveEvents();
    }

    /**
     * Create an instance of {@link WellnessRetrieveEventsResponse }
     * 
     */
    public WellnessRetrieveEventsResponse createWellnessRetrieveEventsResponse() {
        return new WellnessRetrieveEventsResponse();
    }

    /**
     * Create an instance of {@link Overall }
     * 
     */
    public Overall createOverall() {
        return new Overall();
    }

    /**
     * Create an instance of {@link Answers }
     * 
     */
    public Answers createAnswers() {
        return new Answers();
    }

    /**
     * Create an instance of {@link GoalsRequestHeader }
     * 
     */
    public GoalsRequestHeader createGoalsRequestHeader() {
        return new GoalsRequestHeader();
    }

    /**
     * Create an instance of {@link GoalsDataAreaRq }
     * 
     */
    public GoalsDataAreaRq createGoalsDataAreaRq() {
        return new GoalsDataAreaRq();
    }

    /**
     * Create an instance of {@link Goal }
     * 
     */
    public Goal createGoal() {
        return new Goal();
    }

    /**
     * Create an instance of {@link Address }
     * 
     */
    public Address createAddress() {
        return new Address();
    }

    /**
     * Create an instance of {@link AnswerSet }
     * 
     */
    public AnswerSet createAnswerSet() {
        return new AnswerSet();
    }

    /**
     * Create an instance of {@link PushNotificationRequestHeader }
     * 
     */
    public PushNotificationRequestHeader createPushNotificationRequestHeader() {
        return new PushNotificationRequestHeader();
    }

    /**
     * Create an instance of {@link PushNotificationDataAreaRq }
     * 
     */
    public PushNotificationDataAreaRq createPushNotificationDataAreaRq() {
        return new PushNotificationDataAreaRq();
    }

    /**
     * Create an instance of {@link RetrieveEventRequestHeaderDTO }
     * 
     */
    public RetrieveEventRequestHeaderDTO createRetrieveEventRequestHeaderDTO() {
        return new RetrieveEventRequestHeaderDTO();
    }

    /**
     * Create an instance of {@link RetrieveEventRequestTypeDTO }
     * 
     */
    public RetrieveEventRequestTypeDTO createRetrieveEventRequestTypeDTO() {
        return new RetrieveEventRequestTypeDTO();
    }

    /**
     * Create an instance of {@link EventDTO }
     * 
     */
    public EventDTO createEventDTO() {
        return new EventDTO();
    }

    /**
     * Create an instance of {@link ContactDetailsDTO }
     * 
     */
    public ContactDetailsDTO createContactDetailsDTO() {
        return new ContactDetailsDTO();
    }

    /**
     * Create an instance of {@link FeesDTO }
     * 
     */
    public FeesDTO createFeesDTO() {
        return new FeesDTO();
    }

    /**
     * Create an instance of {@link LocationDTO }
     * 
     */
    public LocationDTO createLocationDTO() {
        return new LocationDTO();
    }

    /**
     * Create an instance of {@link EventItemsDTO }
     * 
     */
    public EventItemsDTO createEventItemsDTO() {
        return new EventItemsDTO();
    }

    /**
     * Create an instance of {@link LoadCustomerProfileRequestHeader }
     * 
     */
    public LoadCustomerProfileRequestHeader createLoadCustomerProfileRequestHeader() {
        return new LoadCustomerProfileRequestHeader();
    }

    /**
     * Create an instance of {@link LoadCustomerProfileDataAreaRq }
     * 
     */
    public LoadCustomerProfileDataAreaRq createLoadCustomerProfileDataAreaRq() {
        return new LoadCustomerProfileDataAreaRq();
    }

    /**
     * Create an instance of {@link LoadCustomerProfileResponseHeader }
     * 
     */
    public LoadCustomerProfileResponseHeader createLoadCustomerProfileResponseHeader() {
        return new LoadCustomerProfileResponseHeader();
    }

    /**
     * Create an instance of {@link LoadCustomerProfileDataAreaRs }
     * 
     */
    public LoadCustomerProfileDataAreaRs createLoadCustomerProfileDataAreaRs() {
        return new LoadCustomerProfileDataAreaRs();
    }

    /**
     * Create an instance of {@link PushEventImageRequestHeader }
     * 
     */
    public PushEventImageRequestHeader createPushEventImageRequestHeader() {
        return new PushEventImageRequestHeader();
    }

    /**
     * Create an instance of {@link PushEventImageType }
     * 
     */
    public PushEventImageType createPushEventImageType() {
        return new PushEventImageType();
    }

    /**
     * Create an instance of {@link PushEvent }
     * 
     */
    public PushEvent createPushEvent() {
        return new PushEvent();
    }

    /**
     * Create an instance of {@link DeleteGoalMessageHeader }
     * 
     */
    public DeleteGoalMessageHeader createDeleteGoalMessageHeader() {
        return new DeleteGoalMessageHeader();
    }

    /**
     * Create an instance of {@link DeleteGoalDataAreaRsDTO }
     * 
     */
    public DeleteGoalDataAreaRsDTO createDeleteGoalDataAreaRsDTO() {
        return new DeleteGoalDataAreaRsDTO();
    }

    /**
     * Create an instance of {@link EventListResponseHeader }
     * 
     */
    public EventListResponseHeader createEventListResponseHeader() {
        return new EventListResponseHeader();
    }

    /**
     * Create an instance of {@link EventListDataAreaRs }
     * 
     */
    public EventListDataAreaRs createEventListDataAreaRs() {
        return new EventListDataAreaRs();
    }

    /**
     * Create an instance of {@link CancelScriptRequestHeader }
     * 
     */
    public CancelScriptRequestHeader createCancelScriptRequestHeader() {
        return new CancelScriptRequestHeader();
    }

    /**
     * Create an instance of {@link CancelScriptDataAreaRq }
     * 
     */
    public CancelScriptDataAreaRq createCancelScriptDataAreaRq() {
        return new CancelScriptDataAreaRq();
    }

    /**
     * Create an instance of {@link CancelFutureCollections }
     * 
     */
    public CancelFutureCollections createCancelFutureCollections() {
        return new CancelFutureCollections();
    }

    /**
     * Create an instance of {@link PushEventImageResponseHeader }
     * 
     */
    public PushEventImageResponseHeader createPushEventImageResponseHeader() {
        return new PushEventImageResponseHeader();
    }

    /**
     * Create an instance of {@link PushEventImageResponseType }
     * 
     */
    public PushEventImageResponseType createPushEventImageResponseType() {
        return new PushEventImageResponseType();
    }

    /**
     * Create an instance of {@link Payload }
     * 
     */
    public Payload createPayload() {
        return new Payload();
    }

    /**
     * Create an instance of {@link RetrieveGoalsResponseHeader }
     * 
     */
    public RetrieveGoalsResponseHeader createRetrieveGoalsResponseHeader() {
        return new RetrieveGoalsResponseHeader();
    }

    /**
     * Create an instance of {@link RetrieveGoalsDataAreaRs }
     * 
     */
    public RetrieveGoalsDataAreaRs createRetrieveGoalsDataAreaRs() {
        return new RetrieveGoalsDataAreaRs();
    }

    /**
     * Create an instance of {@link Goal1 }
     * 
     */
    public Goal1 createGoal1() {
        return new Goal1();
    }

    /**
     * Create an instance of {@link CancelScriptResponseHeader }
     * 
     */
    public CancelScriptResponseHeader createCancelScriptResponseHeader() {
        return new CancelScriptResponseHeader();
    }

    /**
     * Create an instance of {@link RetrieveCustomerProfileDataAreaRq }
     * 
     */
    public RetrieveCustomerProfileDataAreaRq createRetrieveCustomerProfileDataAreaRq() {
        return new RetrieveCustomerProfileDataAreaRq();
    }

    /**
     * Create an instance of {@link RetrieveCustomerProfileRequestHeader }
     * 
     */
    public RetrieveCustomerProfileRequestHeader createRetrieveCustomerProfileRequestHeader() {
        return new RetrieveCustomerProfileRequestHeader();
    }

    /**
     * Create an instance of {@link DeleteEventResponseHeader }
     * 
     */
    public DeleteEventResponseHeader createDeleteEventResponseHeader() {
        return new DeleteEventResponseHeader();
    }

    /**
     * Create an instance of {@link DeleteEventResponseType }
     * 
     */
    public DeleteEventResponseType createDeleteEventResponseType() {
        return new DeleteEventResponseType();
    }

    /**
     * Create an instance of {@link MaintainCustomerProfileDataAreaRs }
     * 
     */
    public MaintainCustomerProfileDataAreaRs createMaintainCustomerProfileDataAreaRs() {
        return new MaintainCustomerProfileDataAreaRs();
    }

    /**
     * Create an instance of {@link MaintainCustomerProfilePersonalDetails }
     * 
     */
    public MaintainCustomerProfilePersonalDetails createMaintainCustomerProfilePersonalDetails() {
        return new MaintainCustomerProfilePersonalDetails();
    }

    /**
     * Create an instance of {@link MaintainCustomerProfileMedicalAidDetails }
     * 
     */
    public MaintainCustomerProfileMedicalAidDetails createMaintainCustomerProfileMedicalAidDetails() {
        return new MaintainCustomerProfileMedicalAidDetails();
    }

    /**
     * Create an instance of {@link MaintainCustomerProfileDependantDetails }
     * 
     */
    public MaintainCustomerProfileDependantDetails createMaintainCustomerProfileDependantDetails() {
        return new MaintainCustomerProfileDependantDetails();
    }

    /**
     * Create an instance of {@link MaintainCustomerProfileEvents }
     * 
     */
    public MaintainCustomerProfileEvents createMaintainCustomerProfileEvents() {
        return new MaintainCustomerProfileEvents();
    }

    /**
     * Create an instance of {@link MaintainCustomerProfileResponseHeader }
     * 
     */
    public MaintainCustomerProfileResponseHeader createMaintainCustomerProfileResponseHeader() {
        return new MaintainCustomerProfileResponseHeader();
    }

    /**
     * Create an instance of {@link SubmitScriptResponseHeader }
     * 
     */
    public SubmitScriptResponseHeader createSubmitScriptResponseHeader() {
        return new SubmitScriptResponseHeader();
    }

    /**
     * Create an instance of {@link DeliveryAddress }
     * 
     */
    public DeliveryAddress createDeliveryAddress() {
        return new DeliveryAddress();
    }

    /**
     * Create an instance of {@link MainMember }
     * 
     */
    public MainMember createMainMember() {
        return new MainMember();
    }

    /**
     * Create an instance of {@link Contact }
     * 
     */
    public Contact createContact() {
        return new Contact();
    }

    /**
     * Create an instance of {@link Dependant }
     * 
     */
    public Dependant createDependant() {
        return new Dependant();
    }

    /**
     * Create an instance of {@link RetrieveGoalMessageHeader }
     * 
     */
    public RetrieveGoalMessageHeader createRetrieveGoalMessageHeader() {
        return new RetrieveGoalMessageHeader();
    }

    /**
     * Create an instance of {@link RetrieveGoalDataAreaRsDTO }
     * 
     */
    public RetrieveGoalDataAreaRsDTO createRetrieveGoalDataAreaRsDTO() {
        return new RetrieveGoalDataAreaRsDTO();
    }

    /**
     * Create an instance of {@link GoalDTO }
     * 
     */
    public GoalDTO createGoalDTO() {
        return new GoalDTO();
    }

    /**
     * Create an instance of {@link GoalsResponseHeader }
     * 
     */
    public GoalsResponseHeader createGoalsResponseHeader() {
        return new GoalsResponseHeader();
    }

    /**
     * Create an instance of {@link GoalsDataAreaRs }
     * 
     */
    public GoalsDataAreaRs createGoalsDataAreaRs() {
        return new GoalsDataAreaRs();
    }

    /**
     * Create an instance of {@link EventDetailsRequestHeader }
     * 
     */
    public EventDetailsRequestHeader createEventDetailsRequestHeader() {
        return new EventDetailsRequestHeader();
    }

    /**
     * Create an instance of {@link EventDetailsDataAreaRq }
     * 
     */
    public EventDetailsDataAreaRq createEventDetailsDataAreaRq() {
        return new EventDetailsDataAreaRq();
    }

    /**
     * Create an instance of {@link EventListRequestHeader }
     * 
     */
    public EventListRequestHeader createEventListRequestHeader() {
        return new EventListRequestHeader();
    }

    /**
     * Create an instance of {@link EventListDataAreaRq }
     * 
     */
    public EventListDataAreaRq createEventListDataAreaRq() {
        return new EventListDataAreaRq();
    }

    /**
     * Create an instance of {@link AddEventResponseHeader }
     * 
     */
    public AddEventResponseHeader createAddEventResponseHeader() {
        return new AddEventResponseHeader();
    }

    /**
     * Create an instance of {@link AddEventResponseType }
     * 
     */
    public AddEventResponseType createAddEventResponseType() {
        return new AddEventResponseType();
    }

    /**
     * Create an instance of {@link RetrieveScriptRequestHeader }
     * 
     */
    public RetrieveScriptRequestHeader createRetrieveScriptRequestHeader() {
        return new RetrieveScriptRequestHeader();
    }

    /**
     * Create an instance of {@link RetrieveScriptDataAreaRq }
     * 
     */
    public RetrieveScriptDataAreaRq createRetrieveScriptDataAreaRq() {
        return new RetrieveScriptDataAreaRq();
    }

    /**
     * Create an instance of {@link Question }
     * 
     */
    public Question createQuestion() {
        return new Question();
    }

    /**
     * Create an instance of {@link RetrieveQuestionnaireResponseHeader }
     * 
     */
    public RetrieveQuestionnaireResponseHeader createRetrieveQuestionnaireResponseHeader() {
        return new RetrieveQuestionnaireResponseHeader();
    }

    /**
     * Create an instance of {@link RetrieveQuestionnaireDataAreaRs }
     * 
     */
    public RetrieveQuestionnaireDataAreaRs createRetrieveQuestionnaireDataAreaRs() {
        return new RetrieveQuestionnaireDataAreaRs();
    }

    /**
     * Create an instance of {@link RetrieveQuestionnaireSubCategory }
     * 
     */
    public RetrieveQuestionnaireSubCategory createRetrieveQuestionnaireSubCategory() {
        return new RetrieveQuestionnaireSubCategory();
    }

    /**
     * Create an instance of {@link SubmitScriptRequestHeader }
     * 
     */
    public SubmitScriptRequestHeader createSubmitScriptRequestHeader() {
        return new SubmitScriptRequestHeader();
    }

    /**
     * Create an instance of {@link SubmitScriptDataAreaRq }
     * 
     */
    public SubmitScriptDataAreaRq createSubmitScriptDataAreaRq() {
        return new SubmitScriptDataAreaRq();
    }

    /**
     * Create an instance of {@link OnceOffDependant }
     * 
     */
    public OnceOffDependant createOnceOffDependant() {
        return new OnceOffDependant();
    }

    /**
     * Create an instance of {@link EventDetailsResponseHeader }
     * 
     */
    public EventDetailsResponseHeader createEventDetailsResponseHeader() {
        return new EventDetailsResponseHeader();
    }

    /**
     * Create an instance of {@link EventDetailsDataAreaRs }
     * 
     */
    public EventDetailsDataAreaRs createEventDetailsDataAreaRs() {
        return new EventDetailsDataAreaRs();
    }

    /**
     * Create an instance of {@link EventDetails }
     * 
     */
    public EventDetails createEventDetails() {
        return new EventDetails();
    }

    /**
     * Create an instance of {@link ContactDetails }
     * 
     */
    public ContactDetails createContactDetails() {
        return new ContactDetails();
    }

    /**
     * Create an instance of {@link Fees }
     * 
     */
    public Fees createFees() {
        return new Fees();
    }

    /**
     * Create an instance of {@link Location }
     * 
     */
    public Location createLocation() {
        return new Location();
    }

    /**
     * Create an instance of {@link RetrieveDashboardResponseHeader }
     * 
     */
    public RetrieveDashboardResponseHeader createRetrieveDashboardResponseHeader() {
        return new RetrieveDashboardResponseHeader();
    }

    /**
     * Create an instance of {@link RetrieveDashboardDataAreaRs }
     * 
     */
    public RetrieveDashboardDataAreaRs createRetrieveDashboardDataAreaRs() {
        return new RetrieveDashboardDataAreaRs();
    }

    /**
     * Create an instance of {@link Category }
     * 
     */
    public Category createCategory() {
        return new Category();
    }

    /**
     * Create an instance of {@link DeleteEventRequestHeader }
     * 
     */
    public DeleteEventRequestHeader createDeleteEventRequestHeader() {
        return new DeleteEventRequestHeader();
    }

    /**
     * Create an instance of {@link DeleteEventRequestType }
     * 
     */
    public DeleteEventRequestType createDeleteEventRequestType() {
        return new DeleteEventRequestType();
    }

    /**
     * Create an instance of {@link MyEvents }
     * 
     */
    public MyEvents createMyEvents() {
        return new MyEvents();
    }

    /**
     * Create an instance of {@link SavedEvent }
     * 
     */
    public SavedEvent createSavedEvent() {
        return new SavedEvent();
    }

    /**
     * Create an instance of {@link RetrieveGoalDataAreaRqDTO }
     * 
     */
    public RetrieveGoalDataAreaRqDTO createRetrieveGoalDataAreaRqDTO() {
        return new RetrieveGoalDataAreaRqDTO();
    }

    /**
     * Create an instance of {@link RetrieveQuestionAnswerRequestHeader }
     * 
     */
    public RetrieveQuestionAnswerRequestHeader createRetrieveQuestionAnswerRequestHeader() {
        return new RetrieveQuestionAnswerRequestHeader();
    }

    /**
     * Create an instance of {@link RetrieveQuestionAnswerDataAreaRq }
     * 
     */
    public RetrieveQuestionAnswerDataAreaRq createRetrieveQuestionAnswerDataAreaRq() {
        return new RetrieveQuestionAnswerDataAreaRq();
    }

    /**
     * Create an instance of {@link RetrieveCustomerProfileDataAreaRs }
     * 
     */
    public RetrieveCustomerProfileDataAreaRs createRetrieveCustomerProfileDataAreaRs() {
        return new RetrieveCustomerProfileDataAreaRs();
    }

    /**
     * Create an instance of {@link RetrieveCustomerProfilePersonalDetails }
     * 
     */
    public RetrieveCustomerProfilePersonalDetails createRetrieveCustomerProfilePersonalDetails() {
        return new RetrieveCustomerProfilePersonalDetails();
    }

    /**
     * Create an instance of {@link RetrieveCustomerProfileMedicalAidDetails }
     * 
     */
    public RetrieveCustomerProfileMedicalAidDetails createRetrieveCustomerProfileMedicalAidDetails() {
        return new RetrieveCustomerProfileMedicalAidDetails();
    }

    /**
     * Create an instance of {@link RetrieveCustomerProfileDependantDetails }
     * 
     */
    public RetrieveCustomerProfileDependantDetails createRetrieveCustomerProfileDependantDetails() {
        return new RetrieveCustomerProfileDependantDetails();
    }

    /**
     * Create an instance of {@link RetrieveCustomerProfileEvents }
     * 
     */
    public RetrieveCustomerProfileEvents createRetrieveCustomerProfileEvents() {
        return new RetrieveCustomerProfileEvents();
    }

    /**
     * Create an instance of {@link RetrieveCustomerProfileResponseHeader }
     * 
     */
    public RetrieveCustomerProfileResponseHeader createRetrieveCustomerProfileResponseHeader() {
        return new RetrieveCustomerProfileResponseHeader();
    }

    /**
     * Create an instance of {@link RetrievePreloadDashboardDataAreaRs }
     * 
     */
    public RetrievePreloadDashboardDataAreaRs createRetrievePreloadDashboardDataAreaRs() {
        return new RetrievePreloadDashboardDataAreaRs();
    }

    /**
     * Create an instance of {@link Notifications }
     * 
     */
    public Notifications createNotifications() {
        return new Notifications();
    }

    /**
     * Create an instance of {@link RetrievePreloadDashboardDataAreaRq }
     * 
     */
    public RetrievePreloadDashboardDataAreaRq createRetrievePreloadDashboardDataAreaRq() {
        return new RetrievePreloadDashboardDataAreaRq();
    }

    /**
     * Create an instance of {@link RetrievePreloadDashboardCustomerResponse }
     * 
     */
    public RetrievePreloadDashboardCustomerResponse createRetrievePreloadDashboardCustomerResponse() {
        return new RetrievePreloadDashboardCustomerResponse();
    }

    /**
     * Create an instance of {@link AddEventRequestHeader }
     * 
     */
    public AddEventRequestHeader createAddEventRequestHeader() {
        return new AddEventRequestHeader();
    }

    /**
     * Create an instance of {@link RetrieveQuestionAnswerSubCategory }
     * 
     */
    public RetrieveQuestionAnswerSubCategory createRetrieveQuestionAnswerSubCategory() {
        return new RetrieveQuestionAnswerSubCategory();
    }

    /**
     * Create an instance of {@link AnswerOption }
     * 
     */
    public AnswerOption createAnswerOption() {
        return new AnswerOption();
    }

    /**
     * Create an instance of {@link QuickTip }
     * 
     */
    public QuickTip createQuickTip() {
        return new QuickTip();
    }

    /**
     * Create an instance of {@link RetrieveDashboardDataAreaRq }
     * 
     */
    public RetrieveDashboardDataAreaRq createRetrieveDashboardDataAreaRq() {
        return new RetrieveDashboardDataAreaRq();
    }

    /**
     * Create an instance of {@link RetrieveGoalsRequestHeader }
     * 
     */
    public RetrieveGoalsRequestHeader createRetrieveGoalsRequestHeader() {
        return new RetrieveGoalsRequestHeader();
    }

    /**
     * Create an instance of {@link MaintainCustomerProfileDataAreaRq }
     * 
     */
    public MaintainCustomerProfileDataAreaRq createMaintainCustomerProfileDataAreaRq() {
        return new MaintainCustomerProfileDataAreaRq();
    }

    /**
     * Create an instance of {@link RetrieveQuestionnaireRequestHeader }
     * 
     */
    public RetrieveQuestionnaireRequestHeader createRetrieveQuestionnaireRequestHeader() {
        return new RetrieveQuestionnaireRequestHeader();
    }

    /**
     * Create an instance of {@link RetrievePreloadDashboardResponseHeader }
     * 
     */
    public RetrievePreloadDashboardResponseHeader createRetrievePreloadDashboardResponseHeader() {
        return new RetrievePreloadDashboardResponseHeader();
    }

    /**
     * Create an instance of {@link PushNotificationResponseHeader }
     * 
     */
    public PushNotificationResponseHeader createPushNotificationResponseHeader() {
        return new PushNotificationResponseHeader();
    }

    /**
     * Create an instance of {@link RetrieveQuestionnaireDataAreaRq }
     * 
     */
    public RetrieveQuestionnaireDataAreaRq createRetrieveQuestionnaireDataAreaRq() {
        return new RetrieveQuestionnaireDataAreaRq();
    }

    /**
     * Create an instance of {@link RetrieveScriptResponseHeader }
     * 
     */
    public RetrieveScriptResponseHeader createRetrieveScriptResponseHeader() {
        return new RetrieveScriptResponseHeader();
    }

    /**
     * Create an instance of {@link RetrieveFutureCollections }
     * 
     */
    public RetrieveFutureCollections createRetrieveFutureCollections() {
        return new RetrieveFutureCollections();
    }

    /**
     * Create an instance of {@link HistoryCollections }
     * 
     */
    public HistoryCollections createHistoryCollections() {
        return new HistoryCollections();
    }

    /**
     * Create an instance of {@link RetrievePreloadDashboardRequestHeader }
     * 
     */
    public RetrievePreloadDashboardRequestHeader createRetrievePreloadDashboardRequestHeader() {
        return new RetrievePreloadDashboardRequestHeader();
    }

    /**
     * Create an instance of {@link MaintainCustomerProfileRequestHeader }
     * 
     */
    public MaintainCustomerProfileRequestHeader createMaintainCustomerProfileRequestHeader() {
        return new MaintainCustomerProfileRequestHeader();
    }

    /**
     * Create an instance of {@link AddEventRequestType }
     * 
     */
    public AddEventRequestType createAddEventRequestType() {
        return new AddEventRequestType();
    }

    /**
     * Create an instance of {@link AddEventDetails }
     * 
     */
    public AddEventDetails createAddEventDetails() {
        return new AddEventDetails();
    }

    /**
     * Create an instance of {@link RetrieveQuestionAnswerResponseHeader }
     * 
     */
    public RetrieveQuestionAnswerResponseHeader createRetrieveQuestionAnswerResponseHeader() {
        return new RetrieveQuestionAnswerResponseHeader();
    }

    /**
     * Create an instance of {@link RetrieveQuestionAnswerDataAreaRs }
     * 
     */
    public RetrieveQuestionAnswerDataAreaRs createRetrieveQuestionAnswerDataAreaRs() {
        return new RetrieveQuestionAnswerDataAreaRs();
    }

    /**
     * Create an instance of {@link RetrieveDashboardRequestHeader }
     * 
     */
    public RetrieveDashboardRequestHeader createRetrieveDashboardRequestHeader() {
        return new RetrieveDashboardRequestHeader();
    }

    /**
     * Create an instance of {@link DeleteGoalDataAreaRqDTO }
     * 
     */
    public DeleteGoalDataAreaRqDTO createDeleteGoalDataAreaRqDTO() {
        return new DeleteGoalDataAreaRqDTO();
    }

    /**
     * Create an instance of {@link RetrieveGoalsDataAreaRq }
     * 
     */
    public RetrieveGoalsDataAreaRq createRetrieveGoalsDataAreaRq() {
        return new RetrieveGoalsDataAreaRq();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "Exception")
    public JAXBElement<Exception> createException(Exception value) {
        return new JAXBElement<Exception>(_Exception_QNAME, Exception.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddEvent }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link AddEvent }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "addEvent")
    public JAXBElement<AddEvent> createAddEvent(AddEvent value) {
        return new JAXBElement<AddEvent>(_AddEvent_QNAME, AddEvent.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddEventRequest }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link AddEventRequest }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "addEventRequest")
    public JAXBElement<AddEventRequest> createAddEventRequest(AddEventRequest value) {
        return new JAXBElement<AddEventRequest>(_AddEventRequest_QNAME, AddEventRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddEventResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link AddEventResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "addEventResponse")
    public JAXBElement<AddEventResponse> createAddEventResponse(AddEventResponse value) {
        return new JAXBElement<AddEventResponse>(_AddEventResponse_QNAME, AddEventResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddEventResponseDTO }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link AddEventResponseDTO }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "addEventResponseDTO")
    public JAXBElement<AddEventResponseDTO> createAddEventResponseDTO(AddEventResponseDTO value) {
        return new JAXBElement<AddEventResponseDTO>(_AddEventResponseDTO_QNAME, AddEventResponseDTO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CancelScript }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CancelScript }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "cancelScript")
    public JAXBElement<CancelScript> createCancelScript(CancelScript value) {
        return new JAXBElement<CancelScript>(_CancelScript_QNAME, CancelScript.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CancelScriptRequestDTO }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CancelScriptRequestDTO }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "cancelScriptRequestDTO")
    public JAXBElement<CancelScriptRequestDTO> createCancelScriptRequestDTO(CancelScriptRequestDTO value) {
        return new JAXBElement<CancelScriptRequestDTO>(_CancelScriptRequestDTO_QNAME, CancelScriptRequestDTO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CancelScriptResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CancelScriptResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "cancelScriptResponse")
    public JAXBElement<CancelScriptResponse> createCancelScriptResponse(CancelScriptResponse value) {
        return new JAXBElement<CancelScriptResponse>(_CancelScriptResponse_QNAME, CancelScriptResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CancelScriptResponseDTO }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CancelScriptResponseDTO }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "cancelScriptResponseDTO")
    public JAXBElement<CancelScriptResponseDTO> createCancelScriptResponseDTO(CancelScriptResponseDTO value) {
        return new JAXBElement<CancelScriptResponseDTO>(_CancelScriptResponseDTO_QNAME, CancelScriptResponseDTO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteEvent }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DeleteEvent }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "deleteEvent")
    public JAXBElement<DeleteEvent> createDeleteEvent(DeleteEvent value) {
        return new JAXBElement<DeleteEvent>(_DeleteEvent_QNAME, DeleteEvent.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteEventRequest }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DeleteEventRequest }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "deleteEventRequest")
    public JAXBElement<DeleteEventRequest> createDeleteEventRequest(DeleteEventRequest value) {
        return new JAXBElement<DeleteEventRequest>(_DeleteEventRequest_QNAME, DeleteEventRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteEventResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DeleteEventResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "deleteEventResponse")
    public JAXBElement<DeleteEventResponse> createDeleteEventResponse(DeleteEventResponse value) {
        return new JAXBElement<DeleteEventResponse>(_DeleteEventResponse_QNAME, DeleteEventResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteEventResponseDTO }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DeleteEventResponseDTO }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "deleteEventResponseDTO")
    public JAXBElement<DeleteEventResponseDTO> createDeleteEventResponseDTO(DeleteEventResponseDTO value) {
        return new JAXBElement<DeleteEventResponseDTO>(_DeleteEventResponseDTO_QNAME, DeleteEventResponseDTO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteGoal }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DeleteGoal }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "deleteGoal")
    public JAXBElement<DeleteGoal> createDeleteGoal(DeleteGoal value) {
        return new JAXBElement<DeleteGoal>(_DeleteGoal_QNAME, DeleteGoal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteGoalResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DeleteGoalResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "deleteGoalResponse")
    public JAXBElement<DeleteGoalResponse> createDeleteGoalResponse(DeleteGoalResponse value) {
        return new JAXBElement<DeleteGoalResponse>(_DeleteGoalResponse_QNAME, DeleteGoalResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteGoalsRequestDTO }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DeleteGoalsRequestDTO }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "deleteGoalsRequestDTO")
    public JAXBElement<DeleteGoalsRequestDTO> createDeleteGoalsRequestDTO(DeleteGoalsRequestDTO value) {
        return new JAXBElement<DeleteGoalsRequestDTO>(_DeleteGoalsRequestDTO_QNAME, DeleteGoalsRequestDTO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteGoalsResponseDTO }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DeleteGoalsResponseDTO }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "deleteGoalsResponseDTO")
    public JAXBElement<DeleteGoalsResponseDTO> createDeleteGoalsResponseDTO(DeleteGoalsResponseDTO value) {
        return new JAXBElement<DeleteGoalsResponseDTO>(_DeleteGoalsResponseDTO_QNAME, DeleteGoalsResponseDTO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DischemPushNotification }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DischemPushNotification }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "dischemPushNotification")
    public JAXBElement<DischemPushNotification> createDischemPushNotification(DischemPushNotification value) {
        return new JAXBElement<DischemPushNotification>(_DischemPushNotification_QNAME, DischemPushNotification.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DischemPushNotificationResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DischemPushNotificationResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "dischemPushNotificationResponse")
    public JAXBElement<DischemPushNotificationResponse> createDischemPushNotificationResponse(DischemPushNotificationResponse value) {
        return new JAXBElement<DischemPushNotificationResponse>(_DischemPushNotificationResponse_QNAME, DischemPushNotificationResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EventDetailsRequestDTO }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link EventDetailsRequestDTO }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "eventDetailsRequestDTO")
    public JAXBElement<EventDetailsRequestDTO> createEventDetailsRequestDTO(EventDetailsRequestDTO value) {
        return new JAXBElement<EventDetailsRequestDTO>(_EventDetailsRequestDTO_QNAME, EventDetailsRequestDTO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EventDetailsResponseDTO }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link EventDetailsResponseDTO }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "eventDetailsResponseDTO")
    public JAXBElement<EventDetailsResponseDTO> createEventDetailsResponseDTO(EventDetailsResponseDTO value) {
        return new JAXBElement<EventDetailsResponseDTO>(_EventDetailsResponseDTO_QNAME, EventDetailsResponseDTO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EventListRequestDTO }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link EventListRequestDTO }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "eventListRequestDTO")
    public JAXBElement<EventListRequestDTO> createEventListRequestDTO(EventListRequestDTO value) {
        return new JAXBElement<EventListRequestDTO>(_EventListRequestDTO_QNAME, EventListRequestDTO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EventListResponseDTO }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link EventListResponseDTO }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "eventListResponseDTO")
    public JAXBElement<EventListResponseDTO> createEventListResponseDTO(EventListResponseDTO value) {
        return new JAXBElement<EventListResponseDTO>(_EventListResponseDTO_QNAME, EventListResponseDTO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Init }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Init }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "init")
    public JAXBElement<Init> createInit(Init value) {
        return new JAXBElement<Init>(_Init_QNAME, Init.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InitResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link InitResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "initResponse")
    public JAXBElement<InitResponse> createInitResponse(InitResponse value) {
        return new JAXBElement<InitResponse>(_InitResponse_QNAME, InitResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadCustomerProfile }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LoadCustomerProfile }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "loadCustomerProfile")
    public JAXBElement<LoadCustomerProfile> createLoadCustomerProfile(LoadCustomerProfile value) {
        return new JAXBElement<LoadCustomerProfile>(_LoadCustomerProfile_QNAME, LoadCustomerProfile.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadCustomerProfileRequestDTO }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LoadCustomerProfileRequestDTO }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "loadCustomerProfileRequestDTO")
    public JAXBElement<LoadCustomerProfileRequestDTO> createLoadCustomerProfileRequestDTO(LoadCustomerProfileRequestDTO value) {
        return new JAXBElement<LoadCustomerProfileRequestDTO>(_LoadCustomerProfileRequestDTO_QNAME, LoadCustomerProfileRequestDTO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadCustomerProfileResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LoadCustomerProfileResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "loadCustomerProfileResponse")
    public JAXBElement<LoadCustomerProfileResponse> createLoadCustomerProfileResponse(LoadCustomerProfileResponse value) {
        return new JAXBElement<LoadCustomerProfileResponse>(_LoadCustomerProfileResponse_QNAME, LoadCustomerProfileResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadCustomerProfileResponseDTO }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LoadCustomerProfileResponseDTO }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "loadCustomerProfileResponseDTO")
    public JAXBElement<LoadCustomerProfileResponseDTO> createLoadCustomerProfileResponseDTO(LoadCustomerProfileResponseDTO value) {
        return new JAXBElement<LoadCustomerProfileResponseDTO>(_LoadCustomerProfileResponseDTO_QNAME, LoadCustomerProfileResponseDTO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MaintainCustomerProfile }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link MaintainCustomerProfile }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "maintainCustomerProfile")
    public JAXBElement<MaintainCustomerProfile> createMaintainCustomerProfile(MaintainCustomerProfile value) {
        return new JAXBElement<MaintainCustomerProfile>(_MaintainCustomerProfile_QNAME, MaintainCustomerProfile.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MaintainCustomerProfileRequestDTO }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link MaintainCustomerProfileRequestDTO }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "maintainCustomerProfileRequestDTO")
    public JAXBElement<MaintainCustomerProfileRequestDTO> createMaintainCustomerProfileRequestDTO(MaintainCustomerProfileRequestDTO value) {
        return new JAXBElement<MaintainCustomerProfileRequestDTO>(_MaintainCustomerProfileRequestDTO_QNAME, MaintainCustomerProfileRequestDTO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MaintainCustomerProfileResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link MaintainCustomerProfileResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "maintainCustomerProfileResponse")
    public JAXBElement<MaintainCustomerProfileResponse> createMaintainCustomerProfileResponse(MaintainCustomerProfileResponse value) {
        return new JAXBElement<MaintainCustomerProfileResponse>(_MaintainCustomerProfileResponse_QNAME, MaintainCustomerProfileResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MaintainCustomerProfileResponseDTO }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link MaintainCustomerProfileResponseDTO }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "maintainCustomerProfileResponseDTO")
    public JAXBElement<MaintainCustomerProfileResponseDTO> createMaintainCustomerProfileResponseDTO(MaintainCustomerProfileResponseDTO value) {
        return new JAXBElement<MaintainCustomerProfileResponseDTO>(_MaintainCustomerProfileResponseDTO_QNAME, MaintainCustomerProfileResponseDTO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PushEventImage }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link PushEventImage }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "pushEventImage")
    public JAXBElement<PushEventImage> createPushEventImage(PushEventImage value) {
        return new JAXBElement<PushEventImage>(_PushEventImage_QNAME, PushEventImage.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PushEventImageRequest }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link PushEventImageRequest }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "pushEventImageRequest")
    public JAXBElement<PushEventImageRequest> createPushEventImageRequest(PushEventImageRequest value) {
        return new JAXBElement<PushEventImageRequest>(_PushEventImageRequest_QNAME, PushEventImageRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PushEventImageResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link PushEventImageResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "pushEventImageResponse")
    public JAXBElement<PushEventImageResponse> createPushEventImageResponse(PushEventImageResponse value) {
        return new JAXBElement<PushEventImageResponse>(_PushEventImageResponse_QNAME, PushEventImageResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PushEventImageResponseDTO }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link PushEventImageResponseDTO }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "pushEventImageResponseDTO")
    public JAXBElement<PushEventImageResponseDTO> createPushEventImageResponseDTO(PushEventImageResponseDTO value) {
        return new JAXBElement<PushEventImageResponseDTO>(_PushEventImageResponseDTO_QNAME, PushEventImageResponseDTO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PushNotificationRequestDTO }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link PushNotificationRequestDTO }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "pushNotificationRequestDTO")
    public JAXBElement<PushNotificationRequestDTO> createPushNotificationRequestDTO(PushNotificationRequestDTO value) {
        return new JAXBElement<PushNotificationRequestDTO>(_PushNotificationRequestDTO_QNAME, PushNotificationRequestDTO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PushNotificationResponseDTO }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link PushNotificationResponseDTO }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "pushNotificationResponseDTO")
    public JAXBElement<PushNotificationResponseDTO> createPushNotificationResponseDTO(PushNotificationResponseDTO value) {
        return new JAXBElement<PushNotificationResponseDTO>(_PushNotificationResponseDTO_QNAME, PushNotificationResponseDTO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrieveALLEvents }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RetrieveALLEvents }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "retrieveALLEvents")
    public JAXBElement<RetrieveALLEvents> createRetrieveALLEvents(RetrieveALLEvents value) {
        return new JAXBElement<RetrieveALLEvents>(_RetrieveALLEvents_QNAME, RetrieveALLEvents.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrieveALLEventsResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RetrieveALLEventsResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "retrieveALLEventsResponse")
    public JAXBElement<RetrieveALLEventsResponse> createRetrieveALLEventsResponse(RetrieveALLEventsResponse value) {
        return new JAXBElement<RetrieveALLEventsResponse>(_RetrieveALLEventsResponse_QNAME, RetrieveALLEventsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrieveCustomerProfile }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RetrieveCustomerProfile }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "retrieveCustomerProfile")
    public JAXBElement<RetrieveCustomerProfile> createRetrieveCustomerProfile(RetrieveCustomerProfile value) {
        return new JAXBElement<RetrieveCustomerProfile>(_RetrieveCustomerProfile_QNAME, RetrieveCustomerProfile.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrieveCustomerProfileRequestDTO }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RetrieveCustomerProfileRequestDTO }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "retrieveCustomerProfileRequestDTO")
    public JAXBElement<RetrieveCustomerProfileRequestDTO> createRetrieveCustomerProfileRequestDTO(RetrieveCustomerProfileRequestDTO value) {
        return new JAXBElement<RetrieveCustomerProfileRequestDTO>(_RetrieveCustomerProfileRequestDTO_QNAME, RetrieveCustomerProfileRequestDTO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrieveCustomerProfileResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RetrieveCustomerProfileResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "retrieveCustomerProfileResponse")
    public JAXBElement<RetrieveCustomerProfileResponse> createRetrieveCustomerProfileResponse(RetrieveCustomerProfileResponse value) {
        return new JAXBElement<RetrieveCustomerProfileResponse>(_RetrieveCustomerProfileResponse_QNAME, RetrieveCustomerProfileResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrieveCustomerProfileResponseDTO }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RetrieveCustomerProfileResponseDTO }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "retrieveCustomerProfileResponseDTO")
    public JAXBElement<RetrieveCustomerProfileResponseDTO> createRetrieveCustomerProfileResponseDTO(RetrieveCustomerProfileResponseDTO value) {
        return new JAXBElement<RetrieveCustomerProfileResponseDTO>(_RetrieveCustomerProfileResponseDTO_QNAME, RetrieveCustomerProfileResponseDTO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrieveDashboard }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RetrieveDashboard }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "retrieveDashboard")
    public JAXBElement<RetrieveDashboard> createRetrieveDashboard(RetrieveDashboard value) {
        return new JAXBElement<RetrieveDashboard>(_RetrieveDashboard_QNAME, RetrieveDashboard.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrieveDashboardRequestDTO }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RetrieveDashboardRequestDTO }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "retrieveDashboardRequestDTO")
    public JAXBElement<RetrieveDashboardRequestDTO> createRetrieveDashboardRequestDTO(RetrieveDashboardRequestDTO value) {
        return new JAXBElement<RetrieveDashboardRequestDTO>(_RetrieveDashboardRequestDTO_QNAME, RetrieveDashboardRequestDTO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrieveDashboardResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RetrieveDashboardResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "retrieveDashboardResponse")
    public JAXBElement<RetrieveDashboardResponse> createRetrieveDashboardResponse(RetrieveDashboardResponse value) {
        return new JAXBElement<RetrieveDashboardResponse>(_RetrieveDashboardResponse_QNAME, RetrieveDashboardResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrieveDashboardResponseDTO }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RetrieveDashboardResponseDTO }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "retrieveDashboardResponseDTO")
    public JAXBElement<RetrieveDashboardResponseDTO> createRetrieveDashboardResponseDTO(RetrieveDashboardResponseDTO value) {
        return new JAXBElement<RetrieveDashboardResponseDTO>(_RetrieveDashboardResponseDTO_QNAME, RetrieveDashboardResponseDTO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrieveEventDetails }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RetrieveEventDetails }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "retrieveEventDetails")
    public JAXBElement<RetrieveEventDetails> createRetrieveEventDetails(RetrieveEventDetails value) {
        return new JAXBElement<RetrieveEventDetails>(_RetrieveEventDetails_QNAME, RetrieveEventDetails.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrieveEventDetailsResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RetrieveEventDetailsResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "retrieveEventDetailsResponse")
    public JAXBElement<RetrieveEventDetailsResponse> createRetrieveEventDetailsResponse(RetrieveEventDetailsResponse value) {
        return new JAXBElement<RetrieveEventDetailsResponse>(_RetrieveEventDetailsResponse_QNAME, RetrieveEventDetailsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrieveEventRequestDTO }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RetrieveEventRequestDTO }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "retrieveEventRequestDTO")
    public JAXBElement<RetrieveEventRequestDTO> createRetrieveEventRequestDTO(RetrieveEventRequestDTO value) {
        return new JAXBElement<RetrieveEventRequestDTO>(_RetrieveEventRequestDTO_QNAME, RetrieveEventRequestDTO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrieveGoalList }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RetrieveGoalList }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "retrieveGoalList")
    public JAXBElement<RetrieveGoalList> createRetrieveGoalList(RetrieveGoalList value) {
        return new JAXBElement<RetrieveGoalList>(_RetrieveGoalList_QNAME, RetrieveGoalList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrieveGoalListRequestDTO }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RetrieveGoalListRequestDTO }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "retrieveGoalListRequestDTO")
    public JAXBElement<RetrieveGoalListRequestDTO> createRetrieveGoalListRequestDTO(RetrieveGoalListRequestDTO value) {
        return new JAXBElement<RetrieveGoalListRequestDTO>(_RetrieveGoalListRequestDTO_QNAME, RetrieveGoalListRequestDTO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrieveGoalListResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RetrieveGoalListResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "retrieveGoalListResponse")
    public JAXBElement<RetrieveGoalListResponse> createRetrieveGoalListResponse(RetrieveGoalListResponse value) {
        return new JAXBElement<RetrieveGoalListResponse>(_RetrieveGoalListResponse_QNAME, RetrieveGoalListResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrieveGoalListResponseDTO }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RetrieveGoalListResponseDTO }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "retrieveGoalListResponseDTO")
    public JAXBElement<RetrieveGoalListResponseDTO> createRetrieveGoalListResponseDTO(RetrieveGoalListResponseDTO value) {
        return new JAXBElement<RetrieveGoalListResponseDTO>(_RetrieveGoalListResponseDTO_QNAME, RetrieveGoalListResponseDTO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrieveGoals }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RetrieveGoals }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "retrieveGoals")
    public JAXBElement<RetrieveGoals> createRetrieveGoals(RetrieveGoals value) {
        return new JAXBElement<RetrieveGoals>(_RetrieveGoals_QNAME, RetrieveGoals.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrieveGoalsRequestDTO }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RetrieveGoalsRequestDTO }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "retrieveGoalsRequestDTO")
    public JAXBElement<RetrieveGoalsRequestDTO> createRetrieveGoalsRequestDTO(RetrieveGoalsRequestDTO value) {
        return new JAXBElement<RetrieveGoalsRequestDTO>(_RetrieveGoalsRequestDTO_QNAME, RetrieveGoalsRequestDTO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrieveGoalsResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RetrieveGoalsResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "retrieveGoalsResponse")
    public JAXBElement<RetrieveGoalsResponse> createRetrieveGoalsResponse(RetrieveGoalsResponse value) {
        return new JAXBElement<RetrieveGoalsResponse>(_RetrieveGoalsResponse_QNAME, RetrieveGoalsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrieveGoalsResponseDTO }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RetrieveGoalsResponseDTO }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "retrieveGoalsResponseDTO")
    public JAXBElement<RetrieveGoalsResponseDTO> createRetrieveGoalsResponseDTO(RetrieveGoalsResponseDTO value) {
        return new JAXBElement<RetrieveGoalsResponseDTO>(_RetrieveGoalsResponseDTO_QNAME, RetrieveGoalsResponseDTO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrieveMyEvents }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RetrieveMyEvents }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "retrieveMyEvents")
    public JAXBElement<RetrieveMyEvents> createRetrieveMyEvents(RetrieveMyEvents value) {
        return new JAXBElement<RetrieveMyEvents>(_RetrieveMyEvents_QNAME, RetrieveMyEvents.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrieveMyEventsResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RetrieveMyEventsResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "retrieveMyEventsResponse")
    public JAXBElement<RetrieveMyEventsResponse> createRetrieveMyEventsResponse(RetrieveMyEventsResponse value) {
        return new JAXBElement<RetrieveMyEventsResponse>(_RetrieveMyEventsResponse_QNAME, RetrieveMyEventsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrievePreloadDashboard }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RetrievePreloadDashboard }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "retrievePreloadDashboard")
    public JAXBElement<RetrievePreloadDashboard> createRetrievePreloadDashboard(RetrievePreloadDashboard value) {
        return new JAXBElement<RetrievePreloadDashboard>(_RetrievePreloadDashboard_QNAME, RetrievePreloadDashboard.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrievePreloadDashboardRequestDTO }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RetrievePreloadDashboardRequestDTO }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "retrievePreloadDashboardRequestDTO")
    public JAXBElement<RetrievePreloadDashboardRequestDTO> createRetrievePreloadDashboardRequestDTO(RetrievePreloadDashboardRequestDTO value) {
        return new JAXBElement<RetrievePreloadDashboardRequestDTO>(_RetrievePreloadDashboardRequestDTO_QNAME, RetrievePreloadDashboardRequestDTO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrievePreloadDashboardResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RetrievePreloadDashboardResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "retrievePreloadDashboardResponse")
    public JAXBElement<RetrievePreloadDashboardResponse> createRetrievePreloadDashboardResponse(RetrievePreloadDashboardResponse value) {
        return new JAXBElement<RetrievePreloadDashboardResponse>(_RetrievePreloadDashboardResponse_QNAME, RetrievePreloadDashboardResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrievePreloadDashboardResponseDTO }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RetrievePreloadDashboardResponseDTO }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "retrievePreloadDashboardResponseDTO")
    public JAXBElement<RetrievePreloadDashboardResponseDTO> createRetrievePreloadDashboardResponseDTO(RetrievePreloadDashboardResponseDTO value) {
        return new JAXBElement<RetrievePreloadDashboardResponseDTO>(_RetrievePreloadDashboardResponseDTO_QNAME, RetrievePreloadDashboardResponseDTO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrieveQuestionSubmitAnswer }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RetrieveQuestionSubmitAnswer }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "retrieveQuestionSubmitAnswer")
    public JAXBElement<RetrieveQuestionSubmitAnswer> createRetrieveQuestionSubmitAnswer(RetrieveQuestionSubmitAnswer value) {
        return new JAXBElement<RetrieveQuestionSubmitAnswer>(_RetrieveQuestionSubmitAnswer_QNAME, RetrieveQuestionSubmitAnswer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrieveQuestionSubmitAnswerRequestDTO }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RetrieveQuestionSubmitAnswerRequestDTO }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "retrieveQuestionSubmitAnswerRequestDTO")
    public JAXBElement<RetrieveQuestionSubmitAnswerRequestDTO> createRetrieveQuestionSubmitAnswerRequestDTO(RetrieveQuestionSubmitAnswerRequestDTO value) {
        return new JAXBElement<RetrieveQuestionSubmitAnswerRequestDTO>(_RetrieveQuestionSubmitAnswerRequestDTO_QNAME, RetrieveQuestionSubmitAnswerRequestDTO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrieveQuestionSubmitAnswerResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RetrieveQuestionSubmitAnswerResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "retrieveQuestionSubmitAnswerResponse")
    public JAXBElement<RetrieveQuestionSubmitAnswerResponse> createRetrieveQuestionSubmitAnswerResponse(RetrieveQuestionSubmitAnswerResponse value) {
        return new JAXBElement<RetrieveQuestionSubmitAnswerResponse>(_RetrieveQuestionSubmitAnswerResponse_QNAME, RetrieveQuestionSubmitAnswerResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrieveQuestionSubmitAnswerResponseDTO }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RetrieveQuestionSubmitAnswerResponseDTO }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "retrieveQuestionSubmitAnswerResponseDTO")
    public JAXBElement<RetrieveQuestionSubmitAnswerResponseDTO> createRetrieveQuestionSubmitAnswerResponseDTO(RetrieveQuestionSubmitAnswerResponseDTO value) {
        return new JAXBElement<RetrieveQuestionSubmitAnswerResponseDTO>(_RetrieveQuestionSubmitAnswerResponseDTO_QNAME, RetrieveQuestionSubmitAnswerResponseDTO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrieveQuestionnaire }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RetrieveQuestionnaire }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "retrieveQuestionnaire")
    public JAXBElement<RetrieveQuestionnaire> createRetrieveQuestionnaire(RetrieveQuestionnaire value) {
        return new JAXBElement<RetrieveQuestionnaire>(_RetrieveQuestionnaire_QNAME, RetrieveQuestionnaire.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrieveQuestionnaireRequestDTO }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RetrieveQuestionnaireRequestDTO }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "retrieveQuestionnaireRequestDTO")
    public JAXBElement<RetrieveQuestionnaireRequestDTO> createRetrieveQuestionnaireRequestDTO(RetrieveQuestionnaireRequestDTO value) {
        return new JAXBElement<RetrieveQuestionnaireRequestDTO>(_RetrieveQuestionnaireRequestDTO_QNAME, RetrieveQuestionnaireRequestDTO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrieveQuestionnaireResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RetrieveQuestionnaireResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "retrieveQuestionnaireResponse")
    public JAXBElement<RetrieveQuestionnaireResponse> createRetrieveQuestionnaireResponse(RetrieveQuestionnaireResponse value) {
        return new JAXBElement<RetrieveQuestionnaireResponse>(_RetrieveQuestionnaireResponse_QNAME, RetrieveQuestionnaireResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrieveQuestionnaireResponseDTO }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RetrieveQuestionnaireResponseDTO }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "retrieveQuestionnaireResponseDTO")
    public JAXBElement<RetrieveQuestionnaireResponseDTO> createRetrieveQuestionnaireResponseDTO(RetrieveQuestionnaireResponseDTO value) {
        return new JAXBElement<RetrieveQuestionnaireResponseDTO>(_RetrieveQuestionnaireResponseDTO_QNAME, RetrieveQuestionnaireResponseDTO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrieveScript }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RetrieveScript }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "retrieveScript")
    public JAXBElement<RetrieveScript> createRetrieveScript(RetrieveScript value) {
        return new JAXBElement<RetrieveScript>(_RetrieveScript_QNAME, RetrieveScript.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrieveScriptRequestDTO }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RetrieveScriptRequestDTO }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "retrieveScriptRequestDTO")
    public JAXBElement<RetrieveScriptRequestDTO> createRetrieveScriptRequestDTO(RetrieveScriptRequestDTO value) {
        return new JAXBElement<RetrieveScriptRequestDTO>(_RetrieveScriptRequestDTO_QNAME, RetrieveScriptRequestDTO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrieveScriptResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RetrieveScriptResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "retrieveScriptResponse")
    public JAXBElement<RetrieveScriptResponse> createRetrieveScriptResponse(RetrieveScriptResponse value) {
        return new JAXBElement<RetrieveScriptResponse>(_RetrieveScriptResponse_QNAME, RetrieveScriptResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrieveScriptResponseDTO }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RetrieveScriptResponseDTO }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "retrieveScriptResponseDTO")
    public JAXBElement<RetrieveScriptResponseDTO> createRetrieveScriptResponseDTO(RetrieveScriptResponseDTO value) {
        return new JAXBElement<RetrieveScriptResponseDTO>(_RetrieveScriptResponseDTO_QNAME, RetrieveScriptResponseDTO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrieveSuggestedEvents }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RetrieveSuggestedEvents }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "retrieveSuggestedEvents")
    public JAXBElement<RetrieveSuggestedEvents> createRetrieveSuggestedEvents(RetrieveSuggestedEvents value) {
        return new JAXBElement<RetrieveSuggestedEvents>(_RetrieveSuggestedEvents_QNAME, RetrieveSuggestedEvents.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrieveSuggestedEventsResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RetrieveSuggestedEventsResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "retrieveSuggestedEventsResponse")
    public JAXBElement<RetrieveSuggestedEventsResponse> createRetrieveSuggestedEventsResponse(RetrieveSuggestedEventsResponse value) {
        return new JAXBElement<RetrieveSuggestedEventsResponse>(_RetrieveSuggestedEventsResponse_QNAME, RetrieveSuggestedEventsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveGoal }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SaveGoal }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "saveGoal")
    public JAXBElement<SaveGoal> createSaveGoal(SaveGoal value) {
        return new JAXBElement<SaveGoal>(_SaveGoal_QNAME, SaveGoal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveGoalResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SaveGoalResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "saveGoalResponse")
    public JAXBElement<SaveGoalResponse> createSaveGoalResponse(SaveGoalResponse value) {
        return new JAXBElement<SaveGoalResponse>(_SaveGoalResponse_QNAME, SaveGoalResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveGoalsRequestDTO }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SaveGoalsRequestDTO }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "saveGoalsRequestDTO")
    public JAXBElement<SaveGoalsRequestDTO> createSaveGoalsRequestDTO(SaveGoalsRequestDTO value) {
        return new JAXBElement<SaveGoalsRequestDTO>(_SaveGoalsRequestDTO_QNAME, SaveGoalsRequestDTO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveGoalsResponseDTO }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SaveGoalsResponseDTO }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "saveGoalsResponseDTO")
    public JAXBElement<SaveGoalsResponseDTO> createSaveGoalsResponseDTO(SaveGoalsResponseDTO value) {
        return new JAXBElement<SaveGoalsResponseDTO>(_SaveGoalsResponseDTO_QNAME, SaveGoalsResponseDTO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmitScript }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SubmitScript }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "submitScript")
    public JAXBElement<SubmitScript> createSubmitScript(SubmitScript value) {
        return new JAXBElement<SubmitScript>(_SubmitScript_QNAME, SubmitScript.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmitScriptRequestDTO }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SubmitScriptRequestDTO }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "submitScriptRequestDTO")
    public JAXBElement<SubmitScriptRequestDTO> createSubmitScriptRequestDTO(SubmitScriptRequestDTO value) {
        return new JAXBElement<SubmitScriptRequestDTO>(_SubmitScriptRequestDTO_QNAME, SubmitScriptRequestDTO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmitScriptResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SubmitScriptResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "submitScriptResponse")
    public JAXBElement<SubmitScriptResponse> createSubmitScriptResponse(SubmitScriptResponse value) {
        return new JAXBElement<SubmitScriptResponse>(_SubmitScriptResponse_QNAME, SubmitScriptResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmitScriptResponseDTO }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SubmitScriptResponseDTO }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "submitScriptResponseDTO")
    public JAXBElement<SubmitScriptResponseDTO> createSubmitScriptResponseDTO(SubmitScriptResponseDTO value) {
        return new JAXBElement<SubmitScriptResponseDTO>(_SubmitScriptResponseDTO_QNAME, SubmitScriptResponseDTO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WellnessRetrieveEvents }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link WellnessRetrieveEvents }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "wellnessRetrieveEvents")
    public JAXBElement<WellnessRetrieveEvents> createWellnessRetrieveEvents(WellnessRetrieveEvents value) {
        return new JAXBElement<WellnessRetrieveEvents>(_WellnessRetrieveEvents_QNAME, WellnessRetrieveEvents.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WellnessRetrieveEventsResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link WellnessRetrieveEventsResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.wellness.fnblife.za.co/", name = "wellnessRetrieveEventsResponse")
    public JAXBElement<WellnessRetrieveEventsResponse> createWellnessRetrieveEventsResponse(WellnessRetrieveEventsResponse value) {
        return new JAXBElement<WellnessRetrieveEventsResponse>(_WellnessRetrieveEventsResponse_QNAME, WellnessRetrieveEventsResponse.class, null, value);
    }

}
