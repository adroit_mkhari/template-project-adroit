
package co.za.fnblife.wellness.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for retrieveDashboardResponseDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="retrieveDashboardResponseDTO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="responseHeader" type="{http://service.wellness.fnblife.za.co/}retrieveDashboardResponseHeader" minOccurs="0"/&gt;
 *         &lt;element name="dataAreaRs" type="{http://service.wellness.fnblife.za.co/}retrieveDashboardDataAreaRs" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "retrieveDashboardResponseDTO", propOrder = {
    "responseHeader",
    "dataAreaRs"
})
public class RetrieveDashboardResponseDTO {

    protected RetrieveDashboardResponseHeader responseHeader;
    protected RetrieveDashboardDataAreaRs dataAreaRs;

    /**
     * Gets the value of the responseHeader property.
     * 
     * @return
     *     possible object is
     *     {@link RetrieveDashboardResponseHeader }
     *     
     */
    public RetrieveDashboardResponseHeader getResponseHeader() {
        return responseHeader;
    }

    /**
     * Sets the value of the responseHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link RetrieveDashboardResponseHeader }
     *     
     */
    public void setResponseHeader(RetrieveDashboardResponseHeader value) {
        this.responseHeader = value;
    }

    /**
     * Gets the value of the dataAreaRs property.
     * 
     * @return
     *     possible object is
     *     {@link RetrieveDashboardDataAreaRs }
     *     
     */
    public RetrieveDashboardDataAreaRs getDataAreaRs() {
        return dataAreaRs;
    }

    /**
     * Sets the value of the dataAreaRs property.
     * 
     * @param value
     *     allowed object is
     *     {@link RetrieveDashboardDataAreaRs }
     *     
     */
    public void setDataAreaRs(RetrieveDashboardDataAreaRs value) {
        this.dataAreaRs = value;
    }

}
