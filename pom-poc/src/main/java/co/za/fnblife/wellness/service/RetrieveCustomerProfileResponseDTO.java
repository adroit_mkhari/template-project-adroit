
package co.za.fnblife.wellness.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for retrieveCustomerProfileResponseDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="retrieveCustomerProfileResponseDTO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="dataAreaRs" type="{http://service.wellness.fnblife.za.co/}retrieveCustomerProfileDataAreaRs" minOccurs="0"/&gt;
 *         &lt;element name="responseHeader" type="{http://service.wellness.fnblife.za.co/}retrieveCustomerProfileResponseHeader" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "retrieveCustomerProfileResponseDTO", propOrder = {
    "dataAreaRs",
    "responseHeader"
})
public class RetrieveCustomerProfileResponseDTO {

    protected RetrieveCustomerProfileDataAreaRs dataAreaRs;
    protected RetrieveCustomerProfileResponseHeader responseHeader;

    /**
     * Gets the value of the dataAreaRs property.
     * 
     * @return
     *     possible object is
     *     {@link RetrieveCustomerProfileDataAreaRs }
     *     
     */
    public RetrieveCustomerProfileDataAreaRs getDataAreaRs() {
        return dataAreaRs;
    }

    /**
     * Sets the value of the dataAreaRs property.
     * 
     * @param value
     *     allowed object is
     *     {@link RetrieveCustomerProfileDataAreaRs }
     *     
     */
    public void setDataAreaRs(RetrieveCustomerProfileDataAreaRs value) {
        this.dataAreaRs = value;
    }

    /**
     * Gets the value of the responseHeader property.
     * 
     * @return
     *     possible object is
     *     {@link RetrieveCustomerProfileResponseHeader }
     *     
     */
    public RetrieveCustomerProfileResponseHeader getResponseHeader() {
        return responseHeader;
    }

    /**
     * Sets the value of the responseHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link RetrieveCustomerProfileResponseHeader }
     *     
     */
    public void setResponseHeader(RetrieveCustomerProfileResponseHeader value) {
        this.responseHeader = value;
    }

}
