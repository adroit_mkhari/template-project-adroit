
package co.za.fnblife.wellness.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for deleteEventRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="deleteEventRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="myEvents" type="{http://service.wellness.fnblife.za.co/}myEvents" minOccurs="0"/&gt;
 *         &lt;element name="sessionKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "deleteEventRequestType", propOrder = {
    "myEvents",
    "sessionKey"
})
public class DeleteEventRequestType {

    protected MyEvents myEvents;
    protected String sessionKey;

    /**
     * Gets the value of the myEvents property.
     * 
     * @return
     *     possible object is
     *     {@link MyEvents }
     *     
     */
    public MyEvents getMyEvents() {
        return myEvents;
    }

    /**
     * Sets the value of the myEvents property.
     * 
     * @param value
     *     allowed object is
     *     {@link MyEvents }
     *     
     */
    public void setMyEvents(MyEvents value) {
        this.myEvents = value;
    }

    /**
     * Gets the value of the sessionKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSessionKey() {
        return sessionKey;
    }

    /**
     * Sets the value of the sessionKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSessionKey(String value) {
        this.sessionKey = value;
    }

}
