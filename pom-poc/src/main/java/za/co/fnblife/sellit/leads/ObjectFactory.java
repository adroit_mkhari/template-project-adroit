
package za.co.fnblife.sellit.leads;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the za.co.fnblife.sellit.leads package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: za.co.fnblife.sellit.leads
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Message }
     * 
     */
    public Message createMessage() {
        return new Message();
    }

    /**
     * Create an instance of {@link HeaderType }
     * 
     */
    public HeaderType createHeaderType() {
        return new HeaderType();
    }

    /**
     * Create an instance of {@link LeadRequestType }
     * 
     */
    public LeadRequestType createLeadRequestType() {
        return new LeadRequestType();
    }

    /**
     * Create an instance of {@link LeadResponseType }
     * 
     */
    public LeadResponseType createLeadResponseType() {
        return new LeadResponseType();
    }

    /**
     * Create an instance of {@link LeadType }
     * 
     */
    public LeadType createLeadType() {
        return new LeadType();
    }

}
