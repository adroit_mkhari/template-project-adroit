
package za.co.fnblife.midlife.processsmsmessages;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import za.co.fnblife.midlife.smsmessage.SmsMessageType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ProcessSMSRequest" minOccurs="0" form="qualified"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="SmsMessage" type="{http://midlife.fnblife.co.za/SMSMessage}SmsMessageType" maxOccurs="unbounded" form="qualified"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="ProcessSMSResponse" minOccurs="0" form="qualified"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="errorCode" type="{http://www.w3.org/2001/XMLSchema}int" form="qualified"/&gt;
 *                   &lt;element name="errorMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *                   &lt;element name="errorDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *                   &lt;element name="SmsMessage" type="{http://midlife.fnblife.co.za/SMSMessage}SmsMessageType" maxOccurs="unbounded" minOccurs="0" form="qualified"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "processSMSRequest",
    "processSMSResponse"
})
@XmlRootElement(name = "ProcessSMSMessages")
public class ProcessSMSMessages {

    @XmlElement(name = "ProcessSMSRequest")
    protected ProcessSMSMessages.ProcessSMSRequest processSMSRequest;
    @XmlElement(name = "ProcessSMSResponse")
    protected ProcessSMSMessages.ProcessSMSResponse processSMSResponse;

    /**
     * Gets the value of the processSMSRequest property.
     * 
     * @return
     *     possible object is
     *     {@link ProcessSMSMessages.ProcessSMSRequest }
     *     
     */
    public ProcessSMSMessages.ProcessSMSRequest getProcessSMSRequest() {
        return processSMSRequest;
    }

    /**
     * Sets the value of the processSMSRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcessSMSMessages.ProcessSMSRequest }
     *     
     */
    public void setProcessSMSRequest(ProcessSMSMessages.ProcessSMSRequest value) {
        this.processSMSRequest = value;
    }

    /**
     * Gets the value of the processSMSResponse property.
     * 
     * @return
     *     possible object is
     *     {@link ProcessSMSMessages.ProcessSMSResponse }
     *     
     */
    public ProcessSMSMessages.ProcessSMSResponse getProcessSMSResponse() {
        return processSMSResponse;
    }

    /**
     * Sets the value of the processSMSResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcessSMSMessages.ProcessSMSResponse }
     *     
     */
    public void setProcessSMSResponse(ProcessSMSMessages.ProcessSMSResponse value) {
        this.processSMSResponse = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="SmsMessage" type="{http://midlife.fnblife.co.za/SMSMessage}SmsMessageType" maxOccurs="unbounded" form="qualified"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "smsMessage"
    })
    public static class ProcessSMSRequest {

        @XmlElement(name = "SmsMessage", required = true)
        protected List<SmsMessageType> smsMessage;

        /**
         * Gets the value of the smsMessage property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the smsMessage property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getSmsMessage().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link SmsMessageType }
         * 
         * 
         */
        public List<SmsMessageType> getSmsMessage() {
            if (smsMessage == null) {
                smsMessage = new ArrayList<SmsMessageType>();
            }
            return this.smsMessage;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="errorCode" type="{http://www.w3.org/2001/XMLSchema}int" form="qualified"/&gt;
     *         &lt;element name="errorMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
     *         &lt;element name="errorDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
     *         &lt;element name="SmsMessage" type="{http://midlife.fnblife.co.za/SMSMessage}SmsMessageType" maxOccurs="unbounded" minOccurs="0" form="qualified"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "errorCode",
        "errorMessage",
        "errorDescription",
        "smsMessage"
    })
    public static class ProcessSMSResponse {

        protected int errorCode;
        protected String errorMessage;
        protected String errorDescription;
        @XmlElement(name = "SmsMessage")
        protected List<SmsMessageType> smsMessage;

        /**
         * Gets the value of the errorCode property.
         * 
         */
        public int getErrorCode() {
            return errorCode;
        }

        /**
         * Sets the value of the errorCode property.
         * 
         */
        public void setErrorCode(int value) {
            this.errorCode = value;
        }

        /**
         * Gets the value of the errorMessage property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getErrorMessage() {
            return errorMessage;
        }

        /**
         * Sets the value of the errorMessage property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setErrorMessage(String value) {
            this.errorMessage = value;
        }

        /**
         * Gets the value of the errorDescription property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getErrorDescription() {
            return errorDescription;
        }

        /**
         * Sets the value of the errorDescription property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setErrorDescription(String value) {
            this.errorDescription = value;
        }

        /**
         * Gets the value of the smsMessage property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the smsMessage property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getSmsMessage().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link SmsMessageType }
         * 
         * 
         */
        public List<SmsMessageType> getSmsMessage() {
            if (smsMessage == null) {
                smsMessage = new ArrayList<SmsMessageType>();
            }
            return this.smsMessage;
        }

    }

}
