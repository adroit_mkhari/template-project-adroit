
package za.co.fnb.gts.cdm.client.v9;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Organisation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Organisation"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.fnb.co.za/gts/cdm/client/v9}AbstractParty"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="entityLegalName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="tradingName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="industrialClassification" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="industrialSector" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="depositTakingInstitutionClass" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="primaryBusiness" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="numberOfEmployees" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="countryOfOperation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="countryOfEstablishment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="headOfficeLocation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="taxYearEnd" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="beePercentage" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="beeDateAchieved" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="commercialTurnOver" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="projectedTurnOver" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="controlVerifiedIndicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="registrationNumberVerifiedIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="stateOwnedCompanyIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="vatRegistrationNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="companyRegistrationType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="businessRescueIndicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="businessRescueDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Organisation", propOrder = {
    "entityLegalName",
    "tradingName",
    "industrialClassification",
    "industrialSector",
    "depositTakingInstitutionClass",
    "primaryBusiness",
    "numberOfEmployees",
    "countryOfOperation",
    "countryOfEstablishment",
    "headOfficeLocation",
    "taxYearEnd",
    "beePercentage",
    "beeDateAchieved",
    "commercialTurnOver",
    "projectedTurnOver",
    "controlVerifiedIndicator",
    "registrationNumberVerifiedIndicator",
    "stateOwnedCompanyIndicator",
    "vatRegistrationNumber",
    "companyRegistrationType",
    "businessRescueIndicator",
    "businessRescueDate"
})
public class Organisation
    extends AbstractParty
{

    protected String entityLegalName;
    protected String tradingName;
    protected String industrialClassification;
    protected String industrialSector;
    protected String depositTakingInstitutionClass;
    protected String primaryBusiness;
    protected String numberOfEmployees;
    protected String countryOfOperation;
    protected String countryOfEstablishment;
    protected String headOfficeLocation;
    protected BigInteger taxYearEnd;
    protected BigInteger beePercentage;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar beeDateAchieved;
    @XmlElement(required = true)
    protected String commercialTurnOver;
    protected String projectedTurnOver;
    protected String controlVerifiedIndicator;
    protected Boolean registrationNumberVerifiedIndicator;
    protected Boolean stateOwnedCompanyIndicator;
    protected String vatRegistrationNumber;
    protected String companyRegistrationType;
    protected String businessRescueIndicator;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar businessRescueDate;

    /**
     * Gets the value of the entityLegalName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityLegalName() {
        return entityLegalName;
    }

    /**
     * Sets the value of the entityLegalName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityLegalName(String value) {
        this.entityLegalName = value;
    }

    /**
     * Gets the value of the tradingName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTradingName() {
        return tradingName;
    }

    /**
     * Sets the value of the tradingName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTradingName(String value) {
        this.tradingName = value;
    }

    /**
     * Gets the value of the industrialClassification property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndustrialClassification() {
        return industrialClassification;
    }

    /**
     * Sets the value of the industrialClassification property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndustrialClassification(String value) {
        this.industrialClassification = value;
    }

    /**
     * Gets the value of the industrialSector property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndustrialSector() {
        return industrialSector;
    }

    /**
     * Sets the value of the industrialSector property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndustrialSector(String value) {
        this.industrialSector = value;
    }

    /**
     * Gets the value of the depositTakingInstitutionClass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepositTakingInstitutionClass() {
        return depositTakingInstitutionClass;
    }

    /**
     * Sets the value of the depositTakingInstitutionClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepositTakingInstitutionClass(String value) {
        this.depositTakingInstitutionClass = value;
    }

    /**
     * Gets the value of the primaryBusiness property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryBusiness() {
        return primaryBusiness;
    }

    /**
     * Sets the value of the primaryBusiness property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryBusiness(String value) {
        this.primaryBusiness = value;
    }

    /**
     * Gets the value of the numberOfEmployees property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumberOfEmployees() {
        return numberOfEmployees;
    }

    /**
     * Sets the value of the numberOfEmployees property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumberOfEmployees(String value) {
        this.numberOfEmployees = value;
    }

    /**
     * Gets the value of the countryOfOperation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountryOfOperation() {
        return countryOfOperation;
    }

    /**
     * Sets the value of the countryOfOperation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountryOfOperation(String value) {
        this.countryOfOperation = value;
    }

    /**
     * Gets the value of the countryOfEstablishment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountryOfEstablishment() {
        return countryOfEstablishment;
    }

    /**
     * Sets the value of the countryOfEstablishment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountryOfEstablishment(String value) {
        this.countryOfEstablishment = value;
    }

    /**
     * Gets the value of the headOfficeLocation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHeadOfficeLocation() {
        return headOfficeLocation;
    }

    /**
     * Sets the value of the headOfficeLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHeadOfficeLocation(String value) {
        this.headOfficeLocation = value;
    }

    /**
     * Gets the value of the taxYearEnd property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getTaxYearEnd() {
        return taxYearEnd;
    }

    /**
     * Sets the value of the taxYearEnd property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setTaxYearEnd(BigInteger value) {
        this.taxYearEnd = value;
    }

    /**
     * Gets the value of the beePercentage property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getBeePercentage() {
        return beePercentage;
    }

    /**
     * Sets the value of the beePercentage property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setBeePercentage(BigInteger value) {
        this.beePercentage = value;
    }

    /**
     * Gets the value of the beeDateAchieved property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getBeeDateAchieved() {
        return beeDateAchieved;
    }

    /**
     * Sets the value of the beeDateAchieved property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setBeeDateAchieved(XMLGregorianCalendar value) {
        this.beeDateAchieved = value;
    }

    /**
     * Gets the value of the commercialTurnOver property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommercialTurnOver() {
        return commercialTurnOver;
    }

    /**
     * Sets the value of the commercialTurnOver property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommercialTurnOver(String value) {
        this.commercialTurnOver = value;
    }

    /**
     * Gets the value of the projectedTurnOver property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProjectedTurnOver() {
        return projectedTurnOver;
    }

    /**
     * Sets the value of the projectedTurnOver property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProjectedTurnOver(String value) {
        this.projectedTurnOver = value;
    }

    /**
     * Gets the value of the controlVerifiedIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getControlVerifiedIndicator() {
        return controlVerifiedIndicator;
    }

    /**
     * Sets the value of the controlVerifiedIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setControlVerifiedIndicator(String value) {
        this.controlVerifiedIndicator = value;
    }

    /**
     * Gets the value of the registrationNumberVerifiedIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRegistrationNumberVerifiedIndicator() {
        return registrationNumberVerifiedIndicator;
    }

    /**
     * Sets the value of the registrationNumberVerifiedIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRegistrationNumberVerifiedIndicator(Boolean value) {
        this.registrationNumberVerifiedIndicator = value;
    }

    /**
     * Gets the value of the stateOwnedCompanyIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isStateOwnedCompanyIndicator() {
        return stateOwnedCompanyIndicator;
    }

    /**
     * Sets the value of the stateOwnedCompanyIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setStateOwnedCompanyIndicator(Boolean value) {
        this.stateOwnedCompanyIndicator = value;
    }

    /**
     * Gets the value of the vatRegistrationNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVatRegistrationNumber() {
        return vatRegistrationNumber;
    }

    /**
     * Sets the value of the vatRegistrationNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVatRegistrationNumber(String value) {
        this.vatRegistrationNumber = value;
    }

    /**
     * Gets the value of the companyRegistrationType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompanyRegistrationType() {
        return companyRegistrationType;
    }

    /**
     * Sets the value of the companyRegistrationType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompanyRegistrationType(String value) {
        this.companyRegistrationType = value;
    }

    /**
     * Gets the value of the businessRescueIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusinessRescueIndicator() {
        return businessRescueIndicator;
    }

    /**
     * Sets the value of the businessRescueIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusinessRescueIndicator(String value) {
        this.businessRescueIndicator = value;
    }

    /**
     * Gets the value of the businessRescueDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getBusinessRescueDate() {
        return businessRescueDate;
    }

    /**
     * Sets the value of the businessRescueDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setBusinessRescueDate(XMLGregorianCalendar value) {
        this.businessRescueDate = value;
    }

}
