
package za.co.fnb.fnblife.alis.submitandretrieveuwrequest;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SubmitAndRetrieveUWRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SubmitAndRetrieveUWRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="UWApplicationID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="RequestNextQuestion" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="AnswerSet" type="{http://alis.fnblife.fnb.co.za/submitAndRetrieveUWRequest}AnswerSet" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubmitAndRetrieveUWRequestType", propOrder = {
    "uwApplicationID",
    "requestNextQuestion",
    "answerSet"
})
public class SubmitAndRetrieveUWRequestType {

    @XmlElement(name = "UWApplicationID", required = true)
    protected String uwApplicationID;
    @XmlElement(name = "RequestNextQuestion", required = true)
    protected BigInteger requestNextQuestion;
    @XmlElement(name = "AnswerSet")
    protected List<AnswerSet> answerSet;

    /**
     * Gets the value of the uwApplicationID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUWApplicationID() {
        return uwApplicationID;
    }

    /**
     * Sets the value of the uwApplicationID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUWApplicationID(String value) {
        this.uwApplicationID = value;
    }

    /**
     * Gets the value of the requestNextQuestion property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getRequestNextQuestion() {
        return requestNextQuestion;
    }

    /**
     * Sets the value of the requestNextQuestion property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setRequestNextQuestion(BigInteger value) {
        this.requestNextQuestion = value;
    }

    /**
     * Gets the value of the answerSet property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the answerSet property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAnswerSet().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AnswerSet }
     * 
     * 
     */
    public List<AnswerSet> getAnswerSet() {
        if (answerSet == null) {
            answerSet = new ArrayList<AnswerSet>();
        }
        return this.answerSet;
    }

}
