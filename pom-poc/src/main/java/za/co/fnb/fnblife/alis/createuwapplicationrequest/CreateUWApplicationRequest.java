
package za.co.fnb.fnblife.alis.createuwapplicationrequest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import za.co.fnb.fnblife.midlife.midlife_types.MidLifeHeaderType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Header" type="{http://midlife.fnblife.fnb.co.za/MidLife_Types}MidLifeHeaderType" minOccurs="0"/&gt;
 *         &lt;element name="DataArea" type="{http://alis.fnblife.fnb.co.za/createUWApplicationRequest}CreateUWApplicationRequestType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "header",
    "dataArea"
})
@XmlRootElement(name = "CreateUWApplicationRequest")
public class CreateUWApplicationRequest {

    @XmlElement(name = "Header")
    protected MidLifeHeaderType header;
    @XmlElement(name = "DataArea")
    protected CreateUWApplicationRequestType dataArea;

    /**
     * Gets the value of the header property.
     * 
     * @return
     *     possible object is
     *     {@link MidLifeHeaderType }
     *     
     */
    public MidLifeHeaderType getHeader() {
        return header;
    }

    /**
     * Sets the value of the header property.
     * 
     * @param value
     *     allowed object is
     *     {@link MidLifeHeaderType }
     *     
     */
    public void setHeader(MidLifeHeaderType value) {
        this.header = value;
    }

    /**
     * Gets the value of the dataArea property.
     * 
     * @return
     *     possible object is
     *     {@link CreateUWApplicationRequestType }
     *     
     */
    public CreateUWApplicationRequestType getDataArea() {
        return dataArea;
    }

    /**
     * Sets the value of the dataArea property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreateUWApplicationRequestType }
     *     
     */
    public void setDataArea(CreateUWApplicationRequestType value) {
        this.dataArea = value;
    }

}
