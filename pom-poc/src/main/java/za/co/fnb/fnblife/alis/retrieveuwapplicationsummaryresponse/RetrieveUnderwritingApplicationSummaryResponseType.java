
package za.co.fnb.fnblife.alis.retrieveuwapplicationsummaryresponse;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for RetrieveUnderwritingApplicationSummaryResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RetrieveUnderwritingApplicationSummaryResponseType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ExternalClientReference" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NationalInsuranceRef" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UWApplicationID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="UWResponsesEffectiveDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="ApplicationStatus" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="PolicyDetails" type="{http://alis.fnblife.fnb.co.za/retrieveUWApplicationSummaryResponse}PolicyDetails"/&gt;
 *         &lt;element name="ProductDetails" type="{http://alis.fnblife.fnb.co.za/retrieveUWApplicationSummaryResponse}ProductDetails"/&gt;
 *         &lt;element name="PersonalDetails" type="{http://alis.fnblife.fnb.co.za/retrieveUWApplicationSummaryResponse}PersonalDetails"/&gt;
 *         &lt;element name="AnswerSets" type="{http://alis.fnblife.fnb.co.za/retrieveUWApplicationSummaryResponse}AnswerSets" minOccurs="0"/&gt;
 *         &lt;element name="OutcomeDetails" type="{http://alis.fnblife.fnb.co.za/retrieveUWApplicationSummaryResponse}OutcomeDetails" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RetrieveUnderwritingApplicationSummaryResponseType", propOrder = {
    "externalClientReference",
    "nationalInsuranceRef",
    "uwApplicationID",
    "uwResponsesEffectiveDate",
    "applicationStatus",
    "policyDetails",
    "productDetails",
    "personalDetails",
    "answerSets",
    "outcomeDetails"
})
public class RetrieveUnderwritingApplicationSummaryResponseType {

    @XmlElement(name = "ExternalClientReference")
    protected String externalClientReference;
    @XmlElement(name = "NationalInsuranceRef")
    protected String nationalInsuranceRef;
    @XmlElement(name = "UWApplicationID", required = true)
    protected String uwApplicationID;
    @XmlElement(name = "UWResponsesEffectiveDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar uwResponsesEffectiveDate;
    @XmlElement(name = "ApplicationStatus", required = true)
    protected BigInteger applicationStatus;
    @XmlElement(name = "PolicyDetails", required = true)
    protected PolicyDetails policyDetails;
    @XmlElement(name = "ProductDetails", required = true)
    protected ProductDetails productDetails;
    @XmlElement(name = "PersonalDetails", required = true)
    protected PersonalDetails personalDetails;
    @XmlElement(name = "AnswerSets")
    protected AnswerSets answerSets;
    @XmlElement(name = "OutcomeDetails")
    protected OutcomeDetails outcomeDetails;

    /**
     * Gets the value of the externalClientReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalClientReference() {
        return externalClientReference;
    }

    /**
     * Sets the value of the externalClientReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalClientReference(String value) {
        this.externalClientReference = value;
    }

    /**
     * Gets the value of the nationalInsuranceRef property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNationalInsuranceRef() {
        return nationalInsuranceRef;
    }

    /**
     * Sets the value of the nationalInsuranceRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNationalInsuranceRef(String value) {
        this.nationalInsuranceRef = value;
    }

    /**
     * Gets the value of the uwApplicationID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUWApplicationID() {
        return uwApplicationID;
    }

    /**
     * Sets the value of the uwApplicationID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUWApplicationID(String value) {
        this.uwApplicationID = value;
    }

    /**
     * Gets the value of the uwResponsesEffectiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getUWResponsesEffectiveDate() {
        return uwResponsesEffectiveDate;
    }

    /**
     * Sets the value of the uwResponsesEffectiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setUWResponsesEffectiveDate(XMLGregorianCalendar value) {
        this.uwResponsesEffectiveDate = value;
    }

    /**
     * Gets the value of the applicationStatus property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getApplicationStatus() {
        return applicationStatus;
    }

    /**
     * Sets the value of the applicationStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setApplicationStatus(BigInteger value) {
        this.applicationStatus = value;
    }

    /**
     * Gets the value of the policyDetails property.
     * 
     * @return
     *     possible object is
     *     {@link PolicyDetails }
     *     
     */
    public PolicyDetails getPolicyDetails() {
        return policyDetails;
    }

    /**
     * Sets the value of the policyDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link PolicyDetails }
     *     
     */
    public void setPolicyDetails(PolicyDetails value) {
        this.policyDetails = value;
    }

    /**
     * Gets the value of the productDetails property.
     * 
     * @return
     *     possible object is
     *     {@link ProductDetails }
     *     
     */
    public ProductDetails getProductDetails() {
        return productDetails;
    }

    /**
     * Sets the value of the productDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductDetails }
     *     
     */
    public void setProductDetails(ProductDetails value) {
        this.productDetails = value;
    }

    /**
     * Gets the value of the personalDetails property.
     * 
     * @return
     *     possible object is
     *     {@link PersonalDetails }
     *     
     */
    public PersonalDetails getPersonalDetails() {
        return personalDetails;
    }

    /**
     * Sets the value of the personalDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonalDetails }
     *     
     */
    public void setPersonalDetails(PersonalDetails value) {
        this.personalDetails = value;
    }

    /**
     * Gets the value of the answerSets property.
     * 
     * @return
     *     possible object is
     *     {@link AnswerSets }
     *     
     */
    public AnswerSets getAnswerSets() {
        return answerSets;
    }

    /**
     * Sets the value of the answerSets property.
     * 
     * @param value
     *     allowed object is
     *     {@link AnswerSets }
     *     
     */
    public void setAnswerSets(AnswerSets value) {
        this.answerSets = value;
    }

    /**
     * Gets the value of the outcomeDetails property.
     * 
     * @return
     *     possible object is
     *     {@link OutcomeDetails }
     *     
     */
    public OutcomeDetails getOutcomeDetails() {
        return outcomeDetails;
    }

    /**
     * Sets the value of the outcomeDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link OutcomeDetails }
     *     
     */
    public void setOutcomeDetails(OutcomeDetails value) {
        this.outcomeDetails = value;
    }

}
