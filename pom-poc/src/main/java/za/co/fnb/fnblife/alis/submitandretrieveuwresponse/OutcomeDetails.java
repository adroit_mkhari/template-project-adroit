
package za.co.fnb.fnblife.alis.submitandretrieveuwresponse;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OutcomeDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OutcomeDetails"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PolicyOutcome" type="{http://alis.fnblife.fnb.co.za/submitAndRetrieveUWResponse}PolicyOutcome" minOccurs="0"/&gt;
 *         &lt;element name="CoverOutcomeDetails" type="{http://alis.fnblife.fnb.co.za/submitAndRetrieveUWResponse}CoverOutcomeDetails" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OutcomeDetails", propOrder = {
    "policyOutcome",
    "coverOutcomeDetails"
})
public class OutcomeDetails {

    @XmlElement(name = "PolicyOutcome")
    protected PolicyOutcome policyOutcome;
    @XmlElement(name = "CoverOutcomeDetails", required = true)
    protected List<CoverOutcomeDetails> coverOutcomeDetails;

    /**
     * Gets the value of the policyOutcome property.
     * 
     * @return
     *     possible object is
     *     {@link PolicyOutcome }
     *     
     */
    public PolicyOutcome getPolicyOutcome() {
        return policyOutcome;
    }

    /**
     * Sets the value of the policyOutcome property.
     * 
     * @param value
     *     allowed object is
     *     {@link PolicyOutcome }
     *     
     */
    public void setPolicyOutcome(PolicyOutcome value) {
        this.policyOutcome = value;
    }

    /**
     * Gets the value of the coverOutcomeDetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the coverOutcomeDetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCoverOutcomeDetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CoverOutcomeDetails }
     * 
     * 
     */
    public List<CoverOutcomeDetails> getCoverOutcomeDetails() {
        if (coverOutcomeDetails == null) {
            coverOutcomeDetails = new ArrayList<CoverOutcomeDetails>();
        }
        return this.coverOutcomeDetails;
    }

}
