
package za.co.fnb.fnblife.midlife;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for lookupMandateDetailsResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="lookupMandateDetailsResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://midlife.fnblife.fnb.co.za/MidLifeMessage}lookupMandateDetailsResponseType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "lookupMandateDetailsResponse", propOrder = {
    "lookupMandateDetailsResponseType"
})
public class LookupMandateDetailsResponse {

    @XmlElement(namespace = "http://midlife.fnblife.fnb.co.za/MidLifeMessage")
    protected LookupMandateDetailsResponseType lookupMandateDetailsResponseType;

    /**
     * Gets the value of the lookupMandateDetailsResponseType property.
     * 
     * @return
     *     possible object is
     *     {@link LookupMandateDetailsResponseType }
     *     
     */
    public LookupMandateDetailsResponseType getLookupMandateDetailsResponseType() {
        return lookupMandateDetailsResponseType;
    }

    /**
     * Sets the value of the lookupMandateDetailsResponseType property.
     * 
     * @param value
     *     allowed object is
     *     {@link LookupMandateDetailsResponseType }
     *     
     */
    public void setLookupMandateDetailsResponseType(LookupMandateDetailsResponseType value) {
        this.lookupMandateDetailsResponseType = value;
    }

}
