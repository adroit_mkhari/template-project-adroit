
package za.co.fnb.fnblife.midlife.midlifemessage;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for fraudCheckResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="fraudCheckResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="fraudDetails" type="{http://midlife.fnblife.fnb.co.za/MidLifeMessage}fraudDetails"/&gt;
 *         &lt;element name="matchedField" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="fraudIndicator" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "fraudCheckResponse", propOrder = {
    "fraudDetails",
    "matchedField",
    "fraudIndicator"
})
public class FraudCheckResponse {

    @XmlElement(required = true)
    protected FraudDetails fraudDetails;
    protected List<String> matchedField;
    @XmlElement(required = true)
    protected String fraudIndicator;

    /**
     * Gets the value of the fraudDetails property.
     * 
     * @return
     *     possible object is
     *     {@link FraudDetails }
     *     
     */
    public FraudDetails getFraudDetails() {
        return fraudDetails;
    }

    /**
     * Sets the value of the fraudDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link FraudDetails }
     *     
     */
    public void setFraudDetails(FraudDetails value) {
        this.fraudDetails = value;
    }

    /**
     * Gets the value of the matchedField property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the matchedField property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMatchedField().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getMatchedField() {
        if (matchedField == null) {
            matchedField = new ArrayList<String>();
        }
        return this.matchedField;
    }

    /**
     * Gets the value of the fraudIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFraudIndicator() {
        return fraudIndicator;
    }

    /**
     * Sets the value of the fraudIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFraudIndicator(String value) {
        this.fraudIndicator = value;
    }

}
