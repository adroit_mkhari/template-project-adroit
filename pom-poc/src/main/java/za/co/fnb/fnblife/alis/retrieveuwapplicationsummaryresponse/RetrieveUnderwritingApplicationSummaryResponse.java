
package za.co.fnb.fnblife.alis.retrieveuwapplicationsummaryresponse;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DataArea" type="{http://alis.fnblife.fnb.co.za/retrieveUWApplicationSummaryResponse}RetrieveUnderwritingApplicationSummaryResponseType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "dataArea"
})
@XmlRootElement(name = "RetrieveUnderwritingApplicationSummaryResponse")
public class RetrieveUnderwritingApplicationSummaryResponse {

    @XmlElement(name = "DataArea")
    protected RetrieveUnderwritingApplicationSummaryResponseType dataArea;

    /**
     * Gets the value of the dataArea property.
     * 
     * @return
     *     possible object is
     *     {@link RetrieveUnderwritingApplicationSummaryResponseType }
     *     
     */
    public RetrieveUnderwritingApplicationSummaryResponseType getDataArea() {
        return dataArea;
    }

    /**
     * Sets the value of the dataArea property.
     * 
     * @param value
     *     allowed object is
     *     {@link RetrieveUnderwritingApplicationSummaryResponseType }
     *     
     */
    public void setDataArea(RetrieveUnderwritingApplicationSummaryResponseType value) {
        this.dataArea = value;
    }

}
