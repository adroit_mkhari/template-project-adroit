
package za.co.fnb.fnblife.alis.submitandretrieveuwrequest;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the za.co.fnb.fnblife.alis.submitandretrieveuwrequest package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: za.co.fnb.fnblife.alis.submitandretrieveuwrequest
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SubmitAndRetrieveUWRequest }
     * 
     */
    public SubmitAndRetrieveUWRequest createSubmitAndRetrieveUWRequest() {
        return new SubmitAndRetrieveUWRequest();
    }

    /**
     * Create an instance of {@link SubmitAndRetrieveUWRequestType }
     * 
     */
    public SubmitAndRetrieveUWRequestType createSubmitAndRetrieveUWRequestType() {
        return new SubmitAndRetrieveUWRequestType();
    }

    /**
     * Create an instance of {@link Answer }
     * 
     */
    public Answer createAnswer() {
        return new Answer();
    }

    /**
     * Create an instance of {@link AnswerSet }
     * 
     */
    public AnswerSet createAnswerSet() {
        return new AnswerSet();
    }

}
