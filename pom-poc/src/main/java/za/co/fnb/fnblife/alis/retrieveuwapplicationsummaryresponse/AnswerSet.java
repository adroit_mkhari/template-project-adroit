
package za.co.fnb.fnblife.alis.retrieveuwapplicationsummaryresponse;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AnswerSet complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AnswerSet"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="QuestionID" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="Hierarchy" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="SequenceAnswered" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="QuestionText" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="AnswerType" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="Answers" type="{http://alis.fnblife.fnb.co.za/retrieveUWApplicationSummaryResponse}Answers" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AnswerSet", propOrder = {
    "questionID",
    "hierarchy",
    "sequenceAnswered",
    "questionText",
    "answerType",
    "answers"
})
public class AnswerSet {

    @XmlElement(name = "QuestionID", required = true)
    protected BigInteger questionID;
    @XmlElement(name = "Hierarchy")
    protected BigInteger hierarchy;
    @XmlElement(name = "SequenceAnswered", required = true)
    protected BigInteger sequenceAnswered;
    @XmlElement(name = "QuestionText", required = true)
    protected String questionText;
    @XmlElement(name = "AnswerType", required = true)
    protected BigInteger answerType;
    @XmlElement(name = "Answers")
    protected List<Answers> answers;

    /**
     * Gets the value of the questionID property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getQuestionID() {
        return questionID;
    }

    /**
     * Sets the value of the questionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setQuestionID(BigInteger value) {
        this.questionID = value;
    }

    /**
     * Gets the value of the hierarchy property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getHierarchy() {
        return hierarchy;
    }

    /**
     * Sets the value of the hierarchy property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setHierarchy(BigInteger value) {
        this.hierarchy = value;
    }

    /**
     * Gets the value of the sequenceAnswered property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSequenceAnswered() {
        return sequenceAnswered;
    }

    /**
     * Sets the value of the sequenceAnswered property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSequenceAnswered(BigInteger value) {
        this.sequenceAnswered = value;
    }

    /**
     * Gets the value of the questionText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuestionText() {
        return questionText;
    }

    /**
     * Sets the value of the questionText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuestionText(String value) {
        this.questionText = value;
    }

    /**
     * Gets the value of the answerType property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getAnswerType() {
        return answerType;
    }

    /**
     * Sets the value of the answerType property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setAnswerType(BigInteger value) {
        this.answerType = value;
    }

    /**
     * Gets the value of the answers property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the answers property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAnswers().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Answers }
     * 
     * 
     */
    public List<Answers> getAnswers() {
        if (answers == null) {
            answers = new ArrayList<Answers>();
        }
        return this.answers;
    }

}
