
package za.co.fnb.fnblife.alis.maintainuwapplicationstatusresponse;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DataArea" type="{http://alis.fnblife.fnb.co.za/maintainUWApplicationStatusResponse}MaintainUWApplicationStatusResponseType" minOccurs="0" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "dataArea"
})
@XmlRootElement(name = "MaintainUWApplicationStatusResponse")
public class MaintainUWApplicationStatusResponse {

    @XmlElement(name = "DataArea")
    protected MaintainUWApplicationStatusResponseType dataArea;

    /**
     * Gets the value of the dataArea property.
     * 
     * @return
     *     possible object is
     *     {@link MaintainUWApplicationStatusResponseType }
     *     
     */
    public MaintainUWApplicationStatusResponseType getDataArea() {
        return dataArea;
    }

    /**
     * Sets the value of the dataArea property.
     * 
     * @param value
     *     allowed object is
     *     {@link MaintainUWApplicationStatusResponseType }
     *     
     */
    public void setDataArea(MaintainUWApplicationStatusResponseType value) {
        this.dataArea = value;
    }

}
