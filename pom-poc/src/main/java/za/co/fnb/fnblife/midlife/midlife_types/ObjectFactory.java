
package za.co.fnb.fnblife.midlife.midlife_types;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the za.co.fnb.fnblife.midlife.midlife_types package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: za.co.fnb.fnblife.midlife.midlife_types
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link MidLifeResponseType }
     * 
     */
    public MidLifeResponseType createMidLifeResponseType() {
        return new MidLifeResponseType();
    }

    /**
     * Create an instance of {@link MidLifeHeaderType }
     * 
     */
    public MidLifeHeaderType createMidLifeHeaderType() {
        return new MidLifeHeaderType();
    }

    /**
     * Create an instance of {@link MidLifeRequestType }
     * 
     */
    public MidLifeRequestType createMidLifeRequestType() {
        return new MidLifeRequestType();
    }

    /**
     * Create an instance of {@link MidLifeResponseType.Payload }
     * 
     */
    public MidLifeResponseType.Payload createMidLifeResponseTypePayload() {
        return new MidLifeResponseType.Payload();
    }

}
