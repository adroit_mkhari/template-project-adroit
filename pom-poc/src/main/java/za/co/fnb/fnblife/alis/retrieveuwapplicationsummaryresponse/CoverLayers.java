
package za.co.fnb.fnblife.alis.retrieveuwapplicationsummaryresponse;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CoverLayers complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CoverLayers"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CoverLayerNo" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="ConsequenceID" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="LoadingDetails" type="{http://alis.fnblife.fnb.co.za/retrieveUWApplicationSummaryResponse}LoadingDetails" minOccurs="0"/&gt;
 *         &lt;element name="ExclusionDetail" type="{http://alis.fnblife.fnb.co.za/retrieveUWApplicationSummaryResponse}ExclusionDetail" minOccurs="0"/&gt;
 *         &lt;element name="MedicalEvidenceDetail" type="{http://alis.fnblife.fnb.co.za/retrieveUWApplicationSummaryResponse}MedicalEvidenceDetail" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CoverLayers", propOrder = {
    "coverLayerNo",
    "consequenceID",
    "loadingDetails",
    "exclusionDetail",
    "medicalEvidenceDetail"
})
public class CoverLayers {

    @XmlElement(name = "CoverLayerNo", required = true)
    protected BigInteger coverLayerNo;
    @XmlElement(name = "ConsequenceID", required = true)
    protected BigInteger consequenceID;
    @XmlElement(name = "LoadingDetails")
    protected LoadingDetails loadingDetails;
    @XmlElement(name = "ExclusionDetail")
    protected ExclusionDetail exclusionDetail;
    @XmlElement(name = "MedicalEvidenceDetail")
    protected MedicalEvidenceDetail medicalEvidenceDetail;

    /**
     * Gets the value of the coverLayerNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCoverLayerNo() {
        return coverLayerNo;
    }

    /**
     * Sets the value of the coverLayerNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCoverLayerNo(BigInteger value) {
        this.coverLayerNo = value;
    }

    /**
     * Gets the value of the consequenceID property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getConsequenceID() {
        return consequenceID;
    }

    /**
     * Sets the value of the consequenceID property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setConsequenceID(BigInteger value) {
        this.consequenceID = value;
    }

    /**
     * Gets the value of the loadingDetails property.
     * 
     * @return
     *     possible object is
     *     {@link LoadingDetails }
     *     
     */
    public LoadingDetails getLoadingDetails() {
        return loadingDetails;
    }

    /**
     * Sets the value of the loadingDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link LoadingDetails }
     *     
     */
    public void setLoadingDetails(LoadingDetails value) {
        this.loadingDetails = value;
    }

    /**
     * Gets the value of the exclusionDetail property.
     * 
     * @return
     *     possible object is
     *     {@link ExclusionDetail }
     *     
     */
    public ExclusionDetail getExclusionDetail() {
        return exclusionDetail;
    }

    /**
     * Sets the value of the exclusionDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExclusionDetail }
     *     
     */
    public void setExclusionDetail(ExclusionDetail value) {
        this.exclusionDetail = value;
    }

    /**
     * Gets the value of the medicalEvidenceDetail property.
     * 
     * @return
     *     possible object is
     *     {@link MedicalEvidenceDetail }
     *     
     */
    public MedicalEvidenceDetail getMedicalEvidenceDetail() {
        return medicalEvidenceDetail;
    }

    /**
     * Sets the value of the medicalEvidenceDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link MedicalEvidenceDetail }
     *     
     */
    public void setMedicalEvidenceDetail(MedicalEvidenceDetail value) {
        this.medicalEvidenceDetail = value;
    }

}
