
package za.co.fnb.fnblife.alis.createuwapplicationrequest;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InternalCovers complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InternalCovers"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="InternalCoverNo" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="CoverLayers" type="{http://alis.fnblife.fnb.co.za/createUWApplicationRequest}CoverLayers" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InternalCovers", propOrder = {
    "internalCoverNo",
    "coverLayers"
})
public class InternalCovers {

    @XmlElement(name = "InternalCoverNo", required = true)
    protected BigInteger internalCoverNo;
    @XmlElement(name = "CoverLayers", required = true)
    protected List<CoverLayers> coverLayers;

    /**
     * Gets the value of the internalCoverNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getInternalCoverNo() {
        return internalCoverNo;
    }

    /**
     * Sets the value of the internalCoverNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setInternalCoverNo(BigInteger value) {
        this.internalCoverNo = value;
    }

    /**
     * Gets the value of the coverLayers property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the coverLayers property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCoverLayers().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CoverLayers }
     * 
     * 
     */
    public List<CoverLayers> getCoverLayers() {
        if (coverLayers == null) {
            coverLayers = new ArrayList<CoverLayers>();
        }
        return this.coverLayers;
    }

}
