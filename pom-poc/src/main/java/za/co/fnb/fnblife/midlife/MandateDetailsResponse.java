
package za.co.fnb.fnblife.midlife;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for mandateDetailsResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="mandateDetailsResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://midlife.fnblife.fnb.co.za/MidLifeMessage}mandateRegistered"/&gt;
 *         &lt;element ref="{http://midlife.fnblife.fnb.co.za/MidLifeMessage}mandateReference" minOccurs="0"/&gt;
 *         &lt;element ref="{http://midlife.fnblife.fnb.co.za/MidLifeMessage}ucn"/&gt;
 *         &lt;element ref="{http://midlife.fnblife.fnb.co.za/MidLifeMessage}extrnalPolicyID"/&gt;
 *         &lt;element ref="{http://midlife.fnblife.fnb.co.za/MidLifeMessage}effectiveDate" minOccurs="0"/&gt;
 *         &lt;element ref="{http://midlife.fnblife.fnb.co.za/MidLifeMessage}sendToBankDate" minOccurs="0"/&gt;
 *         &lt;element ref="{http://midlife.fnblife.fnb.co.za/MidLifeMessage}collectionAmount"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "mandateDetailsResponse", propOrder = {
    "mandateRegistered",
    "mandateReference",
    "ucn",
    "extrnalPolicyID",
    "effectiveDate",
    "sendToBankDate",
    "collectionAmount"
})
public class MandateDetailsResponse {

    @XmlElement(namespace = "http://midlife.fnblife.fnb.co.za/MidLifeMessage")
    protected boolean mandateRegistered;
    @XmlElement(namespace = "http://midlife.fnblife.fnb.co.za/MidLifeMessage")
    protected String mandateReference;
    @XmlElement(namespace = "http://midlife.fnblife.fnb.co.za/MidLifeMessage", required = true)
    protected String ucn;
    @XmlElement(namespace = "http://midlife.fnblife.fnb.co.za/MidLifeMessage", required = true)
    protected String extrnalPolicyID;
    @XmlElement(namespace = "http://midlife.fnblife.fnb.co.za/MidLifeMessage")
    protected String effectiveDate;
    @XmlElement(namespace = "http://midlife.fnblife.fnb.co.za/MidLifeMessage")
    protected String sendToBankDate;
    @XmlElement(namespace = "http://midlife.fnblife.fnb.co.za/MidLifeMessage", required = true)
    protected String collectionAmount;

    /**
     * Gets the value of the mandateRegistered property.
     * 
     */
    public boolean isMandateRegistered() {
        return mandateRegistered;
    }

    /**
     * Sets the value of the mandateRegistered property.
     * 
     */
    public void setMandateRegistered(boolean value) {
        this.mandateRegistered = value;
    }

    /**
     * Gets the value of the mandateReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMandateReference() {
        return mandateReference;
    }

    /**
     * Sets the value of the mandateReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMandateReference(String value) {
        this.mandateReference = value;
    }

    /**
     * Gets the value of the ucn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUcn() {
        return ucn;
    }

    /**
     * Sets the value of the ucn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUcn(String value) {
        this.ucn = value;
    }

    /**
     * Gets the value of the extrnalPolicyID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtrnalPolicyID() {
        return extrnalPolicyID;
    }

    /**
     * Sets the value of the extrnalPolicyID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtrnalPolicyID(String value) {
        this.extrnalPolicyID = value;
    }

    /**
     * Gets the value of the effectiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * Sets the value of the effectiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEffectiveDate(String value) {
        this.effectiveDate = value;
    }

    /**
     * Gets the value of the sendToBankDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSendToBankDate() {
        return sendToBankDate;
    }

    /**
     * Sets the value of the sendToBankDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSendToBankDate(String value) {
        this.sendToBankDate = value;
    }

    /**
     * Gets the value of the collectionAmount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCollectionAmount() {
        return collectionAmount;
    }

    /**
     * Sets the value of the collectionAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCollectionAmount(String value) {
        this.collectionAmount = value;
    }

}
