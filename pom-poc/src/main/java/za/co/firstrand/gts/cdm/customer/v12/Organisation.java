
package za.co.firstrand.gts.cdm.customer.v12;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Organisation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Organisation"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="entityLegalName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="tradingName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="industrialClassification" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="industrialSector" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="depositTakingInstitutionClass" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="primaryBusiness" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="numberOfEmployees" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="countryOfOperation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="countryOfEstablishment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="headOfficeLocation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="taxYearEnd" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="beePercentage" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="beeDateAchieved" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="beeLevel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="beeAccreditor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="commercialTurnOver" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="projectedTurnOver" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="controlVerifiedIndicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="registrationNumberVerifiedIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="stateOwnedCompanyIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="vatRegistrationNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="companyRegistrationType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="businessRescueIndicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="businessRescueDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="creditHighRisk" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="creditHighRiskDte" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="distRestructure" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="distDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="busOperationDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="singleMemberEntity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Organisation", propOrder = {
    "entityLegalName",
    "tradingName",
    "industrialClassification",
    "industrialSector",
    "depositTakingInstitutionClass",
    "primaryBusiness",
    "numberOfEmployees",
    "countryOfOperation",
    "countryOfEstablishment",
    "headOfficeLocation",
    "taxYearEnd",
    "beePercentage",
    "beeDateAchieved",
    "beeLevel",
    "beeAccreditor",
    "commercialTurnOver",
    "projectedTurnOver",
    "controlVerifiedIndicator",
    "registrationNumberVerifiedIndicator",
    "stateOwnedCompanyIndicator",
    "vatRegistrationNumber",
    "companyRegistrationType",
    "businessRescueIndicator",
    "businessRescueDate",
    "creditHighRisk",
    "creditHighRiskDte",
    "distRestructure",
    "distDate",
    "busOperationDate",
    "singleMemberEntity"
})
public class Organisation {

    protected String entityLegalName;
    protected String tradingName;
    protected String industrialClassification;
    protected String industrialSector;
    protected String depositTakingInstitutionClass;
    protected String primaryBusiness;
    protected String numberOfEmployees;
    protected String countryOfOperation;
    protected String countryOfEstablishment;
    protected String headOfficeLocation;
    protected BigInteger taxYearEnd;
    protected BigInteger beePercentage;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar beeDateAchieved;
    protected String beeLevel;
    protected String beeAccreditor;
    protected String commercialTurnOver;
    protected String projectedTurnOver;
    protected String controlVerifiedIndicator;
    protected Boolean registrationNumberVerifiedIndicator;
    protected Boolean stateOwnedCompanyIndicator;
    protected String vatRegistrationNumber;
    protected String companyRegistrationType;
    protected String businessRescueIndicator;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar businessRescueDate;
    protected String creditHighRisk;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar creditHighRiskDte;
    protected String distRestructure;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar distDate;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar busOperationDate;
    protected String singleMemberEntity;

    /**
     * Gets the value of the entityLegalName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityLegalName() {
        return entityLegalName;
    }

    /**
     * Sets the value of the entityLegalName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityLegalName(String value) {
        this.entityLegalName = value;
    }

    /**
     * Gets the value of the tradingName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTradingName() {
        return tradingName;
    }

    /**
     * Sets the value of the tradingName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTradingName(String value) {
        this.tradingName = value;
    }

    /**
     * Gets the value of the industrialClassification property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndustrialClassification() {
        return industrialClassification;
    }

    /**
     * Sets the value of the industrialClassification property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndustrialClassification(String value) {
        this.industrialClassification = value;
    }

    /**
     * Gets the value of the industrialSector property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndustrialSector() {
        return industrialSector;
    }

    /**
     * Sets the value of the industrialSector property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndustrialSector(String value) {
        this.industrialSector = value;
    }

    /**
     * Gets the value of the depositTakingInstitutionClass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepositTakingInstitutionClass() {
        return depositTakingInstitutionClass;
    }

    /**
     * Sets the value of the depositTakingInstitutionClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepositTakingInstitutionClass(String value) {
        this.depositTakingInstitutionClass = value;
    }

    /**
     * Gets the value of the primaryBusiness property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryBusiness() {
        return primaryBusiness;
    }

    /**
     * Sets the value of the primaryBusiness property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryBusiness(String value) {
        this.primaryBusiness = value;
    }

    /**
     * Gets the value of the numberOfEmployees property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumberOfEmployees() {
        return numberOfEmployees;
    }

    /**
     * Sets the value of the numberOfEmployees property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumberOfEmployees(String value) {
        this.numberOfEmployees = value;
    }

    /**
     * Gets the value of the countryOfOperation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountryOfOperation() {
        return countryOfOperation;
    }

    /**
     * Sets the value of the countryOfOperation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountryOfOperation(String value) {
        this.countryOfOperation = value;
    }

    /**
     * Gets the value of the countryOfEstablishment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountryOfEstablishment() {
        return countryOfEstablishment;
    }

    /**
     * Sets the value of the countryOfEstablishment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountryOfEstablishment(String value) {
        this.countryOfEstablishment = value;
    }

    /**
     * Gets the value of the headOfficeLocation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHeadOfficeLocation() {
        return headOfficeLocation;
    }

    /**
     * Sets the value of the headOfficeLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHeadOfficeLocation(String value) {
        this.headOfficeLocation = value;
    }

    /**
     * Gets the value of the taxYearEnd property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getTaxYearEnd() {
        return taxYearEnd;
    }

    /**
     * Sets the value of the taxYearEnd property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setTaxYearEnd(BigInteger value) {
        this.taxYearEnd = value;
    }

    /**
     * Gets the value of the beePercentage property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getBeePercentage() {
        return beePercentage;
    }

    /**
     * Sets the value of the beePercentage property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setBeePercentage(BigInteger value) {
        this.beePercentage = value;
    }

    /**
     * Gets the value of the beeDateAchieved property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getBeeDateAchieved() {
        return beeDateAchieved;
    }

    /**
     * Sets the value of the beeDateAchieved property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setBeeDateAchieved(XMLGregorianCalendar value) {
        this.beeDateAchieved = value;
    }

    /**
     * Gets the value of the beeLevel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBeeLevel() {
        return beeLevel;
    }

    /**
     * Sets the value of the beeLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBeeLevel(String value) {
        this.beeLevel = value;
    }

    /**
     * Gets the value of the beeAccreditor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBeeAccreditor() {
        return beeAccreditor;
    }

    /**
     * Sets the value of the beeAccreditor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBeeAccreditor(String value) {
        this.beeAccreditor = value;
    }

    /**
     * Gets the value of the commercialTurnOver property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommercialTurnOver() {
        return commercialTurnOver;
    }

    /**
     * Sets the value of the commercialTurnOver property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommercialTurnOver(String value) {
        this.commercialTurnOver = value;
    }

    /**
     * Gets the value of the projectedTurnOver property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProjectedTurnOver() {
        return projectedTurnOver;
    }

    /**
     * Sets the value of the projectedTurnOver property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProjectedTurnOver(String value) {
        this.projectedTurnOver = value;
    }

    /**
     * Gets the value of the controlVerifiedIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getControlVerifiedIndicator() {
        return controlVerifiedIndicator;
    }

    /**
     * Sets the value of the controlVerifiedIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setControlVerifiedIndicator(String value) {
        this.controlVerifiedIndicator = value;
    }

    /**
     * Gets the value of the registrationNumberVerifiedIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRegistrationNumberVerifiedIndicator() {
        return registrationNumberVerifiedIndicator;
    }

    /**
     * Sets the value of the registrationNumberVerifiedIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRegistrationNumberVerifiedIndicator(Boolean value) {
        this.registrationNumberVerifiedIndicator = value;
    }

    /**
     * Gets the value of the stateOwnedCompanyIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isStateOwnedCompanyIndicator() {
        return stateOwnedCompanyIndicator;
    }

    /**
     * Sets the value of the stateOwnedCompanyIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setStateOwnedCompanyIndicator(Boolean value) {
        this.stateOwnedCompanyIndicator = value;
    }

    /**
     * Gets the value of the vatRegistrationNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVatRegistrationNumber() {
        return vatRegistrationNumber;
    }

    /**
     * Sets the value of the vatRegistrationNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVatRegistrationNumber(String value) {
        this.vatRegistrationNumber = value;
    }

    /**
     * Gets the value of the companyRegistrationType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompanyRegistrationType() {
        return companyRegistrationType;
    }

    /**
     * Sets the value of the companyRegistrationType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompanyRegistrationType(String value) {
        this.companyRegistrationType = value;
    }

    /**
     * Gets the value of the businessRescueIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusinessRescueIndicator() {
        return businessRescueIndicator;
    }

    /**
     * Sets the value of the businessRescueIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusinessRescueIndicator(String value) {
        this.businessRescueIndicator = value;
    }

    /**
     * Gets the value of the businessRescueDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getBusinessRescueDate() {
        return businessRescueDate;
    }

    /**
     * Sets the value of the businessRescueDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setBusinessRescueDate(XMLGregorianCalendar value) {
        this.businessRescueDate = value;
    }

    /**
     * Gets the value of the creditHighRisk property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreditHighRisk() {
        return creditHighRisk;
    }

    /**
     * Sets the value of the creditHighRisk property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreditHighRisk(String value) {
        this.creditHighRisk = value;
    }

    /**
     * Gets the value of the creditHighRiskDte property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCreditHighRiskDte() {
        return creditHighRiskDte;
    }

    /**
     * Sets the value of the creditHighRiskDte property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCreditHighRiskDte(XMLGregorianCalendar value) {
        this.creditHighRiskDte = value;
    }

    /**
     * Gets the value of the distRestructure property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDistRestructure() {
        return distRestructure;
    }

    /**
     * Sets the value of the distRestructure property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDistRestructure(String value) {
        this.distRestructure = value;
    }

    /**
     * Gets the value of the distDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDistDate() {
        return distDate;
    }

    /**
     * Sets the value of the distDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDistDate(XMLGregorianCalendar value) {
        this.distDate = value;
    }

    /**
     * Gets the value of the busOperationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getBusOperationDate() {
        return busOperationDate;
    }

    /**
     * Sets the value of the busOperationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setBusOperationDate(XMLGregorianCalendar value) {
        this.busOperationDate = value;
    }

    /**
     * Gets the value of the singleMemberEntity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSingleMemberEntity() {
        return singleMemberEntity;
    }

    /**
     * Sets the value of the singleMemberEntity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSingleMemberEntity(String value) {
        this.singleMemberEntity = value;
    }

}
