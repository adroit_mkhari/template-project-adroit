
package za.co.firstrand.gts.cussrv.customerservice.inquirecuculink.v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the za.co.firstrand.gts.cussrv.customerservice.inquirecuculink.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: za.co.firstrand.gts.cussrv.customerservice.inquirecuculink.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link InquireCUCULinkRequest }
     * 
     */
    public InquireCUCULinkRequest createInquireCUCULinkRequest() {
        return new InquireCUCULinkRequest();
    }

    /**
     * Create an instance of {@link InquireCUCULinkResponse }
     * 
     */
    public InquireCUCULinkResponse createInquireCUCULinkResponse() {
        return new InquireCUCULinkResponse();
    }

    /**
     * Create an instance of {@link InquireCUCULinkResponse.NextCUCUInfo }
     * 
     */
    public InquireCUCULinkResponse.NextCUCUInfo createInquireCUCULinkResponseNextCUCUInfo() {
        return new InquireCUCULinkResponse.NextCUCUInfo();
    }

    /**
     * Create an instance of {@link InquireCUCULinkResponse.NextCUCUInfo.NextCUCUKey }
     * 
     */
    public InquireCUCULinkResponse.NextCUCUInfo.NextCUCUKey createInquireCUCULinkResponseNextCUCUInfoNextCUCUKey() {
        return new InquireCUCULinkResponse.NextCUCUInfo.NextCUCUKey();
    }

    /**
     * Create an instance of {@link InquireCUCULinkResponse.NextCUCUInfo.NextCUCUCIFKey }
     * 
     */
    public InquireCUCULinkResponse.NextCUCUInfo.NextCUCUCIFKey createInquireCUCULinkResponseNextCUCUInfoNextCUCUCIFKey() {
        return new InquireCUCULinkResponse.NextCUCUInfo.NextCUCUCIFKey();
    }

    /**
     * Create an instance of {@link InquireCUCULinkRequest.NextCUCUInfo }
     * 
     */
    public InquireCUCULinkRequest.NextCUCUInfo createInquireCUCULinkRequestNextCUCUInfo() {
        return new InquireCUCULinkRequest.NextCUCUInfo();
    }

    /**
     * Create an instance of {@link InquireCUCULinkRequest.NextCUCUInfo.NextCUCUKey }
     * 
     */
    public InquireCUCULinkRequest.NextCUCUInfo.NextCUCUKey createInquireCUCULinkRequestNextCUCUInfoNextCUCUKey() {
        return new InquireCUCULinkRequest.NextCUCUInfo.NextCUCUKey();
    }

    /**
     * Create an instance of {@link InquireCUCULinkRequest.NextCUCUInfo.NextCIFKey }
     * 
     */
    public InquireCUCULinkRequest.NextCUCUInfo.NextCIFKey createInquireCUCULinkRequestNextCUCUInfoNextCIFKey() {
        return new InquireCUCULinkRequest.NextCUCUInfo.NextCIFKey();
    }

    /**
     * Create an instance of {@link InquireCUCULinkRequest.CustomerKey }
     * 
     */
    public InquireCUCULinkRequest.CustomerKey createInquireCUCULinkRequestCustomerKey() {
        return new InquireCUCULinkRequest.CustomerKey();
    }

    /**
     * Create an instance of {@link InquireCUCULinkResponse.CustomerInfo }
     * 
     */
    public InquireCUCULinkResponse.CustomerInfo createInquireCUCULinkResponseCustomerInfo() {
        return new InquireCUCULinkResponse.CustomerInfo();
    }

    /**
     * Create an instance of {@link InquireCUCULinkResponse.CUCURulesInfo }
     * 
     */
    public InquireCUCULinkResponse.CUCURulesInfo createInquireCUCULinkResponseCUCURulesInfo() {
        return new InquireCUCULinkResponse.CUCURulesInfo();
    }

    /**
     * Create an instance of {@link InquireCUCULinkResponse.CUCUInfo }
     * 
     */
    public InquireCUCULinkResponse.CUCUInfo createInquireCUCULinkResponseCUCUInfo() {
        return new InquireCUCULinkResponse.CUCUInfo();
    }

    /**
     * Create an instance of {@link InquireCUCULinkResponse.NextCUCUInfo.NextCUCUKey.NextCUCUCompanyID }
     * 
     */
    public InquireCUCULinkResponse.NextCUCUInfo.NextCUCUKey.NextCUCUCompanyID createInquireCUCULinkResponseNextCUCUInfoNextCUCUKeyNextCUCUCompanyID() {
        return new InquireCUCULinkResponse.NextCUCUInfo.NextCUCUKey.NextCUCUCompanyID();
    }

    /**
     * Create an instance of {@link InquireCUCULinkResponse.NextCUCUInfo.NextCUCUKey.NextCUCUTie }
     * 
     */
    public InquireCUCULinkResponse.NextCUCUInfo.NextCUCUKey.NextCUCUTie createInquireCUCULinkResponseNextCUCUInfoNextCUCUKeyNextCUCUTie() {
        return new InquireCUCULinkResponse.NextCUCUInfo.NextCUCUKey.NextCUCUTie();
    }

    /**
     * Create an instance of {@link InquireCUCULinkResponse.NextCUCUInfo.NextCUCUCIFKey.NextCUCUCIFCompanyID }
     * 
     */
    public InquireCUCULinkResponse.NextCUCUInfo.NextCUCUCIFKey.NextCUCUCIFCompanyID createInquireCUCULinkResponseNextCUCUInfoNextCUCUCIFKeyNextCUCUCIFCompanyID() {
        return new InquireCUCULinkResponse.NextCUCUInfo.NextCUCUCIFKey.NextCUCUCIFCompanyID();
    }

    /**
     * Create an instance of {@link InquireCUCULinkResponse.NextCUCUInfo.NextCUCUCIFKey.NextCUCUCIFTie }
     * 
     */
    public InquireCUCULinkResponse.NextCUCUInfo.NextCUCUCIFKey.NextCUCUCIFTie createInquireCUCULinkResponseNextCUCUInfoNextCUCUCIFKeyNextCUCUCIFTie() {
        return new InquireCUCULinkResponse.NextCUCUInfo.NextCUCUCIFKey.NextCUCUCIFTie();
    }

    /**
     * Create an instance of {@link InquireCUCULinkRequest.NextCUCUInfo.NextCUCUKey.NextCUCUCompanyID }
     * 
     */
    public InquireCUCULinkRequest.NextCUCUInfo.NextCUCUKey.NextCUCUCompanyID createInquireCUCULinkRequestNextCUCUInfoNextCUCUKeyNextCUCUCompanyID() {
        return new InquireCUCULinkRequest.NextCUCUInfo.NextCUCUKey.NextCUCUCompanyID();
    }

    /**
     * Create an instance of {@link InquireCUCULinkRequest.NextCUCUInfo.NextCUCUKey.NextCUCUTie }
     * 
     */
    public InquireCUCULinkRequest.NextCUCUInfo.NextCUCUKey.NextCUCUTie createInquireCUCULinkRequestNextCUCUInfoNextCUCUKeyNextCUCUTie() {
        return new InquireCUCULinkRequest.NextCUCUInfo.NextCUCUKey.NextCUCUTie();
    }

    /**
     * Create an instance of {@link InquireCUCULinkRequest.NextCUCUInfo.NextCIFKey.NextCIFCompanyID }
     * 
     */
    public InquireCUCULinkRequest.NextCUCUInfo.NextCIFKey.NextCIFCompanyID createInquireCUCULinkRequestNextCUCUInfoNextCIFKeyNextCIFCompanyID() {
        return new InquireCUCULinkRequest.NextCUCUInfo.NextCIFKey.NextCIFCompanyID();
    }

    /**
     * Create an instance of {@link InquireCUCULinkRequest.NextCUCUInfo.NextCIFKey.NextCIFtie }
     * 
     */
    public InquireCUCULinkRequest.NextCUCUInfo.NextCIFKey.NextCIFtie createInquireCUCULinkRequestNextCUCUInfoNextCIFKeyNextCIFtie() {
        return new InquireCUCULinkRequest.NextCUCUInfo.NextCIFKey.NextCIFtie();
    }

    /**
     * Create an instance of {@link InquireCUCULinkRequest.CustomerKey.CompanyID }
     * 
     */
    public InquireCUCULinkRequest.CustomerKey.CompanyID createInquireCUCULinkRequestCustomerKeyCompanyID() {
        return new InquireCUCULinkRequest.CustomerKey.CompanyID();
    }

}
