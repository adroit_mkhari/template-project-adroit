
package za.co.firstrand.gts.cdm.datatypes.v5;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the za.co.firstrand.gts.cdm.datatypes.v5 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: za.co.firstrand.gts.cdm.datatypes.v5
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Address }
     * 
     */
    public Address createAddress() {
        return new Address();
    }

    /**
     * Create an instance of {@link Error }
     * 
     */
    public Error createError() {
        return new Error();
    }

    /**
     * Create an instance of {@link Amount }
     * 
     */
    public Amount createAmount() {
        return new Amount();
    }

    /**
     * Create an instance of {@link EmailAddress }
     * 
     */
    public EmailAddress createEmailAddress() {
        return new EmailAddress();
    }

    /**
     * Create an instance of {@link TelephoneNumber }
     * 
     */
    public TelephoneNumber createTelephoneNumber() {
        return new TelephoneNumber();
    }

    /**
     * Create an instance of {@link Amount1V7 }
     * 
     */
    public Amount1V7 createAmount1V7() {
        return new Amount1V7();
    }

    /**
     * Create an instance of {@link Error1V7 }
     * 
     */
    public Error1V7 createError1V7() {
        return new Error1V7();
    }

    /**
     * Create an instance of {@link TelephoneNumber1V7 }
     * 
     */
    public TelephoneNumber1V7 createTelephoneNumber1V7() {
        return new TelephoneNumber1V7();
    }

    /**
     * Create an instance of {@link EmailAddress1V7 }
     * 
     */
    public EmailAddress1V7 createEmailAddress1V7() {
        return new EmailAddress1V7();
    }

    /**
     * Create an instance of {@link Address1V7 }
     * 
     */
    public Address1V7 createAddress1V7() {
        return new Address1V7();
    }

}
