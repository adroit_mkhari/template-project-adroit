
package za.co.firstrand.gts.cdm.customer.v12;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Genderv12.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="Genderv12"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="MALE"/&gt;
 *     &lt;enumeration value="FEMALE"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "Genderv12")
@XmlEnum
public enum Genderv12 {

    MALE,
    FEMALE;

    public String value() {
        return name();
    }

    public static Genderv12 fromValue(String v) {
        return valueOf(v);
    }

}
