
package za.co.firstrand.gts.cdm.customer.v12;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MaritalContractv12.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="MaritalContractv12"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="ANC_WITH_ACCRUAL"/&gt;
 *     &lt;enumeration value="ANC_WITHOUT_ACCRUAL"/&gt;
 *     &lt;enumeration value="COMMUNITY_OF_PROPERTY"/&gt;
 *     &lt;enumeration value="OUT_OF_COMMUNITY_OF_PROPERTY"/&gt;
 *     &lt;enumeration value="TRIBAL"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "MaritalContractv12")
@XmlEnum
public enum MaritalContractv12 {

    ANC_WITH_ACCRUAL,
    ANC_WITHOUT_ACCRUAL,
    COMMUNITY_OF_PROPERTY,
    OUT_OF_COMMUNITY_OF_PROPERTY,
    TRIBAL;

    public String value() {
        return name();
    }

    public static MaritalContractv12 fromValue(String v) {
        return valueOf(v);
    }

}
