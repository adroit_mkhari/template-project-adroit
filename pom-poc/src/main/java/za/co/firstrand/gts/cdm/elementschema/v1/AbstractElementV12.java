
package za.co.firstrand.gts.cdm.elementschema.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for abstractElementV1.2 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="abstractElementV1.2"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="elementItem" type="{http://www.firstrand.co.za/gts/cdm/elementSchema/v1}allElementsV1.2" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="tableItem" type="{http://www.firstrand.co.za/gts/cdm/elementSchema/v1}tableV1.2" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "abstractElementV1.2", propOrder = {
    "elementItem",
    "tableItem"
})
public class AbstractElementV12 {

    @XmlElement(nillable = true)
    protected List<AllElementsV12> elementItem;
    @XmlElement(nillable = true)
    protected List<TableV12> tableItem;

    /**
     * Gets the value of the elementItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the elementItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getElementItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AllElementsV12 }
     * 
     * 
     */
    public List<AllElementsV12> getElementItem() {
        if (elementItem == null) {
            elementItem = new ArrayList<AllElementsV12>();
        }
        return this.elementItem;
    }

    /**
     * Gets the value of the tableItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tableItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTableItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TableV12 }
     * 
     * 
     */
    public List<TableV12> getTableItem() {
        if (tableItem == null) {
            tableItem = new ArrayList<TableV12>();
        }
        return this.tableItem;
    }

}
