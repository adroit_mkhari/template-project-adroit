
package za.co.firstrand.gts.accsrv.accountservice.requestrealtimebalance.v1;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import za.co.firstrand.gts.cdm.account.v8.AccountBalance;


/**
 * <p>Java class for ILPBalancesResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ILPBalancesResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="productCode" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/&gt;
 *         &lt;element name="companyId" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/&gt;
 *         &lt;element name="accountNumber" type="{http://www.w3.org/2001/XMLSchema}integer" form="qualified"/&gt;
 *         &lt;element name="currentLoanBalance" type="{http://www.firstrand.co.za/gts/cdm/account/v8}AccountBalance" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="totalBondAmount" type="{http://www.firstrand.co.za/gts/cdm/account/v8}AccountBalance" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="payOffAmount" type="{http://www.firstrand.co.za/gts/cdm/account/v8}AccountBalance" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="loanProceedFirstAdvance" type="{http://www.firstrand.co.za/gts/cdm/account/v8}AccountBalance" minOccurs="0" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ILPBalancesResponse", propOrder = {
    "productCode",
    "companyId",
    "accountNumber",
    "currentLoanBalance",
    "totalBondAmount",
    "payOffAmount",
    "loanProceedFirstAdvance"
})
public class ILPBalancesResponse {

    @XmlElement(required = true)
    protected String productCode;
    @XmlElement(required = true)
    protected String companyId;
    @XmlElement(required = true)
    protected BigInteger accountNumber;
    protected AccountBalance currentLoanBalance;
    protected AccountBalance totalBondAmount;
    protected AccountBalance payOffAmount;
    protected AccountBalance loanProceedFirstAdvance;

    /**
     * Gets the value of the productCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductCode() {
        return productCode;
    }

    /**
     * Sets the value of the productCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductCode(String value) {
        this.productCode = value;
    }

    /**
     * Gets the value of the companyId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompanyId() {
        return companyId;
    }

    /**
     * Sets the value of the companyId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompanyId(String value) {
        this.companyId = value;
    }

    /**
     * Gets the value of the accountNumber property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getAccountNumber() {
        return accountNumber;
    }

    /**
     * Sets the value of the accountNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setAccountNumber(BigInteger value) {
        this.accountNumber = value;
    }

    /**
     * Gets the value of the currentLoanBalance property.
     * 
     * @return
     *     possible object is
     *     {@link AccountBalance }
     *     
     */
    public AccountBalance getCurrentLoanBalance() {
        return currentLoanBalance;
    }

    /**
     * Sets the value of the currentLoanBalance property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountBalance }
     *     
     */
    public void setCurrentLoanBalance(AccountBalance value) {
        this.currentLoanBalance = value;
    }

    /**
     * Gets the value of the totalBondAmount property.
     * 
     * @return
     *     possible object is
     *     {@link AccountBalance }
     *     
     */
    public AccountBalance getTotalBondAmount() {
        return totalBondAmount;
    }

    /**
     * Sets the value of the totalBondAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountBalance }
     *     
     */
    public void setTotalBondAmount(AccountBalance value) {
        this.totalBondAmount = value;
    }

    /**
     * Gets the value of the payOffAmount property.
     * 
     * @return
     *     possible object is
     *     {@link AccountBalance }
     *     
     */
    public AccountBalance getPayOffAmount() {
        return payOffAmount;
    }

    /**
     * Sets the value of the payOffAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountBalance }
     *     
     */
    public void setPayOffAmount(AccountBalance value) {
        this.payOffAmount = value;
    }

    /**
     * Gets the value of the loanProceedFirstAdvance property.
     * 
     * @return
     *     possible object is
     *     {@link AccountBalance }
     *     
     */
    public AccountBalance getLoanProceedFirstAdvance() {
        return loanProceedFirstAdvance;
    }

    /**
     * Sets the value of the loanProceedFirstAdvance property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountBalance }
     *     
     */
    public void setLoanProceedFirstAdvance(AccountBalance value) {
        this.loanProceedFirstAdvance = value;
    }

}
