
package za.co.firstrand.gts.shared.enterprisemessage.v2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import za.co.firstrand.gts.accsrv.accountservice.listaccounts.v3.ListAccountsRequest;
import za.co.firstrand.gts.accsrv.accountservice.listaccounts.v3.ListAccountsResponse;


/**
 * <p>Java class for EnterpriseMessageLA complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EnterpriseMessageLA"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="header" type="{http://www.firstrand.co.za/gts/shared/enterpriseMessage/v2}EnterpriseMessageHeaderLA"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnterpriseMessageLA", propOrder = {
    "header"
})
@XmlSeeAlso({
    ListAccountsResponse.class,
    ListAccountsRequest.class
})
public class EnterpriseMessageLA {

    @XmlElement(required = true)
    protected EnterpriseMessageHeaderLA header;

    /**
     * Gets the value of the header property.
     * 
     * @return
     *     possible object is
     *     {@link EnterpriseMessageHeaderLA }
     *     
     */
    public EnterpriseMessageHeaderLA getHeader() {
        return header;
    }

    /**
     * Sets the value of the header property.
     * 
     * @param value
     *     allowed object is
     *     {@link EnterpriseMessageHeaderLA }
     *     
     */
    public void setHeader(EnterpriseMessageHeaderLA value) {
        this.header = value;
    }

}
