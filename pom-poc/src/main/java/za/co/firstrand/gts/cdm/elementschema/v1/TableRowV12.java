
package za.co.firstrand.gts.cdm.elementschema.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tableRowV1.2 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tableRowV1.2"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="headerItem" type="{http://www.firstrand.co.za/gts/cdm/elementSchema/v1}tableHeaderV1.2" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="dataItem" type="{http://www.firstrand.co.za/gts/cdm/elementSchema/v1}allElementsV1.2" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tableRowV1.2", propOrder = {
    "headerItem",
    "dataItem"
})
public class TableRowV12 {

    @XmlElement(nillable = true)
    protected List<TableHeaderV12> headerItem;
    @XmlElement(nillable = true)
    protected List<AllElementsV12> dataItem;

    /**
     * Gets the value of the headerItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the headerItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getHeaderItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TableHeaderV12 }
     * 
     * 
     */
    public List<TableHeaderV12> getHeaderItem() {
        if (headerItem == null) {
            headerItem = new ArrayList<TableHeaderV12>();
        }
        return this.headerItem;
    }

    /**
     * Gets the value of the dataItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dataItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDataItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AllElementsV12 }
     * 
     * 
     */
    public List<AllElementsV12> getDataItem() {
        if (dataItem == null) {
            dataItem = new ArrayList<AllElementsV12>();
        }
        return this.dataItem;
    }

}
