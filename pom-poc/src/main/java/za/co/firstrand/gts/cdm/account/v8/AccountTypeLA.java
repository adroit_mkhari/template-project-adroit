
package za.co.firstrand.gts.cdm.account.v8;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AccountTypeLA.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AccountTypeLA"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="SAVINGS_ACCOUNT"/&gt;
 *     &lt;enumeration value="TRANSMISSION_ACCOUNT"/&gt;
 *     &lt;enumeration value="CURRENT_ACCOUNT"/&gt;
 *     &lt;enumeration value="SUBSCRIPTION_SHARE"/&gt;
 *     &lt;enumeration value="BOND_ACCOUNT"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "AccountTypeLA")
@XmlEnum
public enum AccountTypeLA {

    SAVINGS_ACCOUNT,
    TRANSMISSION_ACCOUNT,
    CURRENT_ACCOUNT,
    SUBSCRIPTION_SHARE,
    BOND_ACCOUNT;

    public String value() {
        return name();
    }

    public static AccountTypeLA fromValue(String v) {
        return valueOf(v);
    }

}
