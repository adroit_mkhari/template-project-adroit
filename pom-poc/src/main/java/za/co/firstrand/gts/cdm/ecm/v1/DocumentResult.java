
package za.co.firstrand.gts.cdm.ecm.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for documentResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="documentResult"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="generationReference" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="objectId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="signable" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="fillable" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="key" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="responseType" type="{http://www.firstrand.co.za/gts/cdm/ecm/v1}responseType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "documentResult", propOrder = {
    "generationReference",
    "objectId",
    "signable",
    "fillable",
    "key",
    "responseType"
})
public class DocumentResult {

    @XmlElement(required = true)
    protected String generationReference;
    @XmlElement(required = true)
    protected String objectId;
    @XmlElement(required = true, type = Boolean.class, nillable = true)
    protected Boolean signable;
    @XmlElement(required = true, type = Boolean.class, nillable = true)
    protected Boolean fillable;
    @XmlElement(required = true)
    protected String key;
    @XmlElement(required = true)
    protected ResponseType responseType;

    /**
     * Gets the value of the generationReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGenerationReference() {
        return generationReference;
    }

    /**
     * Sets the value of the generationReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGenerationReference(String value) {
        this.generationReference = value;
    }

    /**
     * Gets the value of the objectId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObjectId() {
        return objectId;
    }

    /**
     * Sets the value of the objectId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObjectId(String value) {
        this.objectId = value;
    }

    /**
     * Gets the value of the signable property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSignable() {
        return signable;
    }

    /**
     * Sets the value of the signable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSignable(Boolean value) {
        this.signable = value;
    }

    /**
     * Gets the value of the fillable property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isFillable() {
        return fillable;
    }

    /**
     * Sets the value of the fillable property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFillable(Boolean value) {
        this.fillable = value;
    }

    /**
     * Gets the value of the key property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKey() {
        return key;
    }

    /**
     * Sets the value of the key property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKey(String value) {
        this.key = value;
    }

    /**
     * Gets the value of the responseType property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseType }
     *     
     */
    public ResponseType getResponseType() {
        return responseType;
    }

    /**
     * Sets the value of the responseType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseType }
     *     
     */
    public void setResponseType(ResponseType value) {
        this.responseType = value;
    }

}
