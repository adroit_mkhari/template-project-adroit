
package za.co.firstrand.gts.cdm.ecm.v1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for emailSensitivity.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="emailSensitivity"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Personal"/&gt;
 *     &lt;enumeration value="Private"/&gt;
 *     &lt;enumeration value="Company-Confidential"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "emailSensitivity")
@XmlEnum
public enum EmailSensitivity {

    @XmlEnumValue("Personal")
    PERSONAL("Personal"),
    @XmlEnumValue("Private")
    PRIVATE("Private"),
    @XmlEnumValue("Company-Confidential")
    COMPANY_CONFIDENTIAL("Company-Confidential");
    private final String value;

    EmailSensitivity(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static EmailSensitivity fromValue(String v) {
        for (EmailSensitivity c: EmailSensitivity.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
