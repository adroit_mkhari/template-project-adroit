
package za.co.firstrand.gts.cdm.elementschema.v1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for mediaV1.2.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="mediaV1.2"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="png"/&gt;
 *     &lt;enumeration value="pdf"/&gt;
 *     &lt;enumeration value="jpg"/&gt;
 *     &lt;enumeration value="docx"/&gt;
 *     &lt;enumeration value="txt"/&gt;
 *     &lt;enumeration value="csv"/&gt;
 *     &lt;enumeration value="xlsx"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "mediaV1.2")
@XmlEnum
public enum MediaV12 {

    @XmlEnumValue("png")
    PNG("png"),
    @XmlEnumValue("pdf")
    PDF("pdf"),
    @XmlEnumValue("jpg")
    JPG("jpg"),
    @XmlEnumValue("docx")
    DOCX("docx"),
    @XmlEnumValue("txt")
    TXT("txt"),
    @XmlEnumValue("csv")
    CSV("csv"),
    @XmlEnumValue("xlsx")
    XLSX("xlsx");
    private final String value;

    MediaV12(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static MediaV12 fromValue(String v) {
        for (MediaV12 c: MediaV12 .values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
