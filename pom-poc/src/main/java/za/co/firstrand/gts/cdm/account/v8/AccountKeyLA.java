
package za.co.firstrand.gts.cdm.account.v8;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AccountKeyLA complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AccountKeyLA"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="accountId" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/&gt;
 *         &lt;element name="branchNumber" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="country" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="accountType" type="{http://www.firstrand.co.za/gts/cdm/account/v8}AccountTypeLA" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="accountKeyType" type="{http://www.firstrand.co.za/gts/cdm/account/v8}AccountKeyTypeLA" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccountKeyLA", propOrder = {
    "accountId",
    "branchNumber",
    "country",
    "accountType",
    "accountKeyType"
})
public class AccountKeyLA {

    @XmlElement(required = true)
    protected String accountId;
    protected Long branchNumber;
    protected String country;
    @XmlSchemaType(name = "string")
    protected AccountTypeLA accountType;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected AccountKeyTypeLA accountKeyType;

    /**
     * Gets the value of the accountId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountId() {
        return accountId;
    }

    /**
     * Sets the value of the accountId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountId(String value) {
        this.accountId = value;
    }

    /**
     * Gets the value of the branchNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getBranchNumber() {
        return branchNumber;
    }

    /**
     * Sets the value of the branchNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setBranchNumber(Long value) {
        this.branchNumber = value;
    }

    /**
     * Gets the value of the country property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountry() {
        return country;
    }

    /**
     * Sets the value of the country property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountry(String value) {
        this.country = value;
    }

    /**
     * Gets the value of the accountType property.
     * 
     * @return
     *     possible object is
     *     {@link AccountTypeLA }
     *     
     */
    public AccountTypeLA getAccountType() {
        return accountType;
    }

    /**
     * Sets the value of the accountType property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountTypeLA }
     *     
     */
    public void setAccountType(AccountTypeLA value) {
        this.accountType = value;
    }

    /**
     * Gets the value of the accountKeyType property.
     * 
     * @return
     *     possible object is
     *     {@link AccountKeyTypeLA }
     *     
     */
    public AccountKeyTypeLA getAccountKeyType() {
        return accountKeyType;
    }

    /**
     * Sets the value of the accountKeyType property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountKeyTypeLA }
     *     
     */
    public void setAccountKeyType(AccountKeyTypeLA value) {
        this.accountKeyType = value;
    }

}
