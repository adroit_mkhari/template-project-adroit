
package za.co.firstrand.gts.midlife.customiseservice.customise.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import za.co.firstrand.gts.cdm.elementschema.v1.ListOfElementsV12;
import za.co.firstrand.gts.cdm.midlife.v1.ParametersV12;


/**
 * <p>Java class for sectionV1.2 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="sectionV1.2"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="sectionHeading" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="sectionTitle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="attributes" type="{http://www.firstrand.co.za/gts/cdm/midlife/v1}ParametersV1.2" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="elementList" type="{http://www.firstrand.co.za/gts/cdm/elementSchema/v1}listOfElementsV1.2" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="sectionKey" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "sectionV1.2", propOrder = {
    "sectionHeading",
    "sectionTitle",
    "attributes",
    "elementList"
})
public class SectionV12 {

    protected String sectionHeading;
    protected String sectionTitle;
    protected ParametersV12 attributes;
    @XmlElement(required = true)
    protected ListOfElementsV12 elementList;
    @XmlAttribute(name = "sectionKey")
    protected String sectionKey;

    /**
     * Gets the value of the sectionHeading property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSectionHeading() {
        return sectionHeading;
    }

    /**
     * Sets the value of the sectionHeading property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSectionHeading(String value) {
        this.sectionHeading = value;
    }

    /**
     * Gets the value of the sectionTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSectionTitle() {
        return sectionTitle;
    }

    /**
     * Sets the value of the sectionTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSectionTitle(String value) {
        this.sectionTitle = value;
    }

    /**
     * Gets the value of the attributes property.
     * 
     * @return
     *     possible object is
     *     {@link ParametersV12 }
     *     
     */
    public ParametersV12 getAttributes() {
        return attributes;
    }

    /**
     * Sets the value of the attributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link ParametersV12 }
     *     
     */
    public void setAttributes(ParametersV12 value) {
        this.attributes = value;
    }

    /**
     * Gets the value of the elementList property.
     * 
     * @return
     *     possible object is
     *     {@link ListOfElementsV12 }
     *     
     */
    public ListOfElementsV12 getElementList() {
        return elementList;
    }

    /**
     * Sets the value of the elementList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ListOfElementsV12 }
     *     
     */
    public void setElementList(ListOfElementsV12 value) {
        this.elementList = value;
    }

    /**
     * Gets the value of the sectionKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSectionKey() {
        return sectionKey;
    }

    /**
     * Sets the value of the sectionKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSectionKey(String value) {
        this.sectionKey = value;
    }

}
