
package za.co.firstrand.gts.cdm.ecm.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for requestDocument complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="requestDocument"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="generateAndStore" type="{http://www.firstrand.co.za/gts/cdm/ecm/v1}generateAndStore"/&gt;
 *         &lt;element name="printAndPost" type="{http://www.firstrand.co.za/gts/cdm/ecm/v1}printAndPost" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="sms" type="{http://www.firstrand.co.za/gts/cdm/ecm/v1}Sms" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="email" type="{http://www.firstrand.co.za/gts/cdm/ecm/v1}Email" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "requestDocument", propOrder = {
    "generateAndStore",
    "printAndPost",
    "sms",
    "email"
})
public class RequestDocument {

    @XmlElement(required = true)
    protected GenerateAndStore generateAndStore;
    @XmlElement(nillable = true)
    protected List<PrintAndPost> printAndPost;
    @XmlElement(nillable = true)
    protected List<Sms> sms;
    @XmlElement(nillable = true)
    protected List<Email> email;

    /**
     * Gets the value of the generateAndStore property.
     * 
     * @return
     *     possible object is
     *     {@link GenerateAndStore }
     *     
     */
    public GenerateAndStore getGenerateAndStore() {
        return generateAndStore;
    }

    /**
     * Sets the value of the generateAndStore property.
     * 
     * @param value
     *     allowed object is
     *     {@link GenerateAndStore }
     *     
     */
    public void setGenerateAndStore(GenerateAndStore value) {
        this.generateAndStore = value;
    }

    /**
     * Gets the value of the printAndPost property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the printAndPost property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPrintAndPost().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PrintAndPost }
     * 
     * 
     */
    public List<PrintAndPost> getPrintAndPost() {
        if (printAndPost == null) {
            printAndPost = new ArrayList<PrintAndPost>();
        }
        return this.printAndPost;
    }

    /**
     * Gets the value of the sms property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sms property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSms().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Sms }
     * 
     * 
     */
    public List<Sms> getSms() {
        if (sms == null) {
            sms = new ArrayList<Sms>();
        }
        return this.sms;
    }

    /**
     * Gets the value of the email property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the email property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEmail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Email }
     * 
     * 
     */
    public List<Email> getEmail() {
        if (email == null) {
            email = new ArrayList<Email>();
        }
        return this.email;
    }

}
