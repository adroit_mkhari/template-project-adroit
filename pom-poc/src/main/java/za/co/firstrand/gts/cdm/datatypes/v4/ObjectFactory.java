
package za.co.firstrand.gts.cdm.datatypes.v4;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the za.co.firstrand.gts.cdm.datatypes.v4 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: za.co.firstrand.gts.cdm.datatypes.v4
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Amount }
     * 
     */
    public Amount createAmount() {
        return new Amount();
    }

    /**
     * Create an instance of {@link Address }
     * 
     */
    public Address createAddress() {
        return new Address();
    }

    /**
     * Create an instance of {@link AddressLA }
     * 
     */
    public AddressLA createAddressLA() {
        return new AddressLA();
    }

    /**
     * Create an instance of {@link ErrorLA }
     * 
     */
    public ErrorLA createErrorLA() {
        return new ErrorLA();
    }

    /**
     * Create an instance of {@link AmountLA }
     * 
     */
    public AmountLA createAmountLA() {
        return new AmountLA();
    }

    /**
     * Create an instance of {@link EmailAddressLA }
     * 
     */
    public EmailAddressLA createEmailAddressLA() {
        return new EmailAddressLA();
    }

    /**
     * Create an instance of {@link TelephoneNumberLA }
     * 
     */
    public TelephoneNumberLA createTelephoneNumberLA() {
        return new TelephoneNumberLA();
    }

}
