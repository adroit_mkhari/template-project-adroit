
package za.co.firstrand.gts.midlife.policymaintenanceservice.retrievepolicydetails.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the za.co.firstrand.gts.midlife.policymaintenanceservice.retrievepolicydetails.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _RetrievePolicyDetailsRequest_QNAME = new QName("http://www.firstrand.co.za/gts/midlife/PolicyMaintenanceService/RetrievePolicyDetails/v1", "RetrievePolicyDetailsRequest");
    private final static QName _RetrievePolicyDetailsResponse_QNAME = new QName("http://www.firstrand.co.za/gts/midlife/PolicyMaintenanceService/RetrievePolicyDetails/v1", "RetrievePolicyDetailsResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: za.co.firstrand.gts.midlife.policymaintenanceservice.retrievepolicydetails.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link RetrievePolicyDetailsRequest }
     * 
     */
    public RetrievePolicyDetailsRequest createRetrievePolicyDetailsRequest() {
        return new RetrievePolicyDetailsRequest();
    }

    /**
     * Create an instance of {@link RetrievePolicyDetailsResponse }
     * 
     */
    public RetrievePolicyDetailsResponse createRetrievePolicyDetailsResponse() {
        return new RetrievePolicyDetailsResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrievePolicyDetailsRequest }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RetrievePolicyDetailsRequest }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.firstrand.co.za/gts/midlife/PolicyMaintenanceService/RetrievePolicyDetails/v1", name = "RetrievePolicyDetailsRequest")
    public JAXBElement<RetrievePolicyDetailsRequest> createRetrievePolicyDetailsRequest(RetrievePolicyDetailsRequest value) {
        return new JAXBElement<RetrievePolicyDetailsRequest>(_RetrievePolicyDetailsRequest_QNAME, RetrievePolicyDetailsRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrievePolicyDetailsResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RetrievePolicyDetailsResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.firstrand.co.za/gts/midlife/PolicyMaintenanceService/RetrievePolicyDetails/v1", name = "RetrievePolicyDetailsResponse")
    public JAXBElement<RetrievePolicyDetailsResponse> createRetrievePolicyDetailsResponse(RetrievePolicyDetailsResponse value) {
        return new JAXBElement<RetrievePolicyDetailsResponse>(_RetrievePolicyDetailsResponse_QNAME, RetrievePolicyDetailsResponse.class, null, value);
    }

}
