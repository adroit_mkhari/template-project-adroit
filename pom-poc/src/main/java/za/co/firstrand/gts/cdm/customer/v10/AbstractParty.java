
package za.co.firstrand.gts.cdm.customer.v10;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import za.co.firstrand.gts.cdm.datatypes.v5.Address;


/**
 * <p>Java class for AbstractParty complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AbstractParty"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="postalAddress" type="{http://www.firstrand.co.za/gts/cdm/dataTypes/v5}Address" minOccurs="0"/&gt;
 *         &lt;element name="physicalAddress" type="{http://www.firstrand.co.za/gts/cdm/dataTypes/v5}Address" minOccurs="0"/&gt;
 *         &lt;element name="addressVerified" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="kycVerified" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="kycDateVerified" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="contactDetail" type="{http://www.firstrand.co.za/gts/cdm/customer/v10}ContactDetails" minOccurs="0"/&gt;
 *         &lt;element name="marketing" type="{http://www.firstrand.co.za/gts/cdm/customer/v10}Marketing" minOccurs="0"/&gt;
 *         &lt;element name="identification" type="{http://www.firstrand.co.za/gts/cdm/customer/v10}Identification" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="VIPIndicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="currentVSI" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="domicileBranch" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="officerCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="taxRefNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="operatorID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="idVerifiedIndicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="foreignTaxLiabilityIndicator" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="foreignTaxCityOfBirth" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="foreignTaxIDCounter" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="foreignTaxIDSegment" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="foreignTaxIDKey" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="foreignTaxIDType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="foreignTaxIDOccurrence" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *                             &lt;element name="foreignTaxIDEffectiveDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="foreignTaxIDData" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="foreignTaxIDIssuer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="foreignTaxIDExpiryDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *                             &lt;element name="foreignTaxIDDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="foreignTaxIDClassification" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="foreignTaxIDStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AbstractParty", propOrder = {
    "postalAddress",
    "physicalAddress",
    "addressVerified",
    "kycVerified",
    "kycDateVerified",
    "contactDetail",
    "marketing",
    "identification",
    "vipIndicator",
    "currentVSI",
    "domicileBranch",
    "officerCode",
    "taxRefNo",
    "operatorID",
    "idVerifiedIndicator",
    "foreignTaxLiabilityIndicator",
    "foreignTaxCityOfBirth",
    "foreignTaxIDCounter",
    "foreignTaxIDSegment"
})
@XmlSeeAlso({
    Person.class,
    Organisation.class
})
public abstract class AbstractParty {

    protected Address postalAddress;
    protected Address physicalAddress;
    protected boolean addressVerified;
    protected String kycVerified;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar kycDateVerified;
    protected ContactDetails contactDetail;
    protected Marketing marketing;
    @XmlElement(nillable = true)
    protected List<Identification> identification;
    @XmlElement(name = "VIPIndicator")
    protected String vipIndicator;
    protected String currentVSI;
    protected BigInteger domicileBranch;
    protected String officerCode;
    protected String taxRefNo;
    protected String operatorID;
    protected String idVerifiedIndicator;
    @XmlElement(required = true)
    protected String foreignTaxLiabilityIndicator;
    protected String foreignTaxCityOfBirth;
    @XmlElement(required = true)
    protected BigInteger foreignTaxIDCounter;
    @XmlElement(nillable = true)
    protected List<AbstractParty.ForeignTaxIDSegment> foreignTaxIDSegment;

    /**
     * Gets the value of the postalAddress property.
     * 
     * @return
     *     possible object is
     *     {@link Address }
     *     
     */
    public Address getPostalAddress() {
        return postalAddress;
    }

    /**
     * Sets the value of the postalAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link Address }
     *     
     */
    public void setPostalAddress(Address value) {
        this.postalAddress = value;
    }

    /**
     * Gets the value of the physicalAddress property.
     * 
     * @return
     *     possible object is
     *     {@link Address }
     *     
     */
    public Address getPhysicalAddress() {
        return physicalAddress;
    }

    /**
     * Sets the value of the physicalAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link Address }
     *     
     */
    public void setPhysicalAddress(Address value) {
        this.physicalAddress = value;
    }

    /**
     * Gets the value of the addressVerified property.
     * 
     */
    public boolean isAddressVerified() {
        return addressVerified;
    }

    /**
     * Sets the value of the addressVerified property.
     * 
     */
    public void setAddressVerified(boolean value) {
        this.addressVerified = value;
    }

    /**
     * Gets the value of the kycVerified property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKycVerified() {
        return kycVerified;
    }

    /**
     * Sets the value of the kycVerified property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKycVerified(String value) {
        this.kycVerified = value;
    }

    /**
     * Gets the value of the kycDateVerified property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getKycDateVerified() {
        return kycDateVerified;
    }

    /**
     * Sets the value of the kycDateVerified property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setKycDateVerified(XMLGregorianCalendar value) {
        this.kycDateVerified = value;
    }

    /**
     * Gets the value of the contactDetail property.
     * 
     * @return
     *     possible object is
     *     {@link ContactDetails }
     *     
     */
    public ContactDetails getContactDetail() {
        return contactDetail;
    }

    /**
     * Sets the value of the contactDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactDetails }
     *     
     */
    public void setContactDetail(ContactDetails value) {
        this.contactDetail = value;
    }

    /**
     * Gets the value of the marketing property.
     * 
     * @return
     *     possible object is
     *     {@link Marketing }
     *     
     */
    public Marketing getMarketing() {
        return marketing;
    }

    /**
     * Sets the value of the marketing property.
     * 
     * @param value
     *     allowed object is
     *     {@link Marketing }
     *     
     */
    public void setMarketing(Marketing value) {
        this.marketing = value;
    }

    /**
     * Gets the value of the identification property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the identification property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIdentification().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Identification }
     * 
     * 
     */
    public List<Identification> getIdentification() {
        if (identification == null) {
            identification = new ArrayList<Identification>();
        }
        return this.identification;
    }

    /**
     * Gets the value of the vipIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVIPIndicator() {
        return vipIndicator;
    }

    /**
     * Sets the value of the vipIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVIPIndicator(String value) {
        this.vipIndicator = value;
    }

    /**
     * Gets the value of the currentVSI property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrentVSI() {
        return currentVSI;
    }

    /**
     * Sets the value of the currentVSI property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrentVSI(String value) {
        this.currentVSI = value;
    }

    /**
     * Gets the value of the domicileBranch property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getDomicileBranch() {
        return domicileBranch;
    }

    /**
     * Sets the value of the domicileBranch property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setDomicileBranch(BigInteger value) {
        this.domicileBranch = value;
    }

    /**
     * Gets the value of the officerCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOfficerCode() {
        return officerCode;
    }

    /**
     * Sets the value of the officerCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOfficerCode(String value) {
        this.officerCode = value;
    }

    /**
     * Gets the value of the taxRefNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxRefNo() {
        return taxRefNo;
    }

    /**
     * Sets the value of the taxRefNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxRefNo(String value) {
        this.taxRefNo = value;
    }

    /**
     * Gets the value of the operatorID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperatorID() {
        return operatorID;
    }

    /**
     * Sets the value of the operatorID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperatorID(String value) {
        this.operatorID = value;
    }

    /**
     * Gets the value of the idVerifiedIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdVerifiedIndicator() {
        return idVerifiedIndicator;
    }

    /**
     * Sets the value of the idVerifiedIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdVerifiedIndicator(String value) {
        this.idVerifiedIndicator = value;
    }

    /**
     * Gets the value of the foreignTaxLiabilityIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getForeignTaxLiabilityIndicator() {
        return foreignTaxLiabilityIndicator;
    }

    /**
     * Sets the value of the foreignTaxLiabilityIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setForeignTaxLiabilityIndicator(String value) {
        this.foreignTaxLiabilityIndicator = value;
    }

    /**
     * Gets the value of the foreignTaxCityOfBirth property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getForeignTaxCityOfBirth() {
        return foreignTaxCityOfBirth;
    }

    /**
     * Sets the value of the foreignTaxCityOfBirth property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setForeignTaxCityOfBirth(String value) {
        this.foreignTaxCityOfBirth = value;
    }

    /**
     * Gets the value of the foreignTaxIDCounter property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getForeignTaxIDCounter() {
        return foreignTaxIDCounter;
    }

    /**
     * Sets the value of the foreignTaxIDCounter property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setForeignTaxIDCounter(BigInteger value) {
        this.foreignTaxIDCounter = value;
    }

    /**
     * Gets the value of the foreignTaxIDSegment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the foreignTaxIDSegment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getForeignTaxIDSegment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AbstractParty.ForeignTaxIDSegment }
     * 
     * 
     */
    public List<AbstractParty.ForeignTaxIDSegment> getForeignTaxIDSegment() {
        if (foreignTaxIDSegment == null) {
            foreignTaxIDSegment = new ArrayList<AbstractParty.ForeignTaxIDSegment>();
        }
        return this.foreignTaxIDSegment;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="foreignTaxIDKey" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="foreignTaxIDType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="foreignTaxIDOccurrence" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
     *                   &lt;element name="foreignTaxIDEffectiveDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="foreignTaxIDData" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="foreignTaxIDIssuer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="foreignTaxIDExpiryDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
     *                   &lt;element name="foreignTaxIDDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="foreignTaxIDClassification" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="foreignTaxIDStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "foreignTaxIDKey",
        "foreignTaxIDData"
    })
    public static class ForeignTaxIDSegment {

        protected AbstractParty.ForeignTaxIDSegment.ForeignTaxIDKey foreignTaxIDKey;
        protected AbstractParty.ForeignTaxIDSegment.ForeignTaxIDData foreignTaxIDData;

        /**
         * Gets the value of the foreignTaxIDKey property.
         * 
         * @return
         *     possible object is
         *     {@link AbstractParty.ForeignTaxIDSegment.ForeignTaxIDKey }
         *     
         */
        public AbstractParty.ForeignTaxIDSegment.ForeignTaxIDKey getForeignTaxIDKey() {
            return foreignTaxIDKey;
        }

        /**
         * Sets the value of the foreignTaxIDKey property.
         * 
         * @param value
         *     allowed object is
         *     {@link AbstractParty.ForeignTaxIDSegment.ForeignTaxIDKey }
         *     
         */
        public void setForeignTaxIDKey(AbstractParty.ForeignTaxIDSegment.ForeignTaxIDKey value) {
            this.foreignTaxIDKey = value;
        }

        /**
         * Gets the value of the foreignTaxIDData property.
         * 
         * @return
         *     possible object is
         *     {@link AbstractParty.ForeignTaxIDSegment.ForeignTaxIDData }
         *     
         */
        public AbstractParty.ForeignTaxIDSegment.ForeignTaxIDData getForeignTaxIDData() {
            return foreignTaxIDData;
        }

        /**
         * Sets the value of the foreignTaxIDData property.
         * 
         * @param value
         *     allowed object is
         *     {@link AbstractParty.ForeignTaxIDSegment.ForeignTaxIDData }
         *     
         */
        public void setForeignTaxIDData(AbstractParty.ForeignTaxIDSegment.ForeignTaxIDData value) {
            this.foreignTaxIDData = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="foreignTaxIDIssuer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="foreignTaxIDExpiryDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
         *         &lt;element name="foreignTaxIDDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="foreignTaxIDClassification" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="foreignTaxIDStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "foreignTaxIDIssuer",
            "foreignTaxIDExpiryDate",
            "foreignTaxIDDescription",
            "foreignTaxIDClassification",
            "foreignTaxIDStatus"
        })
        public static class ForeignTaxIDData {

            protected String foreignTaxIDIssuer;
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar foreignTaxIDExpiryDate;
            protected String foreignTaxIDDescription;
            protected String foreignTaxIDClassification;
            protected String foreignTaxIDStatus;

            /**
             * Gets the value of the foreignTaxIDIssuer property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getForeignTaxIDIssuer() {
                return foreignTaxIDIssuer;
            }

            /**
             * Sets the value of the foreignTaxIDIssuer property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setForeignTaxIDIssuer(String value) {
                this.foreignTaxIDIssuer = value;
            }

            /**
             * Gets the value of the foreignTaxIDExpiryDate property.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getForeignTaxIDExpiryDate() {
                return foreignTaxIDExpiryDate;
            }

            /**
             * Sets the value of the foreignTaxIDExpiryDate property.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setForeignTaxIDExpiryDate(XMLGregorianCalendar value) {
                this.foreignTaxIDExpiryDate = value;
            }

            /**
             * Gets the value of the foreignTaxIDDescription property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getForeignTaxIDDescription() {
                return foreignTaxIDDescription;
            }

            /**
             * Sets the value of the foreignTaxIDDescription property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setForeignTaxIDDescription(String value) {
                this.foreignTaxIDDescription = value;
            }

            /**
             * Gets the value of the foreignTaxIDClassification property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getForeignTaxIDClassification() {
                return foreignTaxIDClassification;
            }

            /**
             * Sets the value of the foreignTaxIDClassification property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setForeignTaxIDClassification(String value) {
                this.foreignTaxIDClassification = value;
            }

            /**
             * Gets the value of the foreignTaxIDStatus property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getForeignTaxIDStatus() {
                return foreignTaxIDStatus;
            }

            /**
             * Sets the value of the foreignTaxIDStatus property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setForeignTaxIDStatus(String value) {
                this.foreignTaxIDStatus = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="foreignTaxIDType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="foreignTaxIDOccurrence" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
         *         &lt;element name="foreignTaxIDEffectiveDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "foreignTaxIDType",
            "foreignTaxIDOccurrence",
            "foreignTaxIDEffectiveDate"
        })
        public static class ForeignTaxIDKey {

            protected String foreignTaxIDType;
            protected BigInteger foreignTaxIDOccurrence;
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar foreignTaxIDEffectiveDate;

            /**
             * Gets the value of the foreignTaxIDType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getForeignTaxIDType() {
                return foreignTaxIDType;
            }

            /**
             * Sets the value of the foreignTaxIDType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setForeignTaxIDType(String value) {
                this.foreignTaxIDType = value;
            }

            /**
             * Gets the value of the foreignTaxIDOccurrence property.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getForeignTaxIDOccurrence() {
                return foreignTaxIDOccurrence;
            }

            /**
             * Sets the value of the foreignTaxIDOccurrence property.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setForeignTaxIDOccurrence(BigInteger value) {
                this.foreignTaxIDOccurrence = value;
            }

            /**
             * Gets the value of the foreignTaxIDEffectiveDate property.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getForeignTaxIDEffectiveDate() {
                return foreignTaxIDEffectiveDate;
            }

            /**
             * Sets the value of the foreignTaxIDEffectiveDate property.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setForeignTaxIDEffectiveDate(XMLGregorianCalendar value) {
                this.foreignTaxIDEffectiveDate = value;
            }

        }

    }

}
