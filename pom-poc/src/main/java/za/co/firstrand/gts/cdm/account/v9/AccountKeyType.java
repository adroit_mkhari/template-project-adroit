
package za.co.firstrand.gts.cdm.account.v9;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AccountKeyType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AccountKeyType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="IBAN"/&gt;
 *     &lt;enumeration value="ACCOUNT_NUMBER"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "AccountKeyType")
@XmlEnum
public enum AccountKeyType {

    IBAN,
    ACCOUNT_NUMBER;

    public String value() {
        return name();
    }

    public static AccountKeyType fromValue(String v) {
        return valueOf(v);
    }

}
