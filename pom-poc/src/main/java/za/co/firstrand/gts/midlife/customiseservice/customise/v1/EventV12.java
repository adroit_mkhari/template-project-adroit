
package za.co.firstrand.gts.midlife.customiseservice.customise.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for eventV1.2 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="eventV1.2"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="type" type="{http://www.firstrand.co.za/gts/midlife/CustomiseService/Customise/v1}eventTypeV1.2" form="qualified"/&gt;
 *         &lt;element name="params" type="{http://www.firstrand.co.za/gts/midlife/CustomiseService/Customise/v1}eventparamsV1.2" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "eventV1.2", propOrder = {
    "type",
    "params"
})
public class EventV12 {

    @XmlElement(required = true)
    protected EventTypeV12 type;
    @XmlElement(required = true)
    protected EventparamsV12 params;

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link EventTypeV12 }
     *     
     */
    public EventTypeV12 getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link EventTypeV12 }
     *     
     */
    public void setType(EventTypeV12 value) {
        this.type = value;
    }

    /**
     * Gets the value of the params property.
     * 
     * @return
     *     possible object is
     *     {@link EventparamsV12 }
     *     
     */
    public EventparamsV12 getParams() {
        return params;
    }

    /**
     * Sets the value of the params property.
     * 
     * @param value
     *     allowed object is
     *     {@link EventparamsV12 }
     *     
     */
    public void setParams(EventparamsV12 value) {
        this.params = value;
    }

}
