
package za.co.firstrand.gts.accsrv.accountservice.listaccounts.v3;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import za.co.firstrand.gts.cdm.account.v8.AccountKeyLA;


/**
 * <p>Java class for ByAccount complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ByAccount"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="accountKey" type="{http://www.firstrand.co.za/gts/cdm/account/v8}AccountKeyLA" form="qualified"/&gt;
 *         &lt;element name="accountProductCode" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ByAccount", propOrder = {
    "accountKey",
    "accountProductCode"
})
public class ByAccount {

    @XmlElement(required = true)
    protected AccountKeyLA accountKey;
    @XmlElement(required = true)
    protected String accountProductCode;

    /**
     * Gets the value of the accountKey property.
     * 
     * @return
     *     possible object is
     *     {@link AccountKeyLA }
     *     
     */
    public AccountKeyLA getAccountKey() {
        return accountKey;
    }

    /**
     * Sets the value of the accountKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountKeyLA }
     *     
     */
    public void setAccountKey(AccountKeyLA value) {
        this.accountKey = value;
    }

    /**
     * Gets the value of the accountProductCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountProductCode() {
        return accountProductCode;
    }

    /**
     * Sets the value of the accountProductCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountProductCode(String value) {
        this.accountProductCode = value;
    }

}
