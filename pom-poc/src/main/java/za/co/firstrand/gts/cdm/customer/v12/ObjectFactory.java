
package za.co.firstrand.gts.cdm.customer.v12;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the za.co.firstrand.gts.cdm.customer.v12 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: za.co.firstrand.gts.cdm.customer.v12
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link za.co.firstrand.gts.cdm.customer.v12.AbstractParty.ForeignTaxIDSegment }
     * 
     */
    public za.co.firstrand.gts.cdm.customer.v12.AbstractParty.ForeignTaxIDSegment createAbstractPartyForeignTaxIDSegment() {
        return new za.co.firstrand.gts.cdm.customer.v12.AbstractParty.ForeignTaxIDSegment();
    }

    /**
     * Create an instance of {@link ForeignTaxv12 }
     * 
     */
    public ForeignTaxv12 createForeignTaxv12() {
        return new ForeignTaxv12();
    }

    /**
     * Create an instance of {@link ForeignTaxv12 .ForeignTaxIDSegment }
     * 
     */
    public ForeignTaxv12 .ForeignTaxIDSegment createForeignTaxv12ForeignTaxIDSegment() {
        return new ForeignTaxv12 .ForeignTaxIDSegment();
    }

    /**
     * Create an instance of {@link ForeignTax }
     * 
     */
    public ForeignTax createForeignTax() {
        return new ForeignTax();
    }

    /**
     * Create an instance of {@link ForeignTax.ForeignTaxIDSegment }
     * 
     */
    public ForeignTax.ForeignTaxIDSegment createForeignTaxForeignTaxIDSegment() {
        return new ForeignTax.ForeignTaxIDSegment();
    }

    /**
     * Create an instance of {@link Customer }
     * 
     */
    public Customer createCustomer() {
        return new Customer();
    }

    /**
     * Create an instance of {@link Customer2 }
     * 
     */
    public Customer2 createCustomer2() {
        return new Customer2();
    }

    /**
     * Create an instance of {@link Identification }
     * 
     */
    public Identification createIdentification() {
        return new Identification();
    }

    /**
     * Create an instance of {@link SourceOfFunds }
     * 
     */
    public SourceOfFunds createSourceOfFunds() {
        return new SourceOfFunds();
    }

    /**
     * Create an instance of {@link ContactDetails }
     * 
     */
    public ContactDetails createContactDetails() {
        return new ContactDetails();
    }

    /**
     * Create an instance of {@link Marketing }
     * 
     */
    public Marketing createMarketing() {
        return new Marketing();
    }

    /**
     * Create an instance of {@link za.co.firstrand.gts.cdm.customer.v12.Party }
     * 
     */
    public za.co.firstrand.gts.cdm.customer.v12.Party createParty() {
        return new za.co.firstrand.gts.cdm.customer.v12.Party();
    }

    /**
     * Create an instance of {@link Person }
     * 
     */
    public Person createPerson() {
        return new Person();
    }

    /**
     * Create an instance of {@link Organisation }
     * 
     */
    public Organisation createOrganisation() {
        return new Organisation();
    }

    /**
     * Create an instance of {@link Segment }
     * 
     */
    public Segment createSegment() {
        return new Segment();
    }

    /**
     * Create an instance of {@link CustomerKey }
     * 
     */
    public CustomerKey createCustomerKey() {
        return new CustomerKey();
    }

    /**
     * Create an instance of {@link SourceOfFundsv12 }
     * 
     */
    public SourceOfFundsv12 createSourceOfFundsv12() {
        return new SourceOfFundsv12();
    }

    /**
     * Create an instance of {@link ContactDetailsv12 }
     * 
     */
    public ContactDetailsv12 createContactDetailsv12() {
        return new ContactDetailsv12();
    }

    /**
     * Create an instance of {@link Marketingv12 }
     * 
     */
    public Marketingv12 createMarketingv12() {
        return new Marketingv12();
    }

    /**
     * Create an instance of {@link Identificationv12 }
     * 
     */
    public Identificationv12 createIdentificationv12() {
        return new Identificationv12();
    }

    /**
     * Create an instance of {@link FicaStatus }
     * 
     */
    public FicaStatus createFicaStatus() {
        return new FicaStatus();
    }

    /**
     * Create an instance of {@link Partyv12 }
     * 
     */
    public Partyv12 createPartyv12() {
        return new Partyv12();
    }

    /**
     * Create an instance of {@link Personv12 }
     * 
     */
    public Personv12 createPersonv12() {
        return new Personv12();
    }

    /**
     * Create an instance of {@link Organisationv12 }
     * 
     */
    public Organisationv12 createOrganisationv12() {
        return new Organisationv12();
    }

    /**
     * Create an instance of {@link Segmentv12 }
     * 
     */
    public Segmentv12 createSegmentv12() {
        return new Segmentv12();
    }

    /**
     * Create an instance of {@link za.co.firstrand.gts.cdm.customer.v12.AbstractParty.ForeignTaxIDSegment.ForeignTaxIDKey }
     * 
     */
    public za.co.firstrand.gts.cdm.customer.v12.AbstractParty.ForeignTaxIDSegment.ForeignTaxIDKey createAbstractPartyForeignTaxIDSegmentForeignTaxIDKey() {
        return new za.co.firstrand.gts.cdm.customer.v12.AbstractParty.ForeignTaxIDSegment.ForeignTaxIDKey();
    }

    /**
     * Create an instance of {@link za.co.firstrand.gts.cdm.customer.v12.AbstractParty.ForeignTaxIDSegment.ForeignTaxIDData }
     * 
     */
    public za.co.firstrand.gts.cdm.customer.v12.AbstractParty.ForeignTaxIDSegment.ForeignTaxIDData createAbstractPartyForeignTaxIDSegmentForeignTaxIDData() {
        return new za.co.firstrand.gts.cdm.customer.v12.AbstractParty.ForeignTaxIDSegment.ForeignTaxIDData();
    }

    /**
     * Create an instance of {@link ForeignTaxv12 .ForeignTaxIDSegment.ForeignTaxIDKey }
     * 
     */
    public ForeignTaxv12 .ForeignTaxIDSegment.ForeignTaxIDKey createForeignTaxv12ForeignTaxIDSegmentForeignTaxIDKey() {
        return new ForeignTaxv12 .ForeignTaxIDSegment.ForeignTaxIDKey();
    }

    /**
     * Create an instance of {@link ForeignTaxv12 .ForeignTaxIDSegment.ForeignTaxIDData }
     * 
     */
    public ForeignTaxv12 .ForeignTaxIDSegment.ForeignTaxIDData createForeignTaxv12ForeignTaxIDSegmentForeignTaxIDData() {
        return new ForeignTaxv12 .ForeignTaxIDSegment.ForeignTaxIDData();
    }

    /**
     * Create an instance of {@link ForeignTax.ForeignTaxIDSegment.ForeignTaxIDKey }
     * 
     */
    public ForeignTax.ForeignTaxIDSegment.ForeignTaxIDKey createForeignTaxForeignTaxIDSegmentForeignTaxIDKey() {
        return new ForeignTax.ForeignTaxIDSegment.ForeignTaxIDKey();
    }

    /**
     * Create an instance of {@link ForeignTax.ForeignTaxIDSegment.ForeignTaxIDData }
     * 
     */
    public ForeignTax.ForeignTaxIDSegment.ForeignTaxIDData createForeignTaxForeignTaxIDSegmentForeignTaxIDData() {
        return new ForeignTax.ForeignTaxIDSegment.ForeignTaxIDData();
    }

    /**
     * Create an instance of {@link Customer.Party }
     * 
     */
    public Customer.Party createCustomerParty() {
        return new Customer.Party();
    }

    /**
     * Create an instance of {@link Customer2 .Party }
     * 
     */
    public Customer2 .Party createCustomer2Party() {
        return new Customer2 .Party();
    }

}
