
package za.co.firstrand.gts.cdm.account.v8;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the za.co.firstrand.gts.cdm.account.v8 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AccountKeyLA_QNAME = new QName("http://www.firstrand.co.za/gts/cdm/account/v8", "AccountKeyLA");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: za.co.firstrand.gts.cdm.account.v8
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AccountKeyLA }
     * 
     */
    public AccountKeyLA createAccountKeyLA() {
        return new AccountKeyLA();
    }

    /**
     * Create an instance of {@link AccountKey }
     * 
     */
    public AccountKey createAccountKey() {
        return new AccountKey();
    }

    /**
     * Create an instance of {@link AccountBalance }
     * 
     */
    public AccountBalance createAccountBalance() {
        return new AccountBalance();
    }

    /**
     * Create an instance of {@link CisAccount }
     * 
     */
    public CisAccount createCisAccount() {
        return new CisAccount();
    }

    /**
     * Create an instance of {@link Account }
     * 
     */
    public Account createAccount() {
        return new Account();
    }

    /**
     * Create an instance of {@link Customer }
     * 
     */
    public Customer createCustomer() {
        return new Customer();
    }

    /**
     * Create an instance of {@link CustomerAccountRelationship }
     * 
     */
    public CustomerAccountRelationship createCustomerAccountRelationship() {
        return new CustomerAccountRelationship();
    }

    /**
     * Create an instance of {@link AccountLA }
     * 
     */
    public AccountLA createAccountLA() {
        return new AccountLA();
    }

    /**
     * Create an instance of {@link CustomerLA }
     * 
     */
    public CustomerLA createCustomerLA() {
        return new CustomerLA();
    }

    /**
     * Create an instance of {@link CustomerAccountRelationshipLA }
     * 
     */
    public CustomerAccountRelationshipLA createCustomerAccountRelationshipLA() {
        return new CustomerAccountRelationshipLA();
    }

    /**
     * Create an instance of {@link AccountBalanceLA }
     * 
     */
    public AccountBalanceLA createAccountBalanceLA() {
        return new AccountBalanceLA();
    }

    /**
     * Create an instance of {@link CisAccountLA }
     * 
     */
    public CisAccountLA createCisAccountLA() {
        return new CisAccountLA();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AccountKeyLA }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link AccountKeyLA }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.firstrand.co.za/gts/cdm/account/v8", name = "AccountKeyLA")
    public JAXBElement<AccountKeyLA> createAccountKeyLA(AccountKeyLA value) {
        return new JAXBElement<AccountKeyLA>(_AccountKeyLA_QNAME, AccountKeyLA.class, null, value);
    }

}
