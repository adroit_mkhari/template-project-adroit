
package za.co.firstrand.gts.midlife.customiseservice.customise.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for listOfSectionsV1.2 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="listOfSectionsV1.2"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="aSection" type="{http://www.firstrand.co.za/gts/midlife/CustomiseService/Customise/v1}sectionV1.2" maxOccurs="unbounded" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "listOfSectionsV1.2", propOrder = {
    "aSection"
})
public class ListOfSectionsV12 {

    @XmlElement(required = true)
    protected List<SectionV12> aSection;

    /**
     * Gets the value of the aSection property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the aSection property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getASection().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SectionV12 }
     * 
     * 
     */
    public List<SectionV12> getASection() {
        if (aSection == null) {
            aSection = new ArrayList<SectionV12>();
        }
        return this.aSection;
    }

}
