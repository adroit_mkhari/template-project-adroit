
package za.co.firstrand.gts.midlife.policymaintenanceservice.retrievepolicydetails.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import za.co.firstrand.gts.cdm.policyservicing.v1.BankDetails;
import za.co.firstrand.gts.cdm.policyservicing.v1.Beneficiary;
import za.co.firstrand.gts.cdm.policyservicing.v1.CoverDetails;
import za.co.firstrand.gts.cdm.policyservicing.v1.PayorDetails;
import za.co.firstrand.gts.cdm.policyservicing.v1.PolicyDetails;
import za.co.firstrand.gts.cdm.policyservicing.v1.Role;
import za.co.firstrand.gts.shared.enterprisemessage.v2.EnterpriseMessage;


/**
 * <p>Java class for RetrievePolicyDetailsResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RetrievePolicyDetailsResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.firstrand.co.za/gts/shared/enterpriseMessage/v2}EnterpriseMessage"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="message" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/&gt;
 *         &lt;element name="policyDetails" type="{http://www.firstrand.co.za/gts/cdm/policyservicing/v1}PolicyDetails" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="bankDetails" type="{http://www.firstrand.co.za/gts/cdm/policyservicing/v1}BankDetails" maxOccurs="unbounded" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="payorDetails" type="{http://www.firstrand.co.za/gts/cdm/policyservicing/v1}PayorDetails" maxOccurs="unbounded" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="beneficiaries" type="{http://www.firstrand.co.za/gts/cdm/policyservicing/v1}Beneficiary" maxOccurs="unbounded" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="roles" type="{http://www.firstrand.co.za/gts/cdm/policyservicing/v1}Role" maxOccurs="unbounded" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="coverDetails" type="{http://www.firstrand.co.za/gts/cdm/policyservicing/v1}CoverDetails" maxOccurs="unbounded" minOccurs="0" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RetrievePolicyDetailsResponse", propOrder = {
    "message",
    "policyDetails",
    "bankDetails",
    "payorDetails",
    "beneficiaries",
    "roles",
    "coverDetails"
})
public class RetrievePolicyDetailsResponse
    extends EnterpriseMessage
{

    @XmlElement(required = true)
    protected String message;
    protected PolicyDetails policyDetails;
    @XmlElement(nillable = true)
    protected List<BankDetails> bankDetails;
    @XmlElement(nillable = true)
    protected List<PayorDetails> payorDetails;
    @XmlElement(nillable = true)
    protected List<Beneficiary> beneficiaries;
    @XmlElement(nillable = true)
    protected List<Role> roles;
    @XmlElement(nillable = true)
    protected List<CoverDetails> coverDetails;

    /**
     * Gets the value of the message property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets the value of the message property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessage(String value) {
        this.message = value;
    }

    /**
     * Gets the value of the policyDetails property.
     * 
     * @return
     *     possible object is
     *     {@link PolicyDetails }
     *     
     */
    public PolicyDetails getPolicyDetails() {
        return policyDetails;
    }

    /**
     * Sets the value of the policyDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link PolicyDetails }
     *     
     */
    public void setPolicyDetails(PolicyDetails value) {
        this.policyDetails = value;
    }

    /**
     * Gets the value of the bankDetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the bankDetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBankDetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BankDetails }
     * 
     * 
     */
    public List<BankDetails> getBankDetails() {
        if (bankDetails == null) {
            bankDetails = new ArrayList<BankDetails>();
        }
        return this.bankDetails;
    }

    /**
     * Gets the value of the payorDetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the payorDetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPayorDetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PayorDetails }
     * 
     * 
     */
    public List<PayorDetails> getPayorDetails() {
        if (payorDetails == null) {
            payorDetails = new ArrayList<PayorDetails>();
        }
        return this.payorDetails;
    }

    /**
     * Gets the value of the beneficiaries property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the beneficiaries property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBeneficiaries().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Beneficiary }
     * 
     * 
     */
    public List<Beneficiary> getBeneficiaries() {
        if (beneficiaries == null) {
            beneficiaries = new ArrayList<Beneficiary>();
        }
        return this.beneficiaries;
    }

    /**
     * Gets the value of the roles property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the roles property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRoles().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Role }
     * 
     * 
     */
    public List<Role> getRoles() {
        if (roles == null) {
            roles = new ArrayList<Role>();
        }
        return this.roles;
    }

    /**
     * Gets the value of the coverDetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the coverDetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCoverDetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CoverDetails }
     * 
     * 
     */
    public List<CoverDetails> getCoverDetails() {
        if (coverDetails == null) {
            coverDetails = new ArrayList<CoverDetails>();
        }
        return this.coverDetails;
    }

}
