
package za.co.firstrand.gts.ecm.generationservice.generatecontent.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the za.co.firstrand.gts.ecm.generationservice.generatecontent.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GenerateContentRequest_QNAME = new QName("http://www.firstrand.co.za/gts/ecm/GenerationService/GenerateContent/v1", "generateContentRequest");
    private final static QName _GenerateContentResponse_QNAME = new QName("http://www.firstrand.co.za/gts/ecm/GenerationService/GenerateContent/v1", "generateContentResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: za.co.firstrand.gts.ecm.generationservice.generatecontent.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GenerateContentRequest }
     * 
     */
    public GenerateContentRequest createGenerateContentRequest() {
        return new GenerateContentRequest();
    }

    /**
     * Create an instance of {@link GenerateContentResponse }
     * 
     */
    public GenerateContentResponse createGenerateContentResponse() {
        return new GenerateContentResponse();
    }

    /**
     * Create an instance of {@link TemplateXmlList }
     * 
     */
    public TemplateXmlList createTemplateXmlList() {
        return new TemplateXmlList();
    }

    /**
     * Create an instance of {@link TemplateXmlType }
     * 
     */
    public TemplateXmlType createTemplateXmlType() {
        return new TemplateXmlType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GenerateContentRequest }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GenerateContentRequest }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.firstrand.co.za/gts/ecm/GenerationService/GenerateContent/v1", name = "generateContentRequest")
    public JAXBElement<GenerateContentRequest> createGenerateContentRequest(GenerateContentRequest value) {
        return new JAXBElement<GenerateContentRequest>(_GenerateContentRequest_QNAME, GenerateContentRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GenerateContentResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GenerateContentResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.firstrand.co.za/gts/ecm/GenerationService/GenerateContent/v1", name = "generateContentResponse")
    public JAXBElement<GenerateContentResponse> createGenerateContentResponse(GenerateContentResponse value) {
        return new JAXBElement<GenerateContentResponse>(_GenerateContentResponse_QNAME, GenerateContentResponse.class, null, value);
    }

}
