
package za.co.firstrand.gts.midlife.customiseservice.customise.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import za.co.firstrand.gts.cdm.midlife.v1.ParametersV12;
import za.co.firstrand.gts.shared.enterprisemessage.v2.EnterpriseMessageV12;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.firstrand.co.za/gts/shared/enterpriseMessage/v2}EnterpriseMessageV1.2"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="platformHeader" type="{http://www.firstrand.co.za/gts/midlife/CustomiseService/Customise/v1}headerV1.2" form="qualified"/&gt;
 *         &lt;element name="requestParameters" type="{http://www.firstrand.co.za/gts/cdm/midlife/v1}ParametersV1.2" maxOccurs="unbounded" form="qualified"/&gt;
 *         &lt;element name="linkAction" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "platformHeader",
    "requestParameters",
    "linkAction"
})
@XmlRootElement(name = "CustomiseRequest")
public class CustomiseRequest
    extends EnterpriseMessageV12
{

    @XmlElement(required = true)
    protected HeaderV12 platformHeader;
    @XmlElement(required = true)
    protected List<ParametersV12> requestParameters;
    protected String linkAction;

    /**
     * Gets the value of the platformHeader property.
     * 
     * @return
     *     possible object is
     *     {@link HeaderV12 }
     *     
     */
    public HeaderV12 getPlatformHeader() {
        return platformHeader;
    }

    /**
     * Sets the value of the platformHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link HeaderV12 }
     *     
     */
    public void setPlatformHeader(HeaderV12 value) {
        this.platformHeader = value;
    }

    /**
     * Gets the value of the requestParameters property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the requestParameters property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRequestParameters().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ParametersV12 }
     * 
     * 
     */
    public List<ParametersV12> getRequestParameters() {
        if (requestParameters == null) {
            requestParameters = new ArrayList<ParametersV12>();
        }
        return this.requestParameters;
    }

    /**
     * Gets the value of the linkAction property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLinkAction() {
        return linkAction;
    }

    /**
     * Sets the value of the linkAction property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLinkAction(String value) {
        this.linkAction = value;
    }

}
