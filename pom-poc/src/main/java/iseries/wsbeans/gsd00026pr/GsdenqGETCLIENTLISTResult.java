
package iseries.wsbeans.gsd00026pr;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for gsdenqGETCLIENTLISTResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="gsdenqGETCLIENTLISTResult"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="p_XMLIN_7" type="{http://gsd00026pr.wsbeans.iseries/}pXMLIN7"/&gt;
 *         &lt;element name="p_XMLOUT_7" type="{http://gsd00026pr.wsbeans.iseries/}pXMLOUT7"/&gt;
 *         &lt;element name="returnValue" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "gsdenqGETCLIENTLISTResult", propOrder = {
    "pxmlin7",
    "pxmlout7",
    "returnValue"
})
public class GsdenqGETCLIENTLISTResult {

    @XmlElement(name = "p_XMLIN_7", required = true)
    protected PXMLIN7 pxmlin7;
    @XmlElement(name = "p_XMLOUT_7", required = true)
    protected PXMLOUT7 pxmlout7;
    protected int returnValue;

    /**
     * Gets the value of the pxmlin7 property.
     * 
     * @return
     *     possible object is
     *     {@link PXMLIN7 }
     *     
     */
    public PXMLIN7 getPXMLIN7() {
        return pxmlin7;
    }

    /**
     * Sets the value of the pxmlin7 property.
     * 
     * @param value
     *     allowed object is
     *     {@link PXMLIN7 }
     *     
     */
    public void setPXMLIN7(PXMLIN7 value) {
        this.pxmlin7 = value;
    }

    /**
     * Gets the value of the pxmlout7 property.
     * 
     * @return
     *     possible object is
     *     {@link PXMLOUT7 }
     *     
     */
    public PXMLOUT7 getPXMLOUT7() {
        return pxmlout7;
    }

    /**
     * Sets the value of the pxmlout7 property.
     * 
     * @param value
     *     allowed object is
     *     {@link PXMLOUT7 }
     *     
     */
    public void setPXMLOUT7(PXMLOUT7 value) {
        this.pxmlout7 = value;
    }

    /**
     * Gets the value of the returnValue property.
     * 
     */
    public int getReturnValue() {
        return returnValue;
    }

    /**
     * Sets the value of the returnValue property.
     * 
     */
    public void setReturnValue(int value) {
        this.returnValue = value;
    }

}
