
package iseries.wsbeans.gsd00026pr;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for gsdenqGETPHDETAILSBYCONTACTNOLOCInput complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="gsdenqGETPHDETAILSBYCONTACTNOLOCInput"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="p_XMLIN_4" type="{http://gsd00026pr.wsbeans.iseries/}pXMLIN4"/&gt;
 *         &lt;element name="p_XMLOUT_4" type="{http://gsd00026pr.wsbeans.iseries/}pXMLOUT4"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "gsdenqGETPHDETAILSBYCONTACTNOLOCInput", propOrder = {
    "pxmlin4",
    "pxmlout4"
})
public class GsdenqGETPHDETAILSBYCONTACTNOLOCInput {

    @XmlElement(name = "p_XMLIN_4", required = true)
    protected PXMLIN4 pxmlin4;
    @XmlElement(name = "p_XMLOUT_4", required = true)
    protected PXMLOUT4 pxmlout4;

    /**
     * Gets the value of the pxmlin4 property.
     * 
     * @return
     *     possible object is
     *     {@link PXMLIN4 }
     *     
     */
    public PXMLIN4 getPXMLIN4() {
        return pxmlin4;
    }

    /**
     * Sets the value of the pxmlin4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link PXMLIN4 }
     *     
     */
    public void setPXMLIN4(PXMLIN4 value) {
        this.pxmlin4 = value;
    }

    /**
     * Gets the value of the pxmlout4 property.
     * 
     * @return
     *     possible object is
     *     {@link PXMLOUT4 }
     *     
     */
    public PXMLOUT4 getPXMLOUT4() {
        return pxmlout4;
    }

    /**
     * Sets the value of the pxmlout4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link PXMLOUT4 }
     *     
     */
    public void setPXMLOUT4(PXMLOUT4 value) {
        this.pxmlout4 = value;
    }

}
