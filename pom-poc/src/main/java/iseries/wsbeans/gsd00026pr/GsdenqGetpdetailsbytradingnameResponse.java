
package iseries.wsbeans.gsd00026pr;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for gsdenq_getpdetailsbytradingnameResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="gsdenq_getpdetailsbytradingnameResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="return" type="{http://gsd00026pr.wsbeans.iseries/}gsdenqGETPDETAILSBYTRADINGNAMEResult"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "gsdenq_getpdetailsbytradingnameResponse", propOrder = {
    "_return"
})
public class GsdenqGetpdetailsbytradingnameResponse {

    @XmlElement(name = "return", required = true)
    protected GsdenqGETPDETAILSBYTRADINGNAMEResult _return;

    /**
     * Gets the value of the return property.
     * 
     * @return
     *     possible object is
     *     {@link GsdenqGETPDETAILSBYTRADINGNAMEResult }
     *     
     */
    public GsdenqGETPDETAILSBYTRADINGNAMEResult getReturn() {
        return _return;
    }

    /**
     * Sets the value of the return property.
     * 
     * @param value
     *     allowed object is
     *     {@link GsdenqGETPDETAILSBYTRADINGNAMEResult }
     *     
     */
    public void setReturn(GsdenqGETPDETAILSBYTRADINGNAMEResult value) {
        this._return = value;
    }

}
