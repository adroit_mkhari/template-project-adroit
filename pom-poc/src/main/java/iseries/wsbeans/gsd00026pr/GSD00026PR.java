
package iseries.wsbeans.gsd00026pr;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.3.2
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "GSD00026PR", targetNamespace = "http://gsd00026pr.wsbeans.iseries/", wsdlLocation = "file:./src/main/resources/wsdl/create/fnbpre-GSD00026PR.wsdl")
public class GSD00026PR
    extends Service
{

    private final static URL GSD00026PR_WSDL_LOCATION;
    private final static WebServiceException GSD00026PR_EXCEPTION;
    private final static QName GSD00026PR_QNAME = new QName("http://gsd00026pr.wsbeans.iseries/", "GSD00026PR");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("file:./src/main/resources/wsdl/create/fnbpre-GSD00026PR.wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        GSD00026PR_WSDL_LOCATION = url;
        GSD00026PR_EXCEPTION = e;
    }

    public GSD00026PR() {
        super(__getWsdlLocation(), GSD00026PR_QNAME);
    }

    public GSD00026PR(WebServiceFeature... features) {
        super(__getWsdlLocation(), GSD00026PR_QNAME, features);
    }

    public GSD00026PR(URL wsdlLocation) {
        super(wsdlLocation, GSD00026PR_QNAME);
    }

    public GSD00026PR(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, GSD00026PR_QNAME, features);
    }

    public GSD00026PR(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public GSD00026PR(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns GSD00026PRServices
     */
    @WebEndpoint(name = "GSD00026PRServicesPort")
    public GSD00026PRServices getGSD00026PRServicesPort() {
        return super.getPort(new QName("http://gsd00026pr.wsbeans.iseries/", "GSD00026PRServicesPort"), GSD00026PRServices.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns GSD00026PRServices
     */
    @WebEndpoint(name = "GSD00026PRServicesPort")
    public GSD00026PRServices getGSD00026PRServicesPort(WebServiceFeature... features) {
        return super.getPort(new QName("http://gsd00026pr.wsbeans.iseries/", "GSD00026PRServicesPort"), GSD00026PRServices.class, features);
    }

    private static URL __getWsdlLocation() {
        if (GSD00026PR_EXCEPTION!= null) {
            throw GSD00026PR_EXCEPTION;
        }
        return GSD00026PR_WSDL_LOCATION;
    }

}
