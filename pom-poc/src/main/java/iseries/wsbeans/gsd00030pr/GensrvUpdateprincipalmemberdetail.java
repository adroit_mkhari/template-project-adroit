
package iseries.wsbeans.gsd00030pr;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for gensrv_updateprincipalmemberdetail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="gensrv_updateprincipalmemberdetail"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="arg0" type="{http://gsd00030pr.wsbeans.iseries/}gensrvUPDATEPRINCIPALMEMBERDETAILInput"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "gensrv_updateprincipalmemberdetail", propOrder = {
    "arg0"
})
public class GensrvUpdateprincipalmemberdetail {

    @XmlElement(required = true)
    protected GensrvUPDATEPRINCIPALMEMBERDETAILInput arg0;

    /**
     * Gets the value of the arg0 property.
     * 
     * @return
     *     possible object is
     *     {@link GensrvUPDATEPRINCIPALMEMBERDETAILInput }
     *     
     */
    public GensrvUPDATEPRINCIPALMEMBERDETAILInput getArg0() {
        return arg0;
    }

    /**
     * Sets the value of the arg0 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GensrvUPDATEPRINCIPALMEMBERDETAILInput }
     *     
     */
    public void setArg0(GensrvUPDATEPRINCIPALMEMBERDETAILInput value) {
        this.arg0 = value;
    }

}
