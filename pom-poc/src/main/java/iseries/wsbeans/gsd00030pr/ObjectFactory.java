
package iseries.wsbeans.gsd00030pr;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the iseries.wsbeans.gsd00030pr package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GensrvCalculatearrears_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_calculatearrears");
    private final static QName _GensrvCalculatearrearsResponse_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_calculatearrearsResponse");
    private final static QName _GensrvCancelpolicy_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_cancelpolicy");
    private final static QName _GensrvCancelpolicyResponse_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_cancelpolicyResponse");
    private final static QName _GensrvConvertpolicy_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_convertpolicy");
    private final static QName _GensrvConvertpolicyResponse_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_convertpolicyResponse");
    private final static QName _GensrvDebitorderalignment_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_debitorderalignment");
    private final static QName _GensrvDebitorderalignmentResponse_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_debitorderalignmentResponse");
    private final static QName _GensrvGetaudittrail_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_getaudittrail");
    private final static QName _GensrvGetaudittrailResponse_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_getaudittrailResponse");
    private final static QName _GensrvGetbankcodes_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_getbankcodes");
    private final static QName _GensrvGetbankcodesResponse_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_getbankcodesResponse");
    private final static QName _GensrvGetcancelreasons_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_getcancelreasons");
    private final static QName _GensrvGetcancelreasonsResponse_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_getcancelreasonsResponse");
    private final static QName _GensrvGetcoveramounts_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_getcoveramounts");
    private final static QName _GensrvGetcoveramountsResponse_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_getcoveramountsResponse");
    private final static QName _GensrvGetescalationoptions_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_getescalationoptions");
    private final static QName _GensrvGetescalationoptionsResponse_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_getescalationoptionsResponse");
    private final static QName _GensrvGetfinancialhistory_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_getfinancialhistory");
    private final static QName _GensrvGetfinancialhistoryResponse_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_getfinancialhistoryResponse");
    private final static QName _GensrvGetpolicybasic_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_getpolicybasic");
    private final static QName _GensrvGetpolicybasicResponse_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_getpolicybasicResponse");
    private final static QName _GensrvGetpolicydetail_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_getpolicydetail");
    private final static QName _GensrvGetpolicydetailResponse_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_getpolicydetailResponse");
    private final static QName _GensrvGetpremiumstatuses_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_getpremiumstatuses");
    private final static QName _GensrvGetpremiumstatusesResponse_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_getpremiumstatusesResponse");
    private final static QName _GensrvGetprincipalmemberdetail_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_getprincipalmemberdetail");
    private final static QName _GensrvGetprincipalmemberdetailResponse_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_getprincipalmemberdetailResponse");
    private final static QName _GensrvGetrefunddetails_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_getrefunddetails");
    private final static QName _GensrvGetrefunddetailsResponse_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_getrefunddetailsResponse");
    private final static QName _GensrvGetrefundlist_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_getrefundlist");
    private final static QName _GensrvGetrefundlistResponse_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_getrefundlistResponse");
    private final static QName _GensrvGetrefundreasons_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_getrefundreasons");
    private final static QName _GensrvGetrefundreasonsResponse_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_getrefundreasonsResponse");
    private final static QName _GensrvGetroletypes_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_getroletypes");
    private final static QName _GensrvGetroletypesResponse_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_getroletypesResponse");
    private final static QName _GensrvGetsalesdetail_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_getsalesdetail");
    private final static QName _GensrvGetsalesdetailResponse_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_getsalesdetailResponse");
    private final static QName _GensrvQuoteaddrole_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_quoteaddrole");
    private final static QName _GensrvQuoteaddroleResponse_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_quoteaddroleResponse");
    private final static QName _GensrvQuotedeleterole_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_quotedeleterole");
    private final static QName _GensrvQuotedeleteroleResponse_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_quotedeleteroleResponse");
    private final static QName _GensrvQuoteupdaterole_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_quoteupdaterole");
    private final static QName _GensrvQuoteupdateroleResponse_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_quoteupdateroleResponse");
    private final static QName _GensrvRefundpremium_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_refundpremium");
    private final static QName _GensrvRefundpremiumResponse_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_refundpremiumResponse");
    private final static QName _GensrvReinstatepolicy_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_reinstatepolicy");
    private final static QName _GensrvReinstatepolicyResponse_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_reinstatepolicyResponse");
    private final static QName _GensrvRequestvoicerecording_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_requestvoicerecording");
    private final static QName _GensrvRequestvoicerecordingResponse_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_requestvoicerecordingResponse");
    private final static QName _GensrvSearchaddress_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_searchaddress");
    private final static QName _GensrvSearchaddressResponse_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_searchaddressResponse");
    private final static QName _GensrvSendgenericletter_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_sendgenericletter");
    private final static QName _GensrvSendgenericletterResponse_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_sendgenericletterResponse");
    private final static QName _GensrvUpdateaudittrail_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_updateaudittrail");
    private final static QName _GensrvUpdateaudittrailResponse_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_updateaudittrailResponse");
    private final static QName _GensrvUpdatecoverdates_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_updatecoverdates");
    private final static QName _GensrvUpdatecoverdatesResponse_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_updatecoverdatesResponse");
    private final static QName _GensrvUpdatepolicy_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_updatepolicy");
    private final static QName _GensrvUpdatepolicyResponse_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_updatepolicyResponse");
    private final static QName _GensrvUpdateprincipalmemberdetail_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_updateprincipalmemberdetail");
    private final static QName _GensrvUpdateprincipalmemberdetailResponse_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_updateprincipalmemberdetailResponse");
    private final static QName _GensrvUpdaterefund_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_updaterefund");
    private final static QName _GensrvUpdaterefundResponse_QNAME = new QName("http://gsd00030pr.wsbeans.iseries/", "gensrv_updaterefundResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: iseries.wsbeans.gsd00030pr
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GensrvCalculatearrears }
     * 
     */
    public GensrvCalculatearrears createGensrvCalculatearrears() {
        return new GensrvCalculatearrears();
    }

    /**
     * Create an instance of {@link GensrvCalculatearrearsResponse }
     * 
     */
    public GensrvCalculatearrearsResponse createGensrvCalculatearrearsResponse() {
        return new GensrvCalculatearrearsResponse();
    }

    /**
     * Create an instance of {@link GensrvCancelpolicy }
     * 
     */
    public GensrvCancelpolicy createGensrvCancelpolicy() {
        return new GensrvCancelpolicy();
    }

    /**
     * Create an instance of {@link GensrvCancelpolicyResponse }
     * 
     */
    public GensrvCancelpolicyResponse createGensrvCancelpolicyResponse() {
        return new GensrvCancelpolicyResponse();
    }

    /**
     * Create an instance of {@link GensrvConvertpolicy }
     * 
     */
    public GensrvConvertpolicy createGensrvConvertpolicy() {
        return new GensrvConvertpolicy();
    }

    /**
     * Create an instance of {@link GensrvConvertpolicyResponse }
     * 
     */
    public GensrvConvertpolicyResponse createGensrvConvertpolicyResponse() {
        return new GensrvConvertpolicyResponse();
    }

    /**
     * Create an instance of {@link GensrvDebitorderalignment }
     * 
     */
    public GensrvDebitorderalignment createGensrvDebitorderalignment() {
        return new GensrvDebitorderalignment();
    }

    /**
     * Create an instance of {@link GensrvDebitorderalignmentResponse }
     * 
     */
    public GensrvDebitorderalignmentResponse createGensrvDebitorderalignmentResponse() {
        return new GensrvDebitorderalignmentResponse();
    }

    /**
     * Create an instance of {@link GensrvGetaudittrail }
     * 
     */
    public GensrvGetaudittrail createGensrvGetaudittrail() {
        return new GensrvGetaudittrail();
    }

    /**
     * Create an instance of {@link GensrvGetaudittrailResponse }
     * 
     */
    public GensrvGetaudittrailResponse createGensrvGetaudittrailResponse() {
        return new GensrvGetaudittrailResponse();
    }

    /**
     * Create an instance of {@link GensrvGetbankcodes }
     * 
     */
    public GensrvGetbankcodes createGensrvGetbankcodes() {
        return new GensrvGetbankcodes();
    }

    /**
     * Create an instance of {@link GensrvGetbankcodesResponse }
     * 
     */
    public GensrvGetbankcodesResponse createGensrvGetbankcodesResponse() {
        return new GensrvGetbankcodesResponse();
    }

    /**
     * Create an instance of {@link GensrvGetcancelreasons }
     * 
     */
    public GensrvGetcancelreasons createGensrvGetcancelreasons() {
        return new GensrvGetcancelreasons();
    }

    /**
     * Create an instance of {@link GensrvGetcancelreasonsResponse }
     * 
     */
    public GensrvGetcancelreasonsResponse createGensrvGetcancelreasonsResponse() {
        return new GensrvGetcancelreasonsResponse();
    }

    /**
     * Create an instance of {@link GensrvGetcoveramounts }
     * 
     */
    public GensrvGetcoveramounts createGensrvGetcoveramounts() {
        return new GensrvGetcoveramounts();
    }

    /**
     * Create an instance of {@link GensrvGetcoveramountsResponse }
     * 
     */
    public GensrvGetcoveramountsResponse createGensrvGetcoveramountsResponse() {
        return new GensrvGetcoveramountsResponse();
    }

    /**
     * Create an instance of {@link GensrvGetescalationoptions }
     * 
     */
    public GensrvGetescalationoptions createGensrvGetescalationoptions() {
        return new GensrvGetescalationoptions();
    }

    /**
     * Create an instance of {@link GensrvGetescalationoptionsResponse }
     * 
     */
    public GensrvGetescalationoptionsResponse createGensrvGetescalationoptionsResponse() {
        return new GensrvGetescalationoptionsResponse();
    }

    /**
     * Create an instance of {@link GensrvGetfinancialhistory }
     * 
     */
    public GensrvGetfinancialhistory createGensrvGetfinancialhistory() {
        return new GensrvGetfinancialhistory();
    }

    /**
     * Create an instance of {@link GensrvGetfinancialhistoryResponse }
     * 
     */
    public GensrvGetfinancialhistoryResponse createGensrvGetfinancialhistoryResponse() {
        return new GensrvGetfinancialhistoryResponse();
    }

    /**
     * Create an instance of {@link GensrvGetpolicybasic }
     * 
     */
    public GensrvGetpolicybasic createGensrvGetpolicybasic() {
        return new GensrvGetpolicybasic();
    }

    /**
     * Create an instance of {@link GensrvGetpolicybasicResponse }
     * 
     */
    public GensrvGetpolicybasicResponse createGensrvGetpolicybasicResponse() {
        return new GensrvGetpolicybasicResponse();
    }

    /**
     * Create an instance of {@link GensrvGetpolicydetail }
     * 
     */
    public GensrvGetpolicydetail createGensrvGetpolicydetail() {
        return new GensrvGetpolicydetail();
    }

    /**
     * Create an instance of {@link GensrvGetpolicydetailResponse }
     * 
     */
    public GensrvGetpolicydetailResponse createGensrvGetpolicydetailResponse() {
        return new GensrvGetpolicydetailResponse();
    }

    /**
     * Create an instance of {@link GensrvGetpremiumstatuses }
     * 
     */
    public GensrvGetpremiumstatuses createGensrvGetpremiumstatuses() {
        return new GensrvGetpremiumstatuses();
    }

    /**
     * Create an instance of {@link GensrvGetpremiumstatusesResponse }
     * 
     */
    public GensrvGetpremiumstatusesResponse createGensrvGetpremiumstatusesResponse() {
        return new GensrvGetpremiumstatusesResponse();
    }

    /**
     * Create an instance of {@link GensrvGetprincipalmemberdetail }
     * 
     */
    public GensrvGetprincipalmemberdetail createGensrvGetprincipalmemberdetail() {
        return new GensrvGetprincipalmemberdetail();
    }

    /**
     * Create an instance of {@link GensrvGetprincipalmemberdetailResponse }
     * 
     */
    public GensrvGetprincipalmemberdetailResponse createGensrvGetprincipalmemberdetailResponse() {
        return new GensrvGetprincipalmemberdetailResponse();
    }

    /**
     * Create an instance of {@link GensrvGetrefunddetails }
     * 
     */
    public GensrvGetrefunddetails createGensrvGetrefunddetails() {
        return new GensrvGetrefunddetails();
    }

    /**
     * Create an instance of {@link GensrvGetrefunddetailsResponse }
     * 
     */
    public GensrvGetrefunddetailsResponse createGensrvGetrefunddetailsResponse() {
        return new GensrvGetrefunddetailsResponse();
    }

    /**
     * Create an instance of {@link GensrvGetrefundlist }
     * 
     */
    public GensrvGetrefundlist createGensrvGetrefundlist() {
        return new GensrvGetrefundlist();
    }

    /**
     * Create an instance of {@link GensrvGetrefundlistResponse }
     * 
     */
    public GensrvGetrefundlistResponse createGensrvGetrefundlistResponse() {
        return new GensrvGetrefundlistResponse();
    }

    /**
     * Create an instance of {@link GensrvGetrefundreasons }
     * 
     */
    public GensrvGetrefundreasons createGensrvGetrefundreasons() {
        return new GensrvGetrefundreasons();
    }

    /**
     * Create an instance of {@link GensrvGetrefundreasonsResponse }
     * 
     */
    public GensrvGetrefundreasonsResponse createGensrvGetrefundreasonsResponse() {
        return new GensrvGetrefundreasonsResponse();
    }

    /**
     * Create an instance of {@link GensrvGetroletypes }
     * 
     */
    public GensrvGetroletypes createGensrvGetroletypes() {
        return new GensrvGetroletypes();
    }

    /**
     * Create an instance of {@link GensrvGetroletypesResponse }
     * 
     */
    public GensrvGetroletypesResponse createGensrvGetroletypesResponse() {
        return new GensrvGetroletypesResponse();
    }

    /**
     * Create an instance of {@link GensrvGetsalesdetail }
     * 
     */
    public GensrvGetsalesdetail createGensrvGetsalesdetail() {
        return new GensrvGetsalesdetail();
    }

    /**
     * Create an instance of {@link GensrvGetsalesdetailResponse }
     * 
     */
    public GensrvGetsalesdetailResponse createGensrvGetsalesdetailResponse() {
        return new GensrvGetsalesdetailResponse();
    }

    /**
     * Create an instance of {@link GensrvQuoteaddrole }
     * 
     */
    public GensrvQuoteaddrole createGensrvQuoteaddrole() {
        return new GensrvQuoteaddrole();
    }

    /**
     * Create an instance of {@link GensrvQuoteaddroleResponse }
     * 
     */
    public GensrvQuoteaddroleResponse createGensrvQuoteaddroleResponse() {
        return new GensrvQuoteaddroleResponse();
    }

    /**
     * Create an instance of {@link GensrvQuotedeleterole }
     * 
     */
    public GensrvQuotedeleterole createGensrvQuotedeleterole() {
        return new GensrvQuotedeleterole();
    }

    /**
     * Create an instance of {@link GensrvQuotedeleteroleResponse }
     * 
     */
    public GensrvQuotedeleteroleResponse createGensrvQuotedeleteroleResponse() {
        return new GensrvQuotedeleteroleResponse();
    }

    /**
     * Create an instance of {@link GensrvQuoteupdaterole }
     * 
     */
    public GensrvQuoteupdaterole createGensrvQuoteupdaterole() {
        return new GensrvQuoteupdaterole();
    }

    /**
     * Create an instance of {@link GensrvQuoteupdateroleResponse }
     * 
     */
    public GensrvQuoteupdateroleResponse createGensrvQuoteupdateroleResponse() {
        return new GensrvQuoteupdateroleResponse();
    }

    /**
     * Create an instance of {@link GensrvRefundpremium }
     * 
     */
    public GensrvRefundpremium createGensrvRefundpremium() {
        return new GensrvRefundpremium();
    }

    /**
     * Create an instance of {@link GensrvRefundpremiumResponse }
     * 
     */
    public GensrvRefundpremiumResponse createGensrvRefundpremiumResponse() {
        return new GensrvRefundpremiumResponse();
    }

    /**
     * Create an instance of {@link GensrvReinstatepolicy }
     * 
     */
    public GensrvReinstatepolicy createGensrvReinstatepolicy() {
        return new GensrvReinstatepolicy();
    }

    /**
     * Create an instance of {@link GensrvReinstatepolicyResponse }
     * 
     */
    public GensrvReinstatepolicyResponse createGensrvReinstatepolicyResponse() {
        return new GensrvReinstatepolicyResponse();
    }

    /**
     * Create an instance of {@link GensrvRequestvoicerecording }
     * 
     */
    public GensrvRequestvoicerecording createGensrvRequestvoicerecording() {
        return new GensrvRequestvoicerecording();
    }

    /**
     * Create an instance of {@link GensrvRequestvoicerecordingResponse }
     * 
     */
    public GensrvRequestvoicerecordingResponse createGensrvRequestvoicerecordingResponse() {
        return new GensrvRequestvoicerecordingResponse();
    }

    /**
     * Create an instance of {@link GensrvSearchaddress }
     * 
     */
    public GensrvSearchaddress createGensrvSearchaddress() {
        return new GensrvSearchaddress();
    }

    /**
     * Create an instance of {@link GensrvSearchaddressResponse }
     * 
     */
    public GensrvSearchaddressResponse createGensrvSearchaddressResponse() {
        return new GensrvSearchaddressResponse();
    }

    /**
     * Create an instance of {@link GensrvSendgenericletter }
     * 
     */
    public GensrvSendgenericletter createGensrvSendgenericletter() {
        return new GensrvSendgenericletter();
    }

    /**
     * Create an instance of {@link GensrvSendgenericletterResponse }
     * 
     */
    public GensrvSendgenericletterResponse createGensrvSendgenericletterResponse() {
        return new GensrvSendgenericletterResponse();
    }

    /**
     * Create an instance of {@link GensrvUpdateaudittrail }
     * 
     */
    public GensrvUpdateaudittrail createGensrvUpdateaudittrail() {
        return new GensrvUpdateaudittrail();
    }

    /**
     * Create an instance of {@link GensrvUpdateaudittrailResponse }
     * 
     */
    public GensrvUpdateaudittrailResponse createGensrvUpdateaudittrailResponse() {
        return new GensrvUpdateaudittrailResponse();
    }

    /**
     * Create an instance of {@link GensrvUpdatecoverdates }
     * 
     */
    public GensrvUpdatecoverdates createGensrvUpdatecoverdates() {
        return new GensrvUpdatecoverdates();
    }

    /**
     * Create an instance of {@link GensrvUpdatecoverdatesResponse }
     * 
     */
    public GensrvUpdatecoverdatesResponse createGensrvUpdatecoverdatesResponse() {
        return new GensrvUpdatecoverdatesResponse();
    }

    /**
     * Create an instance of {@link GensrvUpdatepolicy }
     * 
     */
    public GensrvUpdatepolicy createGensrvUpdatepolicy() {
        return new GensrvUpdatepolicy();
    }

    /**
     * Create an instance of {@link GensrvUpdatepolicyResponse }
     * 
     */
    public GensrvUpdatepolicyResponse createGensrvUpdatepolicyResponse() {
        return new GensrvUpdatepolicyResponse();
    }

    /**
     * Create an instance of {@link GensrvUpdateprincipalmemberdetail }
     * 
     */
    public GensrvUpdateprincipalmemberdetail createGensrvUpdateprincipalmemberdetail() {
        return new GensrvUpdateprincipalmemberdetail();
    }

    /**
     * Create an instance of {@link GensrvUpdateprincipalmemberdetailResponse }
     * 
     */
    public GensrvUpdateprincipalmemberdetailResponse createGensrvUpdateprincipalmemberdetailResponse() {
        return new GensrvUpdateprincipalmemberdetailResponse();
    }

    /**
     * Create an instance of {@link GensrvUpdaterefund }
     * 
     */
    public GensrvUpdaterefund createGensrvUpdaterefund() {
        return new GensrvUpdaterefund();
    }

    /**
     * Create an instance of {@link GensrvUpdaterefundResponse }
     * 
     */
    public GensrvUpdaterefundResponse createGensrvUpdaterefundResponse() {
        return new GensrvUpdaterefundResponse();
    }

    /**
     * Create an instance of {@link GensrvGETPOLICYBASICInput }
     * 
     */
    public GensrvGETPOLICYBASICInput createGensrvGETPOLICYBASICInput() {
        return new GensrvGETPOLICYBASICInput();
    }

    /**
     * Create an instance of {@link PXMLIN }
     * 
     */
    public PXMLIN createPXMLIN() {
        return new PXMLIN();
    }

    /**
     * Create an instance of {@link PXMLOUT }
     * 
     */
    public PXMLOUT createPXMLOUT() {
        return new PXMLOUT();
    }

    /**
     * Create an instance of {@link GensrvGETPOLICYBASICResult }
     * 
     */
    public GensrvGETPOLICYBASICResult createGensrvGETPOLICYBASICResult() {
        return new GensrvGETPOLICYBASICResult();
    }

    /**
     * Create an instance of {@link GensrvGETAUDITTRAILInput }
     * 
     */
    public GensrvGETAUDITTRAILInput createGensrvGETAUDITTRAILInput() {
        return new GensrvGETAUDITTRAILInput();
    }

    /**
     * Create an instance of {@link GensrvGETAUDITTRAILResult }
     * 
     */
    public GensrvGETAUDITTRAILResult createGensrvGETAUDITTRAILResult() {
        return new GensrvGETAUDITTRAILResult();
    }

    /**
     * Create an instance of {@link GensrvGETFINANCIALHISTORYInput }
     * 
     */
    public GensrvGETFINANCIALHISTORYInput createGensrvGETFINANCIALHISTORYInput() {
        return new GensrvGETFINANCIALHISTORYInput();
    }

    /**
     * Create an instance of {@link GensrvGETFINANCIALHISTORYResult }
     * 
     */
    public GensrvGETFINANCIALHISTORYResult createGensrvGETFINANCIALHISTORYResult() {
        return new GensrvGETFINANCIALHISTORYResult();
    }

    /**
     * Create an instance of {@link GensrvGETPRINCIPALMEMBERDETAILInput }
     * 
     */
    public GensrvGETPRINCIPALMEMBERDETAILInput createGensrvGETPRINCIPALMEMBERDETAILInput() {
        return new GensrvGETPRINCIPALMEMBERDETAILInput();
    }

    /**
     * Create an instance of {@link GensrvGETPRINCIPALMEMBERDETAILResult }
     * 
     */
    public GensrvGETPRINCIPALMEMBERDETAILResult createGensrvGETPRINCIPALMEMBERDETAILResult() {
        return new GensrvGETPRINCIPALMEMBERDETAILResult();
    }

    /**
     * Create an instance of {@link GensrvUPDATEPRINCIPALMEMBERDETAILInput }
     * 
     */
    public GensrvUPDATEPRINCIPALMEMBERDETAILInput createGensrvUPDATEPRINCIPALMEMBERDETAILInput() {
        return new GensrvUPDATEPRINCIPALMEMBERDETAILInput();
    }

    /**
     * Create an instance of {@link GensrvUPDATEPRINCIPALMEMBERDETAILResult }
     * 
     */
    public GensrvUPDATEPRINCIPALMEMBERDETAILResult createGensrvUPDATEPRINCIPALMEMBERDETAILResult() {
        return new GensrvUPDATEPRINCIPALMEMBERDETAILResult();
    }

    /**
     * Create an instance of {@link GensrvGETSALESDETAILInput }
     * 
     */
    public GensrvGETSALESDETAILInput createGensrvGETSALESDETAILInput() {
        return new GensrvGETSALESDETAILInput();
    }

    /**
     * Create an instance of {@link GensrvGETSALESDETAILResult }
     * 
     */
    public GensrvGETSALESDETAILResult createGensrvGETSALESDETAILResult() {
        return new GensrvGETSALESDETAILResult();
    }

    /**
     * Create an instance of {@link GensrvQUOTEDELETEROLEInput }
     * 
     */
    public GensrvQUOTEDELETEROLEInput createGensrvQUOTEDELETEROLEInput() {
        return new GensrvQUOTEDELETEROLEInput();
    }

    /**
     * Create an instance of {@link GensrvQUOTEDELETEROLEResult }
     * 
     */
    public GensrvQUOTEDELETEROLEResult createGensrvQUOTEDELETEROLEResult() {
        return new GensrvQUOTEDELETEROLEResult();
    }

    /**
     * Create an instance of {@link GensrvGETREFUNDLISTInput }
     * 
     */
    public GensrvGETREFUNDLISTInput createGensrvGETREFUNDLISTInput() {
        return new GensrvGETREFUNDLISTInput();
    }

    /**
     * Create an instance of {@link GensrvGETREFUNDLISTResult }
     * 
     */
    public GensrvGETREFUNDLISTResult createGensrvGETREFUNDLISTResult() {
        return new GensrvGETREFUNDLISTResult();
    }

    /**
     * Create an instance of {@link GensrvQUOTEUPDATEROLEInput }
     * 
     */
    public GensrvQUOTEUPDATEROLEInput createGensrvQUOTEUPDATEROLEInput() {
        return new GensrvQUOTEUPDATEROLEInput();
    }

    /**
     * Create an instance of {@link GensrvQUOTEUPDATEROLEResult }
     * 
     */
    public GensrvQUOTEUPDATEROLEResult createGensrvQUOTEUPDATEROLEResult() {
        return new GensrvQUOTEUPDATEROLEResult();
    }

    /**
     * Create an instance of {@link GensrvSEARCHADDRESSInput }
     * 
     */
    public GensrvSEARCHADDRESSInput createGensrvSEARCHADDRESSInput() {
        return new GensrvSEARCHADDRESSInput();
    }

    /**
     * Create an instance of {@link GensrvSEARCHADDRESSResult }
     * 
     */
    public GensrvSEARCHADDRESSResult createGensrvSEARCHADDRESSResult() {
        return new GensrvSEARCHADDRESSResult();
    }

    /**
     * Create an instance of {@link GensrvGETBANKCODESInput }
     * 
     */
    public GensrvGETBANKCODESInput createGensrvGETBANKCODESInput() {
        return new GensrvGETBANKCODESInput();
    }

    /**
     * Create an instance of {@link GensrvGETBANKCODESResult }
     * 
     */
    public GensrvGETBANKCODESResult createGensrvGETBANKCODESResult() {
        return new GensrvGETBANKCODESResult();
    }

    /**
     * Create an instance of {@link GensrvCONVERTPOLICYInput }
     * 
     */
    public GensrvCONVERTPOLICYInput createGensrvCONVERTPOLICYInput() {
        return new GensrvCONVERTPOLICYInput();
    }

    /**
     * Create an instance of {@link GensrvCONVERTPOLICYResult }
     * 
     */
    public GensrvCONVERTPOLICYResult createGensrvCONVERTPOLICYResult() {
        return new GensrvCONVERTPOLICYResult();
    }

    /**
     * Create an instance of {@link GensrvGETPOLICYDETAILInput }
     * 
     */
    public GensrvGETPOLICYDETAILInput createGensrvGETPOLICYDETAILInput() {
        return new GensrvGETPOLICYDETAILInput();
    }

    /**
     * Create an instance of {@link GensrvGETPOLICYDETAILResult }
     * 
     */
    public GensrvGETPOLICYDETAILResult createGensrvGETPOLICYDETAILResult() {
        return new GensrvGETPOLICYDETAILResult();
    }

    /**
     * Create an instance of {@link GensrvUPDATECOVERDATESInput }
     * 
     */
    public GensrvUPDATECOVERDATESInput createGensrvUPDATECOVERDATESInput() {
        return new GensrvUPDATECOVERDATESInput();
    }

    /**
     * Create an instance of {@link GensrvUPDATECOVERDATESResult }
     * 
     */
    public GensrvUPDATECOVERDATESResult createGensrvUPDATECOVERDATESResult() {
        return new GensrvUPDATECOVERDATESResult();
    }

    /**
     * Create an instance of {@link GensrvDEBITORDERALIGNMENTInput }
     * 
     */
    public GensrvDEBITORDERALIGNMENTInput createGensrvDEBITORDERALIGNMENTInput() {
        return new GensrvDEBITORDERALIGNMENTInput();
    }

    /**
     * Create an instance of {@link GensrvDEBITORDERALIGNMENTResult }
     * 
     */
    public GensrvDEBITORDERALIGNMENTResult createGensrvDEBITORDERALIGNMENTResult() {
        return new GensrvDEBITORDERALIGNMENTResult();
    }

    /**
     * Create an instance of {@link GensrvREFUNDPREMIUMInput }
     * 
     */
    public GensrvREFUNDPREMIUMInput createGensrvREFUNDPREMIUMInput() {
        return new GensrvREFUNDPREMIUMInput();
    }

    /**
     * Create an instance of {@link GensrvREFUNDPREMIUMResult }
     * 
     */
    public GensrvREFUNDPREMIUMResult createGensrvREFUNDPREMIUMResult() {
        return new GensrvREFUNDPREMIUMResult();
    }

    /**
     * Create an instance of {@link GensrvCALCULATEARREARSInput }
     * 
     */
    public GensrvCALCULATEARREARSInput createGensrvCALCULATEARREARSInput() {
        return new GensrvCALCULATEARREARSInput();
    }

    /**
     * Create an instance of {@link GensrvCALCULATEARREARSResult }
     * 
     */
    public GensrvCALCULATEARREARSResult createGensrvCALCULATEARREARSResult() {
        return new GensrvCALCULATEARREARSResult();
    }

    /**
     * Create an instance of {@link GensrvREQUESTVOICERECORDINGInput }
     * 
     */
    public GensrvREQUESTVOICERECORDINGInput createGensrvREQUESTVOICERECORDINGInput() {
        return new GensrvREQUESTVOICERECORDINGInput();
    }

    /**
     * Create an instance of {@link GensrvREQUESTVOICERECORDINGResult }
     * 
     */
    public GensrvREQUESTVOICERECORDINGResult createGensrvREQUESTVOICERECORDINGResult() {
        return new GensrvREQUESTVOICERECORDINGResult();
    }

    /**
     * Create an instance of {@link GensrvGETCANCELREASONSInput }
     * 
     */
    public GensrvGETCANCELREASONSInput createGensrvGETCANCELREASONSInput() {
        return new GensrvGETCANCELREASONSInput();
    }

    /**
     * Create an instance of {@link GensrvGETCANCELREASONSResult }
     * 
     */
    public GensrvGETCANCELREASONSResult createGensrvGETCANCELREASONSResult() {
        return new GensrvGETCANCELREASONSResult();
    }

    /**
     * Create an instance of {@link GensrvREINSTATEPOLICYInput }
     * 
     */
    public GensrvREINSTATEPOLICYInput createGensrvREINSTATEPOLICYInput() {
        return new GensrvREINSTATEPOLICYInput();
    }

    /**
     * Create an instance of {@link GensrvREINSTATEPOLICYResult }
     * 
     */
    public GensrvREINSTATEPOLICYResult createGensrvREINSTATEPOLICYResult() {
        return new GensrvREINSTATEPOLICYResult();
    }

    /**
     * Create an instance of {@link GensrvSENDGENERICLETTERInput }
     * 
     */
    public GensrvSENDGENERICLETTERInput createGensrvSENDGENERICLETTERInput() {
        return new GensrvSENDGENERICLETTERInput();
    }

    /**
     * Create an instance of {@link GensrvSENDGENERICLETTERResult }
     * 
     */
    public GensrvSENDGENERICLETTERResult createGensrvSENDGENERICLETTERResult() {
        return new GensrvSENDGENERICLETTERResult();
    }

    /**
     * Create an instance of {@link GensrvGETCOVERAMOUNTSInput }
     * 
     */
    public GensrvGETCOVERAMOUNTSInput createGensrvGETCOVERAMOUNTSInput() {
        return new GensrvGETCOVERAMOUNTSInput();
    }

    /**
     * Create an instance of {@link GensrvGETCOVERAMOUNTSResult }
     * 
     */
    public GensrvGETCOVERAMOUNTSResult createGensrvGETCOVERAMOUNTSResult() {
        return new GensrvGETCOVERAMOUNTSResult();
    }

    /**
     * Create an instance of {@link GensrvUPDATEAUDITTRAILInput }
     * 
     */
    public GensrvUPDATEAUDITTRAILInput createGensrvUPDATEAUDITTRAILInput() {
        return new GensrvUPDATEAUDITTRAILInput();
    }

    /**
     * Create an instance of {@link GensrvUPDATEAUDITTRAILResult }
     * 
     */
    public GensrvUPDATEAUDITTRAILResult createGensrvUPDATEAUDITTRAILResult() {
        return new GensrvUPDATEAUDITTRAILResult();
    }

    /**
     * Create an instance of {@link GensrvGETREFUNDDETAILSInput }
     * 
     */
    public GensrvGETREFUNDDETAILSInput createGensrvGETREFUNDDETAILSInput() {
        return new GensrvGETREFUNDDETAILSInput();
    }

    /**
     * Create an instance of {@link GensrvGETREFUNDDETAILSResult }
     * 
     */
    public GensrvGETREFUNDDETAILSResult createGensrvGETREFUNDDETAILSResult() {
        return new GensrvGETREFUNDDETAILSResult();
    }

    /**
     * Create an instance of {@link GensrvQUOTEADDROLEInput }
     * 
     */
    public GensrvQUOTEADDROLEInput createGensrvQUOTEADDROLEInput() {
        return new GensrvQUOTEADDROLEInput();
    }

    /**
     * Create an instance of {@link GensrvQUOTEADDROLEResult }
     * 
     */
    public GensrvQUOTEADDROLEResult createGensrvQUOTEADDROLEResult() {
        return new GensrvQUOTEADDROLEResult();
    }

    /**
     * Create an instance of {@link GensrvGETREFUNDREASONSInput }
     * 
     */
    public GensrvGETREFUNDREASONSInput createGensrvGETREFUNDREASONSInput() {
        return new GensrvGETREFUNDREASONSInput();
    }

    /**
     * Create an instance of {@link GensrvGETREFUNDREASONSResult }
     * 
     */
    public GensrvGETREFUNDREASONSResult createGensrvGETREFUNDREASONSResult() {
        return new GensrvGETREFUNDREASONSResult();
    }

    /**
     * Create an instance of {@link GensrvUPDATEREFUNDInput }
     * 
     */
    public GensrvUPDATEREFUNDInput createGensrvUPDATEREFUNDInput() {
        return new GensrvUPDATEREFUNDInput();
    }

    /**
     * Create an instance of {@link GensrvUPDATEREFUNDResult }
     * 
     */
    public GensrvUPDATEREFUNDResult createGensrvUPDATEREFUNDResult() {
        return new GensrvUPDATEREFUNDResult();
    }

    /**
     * Create an instance of {@link GensrvGETPREMIUMSTATUSESInput }
     * 
     */
    public GensrvGETPREMIUMSTATUSESInput createGensrvGETPREMIUMSTATUSESInput() {
        return new GensrvGETPREMIUMSTATUSESInput();
    }

    /**
     * Create an instance of {@link GensrvGETPREMIUMSTATUSESResult }
     * 
     */
    public GensrvGETPREMIUMSTATUSESResult createGensrvGETPREMIUMSTATUSESResult() {
        return new GensrvGETPREMIUMSTATUSESResult();
    }

    /**
     * Create an instance of {@link GensrvCANCELPOLICYInput }
     * 
     */
    public GensrvCANCELPOLICYInput createGensrvCANCELPOLICYInput() {
        return new GensrvCANCELPOLICYInput();
    }

    /**
     * Create an instance of {@link GensrvCANCELPOLICYResult }
     * 
     */
    public GensrvCANCELPOLICYResult createGensrvCANCELPOLICYResult() {
        return new GensrvCANCELPOLICYResult();
    }

    /**
     * Create an instance of {@link GensrvGETESCALATIONOPTIONSInput }
     * 
     */
    public GensrvGETESCALATIONOPTIONSInput createGensrvGETESCALATIONOPTIONSInput() {
        return new GensrvGETESCALATIONOPTIONSInput();
    }

    /**
     * Create an instance of {@link GensrvGETESCALATIONOPTIONSResult }
     * 
     */
    public GensrvGETESCALATIONOPTIONSResult createGensrvGETESCALATIONOPTIONSResult() {
        return new GensrvGETESCALATIONOPTIONSResult();
    }

    /**
     * Create an instance of {@link GensrvUPDATEPOLICYInput }
     * 
     */
    public GensrvUPDATEPOLICYInput createGensrvUPDATEPOLICYInput() {
        return new GensrvUPDATEPOLICYInput();
    }

    /**
     * Create an instance of {@link GensrvUPDATEPOLICYResult }
     * 
     */
    public GensrvUPDATEPOLICYResult createGensrvUPDATEPOLICYResult() {
        return new GensrvUPDATEPOLICYResult();
    }

    /**
     * Create an instance of {@link GensrvGETROLETYPESInput }
     * 
     */
    public GensrvGETROLETYPESInput createGensrvGETROLETYPESInput() {
        return new GensrvGETROLETYPESInput();
    }

    /**
     * Create an instance of {@link GensrvGETROLETYPESResult }
     * 
     */
    public GensrvGETROLETYPESResult createGensrvGETROLETYPESResult() {
        return new GensrvGETROLETYPESResult();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvCalculatearrears }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvCalculatearrears }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_calculatearrears")
    public JAXBElement<GensrvCalculatearrears> createGensrvCalculatearrears(GensrvCalculatearrears value) {
        return new JAXBElement<GensrvCalculatearrears>(_GensrvCalculatearrears_QNAME, GensrvCalculatearrears.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvCalculatearrearsResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvCalculatearrearsResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_calculatearrearsResponse")
    public JAXBElement<GensrvCalculatearrearsResponse> createGensrvCalculatearrearsResponse(GensrvCalculatearrearsResponse value) {
        return new JAXBElement<GensrvCalculatearrearsResponse>(_GensrvCalculatearrearsResponse_QNAME, GensrvCalculatearrearsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvCancelpolicy }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvCancelpolicy }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_cancelpolicy")
    public JAXBElement<GensrvCancelpolicy> createGensrvCancelpolicy(GensrvCancelpolicy value) {
        return new JAXBElement<GensrvCancelpolicy>(_GensrvCancelpolicy_QNAME, GensrvCancelpolicy.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvCancelpolicyResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvCancelpolicyResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_cancelpolicyResponse")
    public JAXBElement<GensrvCancelpolicyResponse> createGensrvCancelpolicyResponse(GensrvCancelpolicyResponse value) {
        return new JAXBElement<GensrvCancelpolicyResponse>(_GensrvCancelpolicyResponse_QNAME, GensrvCancelpolicyResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvConvertpolicy }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvConvertpolicy }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_convertpolicy")
    public JAXBElement<GensrvConvertpolicy> createGensrvConvertpolicy(GensrvConvertpolicy value) {
        return new JAXBElement<GensrvConvertpolicy>(_GensrvConvertpolicy_QNAME, GensrvConvertpolicy.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvConvertpolicyResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvConvertpolicyResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_convertpolicyResponse")
    public JAXBElement<GensrvConvertpolicyResponse> createGensrvConvertpolicyResponse(GensrvConvertpolicyResponse value) {
        return new JAXBElement<GensrvConvertpolicyResponse>(_GensrvConvertpolicyResponse_QNAME, GensrvConvertpolicyResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvDebitorderalignment }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvDebitorderalignment }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_debitorderalignment")
    public JAXBElement<GensrvDebitorderalignment> createGensrvDebitorderalignment(GensrvDebitorderalignment value) {
        return new JAXBElement<GensrvDebitorderalignment>(_GensrvDebitorderalignment_QNAME, GensrvDebitorderalignment.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvDebitorderalignmentResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvDebitorderalignmentResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_debitorderalignmentResponse")
    public JAXBElement<GensrvDebitorderalignmentResponse> createGensrvDebitorderalignmentResponse(GensrvDebitorderalignmentResponse value) {
        return new JAXBElement<GensrvDebitorderalignmentResponse>(_GensrvDebitorderalignmentResponse_QNAME, GensrvDebitorderalignmentResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvGetaudittrail }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvGetaudittrail }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_getaudittrail")
    public JAXBElement<GensrvGetaudittrail> createGensrvGetaudittrail(GensrvGetaudittrail value) {
        return new JAXBElement<GensrvGetaudittrail>(_GensrvGetaudittrail_QNAME, GensrvGetaudittrail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvGetaudittrailResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvGetaudittrailResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_getaudittrailResponse")
    public JAXBElement<GensrvGetaudittrailResponse> createGensrvGetaudittrailResponse(GensrvGetaudittrailResponse value) {
        return new JAXBElement<GensrvGetaudittrailResponse>(_GensrvGetaudittrailResponse_QNAME, GensrvGetaudittrailResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvGetbankcodes }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvGetbankcodes }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_getbankcodes")
    public JAXBElement<GensrvGetbankcodes> createGensrvGetbankcodes(GensrvGetbankcodes value) {
        return new JAXBElement<GensrvGetbankcodes>(_GensrvGetbankcodes_QNAME, GensrvGetbankcodes.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvGetbankcodesResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvGetbankcodesResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_getbankcodesResponse")
    public JAXBElement<GensrvGetbankcodesResponse> createGensrvGetbankcodesResponse(GensrvGetbankcodesResponse value) {
        return new JAXBElement<GensrvGetbankcodesResponse>(_GensrvGetbankcodesResponse_QNAME, GensrvGetbankcodesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvGetcancelreasons }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvGetcancelreasons }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_getcancelreasons")
    public JAXBElement<GensrvGetcancelreasons> createGensrvGetcancelreasons(GensrvGetcancelreasons value) {
        return new JAXBElement<GensrvGetcancelreasons>(_GensrvGetcancelreasons_QNAME, GensrvGetcancelreasons.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvGetcancelreasonsResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvGetcancelreasonsResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_getcancelreasonsResponse")
    public JAXBElement<GensrvGetcancelreasonsResponse> createGensrvGetcancelreasonsResponse(GensrvGetcancelreasonsResponse value) {
        return new JAXBElement<GensrvGetcancelreasonsResponse>(_GensrvGetcancelreasonsResponse_QNAME, GensrvGetcancelreasonsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvGetcoveramounts }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvGetcoveramounts }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_getcoveramounts")
    public JAXBElement<GensrvGetcoveramounts> createGensrvGetcoveramounts(GensrvGetcoveramounts value) {
        return new JAXBElement<GensrvGetcoveramounts>(_GensrvGetcoveramounts_QNAME, GensrvGetcoveramounts.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvGetcoveramountsResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvGetcoveramountsResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_getcoveramountsResponse")
    public JAXBElement<GensrvGetcoveramountsResponse> createGensrvGetcoveramountsResponse(GensrvGetcoveramountsResponse value) {
        return new JAXBElement<GensrvGetcoveramountsResponse>(_GensrvGetcoveramountsResponse_QNAME, GensrvGetcoveramountsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvGetescalationoptions }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvGetescalationoptions }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_getescalationoptions")
    public JAXBElement<GensrvGetescalationoptions> createGensrvGetescalationoptions(GensrvGetescalationoptions value) {
        return new JAXBElement<GensrvGetescalationoptions>(_GensrvGetescalationoptions_QNAME, GensrvGetescalationoptions.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvGetescalationoptionsResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvGetescalationoptionsResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_getescalationoptionsResponse")
    public JAXBElement<GensrvGetescalationoptionsResponse> createGensrvGetescalationoptionsResponse(GensrvGetescalationoptionsResponse value) {
        return new JAXBElement<GensrvGetescalationoptionsResponse>(_GensrvGetescalationoptionsResponse_QNAME, GensrvGetescalationoptionsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvGetfinancialhistory }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvGetfinancialhistory }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_getfinancialhistory")
    public JAXBElement<GensrvGetfinancialhistory> createGensrvGetfinancialhistory(GensrvGetfinancialhistory value) {
        return new JAXBElement<GensrvGetfinancialhistory>(_GensrvGetfinancialhistory_QNAME, GensrvGetfinancialhistory.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvGetfinancialhistoryResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvGetfinancialhistoryResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_getfinancialhistoryResponse")
    public JAXBElement<GensrvGetfinancialhistoryResponse> createGensrvGetfinancialhistoryResponse(GensrvGetfinancialhistoryResponse value) {
        return new JAXBElement<GensrvGetfinancialhistoryResponse>(_GensrvGetfinancialhistoryResponse_QNAME, GensrvGetfinancialhistoryResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvGetpolicybasic }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvGetpolicybasic }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_getpolicybasic")
    public JAXBElement<GensrvGetpolicybasic> createGensrvGetpolicybasic(GensrvGetpolicybasic value) {
        return new JAXBElement<GensrvGetpolicybasic>(_GensrvGetpolicybasic_QNAME, GensrvGetpolicybasic.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvGetpolicybasicResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvGetpolicybasicResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_getpolicybasicResponse")
    public JAXBElement<GensrvGetpolicybasicResponse> createGensrvGetpolicybasicResponse(GensrvGetpolicybasicResponse value) {
        return new JAXBElement<GensrvGetpolicybasicResponse>(_GensrvGetpolicybasicResponse_QNAME, GensrvGetpolicybasicResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvGetpolicydetail }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvGetpolicydetail }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_getpolicydetail")
    public JAXBElement<GensrvGetpolicydetail> createGensrvGetpolicydetail(GensrvGetpolicydetail value) {
        return new JAXBElement<GensrvGetpolicydetail>(_GensrvGetpolicydetail_QNAME, GensrvGetpolicydetail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvGetpolicydetailResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvGetpolicydetailResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_getpolicydetailResponse")
    public JAXBElement<GensrvGetpolicydetailResponse> createGensrvGetpolicydetailResponse(GensrvGetpolicydetailResponse value) {
        return new JAXBElement<GensrvGetpolicydetailResponse>(_GensrvGetpolicydetailResponse_QNAME, GensrvGetpolicydetailResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvGetpremiumstatuses }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvGetpremiumstatuses }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_getpremiumstatuses")
    public JAXBElement<GensrvGetpremiumstatuses> createGensrvGetpremiumstatuses(GensrvGetpremiumstatuses value) {
        return new JAXBElement<GensrvGetpremiumstatuses>(_GensrvGetpremiumstatuses_QNAME, GensrvGetpremiumstatuses.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvGetpremiumstatusesResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvGetpremiumstatusesResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_getpremiumstatusesResponse")
    public JAXBElement<GensrvGetpremiumstatusesResponse> createGensrvGetpremiumstatusesResponse(GensrvGetpremiumstatusesResponse value) {
        return new JAXBElement<GensrvGetpremiumstatusesResponse>(_GensrvGetpremiumstatusesResponse_QNAME, GensrvGetpremiumstatusesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvGetprincipalmemberdetail }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvGetprincipalmemberdetail }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_getprincipalmemberdetail")
    public JAXBElement<GensrvGetprincipalmemberdetail> createGensrvGetprincipalmemberdetail(GensrvGetprincipalmemberdetail value) {
        return new JAXBElement<GensrvGetprincipalmemberdetail>(_GensrvGetprincipalmemberdetail_QNAME, GensrvGetprincipalmemberdetail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvGetprincipalmemberdetailResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvGetprincipalmemberdetailResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_getprincipalmemberdetailResponse")
    public JAXBElement<GensrvGetprincipalmemberdetailResponse> createGensrvGetprincipalmemberdetailResponse(GensrvGetprincipalmemberdetailResponse value) {
        return new JAXBElement<GensrvGetprincipalmemberdetailResponse>(_GensrvGetprincipalmemberdetailResponse_QNAME, GensrvGetprincipalmemberdetailResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvGetrefunddetails }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvGetrefunddetails }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_getrefunddetails")
    public JAXBElement<GensrvGetrefunddetails> createGensrvGetrefunddetails(GensrvGetrefunddetails value) {
        return new JAXBElement<GensrvGetrefunddetails>(_GensrvGetrefunddetails_QNAME, GensrvGetrefunddetails.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvGetrefunddetailsResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvGetrefunddetailsResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_getrefunddetailsResponse")
    public JAXBElement<GensrvGetrefunddetailsResponse> createGensrvGetrefunddetailsResponse(GensrvGetrefunddetailsResponse value) {
        return new JAXBElement<GensrvGetrefunddetailsResponse>(_GensrvGetrefunddetailsResponse_QNAME, GensrvGetrefunddetailsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvGetrefundlist }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvGetrefundlist }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_getrefundlist")
    public JAXBElement<GensrvGetrefundlist> createGensrvGetrefundlist(GensrvGetrefundlist value) {
        return new JAXBElement<GensrvGetrefundlist>(_GensrvGetrefundlist_QNAME, GensrvGetrefundlist.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvGetrefundlistResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvGetrefundlistResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_getrefundlistResponse")
    public JAXBElement<GensrvGetrefundlistResponse> createGensrvGetrefundlistResponse(GensrvGetrefundlistResponse value) {
        return new JAXBElement<GensrvGetrefundlistResponse>(_GensrvGetrefundlistResponse_QNAME, GensrvGetrefundlistResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvGetrefundreasons }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvGetrefundreasons }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_getrefundreasons")
    public JAXBElement<GensrvGetrefundreasons> createGensrvGetrefundreasons(GensrvGetrefundreasons value) {
        return new JAXBElement<GensrvGetrefundreasons>(_GensrvGetrefundreasons_QNAME, GensrvGetrefundreasons.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvGetrefundreasonsResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvGetrefundreasonsResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_getrefundreasonsResponse")
    public JAXBElement<GensrvGetrefundreasonsResponse> createGensrvGetrefundreasonsResponse(GensrvGetrefundreasonsResponse value) {
        return new JAXBElement<GensrvGetrefundreasonsResponse>(_GensrvGetrefundreasonsResponse_QNAME, GensrvGetrefundreasonsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvGetroletypes }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvGetroletypes }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_getroletypes")
    public JAXBElement<GensrvGetroletypes> createGensrvGetroletypes(GensrvGetroletypes value) {
        return new JAXBElement<GensrvGetroletypes>(_GensrvGetroletypes_QNAME, GensrvGetroletypes.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvGetroletypesResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvGetroletypesResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_getroletypesResponse")
    public JAXBElement<GensrvGetroletypesResponse> createGensrvGetroletypesResponse(GensrvGetroletypesResponse value) {
        return new JAXBElement<GensrvGetroletypesResponse>(_GensrvGetroletypesResponse_QNAME, GensrvGetroletypesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvGetsalesdetail }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvGetsalesdetail }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_getsalesdetail")
    public JAXBElement<GensrvGetsalesdetail> createGensrvGetsalesdetail(GensrvGetsalesdetail value) {
        return new JAXBElement<GensrvGetsalesdetail>(_GensrvGetsalesdetail_QNAME, GensrvGetsalesdetail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvGetsalesdetailResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvGetsalesdetailResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_getsalesdetailResponse")
    public JAXBElement<GensrvGetsalesdetailResponse> createGensrvGetsalesdetailResponse(GensrvGetsalesdetailResponse value) {
        return new JAXBElement<GensrvGetsalesdetailResponse>(_GensrvGetsalesdetailResponse_QNAME, GensrvGetsalesdetailResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvQuoteaddrole }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvQuoteaddrole }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_quoteaddrole")
    public JAXBElement<GensrvQuoteaddrole> createGensrvQuoteaddrole(GensrvQuoteaddrole value) {
        return new JAXBElement<GensrvQuoteaddrole>(_GensrvQuoteaddrole_QNAME, GensrvQuoteaddrole.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvQuoteaddroleResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvQuoteaddroleResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_quoteaddroleResponse")
    public JAXBElement<GensrvQuoteaddroleResponse> createGensrvQuoteaddroleResponse(GensrvQuoteaddroleResponse value) {
        return new JAXBElement<GensrvQuoteaddroleResponse>(_GensrvQuoteaddroleResponse_QNAME, GensrvQuoteaddroleResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvQuotedeleterole }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvQuotedeleterole }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_quotedeleterole")
    public JAXBElement<GensrvQuotedeleterole> createGensrvQuotedeleterole(GensrvQuotedeleterole value) {
        return new JAXBElement<GensrvQuotedeleterole>(_GensrvQuotedeleterole_QNAME, GensrvQuotedeleterole.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvQuotedeleteroleResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvQuotedeleteroleResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_quotedeleteroleResponse")
    public JAXBElement<GensrvQuotedeleteroleResponse> createGensrvQuotedeleteroleResponse(GensrvQuotedeleteroleResponse value) {
        return new JAXBElement<GensrvQuotedeleteroleResponse>(_GensrvQuotedeleteroleResponse_QNAME, GensrvQuotedeleteroleResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvQuoteupdaterole }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvQuoteupdaterole }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_quoteupdaterole")
    public JAXBElement<GensrvQuoteupdaterole> createGensrvQuoteupdaterole(GensrvQuoteupdaterole value) {
        return new JAXBElement<GensrvQuoteupdaterole>(_GensrvQuoteupdaterole_QNAME, GensrvQuoteupdaterole.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvQuoteupdateroleResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvQuoteupdateroleResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_quoteupdateroleResponse")
    public JAXBElement<GensrvQuoteupdateroleResponse> createGensrvQuoteupdateroleResponse(GensrvQuoteupdateroleResponse value) {
        return new JAXBElement<GensrvQuoteupdateroleResponse>(_GensrvQuoteupdateroleResponse_QNAME, GensrvQuoteupdateroleResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvRefundpremium }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvRefundpremium }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_refundpremium")
    public JAXBElement<GensrvRefundpremium> createGensrvRefundpremium(GensrvRefundpremium value) {
        return new JAXBElement<GensrvRefundpremium>(_GensrvRefundpremium_QNAME, GensrvRefundpremium.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvRefundpremiumResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvRefundpremiumResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_refundpremiumResponse")
    public JAXBElement<GensrvRefundpremiumResponse> createGensrvRefundpremiumResponse(GensrvRefundpremiumResponse value) {
        return new JAXBElement<GensrvRefundpremiumResponse>(_GensrvRefundpremiumResponse_QNAME, GensrvRefundpremiumResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvReinstatepolicy }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvReinstatepolicy }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_reinstatepolicy")
    public JAXBElement<GensrvReinstatepolicy> createGensrvReinstatepolicy(GensrvReinstatepolicy value) {
        return new JAXBElement<GensrvReinstatepolicy>(_GensrvReinstatepolicy_QNAME, GensrvReinstatepolicy.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvReinstatepolicyResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvReinstatepolicyResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_reinstatepolicyResponse")
    public JAXBElement<GensrvReinstatepolicyResponse> createGensrvReinstatepolicyResponse(GensrvReinstatepolicyResponse value) {
        return new JAXBElement<GensrvReinstatepolicyResponse>(_GensrvReinstatepolicyResponse_QNAME, GensrvReinstatepolicyResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvRequestvoicerecording }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvRequestvoicerecording }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_requestvoicerecording")
    public JAXBElement<GensrvRequestvoicerecording> createGensrvRequestvoicerecording(GensrvRequestvoicerecording value) {
        return new JAXBElement<GensrvRequestvoicerecording>(_GensrvRequestvoicerecording_QNAME, GensrvRequestvoicerecording.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvRequestvoicerecordingResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvRequestvoicerecordingResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_requestvoicerecordingResponse")
    public JAXBElement<GensrvRequestvoicerecordingResponse> createGensrvRequestvoicerecordingResponse(GensrvRequestvoicerecordingResponse value) {
        return new JAXBElement<GensrvRequestvoicerecordingResponse>(_GensrvRequestvoicerecordingResponse_QNAME, GensrvRequestvoicerecordingResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvSearchaddress }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvSearchaddress }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_searchaddress")
    public JAXBElement<GensrvSearchaddress> createGensrvSearchaddress(GensrvSearchaddress value) {
        return new JAXBElement<GensrvSearchaddress>(_GensrvSearchaddress_QNAME, GensrvSearchaddress.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvSearchaddressResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvSearchaddressResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_searchaddressResponse")
    public JAXBElement<GensrvSearchaddressResponse> createGensrvSearchaddressResponse(GensrvSearchaddressResponse value) {
        return new JAXBElement<GensrvSearchaddressResponse>(_GensrvSearchaddressResponse_QNAME, GensrvSearchaddressResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvSendgenericletter }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvSendgenericletter }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_sendgenericletter")
    public JAXBElement<GensrvSendgenericletter> createGensrvSendgenericletter(GensrvSendgenericletter value) {
        return new JAXBElement<GensrvSendgenericletter>(_GensrvSendgenericletter_QNAME, GensrvSendgenericletter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvSendgenericletterResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvSendgenericletterResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_sendgenericletterResponse")
    public JAXBElement<GensrvSendgenericletterResponse> createGensrvSendgenericletterResponse(GensrvSendgenericletterResponse value) {
        return new JAXBElement<GensrvSendgenericletterResponse>(_GensrvSendgenericletterResponse_QNAME, GensrvSendgenericletterResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvUpdateaudittrail }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvUpdateaudittrail }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_updateaudittrail")
    public JAXBElement<GensrvUpdateaudittrail> createGensrvUpdateaudittrail(GensrvUpdateaudittrail value) {
        return new JAXBElement<GensrvUpdateaudittrail>(_GensrvUpdateaudittrail_QNAME, GensrvUpdateaudittrail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvUpdateaudittrailResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvUpdateaudittrailResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_updateaudittrailResponse")
    public JAXBElement<GensrvUpdateaudittrailResponse> createGensrvUpdateaudittrailResponse(GensrvUpdateaudittrailResponse value) {
        return new JAXBElement<GensrvUpdateaudittrailResponse>(_GensrvUpdateaudittrailResponse_QNAME, GensrvUpdateaudittrailResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvUpdatecoverdates }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvUpdatecoverdates }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_updatecoverdates")
    public JAXBElement<GensrvUpdatecoverdates> createGensrvUpdatecoverdates(GensrvUpdatecoverdates value) {
        return new JAXBElement<GensrvUpdatecoverdates>(_GensrvUpdatecoverdates_QNAME, GensrvUpdatecoverdates.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvUpdatecoverdatesResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvUpdatecoverdatesResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_updatecoverdatesResponse")
    public JAXBElement<GensrvUpdatecoverdatesResponse> createGensrvUpdatecoverdatesResponse(GensrvUpdatecoverdatesResponse value) {
        return new JAXBElement<GensrvUpdatecoverdatesResponse>(_GensrvUpdatecoverdatesResponse_QNAME, GensrvUpdatecoverdatesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvUpdatepolicy }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvUpdatepolicy }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_updatepolicy")
    public JAXBElement<GensrvUpdatepolicy> createGensrvUpdatepolicy(GensrvUpdatepolicy value) {
        return new JAXBElement<GensrvUpdatepolicy>(_GensrvUpdatepolicy_QNAME, GensrvUpdatepolicy.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvUpdatepolicyResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvUpdatepolicyResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_updatepolicyResponse")
    public JAXBElement<GensrvUpdatepolicyResponse> createGensrvUpdatepolicyResponse(GensrvUpdatepolicyResponse value) {
        return new JAXBElement<GensrvUpdatepolicyResponse>(_GensrvUpdatepolicyResponse_QNAME, GensrvUpdatepolicyResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvUpdateprincipalmemberdetail }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvUpdateprincipalmemberdetail }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_updateprincipalmemberdetail")
    public JAXBElement<GensrvUpdateprincipalmemberdetail> createGensrvUpdateprincipalmemberdetail(GensrvUpdateprincipalmemberdetail value) {
        return new JAXBElement<GensrvUpdateprincipalmemberdetail>(_GensrvUpdateprincipalmemberdetail_QNAME, GensrvUpdateprincipalmemberdetail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvUpdateprincipalmemberdetailResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvUpdateprincipalmemberdetailResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_updateprincipalmemberdetailResponse")
    public JAXBElement<GensrvUpdateprincipalmemberdetailResponse> createGensrvUpdateprincipalmemberdetailResponse(GensrvUpdateprincipalmemberdetailResponse value) {
        return new JAXBElement<GensrvUpdateprincipalmemberdetailResponse>(_GensrvUpdateprincipalmemberdetailResponse_QNAME, GensrvUpdateprincipalmemberdetailResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvUpdaterefund }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvUpdaterefund }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_updaterefund")
    public JAXBElement<GensrvUpdaterefund> createGensrvUpdaterefund(GensrvUpdaterefund value) {
        return new JAXBElement<GensrvUpdaterefund>(_GensrvUpdaterefund_QNAME, GensrvUpdaterefund.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GensrvUpdaterefundResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GensrvUpdaterefundResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00030pr.wsbeans.iseries/", name = "gensrv_updaterefundResponse")
    public JAXBElement<GensrvUpdaterefundResponse> createGensrvUpdaterefundResponse(GensrvUpdaterefundResponse value) {
        return new JAXBElement<GensrvUpdaterefundResponse>(_GensrvUpdaterefundResponse_QNAME, GensrvUpdaterefundResponse.class, null, value);
    }

}
