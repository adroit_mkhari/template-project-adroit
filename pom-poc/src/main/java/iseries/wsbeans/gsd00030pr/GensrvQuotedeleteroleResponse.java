
package iseries.wsbeans.gsd00030pr;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for gensrv_quotedeleteroleResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="gensrv_quotedeleteroleResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="return" type="{http://gsd00030pr.wsbeans.iseries/}gensrvQUOTEDELETEROLEResult"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "gensrv_quotedeleteroleResponse", propOrder = {
    "_return"
})
public class GensrvQuotedeleteroleResponse {

    @XmlElement(name = "return", required = true)
    protected GensrvQUOTEDELETEROLEResult _return;

    /**
     * Gets the value of the return property.
     * 
     * @return
     *     possible object is
     *     {@link GensrvQUOTEDELETEROLEResult }
     *     
     */
    public GensrvQUOTEDELETEROLEResult getReturn() {
        return _return;
    }

    /**
     * Sets the value of the return property.
     * 
     * @param value
     *     allowed object is
     *     {@link GensrvQUOTEDELETEROLEResult }
     *     
     */
    public void setReturn(GensrvQUOTEDELETEROLEResult value) {
        this._return = value;
    }

}
